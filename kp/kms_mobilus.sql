-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 5.7.19-log - MySQL Community Server (GPL)
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk kms_mobilus
CREATE DATABASE IF NOT EXISTS `kms_mobilus` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kms_mobilus`;

-- membuang struktur untuk table kms_mobilus.kategori
CREATE TABLE IF NOT EXISTS `kategori` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL DEFAULT '0',
  `warna` varchar(50) DEFAULT '0',
  `icon` varchar(100) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel kms_mobilus.kategori: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- membuang struktur untuk table kms_mobilus.materi
CREATE TABLE IF NOT EXISTS `materi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` varchar(255) DEFAULT NULL,
  `judul` varchar(255) NOT NULL,
  `attachment` varchar(255) DEFAULT '',
  `created_by` int(11) NOT NULL,
  `kategori_id` smallint(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `kategori_id` (`kategori_id`),
  CONSTRAINT `FK__kategori` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK__user` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel kms_mobilus.materi: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `materi` DISABLE KEYS */;
/*!40000 ALTER TABLE `materi` ENABLE KEYS */;

-- membuang struktur untuk table kms_mobilus.sub_materi
CREATE TABLE IF NOT EXISTS `sub_materi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) DEFAULT NULL,
  `content` text,
  `attachment` varchar(100) DEFAULT NULL,
  `id_materi` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_materi` (`id_materi`),
  CONSTRAINT `FK__materi` FOREIGN KEY (`id_materi`) REFERENCES `materi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel kms_mobilus.sub_materi: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `sub_materi` DISABLE KEYS */;
/*!40000 ALTER TABLE `sub_materi` ENABLE KEYS */;

-- membuang struktur untuk table kms_mobilus.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `role_id` tinyint(2) unsigned NOT NULL COMMENT '1:admin, 2:user',
  `status_id` tinyint(2) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel kms_mobilus.user: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `email`, `role_id`, `status_id`, `created_at`, `updated_at`) VALUES
	(1, 'admin', '6fcf8c37607e46be2e425319a96d6fe5', 'admin@umroh360.com', 1, 1, '2017-10-11 16:35:35', NULL),
	(2, 'krisna', '5e2af47d0f219ebd00bf6d00170de0bc', 'kfebrianto96@gmail.com', 2, 1, '2017-11-07 04:10:17', '2017-12-30 19:57:14');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
