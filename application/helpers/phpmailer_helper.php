<?php

function phpmailer_send($param = array(), $ishtml = false) {
        $param['from'] = isset($param['from']) ? $param['from'] : MAIL_FROM;
        $param['fromname'] = isset($param['fromname']) ? $param['fromname'] : MAIL_FROMNAME;
        $param['debug'] = isset($param['debug']) ? $param['debug'] : FALSE;

        require_once APPPATH . 'libraries/phpmailer/class.phpmailer.php';
        require_once APPPATH . 'libraries/phpmailer/class.smtp.php';
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        $mail->Host = MAIL_HOST;
//        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = MAIL_AUTH;
        $mail->SMTPSecure = MAIL_SECURE;
        $mail->SMTPDebug = $param['debug'];
//        $mail->SMTPDebug = 1;
        $mail->Username = MAIL_USERNAME;
        $mail->Password = MAIL_PASSWORD;
        $mail->From = $param['from'];
        $mail->FromName = $param['fromname'];
        $mail->CharSet = 'UTF-8';

        if (isset($param['ishtml']))
                $mail->IsHTML($param['ishtml']);

        if (isset($param['attachment']))
                $mail->AddAttachment($param['attachment']);

//        $mail->AddCustomHeader("X-Bounce-Tracking: 999adasi7r34");

        if (isset($param['to'])) {
                if (is_array($param['to'])) {
                        foreach ($param['to'] as $a_to) {
                                $mail->AddAddress($a_to, '');
                        }
                } else {
                        $mail->AddAddress($param['to'], '');
                }
        }

        if (isset($param['bcc'])) {
                if (is_array($param['bcc'])) {
                        foreach ($param['bcc'] as $a_to) {
                                $mail->AddBCC($a_to, '');
                        }
                } else {
                        $mail->AddBCC($param['bcc'], '');
                }
        }

        $mail->WordWrap = 65;
        $mail->IsHTML($ishtml);
        $mail->Subject = $param['subject'];
        $mail->Body = $param['message'];
        $mail->Encoding = 'quoted-printable';

        try {
                return $mail->Send();
        } catch (phpmailerException $e) {
                echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
                echo $e->getMessage(); //Boring error messages from anything else!
        }
        return 0;
}
