<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perkategori extends Common {
	function __construct() {
		parent::__construct("site");

		$this->meta 			= array();
		$this->scripts 			= array('site/perkategori');
		$this->styles 			= array();
		$this->title 			= "Home";
		$this->load->model(array('user','user_session','kategori', 'materi'));
		$this->load->helper('text');
	}

	public function index($id){
		if ($user = $this->user_session->get()) {
			$total_data = $this->materi->count_rows();

			$data = array(
				'user'			=> $this->user_session->get(),
				'kategori' 		=> $this->kategori->get_all(),
				'materi'		=> $this->materi->with_kategori()->limit(3)->order_by('id', 'ASC')->get_all(),
				'total_data'	=> ceil($total_data/6),
			);

			$this->load->view('perkategori/index', $data);
		}else{
			$this->layout = "user_login";
		}
	}

	function logout() {
		$this->user_session->clear();
		$this->layout = 'user_login';
	}

	function get_kategori(){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->kategori->like('kategori', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->kategori->limit(null)->get_all();
        }

        echo json_encode($json);
	}

	function get_materi($id){
		$this->layout = false;

		$data = array(
			'materi'	=> $this->materi->with_kategori()->where('kategori_id', $id)->order_by('judul', 'ASC')->get_all(),
			'populer'	=> $this->materi->with_kategori()->where('kategori_id', $id)->order_by('total_view', 'DESC')->get_all(),
			'terbaru'	=> $this->materi->with_kategori()->where('kategori_id', $id)->order_by('id', 'DESC')->get_all()
		);

		echo json_encode($data);
	}

	function get_search($search){
		$this->layout = false;

		if(!empty($search)){
			$json = [];
			$json = $this->materi->with_kategori()->like('judul', $search)->limit(10)->get_all();
		}
		echo json_encode($json);
	}

	function load_more(){
		$group_no = $this->input->post('group_no');
        $start = ceil($group_no * 9);
        $all_content = $this->materi->limit($start, 9)->get_all();

        if(isset($all_content) && is_array($all_content) && count($all_content)) { 
            foreach ($all_content as $key => $value) {
                 echo '<div class="col-md-4">';
                 echo '<div class="panel" style="border-color: '.$value->kategori->warna.';">';
                 echo '<div class="panel-heading" style="background: '.$value->kategori->warna.';">';
                 echo '<img class="image-sm no-margin-bottom no-top-space" src="'.base_url().PATH_TO_IMG.'kategori/'.$value->kategori->icon.'">';
                 echo '<span class="right"><h3 class="panel-title"><i class="fa fa-clock-o"></i>'.$value->created_at.'</h3></span>';
                 echo '</div>';
                 echo '<div class="panel-body height-100">';
                 echo '<h4>'.$value->judul.'</h4>';
                 echo '</div>';
                 echo '<div class="panel-footer">';
                 echo '<button class="btn-u btn-brd rounded-2x btn-u-dark btn-u-xs" type="button">View <i class="fa fa-arrow-right"></i></button>';
                 echo '<span class="right no-margin-bottom text-footer"></span>';
                  echo '</div></div></div>';           
            }                               
        }
	}
}	
