<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common extends CI_Controller {
	function __construct($module=null) {
		parent::__construct();
		$this->prebooking_sess = (isset($_SESSION['prebooking']) ? $_SESSION['prebooking'] : NULL);
		// $this->load->library('facebook', array('appId' => '372920756478304', 'secret' => '1f4229d88fa36f8226b83d6425068caf'));
		//partial
		// $this->load->library('facebook');
		$this->load->model(array('user_session', 'materi'));
		$this->user_sess = (isset($_SESSION[USER_SESSION]) ? $this->user_session->get() : NULL);
		$data_head['user'] = $this->user_sess;
		$data_head['fb_login_url'] = "asd";
		$data_head['flashdata']	= $this->session->flashdata('form_msg');
		$data_foot['materi'] =  $this->materi->with_kategori()->limit(3)->order_by('id', 'ASC')->get_all();
		$this->parts['head'] = $this->load->view('partial/header', $data_head, true);
		// $this->parts['sidebar'] = $this->load->view(ADMIN_DIR.'partial/sidebar', $data_head, true);
		// $this->parts['quick_sidebar'] = $this->load->view(ADMIN_DIR.'partial/quick_sidebar', $data_head, true);
		$this->parts['foot'] = $this->load->view('partial/footer', $data_foot, true);
	}

	public function breadcrumb($data){
		$data['total_breadcrumbs'] = count($data['breadcrumbs']);
			
		$this->parts['breadcrumbs'] = $this->load->view('partial/breadcrumb', $data, true);
	}

	public function destroy_session(){
		$this->session->sess_destroy();

		redirect(base_url());
	}

	public function dump($var){
		echo "<pre>";
		die(print_r($var, TRUE));
	}

	public function date_formated($value){
		return date_format($value,"D M d, Y g:ia");
	}

	function get_link_image($img, $path){
		$link_img = PATH_TO_POPUP_IMG_DEFAULT;
		if (file_exists(realpath(APPPATH . '../' . $path) . DIRECTORY_SEPARATOR . $img)) {
			$link_img = base_url().$path.'/'.$img;
		}

		return $link_img;
	}


	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	    	if($i == 3 || $i == 6){
	    		$randomString .= '-';
	    	}
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

	    return $this->checkNoBooking($randomString);
	}

	/*upload file*/
	function _upload($name,$attachment,$upload_path,$type="img",$resize=FALSE) {
		$this->load->library('upload');
		$config['file_name'] = $name;
		$config['upload_path'] = $upload_path;
		$config['overwrite'] = TRUE;
		if($type == "img"){
			$config['allowed_types'] = 'png|jpg|gif|bmp|jpeg';
		}else{
			$config['allowed_types'] = 'png|jpg|gif|bmp|jpeg|pdf|docx|doc';
		}
		$config['remove_spaces'] = TRUE;
		
		$this->upload->initialize($config);
		
		if(!$this->upload->do_upload($attachment, TRUE)) {
			echo $this->upload->display_errors();
			return FALSE;
		}else{
			$upload_data = $this->upload->data();
			
			if($resize == TRUE && $type=="img"){
				//[ MAIN IMAGE ]
	            $config['image_library'] = 'gd2';
	            $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
	            //$config['new_image'] = './image_uploads/';
	            $config['maintain_ratio'] = TRUE;
	            $config['width'] = 250;
	            $config['height'] = 250;
				$config['overwrite'] = TRUE;
	            $this->load->library('image_lib',$config);
	            if ( !$this->image_lib->resize()){
					echo $this->image_lib->display_errors();
					return 'error';  
	            }
			}

			return $upload_data['file_name'];
		}

	}
	
	/*remove file*/
	function _remove($file_name,$path){
		if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
			unlink("./".$path."/".$file_name);
		}
	}
	
	/*create directory*/
	function create_dir($path, $id){
		$dir_path = $path.$id; //make new dir names chapter_id

		if (!file_exists(realpath(APPPATH . '../'.$dir_path))) {
			if(!mkdir($dir_path, 0777, true)){
				// die('Failed to create folders...');
				print_r("folder existing");
			}else{
				print_r("folder hasbeen created");
			}
		}
	}

	/*pagination*/
	function _pager($base_url,$total_rows,$per_page){
		$this->load->library('pagination');

		$config['base_url'] 	= $base_url;
		$config['total_rows'] 	= $total_rows;
		$config['per_page'] 	= $per_page; 

		$this->pagination->initialize($config); 

		return $this->pagination->create_links();
	}
	
	function _upload_resize($name,$attachment,$upload_path){
		//Initialise the upload file class
	    $config['upload_path'] = './image_uploads/';
	    $config['allowed_types'] = 'jpg|jpeg|gif|png';
	    $config['max_size'] = '2048';
	    $config['overwrite'] = TRUE;
	    $this->load->library('upload', $config);

	    $userfile = "_upload_image";
	            //check if a file is being uploaded
	            if(strlen($_FILES["_upload_image"]["name"])>0){

	                if ( !$this->upload->do_upload($userfile))//Check if upload is unsuccessful
	                {
	                    $upload_errors = $this->upload->display_errors('', '');
	                    $this->session->set_flashdata('errors', 'Error: '.$upload_errors);   
	                    redirect('admin/view_food/'.$food->course_id);  
	                }
	                else
	                {
	                    //$image_data = $this->upload->data();

	                     ////[ THUMB IMAGE ]
	                    $config2['image_library'] = 'gd2';
	                    $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
	                    $config2['new_image'] = './image_uploads/thumbs';
	                    $config2['maintain_ratio'] = TRUE;
	                    $config2['create_thumb'] = TRUE;
	                    $config2['thumb_marker'] = '_thumb';
	                    $config2['width'] = 75;
	                    $config2['height'] = 50;
	                    $this->load->library('image_lib',$config2); 

	                    if ( !$this->image_lib->resize()){
	                $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));   
	              }

	                    //[ MAIN IMAGE ]
	                    $config['image_library'] = 'gd2';
	                    $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
	                    //$config['new_image'] = './image_uploads/';
	                    $config['maintain_ratio'] = TRUE;
	                    $config['width'] = 250;
	                    $config['height'] = 250;
	                    $this->load->library('image_lib',$config); 

	                    if ( !$this->image_lib->resize()){
	                $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));   
	              }
	               }      
	           }  
	}
}