<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends Common {
	function __construct() {
		parent::__construct("site");

		$this->meta 			= array();
		$this->scripts 			= array('site/home');
		$this->styles 			= array();
		$this->title 			= "Home";
		$this->load->model(array('user','user_session','kategori', 'materi'));
		$this->load->helper('text');
	}

	public function home(){
		if ($user = $this->user_session->get()) {
			$data = array(
				'user'		=> $this->user_session->get(),
				'kategori' 	=> $this->kategori->get_all(),
				'materi'	=> $this->materi->with_kategori()->limit(3)->order_by('id', 'ASC')->get_all(),
				'flashdata'	=> $this->session->flashdata('form_msg')
			);

			$this->load->view('site/index', $data);
		}else{

			redirect(base_url()."site/index");
		}
	}
	public function index(){
		$this->check_is_logged_in();
		$this->layout = "user_login";
	}

	function login() {
		$this->load->model(array('user','user_session'));
		$this->layout = false;
		$login_success = false;
		// $url = $this->input->post('url-segment');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if (isset($username) && isset($password)) {
			if ($this->user_session->create($username, $password)) {
				$login_success = true;
			}
		}

		if(!$login_success){
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Access denied. Incorrect username/password"));
			redirect(base_url());
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Login Success"));
			redirect(base_url());
		}
	}

	function logout() {
		$this->user_session->clear();
		$this->layout = 'user_login';
	}

	function check_is_logged_in(){
		if($user = $this->user_session->get()){
			redirect(base_url()."site/home");
		}
	}

	function get_materi(){
		$this->layout = false;

		$data = array(
			'materi'	=> $this->materi->with_kategori()->limit(9)->order_by('id', 'ASC')->get_all(),
			'populer'	=> $this->materi->with_kategori()->limit(9)->order_by('total_view', 'DESC')->get_all(),
		);

		echo json_encode($data);
	}

	function get_search($search){
		$this->layout = false;

		if(!empty($search)){
			$json = [];
			$json = $this->materi->with_kategori()->like('judul', $search)->limit(10)->get_all();
		}
		echo json_encode($json);
	}

}	
