<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detail extends Common {
	function __construct() {
		parent::__construct("site");

		$this->meta 			= array();
		$this->scripts 			= array('site/perkategori');
		$this->styles 			= array();
		$this->title 			= "Home";
		$this->load->model(array('user','user_session','kategori', 'materi', 'sub_materi'));
		$this->load->helper('text');
	}

	public function index($materi){
		if ($user = $this->user_session->get()) {
			// $this->layout = null;
			$this->parts['head'] = false;
			$this->parts['foot'] = false;
			$data = array(
				'user'		=> $this->user_session->get(),
				'kategori' 	=> $this->kategori->get_all(),
				'materi'	=> $this->materi->with_kategori()->where('id', $materi)->get(),
				'sub'		=> $this->sub_materi->where('id_materi', $materi)->get_all()
			);

			$this->load->view('materi/index', $data);
		}else{
			$this->layout = "user_login";
		}
	}

	function logout() {
		$this->user_session->clear();
		$this->layout = 'user_login';
	}

	function get_kategori(){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->kategori->like('kategori', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->kategori->limit(null)->get_all();
        }

        echo json_encode($json);
	}

	function get_materi($id){
		$this->layout = false;

		$data = $this->materi->with_kategori()->where('id', $id)->get();

		echo json_encode($data);
	}

	function get_search($search){
		$this->layout = false;

		if(!empty($search)){
			$json = [];
			$json = $this->materi->with_kategori()->like('judul', $search)->limit(10)->get_all();
		}
		echo json_encode($json);
	}
}	
