<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_admin extends Common {
	function __construct() {
		parent::__construct("kategori_admin");

		$this->load->model(array('kategori'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/kategori');
		$this->styles 			= array();
		$this->title 			= "Kategori";
		$this->menu = "Kategori";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Kategori',
			'page_description' => 'tambah, ubah, hapus kategori',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Kategori'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('kategori' => $this->kategori->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."kategori/index",$data);
	}

	function save(){
		$this->layout = FALSE;

		$id = $this->input->post('id');
		$data = array('kategori' => $this->input->post('kategori'),
					'warna' => $this->input->post('warna')
					);

		if($id == 0){
			$success = $this->kategori->insert($data);
			if($success){ //create contributor
				if(isset($_FILES['icon']['name']) && !empty($_FILES['icon']['name'])){
					$this->upload_icon($success);
				}
			}
		}else{
			$kategori=$this->kategori->get($id);
			$success = $this->kategori->update($data, $id);
			if(isset($_FILES['icon']['name']) && !empty($_FILES['icon']['name'])){
				if($kategori->icon){
					if(file_exists('assets/images/kategori'.'/'.$kategori->icon)){
						unlink('assets/images/kategori'.'/'.$kategori->icon);
					}
				}
				$this->upload_icon($id);
			}
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Kategori berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Kategori gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."kategori_admin"));
	}

	public function upload_icon($kategori_id)
	{
		$this->layout = false;

		//upload identification attachment
		$icon = array();
		$icon['encrypt_name'] 		= TRUE;
		$icon['upload_path']          = "assets/images/kategori/";
        $icon['allowed_types']        = 'jpg|png';
        $icon['max_size']             = 2048;
        $icon['max_width']            = 2048;
        $icon['max_height']           = 1536;

        $this->load->library('upload', $icon);
        $this->upload->initialize($icon);
        if ( ! $this->upload->do_upload('icon')){
            return 0;
        }else{
        	$upload_data = $this->upload->data();
        	$data_update = array('icon' => $upload_data['file_name']);
        	if($this->kategori->update($data_update, $kategori_id)){
        		// $this->user_data['contributor'] = $this->contributor_model->get($user_id);
        		// $this->user_session->set_front_end($this->user_data);
        		return 1;
        	}
        }
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array('detail' => $this->kategori->get($id));
		echo json_encode($data);		
	}

	function delete(){
		$this->layout = false;
		$id = $this->input->get('id');
		$kategori=$this->kategori->get($id);
		$success = $this->kategori->delete($id);
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Kategori berhasil dihapus."));
			
			if($kategori->icon){
				if(file_exists('assets/images/kategori'.'/'.$kategori->icon)){
					unlink('assets/images/kategori'.'/'.$kategori->icon);
				}
			}
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Kategori gagal dihapus. "));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."kategori_admin"));		
	}


	
}
