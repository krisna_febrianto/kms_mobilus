<?php require_once('Common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends Common {
	function __construct() {
		parent::__construct("site");

		$this->meta 			= array();
		$this->scripts 			= array('administrator/dashboard');
		$this->styles 			= array();
		$this->title 			= "Administrator";
		$this->load->model(array('admin_session','materi'));
	}

	public function index(){
		$this->check_is_logged_in();
		redirect(site_url(ADMIN_DIR."site/login"));
	}

	public function dashboard(){
		if ($user = $this->admin_session->get()) {
			$data_breadcrumb = [
				'page_title' => 'Beranda',
				'page_description' => 'statistics, charts, materi',
				'breadcrumbs' => [
					[
					'link' => base_url(),
					'title' => 'Beranda'
					]
				]
			];
			$data['materi_top'] = $this->materi->limit(10)->order_by("total_view","desc")->get_all();
			$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

			// print_r($data_breadcrumb);die;

			$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

			$this->load->view(ADMIN_DIR."site/index",$data);
		}else{
			redirect(site_url(ADMIN_DIR."site/login"));
		}
	}

	function login() {
		$this->check_is_logged_in();
		$this->layout = "admin_login";
		// $this->parts['head'] = "";
		$data = array('flashdata'	=> $this->session->flashdata('form_msg'));
		$this->load->view(ADMIN_DIR.'site/index',$data);
	}

	function validate() {
		$this->layout = false;
		$login_success = false;
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if (isset($username) && isset($password)) {
			if ($this->admin_session->create($username, $password)) {
				$login_success = true;
			}
		}

		if(!$login_success){
			$this->session->set_flashdata('form_msg', array('success'=>false,'fail' =>true, 'msg' => 'Access denied. Incorrect username/password'));
			redirect(site_url(ADMIN_DIR.'site/login'));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>true, 'fail'=> false, 'msg' => 'Login Success'));
			redirect(site_url(ADMIN_DIR.'site/dashboard'));
		}
	}
	function logout() {
		$this->admin_session->clear();
		redirect(site_url(ADMIN_DIR));
	}
	
	function no_access(){
		redirect(site_url(ADMIN_DIR));
	}

	function check_is_logged_in(){
		if($user = $this->admin_session->get()){
			redirect(site_url(ADMIN_DIR.'site/dashboard'));
		}
	}
	function forgot_password(){
    	$this->layout = false;
		$this->load->model(array('user'));

    	$email = $this->input->post('email-forgot');
    	// $pass = $this->input->post('password-forgot');
    	$get_user = $this->user->where('email',$email)->get();
    	if($get_user != null){
    		
			$new_password = $this->generateRandomString(10);
			$hash_password = $this->admin_session->get_hash($get_user->username,$new_password);
    		// $get_user = $this->user->where('email',$email)->get();
    		// print_r($new_password);
    		// die();
    		$data = array(
    			// 'email' => $email,
    			'password' => $hash_password
    		);

    		if($this->user->update($data,$get_user->id)){
	    		echo $this->send_email_lupa_pass($email, $get_user->id, $new_password);
	    		$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Silahkan cek email untuk melihat password baru anda."));
	    	}else{
	    		$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Gagal Generate Password anda "));
	    	}
    		
    	}else{
    		$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Email anda belum terdaftar sebelumnya"));
    	}

		redirect(site_url().ADMIN_DIR);
    }

    function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function send_email_lupa_pass($email, $user_id, $password){
		$this->layout = false;
		$this->load->helper('phpmailer');

		// $materi = $this->materi->get($user_id);

		$message = "<p>Password anda berhasil direset menjadi : <br/><br/> <b>Password Baru : ".$password."</b> <br/><br/>Silahkan anda mencoba login/masuk dengan password terbaru anda.<br/><br/><br/>Salam,<br/>Umroh360 Support<p><br> 
		</p>";
		$mail_param 	= array(
			'to' 		=> $email,
			'subject' 	=> "Lupa Password Umroh360",
			'message' 	=> $message
			);
		// $domain = 'mail.pjt2-2017.com';

		// $mail = array(
		// 	'from'    => 'Admin Rekrutmen <rekrutmen@pjt2-2017.com>',
		//     'to'      => $email,
		//     'subject' => 'Lupa Password Rekrutmen Perum Jasa Tirta II',
		//     'html'    => $message
		// );

		if(phpmailer_send($mail_param, true)){
			return true;
		}else{
			return false;
		}
		// $mg = Mailgun::create('key-46a9d8ce10599049811a007462cefc02');
		
		# Now, compose and send your message.
		// if($mg->messages()->send($domain, $mail)){
		// 	return TRUE;
		// }else{
		// 	return FALSE;
		// }
		
	}
	public function kategori_materi(){
        // $this->load->model()
        // $data = $this-;
    	$this->load->model(array('materi','kategori'));
        // $data = $this-;
		$this->layout = false;
        
        $category = array();
        $category['name'] = 'Category';

        $series1 = array();
        $series1['name'] = 'Jumlah Materi';
        $category['data'] = [];
        $lembaga = $this->kategori->get_all();
        // if($start_date == '' && $end_date == ''){
        	$year = date('Y-m-d')	;
        	foreach ($lembaga as $key => $value) {
        		# code...
        	
				// echo date('M d, Y', $date);
	        	array_push($category['data'], $value->kategori);
	        	$tot = $this->materi->count_materi_by_("kategori_id = '".$value->id."'")->row_array();
	            $series1['data'][] = ($tot['count_materi']!=null)?$tot['count_materi']:0;
        	}
    	// }else{
    		// $year = date('Y-m-d', strtotime($end_date))	;
    		// $count = (strtotime($end_date) - strtotime($start_date))/(60*60*24);
    		// $count = abs(date_diff(date(strtotime($start_date)),date(strtotime($end_date))));	
    		// print_r($count);
    		// die();
    //     	for ($i = $count;$i>=0;$i--) {
	   //      	$date = strtotime("-".$i." day", strtotime($end_date));
				// // echo date('M d, Y', $date);
	   //      	array_push($category['data'], date('d-M-Y', $date));
	   //      	$tot = $this->materi->count_materi_by_("DATE_FORMAT(created_at,'%Y-%m-%d') = '".date('Y-m-d', $date)."'")->row_array();
	   //          $series1['data'][] = ($tot['count_materi']!=null)?$tot['count_materi']:0;
    //     	}
    // 	}
        
        // for ($i=0; $i < count($category['data']) ; $i++) { 
        //     # code...
        //     $tot = $this->work_request_model->get_monthly_work_hour($i+1,date('Y'))->row_array();
        //     $series1['data'][] = ($tot['total']!=null)?$tot['total']:0;
        // }
        $result = array();
        array_push($result,$category);
        array_push($result,$series1);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }
    public function chart_materi(){
        // $this->load->model()
        // $data = $this-;
    	$this->load->model(array('materi','kategori'));
        // $data = $this-;
		$this->layout = false;
        
        $category = array();
        $category['name'] = 'Category';

        $series1 = array();
        $series1['name'] = 'Jumlah Lihat';
        $category['data'] = [];
        $lembaga = $this->materi->get_all();
        // if($start_date == '' && $end_date == ''){
        	$year = date('Y-m-d')	;
        	foreach ($lembaga as $key => $value) {
        		# code...
        	
				// echo date('M d, Y', $date);
	        	array_push($category['data'], $value->judul);
	            $series1['data'][] = $value->total_view;
        	}
    	// }else{
    		// $year = date('Y-m-d', strtotime($end_date))	;
    		// $count = (strtotime($end_date) - strtotime($start_date))/(60*60*24);
    		// $count = abs(date_diff(date(strtotime($start_date)),date(strtotime($end_date))));	
    		// print_r($count);
    		// die();
    //     	for ($i = $count;$i>=0;$i--) {
	   //      	$date = strtotime("-".$i." day", strtotime($end_date));
				// // echo date('M d, Y', $date);
	   //      	array_push($category['data'], date('d-M-Y', $date));
	   //      	$tot = $this->materi->count_materi_by_("DATE_FORMAT(created_at,'%Y-%m-%d') = '".date('Y-m-d', $date)."'")->row_array();
	   //          $series1['data'][] = ($tot['count_materi']!=null)?$tot['count_materi']:0;
    //     	}
    // 	}
        
        // for ($i=0; $i < count($category['data']) ; $i++) { 
        //     # code...
        //     $tot = $this->work_request_model->get_monthly_work_hour($i+1,date('Y'))->row_array();
        //     $series1['data'][] = ($tot['total']!=null)?$tot['total']:0;
        // }
        $result = array();
        array_push($result,$category);
        array_push($result,$series1);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }
}
