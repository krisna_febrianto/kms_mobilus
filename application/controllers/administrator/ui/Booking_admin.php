<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_admin extends Common {
	function __construct() {
		parent::__construct("booking_admin");

		$this->load->model(array('booking','status_booking', 'user', 'paket_umroh', 'rute_penerbangan', 'paket_plus'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/booking');
		$this->styles 			= array();
		$this->title 			= "Booking";
		$this->menu = "booking";
	}

	function index($id_booking = null){
		$data_breadcrumb = [
			'page_title' => 'Booking',
			'page_description' => 'Daftar Booking',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Booking'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$booking = $this->booking->with_user()
								->with_paket_umroh()
								->with_status_booking()
								->get_all();

		$data = array(
			'new_booking'	=> $this->booking->where('is_view', 0)->get_all(),
			'booking'		=> $this->booking->where('is_view', 1)->get_all(),
			'flashdata'		=> $this->session->flashdata('form_msg'),
			'id_booking'	=> ($id_booking != null ? $id_booking : 0)
		);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."booking/index",$data);
	}

	function detail($id){
		$this->layout = false;
		//$id = $this->input->get('id');
		$detailBooking 	= $this->booking
								->with_status_booking()
								->with_user(array(
										'with' => array(
											array('relation' => 'profile')
										)
									))
								->with_paket_umroh(array(
										'with' => array(
											array('relation' => 'hotel_mekkah'),
											array('relation' => 'hotel_madinah'),
											array('relation' => 'kamar_mekkah'),
											array('relation' => 'kamar_madinah'),
											array('relation' => 'mata_uang'),
											array('relation' => 'jenis_paket'),
											array('relation' => 'handlings'),
										)
									)
								)
								->get($id);

		$data = array(
			'booking' 		=> $detailBooking,
			// 'paket_umroh' 	=> $detailPaket, 
			'rute'			=> $this->rute_penerbangan->where('id', $detailBooking->paket_umroh->rute_penerbangan_id)->get(),
			'paket_plus'	=> $this->paket_plus->where('id', $detailBooking->paket_umroh->paket_plus_id)->get(),
			'mode' => "VIEW"
		);

		$this->load->view(ADMIN_DIR."booking/form",$data);		
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');

		$detailBooking 	= $this->booking
								->with_status_booking()
								->with_user(array(
										'with' => array(
											array('relation' => 'profile')
										)
									))
								->with_paket_umroh(array(
										'with' => array(
											array('relation' => 'hotel_mekkah'),
											array('relation' => 'hotel_madinah'),
											array('relation' => 'kamar_mekkah'),
											array('relation' => 'kamar_madinah'),
											array('relation' => 'mata_uang'),
											array('relation' => 'jenis_paket'),
											array('relation' => 'handlings'),
										)
									)
								)
								->get($id);

		$data = array(
			'booking' 		=> $detailBooking,
			// 'paket_umroh' 	=> $detailPaket, 
			'rute'			=> $this->rute_penerbangan->where('id', $detailBooking->paket_umroh->rute_penerbangan_id)->get(),
			'paket_plus'	=> $this->paket_plus->where('id', $detailBooking->paket_umroh->paket_plus_id)->get(),
			'mode' => "EDIT"
		);
		$this->load->view(ADMIN_DIR."booking/form",$data);		
	}

	function update(){
		$this->layout = FALSE;

		$id = $this->input->post('id');
		$status_id = $this->input->post('status');

		if($success = $this->booking->update(array('status_id' => $status_id), $id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Data berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Data gagal disimpan."));
		}

		redirect(base_url().ADMIN_DIR."ui/booking_admin");
	}

	function get_list(){
		$this->layout = false;
		$this->load->library('datatables');

        $this->datatables->select('booking.id as id, booking.status_id, paket_umroh.nama as paket_umroh, status_booking.tag as status_tag, status_booking.color as status_color, CONCAT(profile.nama_depan, " ", profile.nama_tengah, " ", nama_belakang) as nama, booking.no_booking as no_booking');

        $this->datatables->from('booking');
        $this->datatables->join('user', 'booking.user_id = user.id', 'left');
        $this->datatables->join('profile', 'booking.user_id = profile.user_id', 'left');
        $this->datatables->join('paket_umroh', 'booking.paket_umroh_id = paket_umroh.id', 'left');
        $this->datatables->join('status_booking', 'booking.status_id = status_booking.id', 'left');
        //$this->datatables->where(array('booking.is_view'=>1));
        echo $this->datatables->generate('json','');
	}

	function get_list_new(){
		$this->layout = false;
		$this->load->library('datatables');

        $this->datatables->select('booking.id as id, booking.status_id, paket_umroh.nama as paket_umroh, status_booking.tag as status_tag, status_booking.color as status_color, CONCAT(profile.nama_depan, " ", profile.nama_tengah, " ", nama_belakang) as nama, booking.no_booking as no_booking');

        $this->datatables->from('booking');
        $this->datatables->join('user', 'booking.user_id = user.id', 'left');
        $this->datatables->join('profile', 'booking.user_id = profile.user_id', 'left');
        $this->datatables->join('paket_umroh', 'booking.paket_umroh_id = paket_umroh.id', 'left');
        $this->datatables->join('status_booking', 'booking.status_id = status_booking.id', 'left');
        //$this->datatables->where(array('booking.is_view'=>0));
        echo $this->datatables->generate('json','');
	}

	function updateIsView(){
		$this->layout = false;
		$is = array('is_view' => 1);
		$this->booking->update($is);
	}

}
