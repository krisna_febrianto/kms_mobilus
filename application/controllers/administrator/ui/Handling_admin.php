<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Handling_admin extends Common {
	function __construct() {
		parent::__construct("handling_admin");

		$this->load->model(array('handling', 'mata_uang', 'harga_handling'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/handling');
		$this->styles 			= array();
		$this->title 			= "Handling";
		$this->menu = "handling";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Handling',
			'page_description' => 'tambah, ubah, hapus Handling',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Handling'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('range_jumlah_jamaah' => unserialize(RANGE_JUMLAH_JAMAAH),
					'range_jumlah_jamaah_to' => unserialize(RANGE_JUMLAH_JAMAAH_TO),
					'mata_uang' => $this->mata_uang->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."handling/index",$data);
	}

	function save(){
		$this->layout = FALSE;

		$id = $this->input->post('id');
		$data = array('nama' => $this->input->post('nama'),
					'deskripsi' => $this->input->post('deskripsi')
					);

		$mandatory = $this->input->post('is_mandatory');
		if(!$mandatory){
			$data['is_mandatory'] = 0;
		}else{
			$data['is_mandatory'] = 1;
		}

		$range_from = $this->input->post('range_from');
		$range_to = $this->input->post('range_to');
		$price = $this->input->post('price');
		$currency = $this->input->post('currency');
		$price_id = $this->input->post('price_id');

		if($id == 0){
			if($success = $this->handling->insert($data)){
				$id = $success;
			}
		}else{
			$success = $this->handling->update($data, $id);
		}

		//delete price
		$deleted_price = $this->input->post('deleted_price_id[]');
		if(count($deleted_price) > 0){
			foreach ($deleted_price as $key => $value) {
				$this->harga_handling->delete($value);
			}
		}

		if($success){
			for ($i=0; $i < count($range_from); $i++) { 
				$data_price = array('handling_id' => $id, 
									'limit_bawah_peserta' => ($range_from[$i] == RANGE_JUMLAH_JAMAAH_UNLIMITED ? NULL : $range_from[$i]),
									'limit_atas_peserta' => ($range_to[$i] == RANGE_JUMLAH_JAMAAH_UNLIMITED ? NULL : $range_to[$i]),
									'harga' => str_replace(",", "", (str_replace(".", "", $price[$i]))),
									'mata_uang_id' => $currency[$i]
								);
				if($price_id[$i] > 0){
					$this->harga_handling->update($data_price, $price_id[$i]);	
				}else{
					$this->harga_handling->insert($data_price);	
				}
			}

			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Handling berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Handling gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/handling_admin"));
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array('detail' => $this->handling->with_hargas()->get($id));
		echo json_encode($data);		
	}

	function delete(){
		$this->layout = false;
		$id = $this->input->get('id');
		$this->harga_handling->delete_by_handling($id);
		$success = $this->handling->delete($id);
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Handling berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Handling gagal dihapus. "));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/handling_admin"));	
	}	

	function get_list(){
		$this->layout = false;
		$this->load->library('datatables');

        $this->datatables->select('id, nama, deskripsi, is_mandatory');
        $this->datatables->from('handling');

        echo $this->datatables->generate('json','');
	}
}
