<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paket_umroh_admin extends Common {
	function __construct() {
		parent::__construct("paket_umroh_admin");

		$this->load->model(array('paket_umroh', 'hotel', 'handling', 'mata_uang', 'handling_paket_umroh', 'jenis_paket_umroh', 'rute_penerbangan', 'paket_plus', 'gambar_paket_umroh', 'kamar_hotel', 'handling_pre_book'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/paket_umroh');
		$this->styles 			= array('');
		$this->title 			= "Paket umroh";
	}

	function index($pre_book_id = null){
		$data_breadcrumb = [
			'page_title' => 'Paket umroh',
			'page_description' => 'tambah, ubah, hapus Paket umroh',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'paket_umroh'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('paket_umroh' => $this->paket_umroh->with_hotel_mekkah('fields:nama')->with_hotel_madinah('fields:nama')->with_handlings('fields:nama')->with_mata_uang()->get_all(),
					'handling'	=> $this->handling->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg'),
					'trigger_convert' => ($pre_book_id != null ? $pre_book_id : 0)
					);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."paket_umroh/index",$data);
	}

	function get_datatables(){
		$this->layout = false;
		$this->load->library('datatables');
        // $this->datatables->add_column('foto', '<img src="http://www.rutlandherald.com/wp-content/uploads/2017/03/default-user.png" width=20>', 'foto');
        //$this->datatables->join('mata_uang');
        $this->datatables->select('id,nama,durasi,tanggal_berangkat,harga,kuota, sisa_kuota, (kuota - sisa_kuota) as kuota_terpakai');
        // $this->datatables->add_column('action', anchor('karyawan/edit/.$1','Edit',array('class'=>'btn btn-danger btn-sm')), 'id_pegawai');
        $this->datatables->from('paket_umroh');
        echo $this->datatables->generate('json','');
	}
	
	function view_detail($id){
		$this->layout = false;

		$dataPaket = $this->paket_umroh
						->with_hotel_mekkah('fields:nama')
						->with_hotel_madinah('fields:nama')
						->with_kamar_mekkah('fields:tipe_kamar')
						->with_kamar_madinah('fields:tipe_kamar')
						->with_mata_uang()
						->with_jenis_paket()
						->with_handlings()
						->get($id);

		$data = array(
					'detail' 		=> $dataPaket,
					'rute' 			=> $this->rute_penerbangan->get_custom(array('r.id'=> $dataPaket->rute_penerbangan_id))->row_array(),
					'paket_plus'	=> $this->paket_plus->where('id', $dataPaket->paket_plus_id)->get()
		);

		$this->load->view(ADMIN_DIR."paket_umroh/form", $data);
	}
	
	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');

		$dataPaket = $this->paket_umroh
						->with_hotel_mekkah('fields:nama')
						->with_hotel_madinah('fields:nama')
						->with_kamar_mekkah('fields:tipe_kamar')
						->with_kamar_madinah('fields:tipe_kamar')
						->with_mata_uang()
						->with_jenis_paket()
						->with_handlings()
						->get($id);

		$data = array(
			'detail'  => $dataPaket,
			'handlings'	=> $this->handling_paket_umroh->where('paket_umroh_id', $dataPaket->id)->get_all(),
			'rute'	=> $this->rute_penerbangan->get_custom(array('r.id'=> $dataPaket->rute_penerbangan_id))->row_array()
			);

		//print_r($data['handling']);
		echo json_encode($data);	
	}

	function save(){

		$this->layout = FALSE;

		$id = $this->input->post('id');
		$data = array(
			'jenis_id' => $this->input->post('jenis'),
			'nama' => $this->input->post('nama'),
			'deskripsi' => $this->input->post('deskripsi'),
			'tanggal_berangkat' => $this->input->post('tanggal_berangkat'),
			'durasi' => $this->input->post('durasi'),
			'rute_penerbangan_id' =>$this->input->post('rute_penerbangan'),
			'jumlah_hari_mekkah' => $this->input->post('hari_mekkah'),
			'jumlah_hari_madinah' => $this->input->post('hari_madinah'),
			'hotel_mekkah_id' => $this->input->post('hotel_mekkah'),
			'hotel_madinah_id' => $this->input->post('hotel_madinah'),
			'paket_plus_id' => $this->input->post('paket_plus'),
			'harga' => $this->input->post('harga'),
			'mata_uang_id' => $this->input->post('mata_uang'),
			'kuota' => $this->input->post('kuota'),
			'sisa_kuota' => $this->input->post('sisa_kuota'),
			'kamar_mekkah_id' => $this->input->post('kamar_mekkah'),
			'kamar_madinah_id' => $this->input->post('kamar_madinah')
		);

		if($this->input->post('handling[]') != 0){
            $handling = $this->input->post('handling[]');
        }

		if($id == 0){
			if ($paket_umroh = $this->paket_umroh->insert($data)) {
				if ($this->input->post('handling[]') != 0) {
					$paket_id = $paket_umroh;
					$handling = $this->input->post('handling[]');
					foreach ($handling as $key) {
						$dataHandling = array(
							'paket_umroh_id' => $paket_id,
							'handling_id' => $key
						);
						$this->handling_paket_umroh->insert($dataHandling);
					}
				}
				$success = true;
			}
		}else{
			if ($this->paket_umroh->update($data, $id)) {
				if ($this->input->post('handling[]') != 0) {
					$query = $this->handling_paket_umroh->where('paket_umroh_id', $id)->get_all();
					if ($query != null) {
						$delete = $this->handling_paket_umroh->where('paket_umroh_id', $id)->delete();
						if ($delete) {
							foreach ($handling as $key) {
								$dataHandling = array(
									'paket_umroh_id' => $id,
									'handling_id' => $key
								);
								$this->handling_paket_umroh->insert($dataHandling);
							}
						}
					}else{
						foreach ($handling as $key) {
							$dataHandling = array(
								'paket_umroh_id' => $id,
								'handling_id' => $key
							);
							$this->handling_paket_umroh->insert($dataHandling);
						}
					}
				}
				$success = true;
			}
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Paket umroh berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Paket umroh gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/paket_umroh_admin"));
	}
	
	function delete($id){
		$this->layout = false;
		//$id = $this->paket_umroh->get('id');
		$this->handling_paket_umroh->where('paket_umroh_id',$id)->delete();
		if($this->paket_umroh->delete($id)){
			$this->delete_folder('assets/images/paket_umroh/'.$id.'/');
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Paket umroh berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Paket umroh pendidikan gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/paket_umroh_admin"));	
	}
	
	function get_jenis($id=null){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

        if ($id != null) {
        	$json = $this->jenis_paket_umroh->get($id);
        }else{
        	if(!empty($keyword)){
	            $json = $this->jenis_paket_umroh->like('nama', $keyword)->limit(10)->get_all();
	        }else{
	            $json = $this->jenis_paket_umroh->limit(null)->get_all();
	        }
        }

        echo json_encode($json);	
	}

	function get_rute($id, $tanggal){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");
		$where = [
			'jenis_paket_umroh_id' => $id,
			'bb_kode LIKE' => $keyword
		];

		if(!empty($keyword)){
            //$json = $this->rute_penerbangan->like('nama', $keyword)->limit(10)->where('jenis_paket_umroh_id', $id)->get_all();
            $json = $this->rute_penerbangan->get_custom(array('jenis_paket_umroh_id' => $id, 'r.tanggal_berangkat'=>$tanggal, 'bb1_kode LIKE' => $keyword))->result_array();
        }else{
            //$json = $this->rute_penerbangan->limit(null)->where('jenis_paket_umroh_id', $id)->get_all();
            $json = $this->rute_penerbangan->get_custom(array('jenis_paket_umroh_id' => $id, 'r.tanggal_berangkat'=>$tanggal))->result_array();
        }

        echo json_encode($json);	
	}

	function get_paket_plus($id){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->paket_plus->like('nama', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->paket_plus->limit(null)->get_all();
        }

        echo json_encode($json);	
	}	

	function get_hotel($id, $kota){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if ($kota == "Mekkah") {
			if(!empty($keyword)){
	            $json = $this->hotel->like('nama', $keyword)->limit(10)->where('kota', $kota)->get_all();
	        }else{
	            $json = $this->hotel->limit(null)->where('kota', $kota)->get_all();
	        }
		}else if ($kota == "Madinah") {
			if(!empty($keyword)){
	            $json = $this->hotel->like('nama', $keyword)->limit(10)->where('kota', $kota)->get_all();
	        }else{
	            $json = $this->hotel->limit(null)->where('kota', $kota)->get_all();
	        }
		}

        echo json_encode($json);
	}

	function get_kamar($hotel_id){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->kamar_hotel->like('nama', $keyword)->limit(10)->where('hotel_id', $hotel_id)->get_all();
        }else{
            $json = $this->kamar_hotel->limit(null)->where('hotel_id', $hotel_id)->get_all();
        }

        echo json_encode($json);
	}

	function get_mata_uang($id){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->mata_uang->like('nama', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->mata_uang->limit(null)->get_all();
        }

        echo json_encode($json);
	}

	function get_handling($id){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->handling->like('nama', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->handling->limit(null)->get_all();
        }

        echo json_encode($json);
	}

	function get_pre_book(){
		$this->layout = false;
		$this->load->model('pre_book');

		$id = $this->input->get('id');

		$dataPreBook = $this->pre_book->with_user('fields:username')
										->with_hotel_mekkah('fields:nama')
										->with_hotel_madinah('fields:nama')
										->with_kamar_mekkah('fields:tipe_kamar')
										->with_kamar_madinah('fields:tipe_kamar')
										->with_mata_uang()
										->with_jenis_paket()
										->with_list_handling()
										->get($id);

		$data = array(
			'detail'  => $dataPreBook,
			'handlings'	=> $this->handling_pre_book->where('pre_book_id', $dataPreBook->id)->get_all(),
			'rute'	=> $this->rute_penerbangan->get_custom(array('r.id'=> $dataPreBook->rute_penerbangan_id))->row_array()
			);

		echo json_encode($data);	
	}

	public function upload_foto_paket($file,$id)
	{
		$this->load->model(array('gambar_paket_umroh'));
		$cpt = count($_FILES[$file]['name']);
	    if($cpt > 0){
			$output_dir = 'assets/images/paket_umroh/'.$id.'/';
	        if(!is_dir($output_dir)) {
	            mkdir($output_dir);
	        }
	        $foto = array();
	        $foto['upload_path']          = 'assets/images/paket_umroh/'.$id.'/';
	        $foto['allowed_types']        = 'gif|jpg|png';
	        //$foto['max_size']             = 100; //set max size allowed in Kilobyte
	        //$foto['max_width']            = 1000; // set max width image allowed
	        //$foto['max_height']           = 1000; // set max height allowed
	        $foto['file_name']            = round(microtime(true) * 1000)+rand(5,1000); //just milisecond timestamp fot unique name
	 		
			$files = $_FILES;
		    $this->load->library('upload', $foto);
	    
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES[$file]['name']= $files[$file]['name'][$i];
		        $_FILES[$file]['type']= $files[$file]['type'][$i];
		        $_FILES[$file]['tmp_name']= $files[$file]['tmp_name'][$i];
		        $_FILES[$file]['error']= $files[$file]['error'][$i];
		        $_FILES[$file]['size']= $files[$file]['size'][$i];    

		        $this->upload->initialize($foto);
		        // $this->upload->do_upload('files[]');
		        if (!$this->upload->do_upload($file))
		        {  
		            $data['inputerror'][] = 'image';
		            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
		            $data['status'] = FALSE;
		            echo json_encode($data);
		            exit();
		        }else{
		        	$data_post = array(
		        				'paket_umroh_id'=>$id,
		        				'nama_file'=>$this->upload->data('file_name')
		        				);
		        	$this->gambar_paket_umroh->insert($data_post);
		        }

		    }
		}
	}

	function post_gambar(){
		$this->layout = false;
		
		$id = $this->input->post('id');
		if($this->upload_foto_paket('image',$id)){
			echo json_encode(array('id' => $id,'tipe' => 'paket_umroh'));
			// echo $id;
		}else{
			echo json_encode(array('id' => $id,'tipe' => 'paket_umroh'));
		}

	}

	public function get_gallery(){
		$this->layout = false;
		$this->load->model('gambar_paket_umroh');
		$id = $this->input->post('id');
        $result = $this->gambar_paket_umroh->where(array("paket_umroh_id"=>$id))->get_all();

        echo json_encode($result);
	}

	function hapus_gambar(){
		$this->layout = false;
		$id = $this->input->post('id');
		$this->load->model('gambar_paket_umroh');
        $foto = 'assets/images/paket_umroh/'.$this->input->post('id_paket').'/'.$this->input->post('foto');
        // print_r($foto);
        if($this->gambar_paket_umroh->delete($id)){
            
            if(file_exists($foto)){
            	unlink($foto);
                echo "1";
            }else{
                echo "1";
            }
        }
	}

	function delete_folder($path)
	{
	    if (is_dir($path) === true)
	    {
	        $files = array_diff(scandir($path), array('.', '..'));

	        foreach ($files as $file)
	        {
	            $this->delete_folder(realpath($path) . '/' . $file);
	        }

	        return rmdir($path);
	    }

	    else if (is_file($path) === true)
	    {
	        return unlink($path);
	    }

	    return false;
	}
}
