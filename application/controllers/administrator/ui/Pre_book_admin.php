<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pre_book_admin extends Common {
	function __construct() {
		parent::__construct("pre_book_admin");

		$this->load->model(array('pre_book', 'rute_penerbangan', 'booking', 'konfirmasi_pembayaran', 'handling_paket_umroh'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/pre_book');
		$this->styles 			= array();
		$this->title 			= "Pre Book";
		$this->menu = "pre_book";
	}

	function index($pre_book_id = null){
		$data_breadcrumb = [
			'page_title' => 'Pre Book',
			'page_description' => 'Daftar Pre Booking',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Pre Book'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array(
			'new_pre_book'	=> $this->pre_book->where('is_view', 0)->get_all(),
			'pre_book'		=> $this->pre_book->where('is_view', 1)->get_all(),
			'flashdata'		=> $this->session->flashdata('form_msg'),
			'pre_book_id'	=> ($pre_book_id != null ? $pre_book_id : 0)
		);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."pre_book/index",$data);
	}

	function detail($id){
		$this->layout = false;
		// $id = $this->input->get('id');
		$detail = $this->pre_book->with_user('fields:username')
										->with_hotel_mekkah('fields:nama')
										->with_hotel_madinah('fields:nama')
										->with_kamar_mekkah('fields:tipe_kamar')
										->with_kamar_madinah('fields:tipe_kamar')
										->with_mata_uang()
										->with_jenis_paket()
										->with_list_handling()
										->get($id);
		$data = array(
			'detail' 	=> $detail,
			'handlings'	=> $this->handling_paket_umroh->where('paket_umroh_id', $detail->id)->get_all(),
			'rute'		=> $this->rute_penerbangan->get_custom(array('r.id'=> $detail->rute_penerbangan_id))->row_array()
		);

		$this->load->view(ADMIN_DIR."pre_book/form",$data);		
	}

	function get_list(){
		$this->layout = false;
		$this->load->library('datatables');

        $this->datatables->select('pre_book.id as id, pre_book.nama, user.username, pre_book.tanggal_berangkat');

        $this->datatables->from('pre_book');
        $this->datatables->join('user', 'pre_book.user_id = user.id');
        $this->datatables->where(array('is_view'=>1));
        echo $this->datatables->generate('json','');
	}

	function get_list_new(){
		$this->layout = false;
		$this->load->library('datatables');

        $this->datatables->select('pre_book.id as id, pre_book.nama, user.username, pre_book.tanggal_berangkat');

        $this->datatables->from('pre_book');
        $this->datatables->join('user', 'pre_book.user_id = user.id');
        $this->datatables->where(array('is_view'=>0));
        echo $this->datatables->generate('json','');
	}

	function updateIsView(){
		$this->layout = false;
		$is = array('is_view' => 1);
		$this->pre_book->update($is);
	}

	//----------------------- NOTIFICATION -------------------------------

	function getCountNotif(){
		$this->layout = false;

		$dataPreBook = $this->pre_book->where('is_view', 0)->count_rows();
		$dataBooking = $this->booking->where('is_view', 0)->count_rows();
		$dataKonfirmasi = $this->konfirmasi_pembayaran->where('is_view', 0)->count_rows();

		$data = $dataPreBook + $dataBooking + $dataKonfirmasi;

		echo json_encode($data);
	}

	function getCountPreBook(){
		$this->layout = false;
		$data = $this->pre_book->where('is_view', 0)->count_rows();
		echo json_encode($data);
	}

	function getCountBooking(){
		$this->layout = false;
		$data = $this->booking->where('is_view', 0)->count_rows();
		echo json_encode($data);
	}

	function getCountKonfirmasi(){
		$this->layout = false;
		$data = $this->konfirmasi_pembayaran->where('is_view', 0)->count_rows();
		echo json_encode($data);
	}

	function getDataNotif(){
		$this->layout = false;

		$this->load->helper('Date');

		$data = [];
		$now  = time();

		$dataPreBook = $this->pre_book->with_profile()->where('is_view', 0)->get_all();
		if ($dataPreBook) {
			foreach ($dataPreBook as $key => $value) {
				// $date = strtotime($value->created_at);
				// $value->created_at = timespan($date, $now);
				array_push($data, $value);
			}
		}

		$dataBooking = $this->booking->with_profile()->where('is_view', 0)->get_all();
		if ($dataBooking) {
			foreach ($dataBooking as $key => $value) {
				// $date = strtotime($value->created_at);
				// $value->created_at = timespan($date, $now);
				array_push($data, $value);
			}
		}

		$dataKonfirmasi = $this->konfirmasi_pembayaran->with_booking()->where('is_view', 0)->get_all();
		if ($dataKonfirmasi) {
			foreach ($dataKonfirmasi as $key => $value) {
				// $date = strtotime($value->created_at);
				// $value->created_at = timespan($date, $now);
				array_push($data, $value);
			}
		}

		echo json_encode($data);
	}

	function getLastDataPreBook(){
		$this->layout = false;
		$data = $this->pre_book->with_profile()->order_by('id', 'DESC')->limit(1)->where('is_view', 0)->get();
		echo json_encode($data);		
	}

	function getLastDataBooking(){
		$this->layout = false;
		$data = $this->booking->with_profile()->order_by('id', 'DESC')->limit(1)->where('is_view', 0)->get();
		echo json_encode($data);		
	}

	function getLastDataKonfirmasi(){
		$this->layout = false;
		$data = $this->konfirmasi_pembayaran->with_booking()->order_by('id', 'DESC')->limit(1)->where('is_view', 0)->get();
		echo json_encode($data);		
	}

	// function updateStatusNotif($id){
	// 	$this->layout = false;
	// 	$data = array('is_view' => 1);
	// 	$success = $this->pre_book->update($data, $id);
	// 	echo json_encode($success);
	// }
}
