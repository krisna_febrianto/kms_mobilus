<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bandara_admin extends Common {
	function __construct() {
		parent::__construct("bandara_admin");

		$this->load->model(array('bandara','kota'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/bandara');
		$this->styles 			= array('');
		$this->title 			= "bandara";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Bandara',
			'page_description' => 'tambah, ubah, hapus Bandara',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Bandara'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		// print_r($data_breadcrumb);die;

		$data = array('bandara' => $this->bandara->order_by('id', $order = 'DESC')->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);


		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."bandara/index",$data);
	}
	function add(){
		$this->layout = false;
		$data = array('mode' 		=> 'add',
					'kota' => $this->kota->get_all()
					);
		$this->load->view(ADMIN_DIR."bandara/form",$data);
	}
	
	function edit($id){
		$this->layout = false;
		$data = array('detail'  	=> $this->bandara->get($id),
					  'mode'    	=> 'edit',
					  'kota' => $this->kota->get_all()
					  );
		$this->load->view(ADMIN_DIR."bandara/form",$data);
	}

	function save(){
		$mode = $this->input->post('mode');
		$negara = $this->kota->where(array('nama' => $this->input->post('kota')))->get();
		$data_post = array('nama' => $this->input->post('nama'),
						   'kode' => $this->input->post('kode'),
						   'kota' => $this->input->post('kota'),
						   'negara' => $negara->negara
						);
		if($mode == 'add'){

			$success = $this->bandara->insert($data_post);
			
		}else{
			
			$success = $this->bandara->update($data_post, $this->input->post('id'));
		}

		
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Bandara berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Bandara gagal disimpan. "));
		}
		redirect(base_url().ADMIN_DIR."ui/bandara_admin");
	}
	
	function delete(){
		$this->layout = false;
		$id = $this->input->get('id');
		if($this->bandara->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Bandara berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Bandara gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/bandara_admin"));	
	}

}
