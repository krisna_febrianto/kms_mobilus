<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerbangan_admin extends Common {
	function __construct() {
		parent::__construct("penerbangan_admin");

		$this->load->model(array('penerbangan','bandara','mata_uang','maskapai'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/penerbangan','fullcalendar.min');
		$this->styles 			= array('assets/global/plugins/fullcalendar/fullcalendar.min.css');
		$this->title 			= "Penerbangan";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Penerbangan',
			'page_description' => 'tambah, ubah, hapus Penerbangan',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Penerbangan'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		// print_r($data_breadcrumb);die;

		$data = array(
					'maskapai' => $this->maskapai->order_by('nama', $order = 'ASC')->get_all(),
					'bandara' => $this->bandara->order_by('nama', $order = 'ASC')->get_all(),
					'mata_uang' => $this->mata_uang->order_by('nama', $order = 'ASC')->get_all()
					);


		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."penerbangan/index",$data);
	}
    
	public function search_maskapai()
    {
    	$this->layout = false;
     	$result = [];
        if(!empty($this->input->get("q"))){
            $keyword = $this->input->get("q");
            // $where = "name LIKE '%$keyword%' or student_id LIKE '%$keyword%'";
            $result = $this->maskapai->fields('id,nama as text')->where('nama like','%'.$keyword.'%')->get_all();
        }
    	if(!$result){
    		$result = [];
    	}
        echo json_encode($result);
    }
    public function getAllBandara()
    {
    	$this->layout = false;
     	$result = [];
     	$result = $this->bandara->fields('id,nama as text')->get_all();
        echo json_encode($result);
    }
	function getPenerbangan($id){
		$this->layout = FALSE;
		$penerbangan = $this->penerbangan->where(array('maskapai_id'=>$id,'tanggal_berangkat >='=>$_GET['start'],'tanggal_tiba <='=>$_GET['end']))->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama,kode')->with_bandara_tujuan('fields:nama,kode')->with_mata_uang('fields:nama,symbol')->get_all();
		$data = [];
		foreach ($penerbangan as $key => $value) {
			if ($value->is_low_season == 1) {
				$color = '#87CEFA'; 	
			}else{
				$color = '';
			} 
			if ($value->tanggal_berangkat == $value->tanggal_tiba) {
				$width = '109px';
				$dua_hari =0;
			}else{
				$width = '238px';
				$dua_hari =1;
			}
		 	$event = array(
				'id' => $value->id,
				'logo' => $value->maskapai->logo,
				'kode_mata_uang' => $value->mata_uang->symbol." ",
				'start' => $value->tanggal_berangkat.'T'.$value->jam_berangkat,
				'end'   => $value->tanggal_tiba.'T'.$value->jam_tiba,
				'maskapai_id' => $value->maskapai_id,
				'maskapai_nama' => $value->maskapai->nama,
				'bandara_berangkat_id' => $value->bandara_berangkat_id,
				'bandara_berangkat_nama' => $value->bandara_berangkat->nama,
				'bandara_berangkat_kode' => $value->bandara_berangkat->kode,
				'bandara_tujuan_id' => $value->bandara_tujuan_id,
				'bandara_tujuan_nama' => $value->bandara_tujuan->nama,
				'bandara_tujuan_kode' => $value->bandara_tujuan->kode,
				'tanggal_berangkat' => $value->tanggal_berangkat,
				'tanggal_tiba' => $value->tanggal_tiba,
				'jam_berangkat' =>  date("H:i", strtotime($value->jam_berangkat)),
				'jam_tiba' => date("H:i", strtotime($value->jam_tiba)),
				'harga' => $value->harga,
				'mata_uang_id' => $value->mata_uang_id,
				'mata_uang_nama' => $value->mata_uang->nama,
				'is_low_season' => $value->is_low_season,
				'color' => $color,
				'dua_hari' => $dua_hari,
				'width' => $width
				);
			array_push($data, $event);
		 }
		echo json_encode($data);	
	}
	function getPenerbanganAll(){
		$this->layout = FALSE;
		$penerbangan = $this->penerbangan->where(array('tanggal_berangkat >='=>$_GET['start'],'tanggal_tiba <='=>$_GET['end']))->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama,kode')->with_bandara_tujuan('fields:nama,kode')->with_mata_uang('fields:nama,symbol')->get_all();
		$data = [];
		foreach ($penerbangan as $key => $value) {
			if ($value->is_low_season == 1) {
				$color = '#87CEFA'; 	
			}else{
				$color = '';
			} 
			if ($value->tanggal_berangkat == $value->tanggal_tiba) {
				$width = '109px';
				$dua_hari =0;
			}else{
				$width = '238px';
				$dua_hari =1;
			}
		 	$event = array(
				'id' => $value->id,
				'logo' => $value->maskapai->logo,
				'kode_mata_uang' => $value->mata_uang->symbol." ",
				'start' => $value->tanggal_berangkat.'T'.$value->jam_berangkat,
				'end'   => $value->tanggal_tiba.'T'.$value->jam_tiba,
				'maskapai_id' => $value->maskapai_id,
				'maskapai_nama' => $value->maskapai->nama,
				'bandara_berangkat_id' => $value->bandara_berangkat_id,
				'bandara_berangkat_nama' => $value->bandara_berangkat->nama,
				'bandara_berangkat_kode' => $value->bandara_berangkat->kode,
				'bandara_tujuan_id' => $value->bandara_tujuan_id,
				'bandara_tujuan_nama' => $value->bandara_tujuan->nama,
				'bandara_tujuan_kode' => $value->bandara_tujuan->kode,
				'tanggal_berangkat' => $value->tanggal_berangkat,
				'tanggal_tiba' => $value->tanggal_tiba,
				'jam_berangkat' =>  date("H:i", strtotime($value->jam_berangkat)),
				'jam_tiba' => date("H:i", strtotime($value->jam_tiba)),
				'harga' => $value->harga,
				'mata_uang_id' => $value->mata_uang_id,
				'mata_uang_nama' => $value->mata_uang->nama,
				'is_low_season' => $value->is_low_season,
				'color' => $color,
				'dua_hari' => $dua_hari,
				'width' => $width
				);
			array_push($data, $event);
		 }
		echo json_encode($data);	
	}
	function save($id){

		$this->layout = FALSE;

		if ($this->input->post('is_low_season') == NULL) {
			$is_low_season = 0;
		}else{
			$is_low_season = 1;
		}
		// if ($this->input->post('maskapai_id') == NULL || $this->input->post('maskapai_id') == 'all') {
		// 	$maskapai_id = $this->input->post('maskapai_ids');

		// }else{
		// 	$maskapai_id = $this->input->post('maskapai_id');
		// }
		if ($this->input->post('dua_hari') == NULL) {
			$tanggal_tiba = $this->input->post('tanggal_berangkat');
		}else{
			$date = new DateTime($this->input->post('tanggal_berangkat'));
			$date->modify("+1 DAYS");
			$tanggal_tiba = $date->format('Y-m-d');
		}
		$data = array(
				'maskapai_id' => $this->input->post('maskapai_ids'),
				'bandara_berangkat_id' => $this->input->post('bandara_berangkat_id'),
				'bandara_tujuan_id' => $this->input->post('bandara_tujuan_id'),
				'tanggal_berangkat' => $this->input->post('tanggal_berangkat'),
				'tanggal_tiba' => $tanggal_tiba,
				'jam_berangkat' => $this->input->post('jam_berangkat'),
				'jam_tiba' => $this->input->post('jam_tiba'),
				'harga' => $this->input->post('harga'),
				'mata_uang_id' => $this->input->post('mata_uang_id'),
				'is_low_season' => $is_low_season,
				
			);
		if ($this->input->post('save_tanggal_mulai') != null && $this->input->post('save_tanggal_selsai') != null) {
			$start  	= new DateTime($this->input->post('save_tanggal_mulai'));
			$end 		= new DateTime($this->input->post('save_tanggal_selsai'));
			$end 		= $end->modify( '+1 day' );
			$dateRange 	= new DatePeriod($start, new DateInterval('P1D'), $end);

				foreach ($dateRange as $date) {
					$tanggal = $date->format('Y-m-d');
					$data['tanggal_berangkat'] = $tanggal;
					if ($this->input->post('dua_hari') != NULL) {
						$tanggal 		= new DateTime($tanggal);
						$tanggal 		= $tanggal->modify( '+1 day' );
						$tanggal 		= $tanggal->format('Y-m-d');
						$data['tanggal_tiba'] = $tanggal;
					}else{
						$data['tanggal_tiba'] = $tanggal;
					}
					$success = $this->penerbangan->insert($data);
				}
		}else{
			if($id == 0){
				$success = $this->penerbangan->insert($data);
			}else{
				$success = $this->penerbangan->update($data, $id);
			}
		}
		
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "penerbangan berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "penerbangan gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/penerbangan_admin"));
	}
	
	function delete($id){
		$this->layout = false;
	
		if($this->penerbangan->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "penerbangan berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "penerbangan pendidikan gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/penerbangan_admin"));	
	}
	
	
}
