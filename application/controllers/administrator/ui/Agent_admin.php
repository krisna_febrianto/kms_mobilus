<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agent_admin extends Common {
	function __construct() {
		parent::__construct("agent_admin");

		$this->load->model(array('agent', 'user', 'profile'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/agent');
		$this->styles 			= array();
		$this->title 			= "Agent";
		$this->menu 			= "agent";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Agent',
			'page_description' => 'Daftar Agent',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Agent'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('flashdata'	=> $this->session->flashdata('form_msg'));

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."agent/index",$data);
	}

	function get_list(){
		$this->layout = false;
		$this->load->library('datatables');

        $this->datatables->select('agent.user_id as id, user.email as email,  agent.is_active as status, agent.promo_code as promo_code, CONCAT(profile.nama_depan, " ", profile.nama_tengah, " ", nama_belakang) as nama, profile.paspor as paspor, profile.ktp as ktp');

        $this->datatables->from('agent');
        $this->datatables->join('user', 'agent.user_id = user.id');
        $this->datatables->join('profile', 'profile.user_id = user.id');
        echo $this->datatables->generate('json','');
	}

	function check_email(){
		$this->layout = false; 
		$email = $_POST['email'];

		$data = $this->user->where('email', $email)->get();
		if ($data) {
			$result = false;
		}else{
			$result = true;
		}


		echo json_encode($result);
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array(
			'detail' 	 	=> $this->user->with_agent()->with_profile()->get($id)
			);
		echo json_encode($data);
	}

	function save(){
		$this->layout = false;

		$id 		= $this->input->post('id');
		$email 		= $this->input->post('email');
		$username 	= strtolower($this->input->post('nama_depan')).$this->generateRandomString(3);
		$password 	= $this->generateRandomString(10);
		$promo_code = $this->generatePromoCode(6);

		$dataUser = array(
			'username' 	=> $username,
			'email' 	=> $email,
			'password' 	=> md5($password),
			'role_id' 	=> 3,
			'status_id' => 1
		);

		if($id == 0){
			if ($user = $this->user->insert($dataUser)) {
				$data = array(
					'user_id' 		=> $user,
					'promo_code'	=> $promo_code,
					'is_active'		=> $this->input->post('status')
				);

				$this->agent->insert($data);
				$this->send_email($email, $password, $promo_code, $username);

				$dataProfile = array(
					'user_id' 		=> $user,
					'nama_depan'	=> $this->input->post('nama_depan'),
					'nama_tengah'	=> $this->input->post('nama_tengah'),
					'nama_belakang' => $this->input->post('nama_belakang'),
					'tempat_lahir'	=> $this->input->post('tempat_lahir'),
					'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
					'ktp'		=> $this->input->post('ktp'),
					'paspor'		=> $this->input->post('paspor')
				);
				$this->profile->insert($dataProfile);	
			} 
			$success = true;
		}else{
			$data = array(
				'is_active'		=> $this->input->post('status')
			);

			$this->agent->where('user_id', $id)->update($data);

			$dataProfile = array(
				'nama_depan'	=> $this->input->post('nama_depan'),
				'nama_tengah'	=> $this->input->post('nama_tengah'),
				'nama_belakang' => $this->input->post('nama_belakang'),
				'tempat_lahir'	=> $this->input->post('tempat_lahir'),
				'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
				'ktp'		=> $this->input->post('ktp'),
				'paspor'		=> $this->input->post('paspor')
			);
			$success = $this->profile->where('user_id', $id)->update($dataProfile);
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Agent berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Agent gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/agent_admin"));
	}

	function generateRandomString($length = null) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function generatePromoCode($length = null) {
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function send_email($email,$password,$promo_code,$username){
		$this->layout = false;

		$message = "<p>Anda telah terdaftar menjadi agent <br/><br/><b>Username : ".$username."</b><br/> <b>Password : ".$password."</b><br/><b>Promo code : ".$promo_code."</b> <br/><br/>Silahkan login/masuk dengan akun anda.<br/><br/><br/>Salam,<br/>Umroh360 Support<p><br> 
		</p>";
		$this->load->library('CI_Mailer');
        $result = $this->ci_mailer->send(array(
            'to' => $email,
            'subject' => 'Akun agent Umroh360',
            'message' => $message,
        ), TRUE);

		if($result){
			return true;
		}else{
			return false;
		}
	}

	function delete($id){
		$this->layout = false;
		//$id = $this->kurs->get('id');
	
		if($this->user->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Agent berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Agent pendidikan gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/agent_admin"));	
	}
}
