<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerbangan_admin_table extends Common {
	function __construct() {
		parent::__construct("penerbangan_admin_table");

		$this->load->model(array('penerbangan','bandara','mata_uang','maskapai'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/penerbangan_table');
		$this->styles 			= array();
		$this->title 			= "Penerbangan";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Penerbangan',
			'page_description' => 'tambah, ubah, hapus Penerbangan',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Penerbangan'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);
		$data = array(
					'maskapai' => $this->maskapai->order_by('nama', $order = 'ASC')->get_all(),
					'bandara' => $this->bandara->order_by('nama', $order = 'ASC')->get_all(),
					'mata_uang' => $this->mata_uang->order_by('nama', $order = 'ASC')->get_all()
					);


		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."penerbangan/index_table",$data);
	}
    
	function get_datatables($bandara_tujuan_filter,$bandara_berangkat_filter,$maskapai_filter,$tanggal_berangkat_filter,$tanggal_tiba_filter){
		$this->layout = FALSE;
		$result = [];
		$a = [];
		$data = [];
		if ($maskapai_filter != 'default') {
			$f1= array(
				'maskapai_id' => $maskapai_filter,
			);
		}else{
			$f1= array();
		}
		if($bandara_berangkat_filter != 'default'){
			$f2= array(
				'bandara_berangkat_id' => $bandara_berangkat_filter,
			);
		}else{
			$f2= array();
		}
		if($bandara_tujuan_filter != 'default'){
			$f3= array(
				'bandara_tujuan_id' => $bandara_tujuan_filter,
			);
		}else{
			$f3= array();
		}
		if($tanggal_berangkat_filter != 'default'){
			$f4= array(
				'tanggal_berangkat' => $tanggal_berangkat_filter,
			);
		}else{
			$f4= array();
		}
		if($tanggal_tiba_filter != 'default'){
			$f5= array(
				'tanggal_tiba' => $tanggal_tiba_filter,
			);
		}else{
			$f5= array();
		}
		$filter = array_merge($f1,$f2,$f3,$f4,$f5);
		$this->load->library('datatables');
        $this->datatables->select('penerbangan.*, CONCAT(berangkat.kota," - ",berangkat.kode) as bandara_berangkat_tampil, CONCAT(tujuan.kota," - ",tujuan.kode) as bandara_tujuan_tampil, maskapai.nama as maskapai_nama, mata_uang.nama as mata_uang_nama, mata_uang.symbol as symbol, DATE_FORMAT(tanggal_berangkat, "%d-%m-%Y") as tanggal_berangkat_format, DATE_FORMAT(tanggal_tiba, "%d-%m-%Y") as tanggal_tiba_format');
        $this->datatables->from('penerbangan');
        $this->datatables->join('bandara as berangkat' ,'penerbangan.bandara_berangkat_id = berangkat.id');
        $this->datatables->join('bandara as tujuan' ,'penerbangan.bandara_tujuan_id = tujuan.id');
        $this->datatables->join('maskapai', 'penerbangan.maskapai_id = maskapai.id');
        $this->datatables->join('mata_uang', 'penerbangan.mata_uang_id = mata_uang.id');
        $this->datatables->where($filter);
        echo $this->datatables->generate('json','');	
	}
	function edit($id){
		$this->layout = FALSE;
		$penerbangan = $this->penerbangan->with_maskapai('fields:nama')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get($id);
		echo json_encode($penerbangan);	
	}
	function save(){

		$this->layout = FALSE;

		if ($this->input->post('is_low_season') == NULL) {
			$is_low_season = 0;
		}else{
			$is_low_season = 1;
		}
		if ($this->input->post('dua_hari') == NULL) {
			$tanggal_tiba = $this->input->post('tanggal_berangkat');
		}else{
			$date = new DateTime($this->input->post('tanggal_berangkat'));
			$date->modify("+1 DAYS");
			$tanggal_tiba = $date->format('Y-m-d');
		}
		$id = $this->input->post('id');
		$data = array(
				'maskapai_id' => $this->input->post('maskapai_id'),
				'bandara_berangkat_id' => $this->input->post('bandara_berangkat_id'),
				'bandara_tujuan_id' => $this->input->post('bandara_tujuan_id'),
				'tanggal_berangkat' => $this->input->post('tanggal_berangkat'),
				'tanggal_tiba' => $this->input->post('tanggal_tiba'),
				'jam_berangkat' => $this->input->post('jam_berangkat'),
				'jam_tiba' => $this->input->post('jam_tiba'),
				'harga' => $this->input->post('harga'),
				'mata_uang_id' => $this->input->post('mata_uang_id'),
				'is_low_season' => $is_low_season,
				
			);
		
		if ($this->input->post('save_tanggal_mulai') != null && $this->input->post('save_tanggal_selsai') != null) {
			$start  	= new DateTime($this->input->post('save_tanggal_mulai'));
			$end 		= new DateTime($this->input->post('save_tanggal_selsai'));
			$end 		= $end->modify( '+1 day' );
			$dateRange 	= new DatePeriod($start, new DateInterval('P1D'), $end);

				foreach ($dateRange as $date) {
					$tanggal = $date->format('Y-m-d');
					$data['tanggal_berangkat'] = $tanggal;
					if ($this->input->post('dua_hari') != NULL) {
						$tanggal 		= new DateTime($tanggal);
						$tanggal 		= $tanggal->modify( '+1 day' );
						$tanggal 		= $tanggal->format('Y-m-d');
						$data['tanggal_tiba'] = $tanggal;
					}else{
						$data['tanggal_tiba'] = $tanggal;
					}
					$success = $this->penerbangan->insert($data);
				}
		}else{
			if($id == 0){
				$success = $this->penerbangan->insert($data);
			}else{
				$success = $this->penerbangan->update($data, $id);
			}
		}

		if($success){
			echo 1;
		}else{
			echo 1;
		}
	}
	function delete($id){
		$this->layout = false;
	
		if($this->penerbangan->delete($id)){
			echo 1;
		}else{
			echo 0;
		}
	}
	
	
}
