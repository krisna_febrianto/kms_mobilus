<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel_admin extends Common {
	function __construct() {
		parent::__construct("hotel_admin");

		$this->load->model(array('hotel','fasilitas_hotel'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/hotel','../themes/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min','../themes/assets/global/plugins/jquery-repeater/jquery.repeater');
		$this->styles 			= array();
		$this->title 			= "Hotel";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Hotel',
			'page_description' => 'Tambah, ubah, hapus hotel',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Hotel'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		// print_r($data_breadcrumb);die;

		$data = array('hotel' => $this->hotel->get_all(),
					'fasilitas' => $this->fasilitas_hotel->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);


		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."hotel/index",$data);
	}
	function get_datatables(){
		$this->layout = false;
		$this->load->library('datatables');
        // $this->datatables->add_column('foto', '<img src="http://www.rutlandherald.com/wp-content/uploads/2017/03/default-user.png" width=20>', 'foto');
        $this->datatables->select('nama,bintang,alamat,id');
        // $this->datatables->add_column('action', anchor('karyawan/edit/.$1','Edit',array('class'=>'btn btn-danger btn-sm')), 'id_pegawai');
        $this->datatables->from('hotel');
        echo $this->datatables->generate('json','');
	}
	function add(){
		$this->layout = false;
		$data = array('mode' 		=> 'add');
		$this->load->view(ADMIN_DIR."hotel/form-wizard",$data);
	}
	
	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array('detail' => $this->hotel->with_fasilitass()->get($id));
		echo json_encode($data);		
	}

	function save_edit(){
		$this->layout = FALSE;
		$this->load->model(array('kamar_hotel','fasilitas_hotel_hotel'));
		$id = $this->input->post('id');
		$data = array(
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'kota' => $this->input->post('kota'),
					'keterangan' => $this->input->post('keterangan'),
					'bintang' => $this->input->post('bintang'),
					'deskripsi' => $this->input->post('deskripsi'),
					'longitude' => $this->input->post('longitude'),
					'latitude' => $this->input->post('latitude'),
					);
		$fasilitas_list = 0;
		if($_POST['fasilitas_list'] != 0){
            $fasilitas_list = $_POST['fasilitas_list'];
            $fasilitas_list = explode( ',', $fasilitas_list );
        }
		// $mandatory = $this->input->post('is_mandatory');
		// if(!$mandatory){
		// 	$data['is_mandatory'] = 0;
		// }else{
		// 	$data['is_mandatory'] = 1;
		// }

		// if($id == 0){
		// 	$success = $this->handling->insert($data);
		// }else{

		$success = $this->hotel->update($data, $id);
		// }
		// die();
		if($success){
			if(!empty($_FILES['path']['name']))
		    {
		    	$hotel = $this->hotel->get($id);
		    	if($hotel->path){
		    		unlink('assets/images/hotel/'.$id.'/'.$hotel->path);
		    	}
		       	$this->_do_upload($id);
		       // die();
		    }
			if($fasilitas_list != 0){
                $query = $this->fasilitas_hotel_hotel->where(array("hotel_id"=>$id))->get_all();
                $result = [];
                if($query){
	                foreach ($query as $row) {
	                    array_push($result, $row->fasilitas_id);
                	}
           		}
                // print_r($result);
                // print_r($trc);
                $added = array_values(array_diff($fasilitas_list,$result));
                // echo"added";
                // print_r($added);
                if(count($added)>0){
                    for ($i=0; $i < count($added); $i++) { 
                        $data1 = [];
                        $data1['hotel_id'] = $id;
                        $data1['fasilitas_id'] = $added[$i];
                        $this->fasilitas_hotel_hotel->insert($data1);
                    }
                }
                // echo"deleted";
            	$deleted = array_values(array_diff($result,$fasilitas_list));
            // print_r($deleted);
                if(count($deleted)>0){
                    for ($i=0; $i < count($deleted); $i++) { 
                        $this->fasilitas_hotel_hotel->delete(array('hotel_id'=>$id,'fasilitas_id'=>$deleted[$i]));
                    }
                }
            }else{
            	$this->fasilitas_hotel_hotel->delete(array('hotel_id'=>$id));
            }
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Hotel berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Hotel gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/hotel_admin"));
	}
	function get_kota(){
		$this->layout = false;
		$this->load->model(array('kota'));
		$data = $this->kota->get_all();
		echo json_encode($data);
	}
	function save(){
		$this->layout = false;
		$this->load->model(array('kamar_hotel','fasilitas_hotel_hotel'));
		// print_r($this->input->post());
		// die();
		// $mode = $this->input->post('mode');
		// print_r($_FILES['group-a']);
		// die();

		$data_post = array(
					'nama' => $this->input->post('nama'),
					'alamat' => $this->input->post('alamat'),
					'bintang' => $this->input->post('bintang'),
					'kota' => $this->input->post('kota'),
					'keterangan' => $this->input->post('keterangan'),
					'deskripsi' => $this->input->post('deskripsi'),
					'longitude' => $this->input->post('longitude'),
					'latitude' => $this->input->post('latitude'),
					);
		// if($mode == 'add'){
			// if(!empty($_FILES['image']['name']))
	  //       {
	  //           $upload = $this->_do_upload();
				// $data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);

	        // }
		$fasilitas_list = 0;
		if($_POST['fasilitas_list'] != 0){
            $fasilitas_list = $_POST['fasilitas_list'];
            $fasilitas_list = explode( ',', $fasilitas_list );
        }
        // print_r($fasilitas_list);
        // die();
		$success = $this->hotel->insert($data_post);
		if($success && !empty($_FILES['foto']['name'][0])){
			// print_r($_FILES['foto']['name'][0]);
			$this->upload_foto_hotel('foto',$success);

		}
		if($success){
			if(!empty($_FILES['path']['name']))
		    {
		       $this->_do_upload($success);
		       // die();
		    }
		    
			for ($i=0; $i < count($this->input->post('group-a')); $i++) { 
				# code...
				$data_kamar = array(
					'hotel_id' => $success,
					'tipe_kamar' => $this->input->post('group-a')[$i]['tipe_kamar'],
					'kapasitas' => $this->input->post('group-a')[$i]['kapasitas'],
					'jenis_makanan' => $this->input->post('group-a')[$i]['jenis_makanan']
					);
				$success_kamar = $this->kamar_hotel->insert($data_kamar);
				if($success_kamar && !empty($_FILES['group-a']['name'][$i]['foto_kamar'][0])){
					// for ($j=0; $j < count($_FILES['group-a']['name'][$i]['foto_kamar']) ; $j++) { 
						# code...
						$this->upload_foto_kamar_hotel($i,$success_kamar);
					// }
				}
			}
			if($fasilitas_list != 0){
                // $last_id = $succ;
                for ($i=0; $i < count($fasilitas_list); $i++) {
                    $data1 = [];
                    $data1['hotel_id'] = $success;
                    $data1['fasilitas_id'] = $fasilitas_list[$i];
                    $this->fasilitas_hotel_hotel->insert($data1);
                }
            }
		}
			// print_r($success);
			// die();

		
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Hotel berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Hotel gagal disimpan. "));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/hotel_admin"));
	}
	
	function delete($id){
		$hotel = $this->hotel->get($id);
	   	
		if($this->hotel->delete($id)){
			$this->delete_folder('assets/images/hotel/'.$id.'/');
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Hotel berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Hotel gagal dihapus."));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

    public function upload_foto_hotel($file,$id)
	{
		$this->load->model(array('gambar_hotel'));
		$cpt = count($_FILES[$file]['name']);
	    if($cpt > 0){
			$output_dir = 'assets/images/hotel/'.$id.'/';
	        if(!is_dir($output_dir)) {
	            mkdir($output_dir);
	        }
	        $foto = array();
	        $foto['upload_path']          = 'assets/images/hotel/'.$id.'/';
	        $foto['allowed_types']        = 'gif|jpg|png';
	        //$foto['max_size']             = 100; //set max size allowed in Kilobyte
	        //$foto['max_width']            = 1000; // set max width image allowed
	        //$foto['max_height']           = 1000; // set max height allowed
	        $foto['file_name']            = round(microtime(true) * 1000)+rand(5,1000); //just milisecond timestamp fot unique name
	 		
			$files = $_FILES;
		    $this->load->library('upload', $foto);
	    
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES[$file]['name']= $files[$file]['name'][$i];
		        $_FILES[$file]['type']= $files[$file]['type'][$i];
		        $_FILES[$file]['tmp_name']= $files[$file]['tmp_name'][$i];
		        $_FILES[$file]['error']= $files[$file]['error'][$i];
		        $_FILES[$file]['size']= $files[$file]['size'][$i];    

		        $this->upload->initialize($foto);
		        // $this->upload->do_upload('files[]');
		        if (!$this->upload->do_upload($file))
		        {  
		            $data['inputerror'][] = 'image';
		            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
		            $data['status'] = FALSE;
		            echo json_encode($data);
		            exit();
		        }else{
		        	$data_post = array(
		        				'hotel_id'=>$id,
		        				'nama_file'=>$this->upload->data('file_name')
		        				);
		        	$this->gambar_hotel->insert($data_post);
		        }

		    }
		}
	}
	public function upload_foto_kamar_hotel($x,$id)
	{
		$this->load->model(array('gambar_kamar_hotel'));
		$cpt = count($_FILES['group-a']['name'][$x]['foto_kamar']);
	    // if($cpt > 0){
			$output_dir = 'assets/images/hotel/room/'.$id.'/';
	        if(!is_dir($output_dir)) {
	            mkdir($output_dir);
	        }
	        $kamar = array();
	        $kamar['upload_path']          = 'assets/images/hotel/room/'.$id.'/';
	        $kamar['allowed_types']        = 'gif|jpg|png';
	        //$kamar['max_size']             = 100; //set max size allowed in Kilobyte
	        //$kamar['max_width']            = 1000; // set max width image allowed
	        //$kamar['max_height']           = 1000; // set max height allowed
	        $kamar['file_name']            = round(microtime(true) * 1000)+rand(5,1000); //just milisecond timestamp fot unique name
	 
		    $this->load->library('upload', $kamar, 'kamar');

		    $files = $_FILES;
	    
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES['kamar_hotel']['name']= $files['group-a']['name'][$x]['foto_kamar'][$i];
		        $_FILES['kamar_hotel']['type']= $files['group-a']['type'][$x]['foto_kamar'][$i];
		        $_FILES['kamar_hotel']['tmp_name']= $files['group-a']['tmp_name'][$x]['foto_kamar'][$i];
		        $_FILES['kamar_hotel']['error']= $files['group-a']['error'][$x]['foto_kamar'][$i];
		        $_FILES['kamar_hotel']['size']= $files['group-a']['size'][$x]['foto_kamar'][$i];    

		        $this->upload->initialize($kamar);
		        // $this->upload->do_upload('files[]');
		        if (!$this->upload->do_upload('kamar_hotel'))
		        {  
		            $data['inputerror'][] = 'image';
		            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
		            $data['status'] = FALSE;
		            echo json_encode($data);
		            exit();
		        }else{
		        	$data_post = array(
		        				'kamar_hotel_id'=>$id,
		        				'nama_file'=>$this->upload->data('file_name')
		        				);
		        	$this->gambar_kamar_hotel->insert($data_post);
		        }

		    // }
		}
	}
	function delete_folder($path)
	{
	    if (is_dir($path) === true)
	    {
	        $files = array_diff(scandir($path), array('.', '..'));

	        foreach ($files as $file)
	        {
	            $this->delete_folder(realpath($path) . '/' . $file);
	        }

	        return rmdir($path);
	    }

	    else if (is_file($path) === true)
	    {
	        return unlink($path);
	    }

	    return false;
	}
	public function get_gallery(){
		$this->layout = false;
		$this->load->model('gambar_hotel');
		$id = $this->input->post('id');
        $result = $this->gambar_hotel->where(array("hotel_id"=>$id))->get_all();

        echo json_encode($result);
	}
	public function get_kamar(){
		$this->layout = false;
		$this->load->model('kamar_hotel');
		$id = $this->input->post('idx');
        $result = $this->kamar_hotel->where(array("hotel_id"=>$id))->get_all();

        echo json_encode($result);
	}
	function hapus_gambar(){
		$this->layout = false;
		$id = $this->input->post('id');
		$this->load->model('gambar_hotel');
        $foto = 'assets/images/hotel/'.$this->input->post('id_provider').'/'.$this->input->post('foto');
        // print_r($foto);
        if($this->gambar_hotel->delete($id)){
            
            if(file_exists($foto)){
            	unlink($foto);
                echo "1";
            }else{
                echo "1";
            }
        }
	}
	function post_gambar(){
		$this->layout = false;
		
		$id = $this->input->post('id');
		if($this->upload_foto_hotel('image',$id)){
			echo json_encode(array('id' => $id,'tipe' => 'hotel'));
			// echo $id;
		}else{
			echo json_encode(array('id' => $id,'tipe' => 'hotel'));
		}

	}
	public function post_kamar(){
		$this->layout = false;
    	$id = $this->input->post('id');
    	$this->load->model('kamar_hotel');
    	for ($i=0; $i <count($this->input->post('tipe_kamar')) ; $i++) { 
    		$data['tipe_kamar'] = $this->input->post('tipe_kamar')[$i];
    		$data['kapasitas'] = $this->input->post('kapasitas')[$i];
    		$data['hotel_id'] = $this->input->post('id');
    		$data['jenis_makanan'] =  $this->input->post('jenis_makanan')[$i];
    		$id_kamar = $this->input->post('id_kamar')[$i];
    		if($id_kamar!=0){
    			$this->kamar_hotel->update($data,$id_kamar);
    		}else{
    			$this->kamar_hotel->insert($data);
    		}
    	}
    	echo"1";
    }
	public function delete_kamar(){
		$this->layout = false;
		$this->load->model('kamar_hotel');
        $id = $this->input->post('id');
        if($this->kamar_hotel->delete($id)){
        	$this->delete_folder('assets/images/hotel/kamar/'.$id.'/');
            echo "1";
        }else{
            echo "0";
        }
    }
    public function get_gallery_kamar(){
		$this->layout = false;
		$this->load->model('gambar_kamar_hotel');
		$id = $this->input->post('id');
        $result = $this->gambar_kamar_hotel->where(array("kamar_hotel_id"=>$id))->get_all();

        echo json_encode($result);
	}
	function post_gambar_kamar(){
		$this->layout = false;
		
		$id = $this->input->post('id');
		if($this->upload_foto_kamar('image',$id)){
			echo json_encode(array('id' => $id,'tipe' => 'kamar'));
			// echo $id;
		}else{
			echo json_encode(array('id' => $id,'tipe' => 'kamar'));
		}

	}
	public function upload_foto_kamar($file,$id)
	{
		$this->load->model(array('gambar_kamar_hotel'));
		$cpt = count($_FILES[$file]['name']);
	    if($cpt > 0){
			$output_dir = 'assets/images/hotel/room/'.$id.'/';
	        if(!is_dir($output_dir)) {
	            mkdir($output_dir);
	        }
	        $foto = array();
	        $foto['upload_path']          = 'assets/images/hotel/room/'.$id.'/';
	        $foto['allowed_types']        = 'gif|jpg|png';
	        //$foto['max_size']             = 100; //set max size allowed in Kilobyte
	        //$foto['max_width']            = 1000; // set max width image allowed
	        //$foto['max_height']           = 1000; // set max height allowed
	        $foto['file_name']            = round(microtime(true) * 1000)+rand(5,1000); //just milisecond timestamp fot unique name
	 		
			$files = $_FILES;
		    $this->load->library('upload', $foto);
	    
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES[$file]['name']= $files[$file]['name'][$i];
		        $_FILES[$file]['type']= $files[$file]['type'][$i];
		        $_FILES[$file]['tmp_name']= $files[$file]['tmp_name'][$i];
		        $_FILES[$file]['error']= $files[$file]['error'][$i];
		        $_FILES[$file]['size']= $files[$file]['size'][$i];    

		        $this->upload->initialize($foto);
		        // $this->upload->do_upload('files[]');
		        if (!$this->upload->do_upload($file))
		        {  
		            $data['inputerror'][] = 'image';
		            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
		            $data['status'] = FALSE;
		            echo json_encode($data);
		            exit();
		        }else{
		        	$data_post = array(
		        				'kamar_hotel_id'=>$id,
		        				'nama_file'=>$this->upload->data('file_name')
		        				);
		        	$this->gambar_kamar_hotel->insert($data_post);
		        }

		    }
		}
	}
	function hapus_gambar_kamar(){
		$this->layout = false;
		$id = $this->input->post('id');
		$this->load->model('gambar_kamar_hotel');
        $foto = 'assets/images/hotel/room/'.$this->input->post('id_provider').'/'.$this->input->post('foto');
        // print_r($foto);
        if($this->gambar_kamar_hotel->delete($id)){
            
            if(file_exists($foto)){
            	unlink($foto);
                echo "1";
            }else{
                echo "1";
            }
        }
	}
	private function _do_upload($id)
    {
    	$output_dir = 'assets/images/hotel/'.$id.'/';
        if(!is_dir($output_dir)) {
            mkdir($output_dir);
        }
        $config['upload_path']          = 'assets/images/hotel/'.$id.'/';
        $config['allowed_types']        = 'gif|jpg|png';
        //$config['max_size']             = 100; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = "path_hotel_"+$id; //just milisecond timestamp fot unique name
 
        $this->load->library('upload', $config);
 
        if(!$this->upload->do_upload('path')) //upload and validate
        {
            $data['inputerror'][] = 'image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        $data['path'] =  $this->upload->data('file_name');
        $this->hotel->update($data, $id);
    }
}
