<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengguna_admin extends Common {
	function __construct() {
		parent::__construct("agent_admin");

		$this->load->model(array('user'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/pengguna');
		$this->styles 			= array();
		$this->title 			= "Agent";
		$this->menu 			= "agent";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Pengguna',
			'page_description' => 'Pengolahan data pengguna',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Pengguna'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('flashdata'	=> $this->session->flashdata('form_msg'));

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."pengguna/index",$data);
	}

	function get_list(){
		$this->layout = false;
		$this->load->library('datatables');

        $this->datatables->select('nama,username,email,role_id,created_at');

        $this->datatables->from('user');
        echo $this->datatables->generate('json','');
	}
	function save(){
		$this->layout = FALSE;
		$user = $this->admin_session->get();
		$id = $this->input->post('id');
		$email = $this->input->post('email');
		$nama = $this->input->post('nama'); //create user
		$password = $this->input->post('new_password');
		$role_id = $this->input->post('role_id');
		$is_active = $this->input->post('is_active');
		if($id == 0){
			if(!$this->user->is_email_exist($email)){ //check email is not exis
				$user_id = $this->user->insert(array(
				    'email' => $email,
				    'password' => $password,
				    'nama' => $nama,
				    'role_id' => $role_id
				));
			}else{
				$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Failed to create new user. Email already exist."));
			}
		}else{
			$data_user = array(
				    'email' => $email,
				    'nama' => $nama,
				    // 'password' => $password,
				    'role_id' => $role_id);
			if($password != ''){
				$data_user['password'] = $password;
			}
			
			$user_id = $this->user->update($data_user,$id);
		}
		echo json_encode(array('url' => base_url().PATH_TO_ADMIN."pengguna_admin"));
	}
	function check_email(){
		$this->layout = false; 
		$email = $_POST['email'];

		$data = $this->user->where('email', $email)->get();
		if ($data) {
			$result = false;
		}else{
			$result = true;
		}


		echo json_encode($result);
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array(
			'detail' 	 	=> $this->user->get($id)
			);
		echo json_encode($data);
	}

	// function save(){
	// 	$this->layout = false;

	// 	$id 		= $this->input->post('id');
	// 	$email 		= $this->input->post('email');
	// 	$username 	= strtolower($this->input->post('nama_depan')).$this->generateRandomString(3);
	// 	$password 	= $this->generateRandomString(10);
	// 	$promo_code = $this->generatePromoCode(6);

	// 	$dataUser = array(
	// 		'username' 	=> $username,
	// 		'email' 	=> $email,
	// 		'password' 	=> md5($password),
	// 		'role_id' 	=> 3,
	// 		'status_id' => 1
	// 	);

	// 	if($id == 0){
	// 		if ($user = $this->user->insert($dataUser)) {
	// 			$data = array(
	// 				'user_id' 		=> $user,
	// 				'promo_code'	=> $promo_code,
	// 				'is_active'		=> $this->input->post('status')
	// 			);

	// 			$this->agent->insert($data);
	// 			$this->send_email($email, $password, $promo_code, $username);

	// 			$dataProfile = array(
	// 				'user_id' 		=> $user,
	// 				'nama_depan'	=> $this->input->post('nama_depan'),
	// 				'nama_tengah'	=> $this->input->post('nama_tengah'),
	// 				'nama_belakang' => $this->input->post('nama_belakang'),
	// 				'tempat_lahir'	=> $this->input->post('tempat_lahir'),
	// 				'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
	// 				'ktp'		=> $this->input->post('ktp'),
	// 				'paspor'		=> $this->input->post('paspor')
	// 			);
	// 			$this->profile->insert($dataProfile);	
	// 		} 
	// 		$success = true;
	// 	}else{
	// 		$data = array(
	// 			'is_active'		=> $this->input->post('status')
	// 		);

	// 		$this->agent->where('user_id', $id)->update($data);

	// 		$dataProfile = array(
	// 			'nama_depan'	=> $this->input->post('nama_depan'),
	// 			'nama_tengah'	=> $this->input->post('nama_tengah'),
	// 			'nama_belakang' => $this->input->post('nama_belakang'),
	// 			'tempat_lahir'	=> $this->input->post('tempat_lahir'),
	// 			'tanggal_lahir'	=> $this->input->post('tanggal_lahir'),
	// 			'ktp'		=> $this->input->post('ktp'),
	// 			'paspor'		=> $this->input->post('paspor')
	// 		);
	// 		$success = $this->profile->where('user_id', $id)->update($dataProfile);
	// 	}

	// 	if($success){
	// 		$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Agent berhasil disimpan."));
	// 	}else{
	// 		$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Agent gagal disimpan. "));
	// 	}

	// 	echo json_encode(array('url' => base_url().ADMIN_DIR."ui/agent_admin"));
	// }

	function generateRandomString($length = null) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}


	function delete($id){
		$this->layout = false;
		//$id = $this->kurs->get('id');
	
		if($this->user->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Pengguna berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Pengguna pendidikan gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/pengguna_admin"));	
	}
}
