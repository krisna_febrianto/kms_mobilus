<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rute_penerbangan_admin extends Common {
	function __construct() {
		parent::__construct("rute_penerbangan_admin");

		$this->load->model(array('jenis_paket_umroh','rute_penerbangan','penerbangan','bandara','maskapai','kurs'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/rute_penerbangan');
		$this->styles 			= array();
		$this->title 			= "Rute Penerbangan";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Rute Penerbangan',
			'page_description' => 'tambah, ubah, hapus Rute Penerbangan',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Rute Penerbangan'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		// print_r($data_breadcrumb);die;

		$data = array('jenis_paket_umroh' => $this->jenis_paket_umroh->get_all(),
					  'flashdata'	=> $this->session->flashdata('form_msg')
					);


		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."rute_penerbangan/index",$data);
	}
	function get_datatables(){
		$this->layout = FALSE;
		$this->load->library('datatables');
        $this->datatables->select('rute_penerbangan.*, CONCAT(bandara_berangkat1.kode, " - ",bandara_tujuan_berangkat1.kode) as tampil_berangkat1, CONCAT(bandara_berangkat2.kode, " - ",bandara_tujuan_berangkat2.kode) as tampil_berangkat2, CONCAT(bandara_plus1.kode, " - ", bandara_tujuan_plus1.kode) as tampil_plus1, CONCAT(bandara_plus2.kode, " - ",bandara_tujuan_plus2.kode) as tampil_plus2, CONCAT(bandara_pulang1.kode, " - ",bandara_tujuan_pulang1.kode) as tampil_pulang1, CONCAT(bandara_pulang2.kode, " - ", bandara_tujuan_pulang2.kode) as tampil_pulang2, jenis_paket_umroh.nama as jenis_paket_umroh_nama, maskapai_berangkat1.logo as berangkat1_logo, maskapai_berangkat2.logo as berangkat2_logo, maskapai_plus1.logo as plus1_logo, maskapai_pulang1.logo as pulang1_logo, maskapai_pulang2.logo as pulang2_logo, maskapai_berangkat1.logo as berangkat1_logo, DATE_FORMAT(rute_penerbangan.tanggal_berangkat, "%d-%m-%Y") as tanggal_berangkat_format, DATE_FORMAT(rute_penerbangan.tanggal_pulang, "%d-%m-%Y") as tanggal_pulang_format,  DATE_FORMAT(rute_penerbangan.tanggal_berangkat_plus, "%d-%m-%Y") as tanggal_berangkat_plus_format');
        $this->datatables->from('rute_penerbangan');
        $this->datatables->from('rute_penerbangan');

        $this->datatables->join('penerbangan as berangkat1' ,'rute_penerbangan.p_berangkat1 = berangkat1.id', 'left');
        $this->datatables->join('penerbangan as berangkat2' ,'rute_penerbangan.p_berangkat2 = berangkat2.id', 'left');
        $this->datatables->join('penerbangan as plus1' ,'rute_penerbangan.p_plus1 = plus1.id', 'left');
        $this->datatables->join('penerbangan as plus2' ,'rute_penerbangan.p_plus2 = plus2.id', 'left');
        $this->datatables->join('penerbangan as pulang1' ,'rute_penerbangan.p_pulang1 = pulang1.id', 'left');
        $this->datatables->join('penerbangan as pulang2' ,'rute_penerbangan.p_pulang2 = pulang2.id', 'left');

        $this->datatables->join('bandara as bandara_berangkat1' ,'berangkat1.bandara_berangkat_id = bandara_berangkat1.id', 'left');
        $this->datatables->join('bandara as bandara_berangkat2' ,'berangkat2.bandara_berangkat_id = bandara_berangkat2.id', 'left');
        $this->datatables->join('bandara as bandara_plus1' ,'plus1.bandara_berangkat_id = bandara_plus1.id', 'left');
        $this->datatables->join('bandara as bandara_plus2' ,'plus2.bandara_berangkat_id = bandara_plus2.id', 'left');
        $this->datatables->join('bandara as bandara_pulang1' ,'pulang1.bandara_berangkat_id = bandara_pulang1.id', 'left');
        $this->datatables->join('bandara as bandara_pulang2' ,'pulang2.bandara_berangkat_id = bandara_pulang2.id', 'left');

        $this->datatables->join('bandara as bandara_tujuan_berangkat1' ,'berangkat1.bandara_tujuan_id = bandara_tujuan_berangkat1.id', 'left');
        $this->datatables->join('bandara as bandara_tujuan_berangkat2' ,'berangkat2.bandara_tujuan_id = bandara_tujuan_berangkat2.id', 'left');
        $this->datatables->join('bandara as bandara_tujuan_plus1' ,'plus1.bandara_tujuan_id = bandara_tujuan_plus1.id', 'left');
        $this->datatables->join('bandara as bandara_tujuan_plus2' ,'plus2.bandara_tujuan_id = bandara_tujuan_plus2.id', 'left');
        $this->datatables->join('bandara as bandara_tujuan_pulang1' ,'pulang1.bandara_tujuan_id = bandara_tujuan_pulang1.id', 'left');
        $this->datatables->join('bandara as bandara_tujuan_pulang2' ,'pulang2.bandara_tujuan_id = bandara_tujuan_pulang2.id', 'left');
        
        $this->datatables->join('maskapai as maskapai_berangkat1', 'berangkat1.maskapai_id = maskapai_berangkat1.id', 'left');
        $this->datatables->join('maskapai as maskapai_berangkat2', 'berangkat2.maskapai_id = maskapai_berangkat2.id', 'left');
        $this->datatables->join('maskapai as maskapai_plus1', 'plus1.maskapai_id = maskapai_plus1.id', 'left');
        $this->datatables->join('maskapai as maskapai_plus2', 'plus2.maskapai_id = maskapai_plus2.id', 'left');
        $this->datatables->join('maskapai as maskapai_pulang1', 'pulang1.maskapai_id = maskapai_pulang1.id', 'left');
        $this->datatables->join('maskapai as maskapai_pulang2', 'pulang2.maskapai_id = maskapai_pulang2.id', 'left');

        $this->datatables->join('jenis_paket_umroh' ,'rute_penerbangan.jenis_paket_umroh_id = jenis_paket_umroh.id');
        echo $this->datatables->generate('json','');	
	}
	function get_paket_umroh($id){
		$this->layout = FALSE;
		$paket = $this->jenis_paket_umroh->get($id);
		echo json_encode($paket);	
	}
	function get_rute($tanggal_berangkat,$is_edit,$ids){
		$this->layout = FALSE;
		$data = [];
		$date = new DateTime($tanggal_berangkat);
		$date->modify("+7 DAYS");
		$jam_tanggal = $date->format('Y-m-d');
		$penerbangan = $this->penerbangan->where(array('tanggal_berangkat >='=>$tanggal_berangkat,'tanggal_berangkat <='=>$jam_tanggal))->order_by('tanggal_berangkat', $order = 'ASC')->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get_all();
		if ($is_edit != 0) {
			$penerbangan_select = $this->penerbangan->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get($ids);
			if ($penerbangan_select->tanggal_berangkat == $penerbangan_select->tanggal_tiba) {
				$tanggal = $penerbangan_select->tanggal_berangkat;
			}else{
				$tanggal = $penerbangan_select->tanggal_berangkat." - ".$penerbangan_select->tanggal_tiba;
			}
			$a = array(
				'id' => $penerbangan_select->id,
				'text' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($penerbangan_select->jam_berangkat))." - ".date("H:i", strtotime($penerbangan_select->jam_tiba))."</small><br><small>".$penerbangan_select->mata_uang->symbol." ".number_format($penerbangan_select->harga)."</small></div>",

				'select' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."</div>",
				);
			array_push($data, $a);
		}
		if ($penerbangan) {
			if ($is_edit == 0) {
				$a = array('id' => 'default','text' => '<div>-- Pilih Penerbangan --</div>', "select" => '<div>-- Pilih Penerbangan --</div>');
				array_push($data, $a);
			}
			foreach ($penerbangan as $key => $value) {
				if ($value->id != $ids) {
					if ($value->tanggal_berangkat == $value->tanggal_tiba) {
						$tanggal = $value->tanggal_berangkat;
					}else{
						$tanggal = $value->tanggal_berangkat." - ".$value->tanggal_tiba;
					}
				 	$event = array(
						'id' => $value->id,
						'text' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($value->jam_berangkat))." - ".date("H:i", strtotime($value->jam_tiba))."</small><br><small>".$value->mata_uang->symbol." ".number_format($value->harga)."</small></div>",

						'select' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."</div>",
					);
					array_push($data, $event);
				}
		 	}
		}
		if ($data) {
			echo json_encode($data);
		}else{
			echo 0;
		}
			
	}
	function get_berangkat2($tanggal_tiba,$jam_tiba,$id,$is_edit,$ids){
		$this->layout = FALSE;
		$data = [];
		$jam_tanggal =$tanggal_tiba." ".$jam_tiba;
		$jam7 =date('H:i', strtotime($jam_tiba)+25200);
		$date = new DateTime($jam_tanggal);
		$date->modify("+7 hours");
		$jam_tanggal7 = $date->format('Y-m-d');
		$penerbangan = $this->penerbangan->where(array('tanggal_berangkat >='=>$tanggal_tiba,'tanggal_berangkat <='=> $jam_tanggal7, 'jam_berangkat >=' => $jam_tiba, 'jam_berangkat <=' => $jam7))->order_by('tanggal_berangkat', $order = 'ASC')->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get_all();
		if ($is_edit != 0) {
			$penerbangan_select = $this->penerbangan->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get($ids);
			if ($penerbangan_select->tanggal_berangkat == $penerbangan_select->tanggal_tiba) {
				$tanggal = $penerbangan_select->tanggal_berangkat;
			}else{
				$tanggal = $penerbangan_select->tanggal_berangkat." - ".$penerbangan_select->tanggal_tiba;
			}
			$a = array(
				'id' => $penerbangan_select->id,
				'text' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($penerbangan_select->jam_berangkat))." - ".date("H:i", strtotime($penerbangan_select->jam_tiba))."</small><br><small>".$penerbangan_select->mata_uang->symbol." ".number_format($penerbangan_select->harga)."</small></div>",

				'select' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."</div>",
				);
			array_push($data, $a);
		}
		if ($penerbangan) {
			if ($is_edit == 0) {
				$a = array('id' => 'default','text' => '<div>-- Pilih Penerbangan --</div>', "select" => '<div>-- Pilih Penerbangan --</div>');
				array_push($data, $a);
			}
			foreach ($penerbangan as $key => $value) {
				if ($value->id != $id) {
					if ($value->tanggal_berangkat == $value->tanggal_tiba) {
						$tanggal = $value->tanggal_berangkat;
					}else{
						$tanggal = $value->tanggal_berangkat." - ".$value->tanggal_tiba;
					}
				 	$event = array(
						'id' => $value->id,
						'text' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($value->jam_berangkat))." - ".date("H:i", strtotime($value->jam_tiba))."</small><br><small>".$value->mata_uang->symbol." ".number_format($value->harga)."</small></div>",

						'select' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."</div>",
						'tanggal_berangkat' => $tanggal_tiba,
						'tt' => $jam_tanggal7
						);
					array_push($data, $event);
			 	}
			}
				
		}
		if ($data) {
			echo json_encode($data);
		}else{
			echo 0;
		}
			
	}
	function get_pulang1($tanggal_tiba,$durasi,$is_edit,$ids){
		$this->layout = FALSE;
		$data = [];
		$date = new DateTime($tanggal_tiba);
		$date->modify("+".$durasi."DAYS");
		$jam_tanggal7 = $date->format('Y-m-d');
		$date2 = new DateTime($jam_tanggal7);
		$date2->modify("+2 DAYS");
		$jam_tanggal1 = $date2->format('Y-m-d');
		$penerbangan = $this->penerbangan->where(array('tanggal_berangkat >='=>$jam_tanggal7,'tanggal_berangkat <='=> $jam_tanggal1))->order_by('tanggal_berangkat', $order = 'ASC')->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get_all();
		if ($is_edit != 0) {
			$penerbangan_select = $this->penerbangan->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get($ids);
			if ($penerbangan_select->tanggal_berangkat == $penerbangan_select->tanggal_tiba) {
				$tanggal = $penerbangan_select->tanggal_berangkat;
			}else{
				$tanggal = $penerbangan_select->tanggal_berangkat." - ".$penerbangan_select->tanggal_tiba;
			}
			$a = array(
				'id' => $penerbangan_select->id,
				'text' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($penerbangan_select->jam_berangkat))." - ".date("H:i", strtotime($penerbangan_select->jam_tiba))."</small><br><small>".$penerbangan_select->mata_uang->symbol." ".number_format($penerbangan_select->harga)."</small></div>",

				'select' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."</div>",
				);
			array_push($data, $a);
		}
		if ($penerbangan) {
			if ($is_edit == 0) {
				$a = array('id' => 'default','text' => '<div>-- Pilih Penerbangan --</div>', "select" => '<div>-- Pilih Penerbangan --</div>');
				array_push($data, $a);
			}
			foreach ($penerbangan as $key => $value) {
				if ($value->tanggal_berangkat == $value->tanggal_tiba) {
					$tanggal = $value->tanggal_berangkat;
				}else{
					$tanggal = $value->tanggal_berangkat." - ".$value->tanggal_tiba;
				}
			 	$event = array(
					'id' => $value->id,
					'text' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($value->jam_berangkat))." - ".date("H:i", strtotime($value->jam_tiba))."</small><br><small>".$value->mata_uang->symbol." ".number_format($value->harga)."</small></div>",

					'select' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."</div>",
					'tanggal_berangkat' => $tanggal_tiba,
					'tt' => $jam_tanggal7
					);
				array_push($data, $event);
			}
				
		}
		if ($data) {
			echo json_encode($data);
		}else{
			echo 0;
		}
			
	}
	function get_pulang2($tanggal_tiba,$jam_tiba,$id,$is_edit,$ids){
		$this->layout = FALSE;
		$data = [];
		$jam_tanggal =$tanggal_tiba." ".$jam_tiba;
		$jam7 =date('H:i', strtotime($jam_tiba)+25200);
		$date = new DateTime($jam_tanggal);
		$date->modify("+7 hours");
		$jam_tanggal7 = $date->format('Y-m-d');
		$penerbangan = $this->penerbangan->where(array('tanggal_berangkat >='=>$tanggal_tiba,'tanggal_berangkat <='=> $jam_tanggal7, 'jam_berangkat >=' => $jam_tiba, 'jam_berangkat <=' => $jam7))->order_by('tanggal_berangkat', $order = 'ASC')->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get_all();
		if ($is_edit != 0) {
			$penerbangan_select = $this->penerbangan->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get($ids);
			if ($penerbangan_select->tanggal_berangkat == $penerbangan_select->tanggal_tiba) {
				$tanggal = $penerbangan_select->tanggal_berangkat;
			}else{
				$tanggal = $penerbangan_select->tanggal_berangkat." - ".$penerbangan_select->tanggal_tiba;
			}
			$a = array(
				'id' => $penerbangan_select->id,
				'text' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($penerbangan_select->jam_berangkat))." - ".date("H:i", strtotime($penerbangan_select->jam_tiba))."</small><br><small>".$penerbangan_select->mata_uang->symbol." ".number_format($penerbangan_select->harga)."</small></div>",

				'select' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."</div>",
				);
			array_push($data, $a);
		}
		if ($penerbangan) {
			if ($is_edit == 0) {
				$a = array('id' => 'default','text' => '<div>-- Pilih Penerbangan --</div>', "select" => '<div>-- Pilih Penerbangan --</div>');
				array_push($data, $a);
			}
			foreach ($penerbangan as $key => $value) {
				if ($value->id != $id) {
					if ($value->tanggal_berangkat == $value->tanggal_tiba) {
						$tanggal = $value->tanggal_berangkat;
					}else{
						$tanggal = $value->tanggal_berangkat." - ".$value->tanggal_tiba;
					}
				 	$event = array(
						'id' => $value->id,
						'text' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($value->jam_berangkat))." - ".date("H:i", strtotime($value->jam_tiba))."</small><br><small>".$value->mata_uang->symbol." ".number_format($value->harga)."</small></div>",

						'select' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."</div>",
						'tanggal_berangkat' => $tanggal_tiba,
						'tt' => $jam_tanggal7
						);
					array_push($data, $event);
			 	}
			}
				
		}
		if ($data) {
			echo json_encode($data);
		}else{
			echo 0;
		}
			
	}
	function get_plus1($tanggal_berangkat,$id_berangat1,$id_berangat2,$is_edit,$ids){
		$this->layout = FALSE;
		$data = [];
		$date = new DateTime($tanggal_berangkat);
		$date->modify("+1 DAYS");
		$jam_tanggal = $date->format('Y-m-d');
		$penerbangan = $this->penerbangan->where(array('tanggal_berangkat >='=>$tanggal_berangkat,'tanggal_berangkat <='=>$jam_tanggal))->order_by('tanggal_berangkat', $order = 'ASC')->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get_all();
		if ($is_edit != 0) {
			$penerbangan_select = $this->penerbangan->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get($ids);
			if ($penerbangan_select->tanggal_berangkat == $penerbangan_select->tanggal_tiba) {
				$tanggal = $penerbangan_select->tanggal_berangkat;
			}else{
				$tanggal = $penerbangan_select->tanggal_berangkat." - ".$penerbangan_select->tanggal_tiba;
			}
			$a = array(
				'id' => $penerbangan_select->id,
				'text' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($penerbangan_select->jam_berangkat))." - ".date("H:i", strtotime($penerbangan_select->jam_tiba))."</small><br><small>".$penerbangan_select->mata_uang->symbol." ".number_format($penerbangan_select->harga)."</small></div>",

				'select' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."</div>",
				);
			array_push($data, $a);
		}
		if ($penerbangan) {
			if ($is_edit == 0) {
				$a = array('id' => 'default','text' => '<div>-- Pilih Penerbangan --</div>', "select" => '<div>-- Pilih Penerbangan --</div>');
				array_push($data, $a);
			}
			foreach ($penerbangan as $key => $value) {
				if ($value->id != $id_berangat1 && $value->id != $id_berangat2) {
					if ($value->tanggal_berangkat == $value->tanggal_tiba) {
						$tanggal = $value->tanggal_berangkat;
					}else{
						$tanggal = $value->tanggal_berangkat." - ".$value->tanggal_tiba;
					}
				 	$event = array(
						'id' => $value->id,
						'text' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($value->jam_berangkat))." - ".date("H:i", strtotime($value->jam_tiba))."</small><br><small>".$value->mata_uang->symbol." ".number_format($value->harga)."</small></div>",

						'select' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."</div>",
						);
					array_push($data, $event);
			 	}
			 }
		}
		if ($data) {
			echo json_encode($data);
		}else{
			echo 0;
		}
			
	}
	function get_plus2($tanggal_tiba,$jam_tiba,$id,$id_berangat1,$id_berangat2,$is_edit,$ids){
		$this->layout = FALSE;
		$data = [];
		$jam_tanggal =$tanggal_tiba." ".$jam_tiba;
		$jam7 =date('H:i', strtotime($jam_tiba)+25200);
		$date = new DateTime($jam_tanggal);
		$date->modify("+7 hours");
		$jam_tanggal7 = $date->format('Y-m-d');
		$penerbangan = $this->penerbangan->where(array('tanggal_berangkat >='=>$tanggal_tiba,'tanggal_berangkat <='=> $jam_tanggal7, 'jam_berangkat >=' => $jam_tiba, 'jam_berangkat <=' => $jam7))->order_by('tanggal_berangkat', $order = 'ASC')->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get_all();
		if ($is_edit != 0) {
			$penerbangan_select = $this->penerbangan->with_maskapai('fields:nama,logo')->with_bandara_berangkat('fields:nama')->with_bandara_tujuan('fields:nama')->with_mata_uang('fields:nama,symbol')->get($ids);
			if ($penerbangan_select->tanggal_berangkat == $penerbangan_select->tanggal_tiba) {
				$tanggal = $penerbangan_select->tanggal_berangkat;
			}else{
				$tanggal = $penerbangan_select->tanggal_berangkat." - ".$penerbangan_select->tanggal_tiba;
			}
			$a = array(
				'id' => $penerbangan_select->id,
				'text' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($penerbangan_select->jam_berangkat))." - ".date("H:i", strtotime($penerbangan_select->jam_tiba))."</small><br><small>".$penerbangan_select->mata_uang->symbol." ".number_format($penerbangan_select->harga)."</small></div>",

				'select' => "<div ><img src='".base_url("assets/images/maskapai/".$penerbangan_select->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$penerbangan_select->bandara_berangkat->nama." - ".$penerbangan_select->bandara_tujuan->nama."</div>",
				);
			array_push($data, $a);
		}
		if ($penerbangan) {
			if ($is_edit == 0) {
				$a = array('id' => 'default','text' => '<div>-- Pilih Penerbangan --</div>', "select" => '<div>-- Pilih Penerbangan --</div>');
				array_push($data, $a);
			}
			foreach ($penerbangan as $key => $value) {
				if ($value->id != $id && $value->id != $id_berangat1 && $value->id != $id_berangat2) {
					if ($value->tanggal_berangkat == $value->tanggal_tiba) {
						$tanggal = $value->tanggal_berangkat;
					}else{
						$tanggal = $value->tanggal_berangkat." - ".$value->tanggal_tiba;
					}
				 	$event = array(
						'id' => $value->id,
						'text' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' width='42' class='pull-left gap-right' style='margin-right: 3px; margin-bottom: 75px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."<br><small>".$tanggal."</small><br><small>".date("H:i", strtotime($value->jam_berangkat))." - ".date("H:i", strtotime($value->jam_tiba))."</small><br><small>".$value->mata_uang->symbol." ".number_format($value->harga)."</small></div>",

						'select' => "<div ><img src='".base_url("assets/images/maskapai/".$value->maskapai->logo)."' height='20' class='pull-left gap-right' style='margin-right: 3px; margin-top: 3px'/>".$value->bandara_berangkat->nama." - ".$value->bandara_tujuan->nama."</div>",
						'tanggal_berangkat' => $tanggal_tiba,
						'tt' => $jam_tanggal7
						);
					array_push($data, $event);
			 	}
			}
				
		}
		if ($data) {
			echo json_encode($data);
		}else{
			echo 0;
		}
			
	}
	function get_penerbangan($id){
		$this->layout = FALSE;
		$penerbangan = $this->penerbangan->get($id);
		echo json_encode($penerbangan);
	}
	function get_kurs(){
		$this->layout = FALSE;
		$kurs = $this->kurs->get_all();
		echo json_encode($kurs);
	}
	function edit($id){
		$this->layout = FALSE;
		$rute_penerbangan = $this->rute_penerbangan->with_jenis_paket_umroh('fields:durasi,is_plus')->rute_penerbangan->with_penerbangan_berangkat('fields:harga,mata_uang_id,tanggal_tiba,jam_tiba')->with_penerbangan_berangkat_2('fields:harga,mata_uang_id')->with_penerbangan_plus('fields:harga,mata_uang_id,tanggal_tiba,jam_tiba')->with_penerbangan_plus_2('fields:harga,mata_uang_id')->with_penerbangan_pulang('fields:harga,mata_uang_id,tanggal_tiba,jam_tiba')->with_penerbangan_pulang_2('fields:harga,mata_uang_id')->get($id);
		echo json_encode($rute_penerbangan);	
	}
	function save(){
		$this->layout = FALSE;
		$id = $this->input->post('id');
		 
		if ($this->input->post('penerbangan_berangkat2') != 'default') {
			$a = array(
				'p_berangkat2' => $this->input->post('penerbangan_berangkat2'),
			);
		}else{
			$a = array();
		}
		if ($this->input->post('penerbangan_pulang2') != 'default') {
			$b = array(
				'p_pulang2' => $this->input->post('penerbangan_pulang2'),
			);
		}else{
			$b = array();
		}
		if ($this->input->post('penerbangan_plus1') != 'default') {
			$c = array(
				'p_plus1' => $this->input->post('penerbangan_plus1'),
				'tanggal_berangkat_plus' => $this->input->post('tanggal_plus'),
			);
		}else{
			$c = array();
		}
		if ($this->input->post('penerbangan_plus2') != 'default') {
			$d = array(
				'p_plus2' => $this->input->post('penerbangan_plus2'),
			);
		}else{
			$d = array();
		}
		$e = array(
				'jenis_paket_umroh_id' => $this->input->post('jenis_paket_umroh'),
				'p_berangkat1' => $this->input->post('penerbangan_berangkat1'),
				'tanggal_berangkat' => $this->input->post('tanggal_berangkat_fix'),
				'p_pulang1' => $this->input->post('penerbangan_pulang1'),
				'tanggal_pulang' => $this->input->post('tanggal_pulang'),
				'tanggal_tiba' => $this->input->post('tanggal_tiba'),
				'harga' => $this->input->post('harga')
			);
		$data = array_merge($a,$b,$c,$d,$e);
		if($id == 0){
			$success = $this->rute_penerbangan->insert($data);
		}else{
			$success = $this->rute_penerbangan->update($data, $id);
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Rute Penerbangan berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Rute Penerbangan gagal disimpan. "));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/rute_penerbangan_admin"));

	}
	
	function delete($id){
		$this->layout = false;
	
		if($this->rute_penerbangan->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Rute penerbangan berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Rute penerbangan pendidikan gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/rute_penerbangan_admin"));
	}
	
	
}
