<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kamar_hotel_admin extends Common {
	function __construct() {
		parent::__construct("kamar_hotel_admin");

		$this->load->model(array('kamar_hotel','hotel','gambar_kamar_hotel','kota'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/kamar_hotel');
		$this->styles 			= array();
		$this->title 			= "Kamar Hotel";
		$this->menu = "kamar_hotel";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Kamar Hotel',
			'page_description' => 'tambah, ubah, hapus Kamar Hotel',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Kamar Hotel'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('kamar_hotel' => $this->kamar_hotel->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."kamar_hotel/index",$data);
	}

	function save(){
		$this->layout = FALSE;

		$id = $this->input->post('id');
		$data = array('tipe_kamar' => $this->input->post('tipe_kamar'),
					'jenis_makanan' => $this->input->post('jenis_makanan'),
					'kapasitas' => $this->input->post('kapasitas'),
					'hotel_id' => $this->input->post('hotel_id')
					);

		

		if($id == 0){
			$success = $this->kamar_hotel->insert($data);
		}else{
			$success = $this->kamar_hotel->update($data, $id);
		}

		// if($success){
		// 	$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Kamar Hotel berhasil disimpan."));
		// }else{
		// 	$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Kamar Hotel gagal disimpan. "));
		// }

		echo $this->input->post('hotel_id');
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array('detail' => $this->kamar_hotel->get($id));
		echo json_encode($data);		
	}

	function delete(){
		$this->layout = false;
		$this->load->model('kamar_hotel');
        $id = $this->input->get('id');
        if($this->kamar_hotel->delete($id)){
        	$this->delete_folder('assets/images/hotel/kamar/'.$id.'/');
            echo "1";
        }else{
            echo "0";
        }		
	}
	public function search_hotel()
    {
    	$this->layout = false;
     	$result = [];
        if(!empty($this->input->get("q"))){
            $keyword = $this->input->get("q");
            // $where = "name LIKE '%$keyword%' or student_id LIKE '%$keyword%'";
            $result = $this->hotel->fields('id,nama as text')->where('nama like','%'.$keyword.'%')->limit(10)->get_all();
        }
    	if(!$result){
    		$result = [];
    	}
        echo json_encode($result);
    }
    function get_kota(){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->kota->like('nama', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->kota->limit(null)->get_all();
            // print_r($json);
        }

        echo json_encode($json);
	}

	function get_hotel($kota){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		$newCity = str_replace('%20', ' ', $kota);

		if(!empty($keyword)){
            $json = $this->hotel->like('nama', $keyword)->limit(10)->where('kota', $newCity)->get_all();
        }else{
            $json = $this->hotel->limit(null)->where('kota', $newCity)->get_all();
            // print_r($json);
        }

        echo json_encode($json);
	}

	function get_kamar_select($hotel_id){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->kamar_hotel->like('nama', $keyword)->limit(10)->where('hotel_id', $hotel_id)->get_all();
        }else{
            $json = $this->kamar_hotel->limit(null)->where('hotel_id', $hotel_id)->get_all();
            // print_r($json);
        }
        $semua = [];
        $semua['id'] = 0;
        $semua['tipe_kamar'] = 'Semua Kamar';
        array_unshift($json,$semua);
        echo json_encode($json);
	}
    function get_kamar($id){
    	$this->layout = false;
    	$data = [];
		if($id){
		// $id = $this->input->get('id');
			// if($id_kamar != 0){
			// 	$data = array('data' => $this->kamar_hotel->where(array('hotel_id'=>$id,'id'=>$id_kamar))->get_all());
			// }else{
			// 	$data = array('data' => $this->kamar_hotel->where(array('hotel_id'=>$id))->get_all());
			// }

			$data = array('data' => $this->kamar_hotel->where(array('hotel_id'=>$id))->get_all());
			if(!$data){
				$data = [];
			}
		}
		echo json_encode($data);
    }
    function delete_folder($path)
	{
	    if (is_dir($path) === true)
	    {
	        $files = array_diff(scandir($path), array('.', '..'));

	        foreach ($files as $file)
	        {
	            $this->delete_folder(realpath($path) . '/' . $file);
	        }

	        return rmdir($path);
	    }

	    else if (is_file($path) === true)
	    {
	        return unlink($path);
	    }

	    return false;
	}
/*	function add(){
		$this->layout = false;
		$data = array('mode' 		=> 'add');
		$this->load->view(ADMIN_DIR."maskapai/form",$data);
	}
	
	function edit($id){
		$this->layout = false;
		$data = array('detail'  	=> $this->maskapai->get($id),
					  'mode'    	=> 'edit'
					  );
		$this->load->view(ADMIN_DIR."maskapai/form",$data);
	}

	function save(){
		$mode = $this->input->post('mode');
		
		$data_post = array('nama' => $this->input->post('nama'));
		if($mode == 'add'){
			if(!empty($_FILES['image']['name']))
	        {
	            $upload = $this->_do_upload();
				$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);

	        }

			$success = $this->maskapai->insert($data_post);
			// print_r($success);
			// die();
		}else{
			if(!empty($_FILES['image']['name']))
	        {
	            $upload = $this->_do_upload();
	             
	            //delete file
	            $maskapai = $this->maskapai->get($this->input->post('id'));
	            
	            if(file_exists('assets/images/logo_maskapai/'.$maskapai['logo']) && $maskapai['logo'])
	                unlink('assets/images/logo_maskapai/'.$maskapai['logo']);
				$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);

	        }
			$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);
			$success = $this->maskapai->update($data_post, $this->input->post('id'));
		}

		
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Maskapai berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Maskapai gagal disimpan. "));
		}
		redirect(base_url().ADMIN_DIR."maskapai");
	}
	
	function delete($id){
		$maskapai = $this->maskapai->get($id);
	    if(file_exists('assets/images/logo_maskapai/'.$maskapai['logo']) && $maskapai['logo'])
	            unlink('assets/images/logo_maskapai/'.$maskapai['logo']);
		if($this->maskapai->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Maskapai berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Maskapai pendidikan gagal dihapus."));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	 private function _do_upload()
    {
        $config['upload_path']          = 'assets/images/logo_maskapai/';
        $config['allowed_types']        = 'gif|jpg|png';
        //$config['max_size']             = 100; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 
        $this->load->library('upload', $config);
 
        if(!$this->upload->do_upload('image')) //upload and validate
        {
            $data['inputerror'][] = 'image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }*/
	
}
