<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mata_uang_admin extends Common {
	function __construct() {
		parent::__construct("mata_uang_admin");

		$this->load->model(array('mata_uang'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/mata_uang');
		$this->styles 			= array();
		$this->title 			= "Mata uang";
		$this->menu 			= "mata_uang";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Mata Uang',
			'page_description' => 'Daftar Mata uang',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Mata Uang'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('flashdata'	=> $this->session->flashdata('form_msg'));

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."mata_uang/index",$data);
	}

	function get_list(){
		$this->layout = false;
		$this->load->library('datatables');

        $this->datatables->select('*');

        $this->datatables->from('mata_uang');
        echo $this->datatables->generate('json','');
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array(
			'detail' 	 	=> $this->mata_uang->get($id)
			);
		echo json_encode($data);
	}

	function save(){
		$this->layout = false;

		$id = $this->input->post('id');

		$data = array(
			'nama' 		=> $this->input->post('nama'),
			'kode' 		=> $this->input->post('kode'),
			'symbol' 	=> $this->input->post('symbol')
		);

		if($id == 0){
			$success = $this->mata_uang->insert($data);	
		}else{
			$success = $this->mata_uang->update($data, $id);
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Mata uang berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Mata uang gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/mata_uang_admin"));
	}

	function delete($id){
		$this->layout = false;
		//$id = $this->kurs->get('id');
	
		if($this->mata_uang->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Mata uang berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Mata uang pendidikan gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/mata_uang_admin"));	
	}
}
