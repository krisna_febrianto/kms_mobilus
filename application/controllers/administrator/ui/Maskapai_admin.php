<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maskapai_admin extends Common {
	function __construct() {
		parent::__construct("maskapai_admin");

		$this->load->model(array('maskapai'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/maskapai');
		$this->styles 			= array('');
		$this->title 			= "Maskapai";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Maskapai',
			'page_description' => 'tambah, ubah, hapus Maskapai',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Maskapai'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		// print_r($data_breadcrumb);die;

		$data = array('maskapai' => $this->maskapai->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);


		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."maskapai/index",$data);
	}
	function add(){
		$this->layout = false;
		$data = array('mode' 		=> 'add');
		$this->load->view(ADMIN_DIR."maskapai/form",$data);
	}
	
	function edit($id){
		$this->layout = false;
		$data = array('detail'  	=> $this->maskapai->get($id),
					  'mode'    	=> 'edit'
					  );
		$this->load->view(ADMIN_DIR."maskapai/form",$data);
	}

	function save(){
		$mode = $this->input->post('mode');
		
		$data_post = array('nama' => $this->input->post('nama'));
		if($mode == 'add'){
			if(!empty($_FILES['image']['name']))
	        {
	            $upload = $this->_do_upload();
				$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);

	        }

			$success = $this->maskapai->insert($data_post);
			// print_r($success);
			// die();
		}else{
			if(!empty($_FILES['image']['name']))
	        {
	            $upload = $this->_do_upload();
	             
	            //delete file
	            $maskapai = $this->maskapai->get($this->input->post('id'));
	            
	            if(file_exists('assets/images/maskapai/'.$maskapai->logo) && $maskapai->logo)
	                unlink('assets/images/maskapai/'.$maskapai->logo);
				$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);

	        }
			$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);
			$success = $this->maskapai->update($data_post, $this->input->post('id'));
		}

		
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Maskapai berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Maskapai gagal disimpan. "));
		}
		redirect(base_url().ADMIN_DIR."ui/maskapai_admin");
	}
	
	function delete(){
		$this->layout = false;
		$id = $this->input->get('id');
		$maskapai = $this->maskapai->get($id);
	    if(file_exists('assets/images/maskapai/'.$maskapai->logo) && $maskapai->logo)
	            unlink('assets/images/maskapai/'.$maskapai->logo);
		if($this->maskapai->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Maskapai berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Maskapai pendidikan gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/maskapai_admin"));	
	}

	 private function _do_upload()
    {
        $config['upload_path']          = 'assets/images/maskapai/';
        $config['allowed_types']        = 'gif|jpg|png';
        //$config['max_size']             = 100; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 
        $this->load->library('upload', $config);
 
        if(!$this->upload->do_upload('image')) //upload and validate
        {
            $data['inputerror'][] = 'image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
	
}
