<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Konfirmasi_pembayaran_admin extends Common {
	function __construct() {
		parent::__construct("konfirmasi_pembayaran_admin");

		$this->load->model(array('konfirmasi_pembayaran','status_booking','user','paket_umroh','jenis_paket_umroh','booking'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/konfirmasi_pembayaran');
		$this->styles 			= array();
		$this->title 			= "Konfirmasi Pembayaran";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Konfirmasi Pembayaran',
			'page_description' => 'tambah, ubah, hapus Konfirmasi Pembayaran',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Konfirmasi Pembayaran'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		// print_r($data_breadcrumb);die;

		$data = array(
					  'flashdata'	=> $this->session->flashdata('form_msg')
					);


		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."konfirmasi_pembayaran/index",$data);
	}
	function get_datatables(){
		$this->layout = FALSE;
		$this->load->library('datatables');
        $this->datatables->select('konfirmasi_pembayaran.*,booking.no_booking as no_booking, booking.harga_total as harga, DATE_FORMAT(paket_umroh.tanggal_berangkat, "%d-%m-%Y") as tanggal_berangkat, jenis_paket_umroh.nama as nama_jenis_paket, user.username as username, status_booking.id as status_id, status_booking.tag as status_tag, status_booking.color as status_color');
        $this->datatables->from('konfirmasi_pembayaran');
        $this->datatables->join('booking','konfirmasi_pembayaran.booking_id = booking.id', 'left');
        $this->datatables->join('status_booking','booking.status_id = status_booking.id', 'left');
        $this->datatables->join('user','booking.user_id = user.id', 'left');
        $this->datatables->join('paket_umroh','booking.paket_umroh_id = paket_umroh.id', 'left');
        $this->datatables->join('jenis_paket_umroh','paket_umroh.jenis_id = jenis_paket_umroh.id', 'left');
        echo $this->datatables->generate('json','');	
	}
	function view($id){
		$this->layout = FALSE;
		$konfirmasi_pembayaran = $this->konfirmasi_pembayaran->with_booking()->with_paket_umroh()->get($id);
		$paket_umroh = $this->paket_umroh->get($konfirmasi_pembayaran->booking->paket_umroh_id);
		$jenis_paket_umroh = $this->jenis_paket_umroh->get($paket_umroh->jenis_id);

		$data = array(
			'detail'  => $konfirmasi_pembayaran,
			'status'	=> $this->status_booking->get($konfirmasi_pembayaran->booking->status_id),
			'user'	=> $this->user->get($konfirmasi_pembayaran->booking->user_id),
			'paket_umroh'	=> $paket_umroh,
			'jenis_paket_umroh'	=> $jenis_paket_umroh,
			);

		echo json_encode($data);	
	}
	function confirm($id){
		$this->layout = false;
		$konfirmasi_pembayaran = $this->konfirmasi_pembayaran->get($id);
		if($this->booking->update(array('status_id'  => 9), $konfirmasi_pembayaran->booking_id) && $this->konfirmasi_pembayaran->update(array('pesan'  => null), $id)){
			$is = array('is_view' => 1);
			$this->konfirmasi_pembayaran->update($is, $id);
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Pembayaran berhasil dikonfirmasi."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Pembayaran gagal dikonfirmasi."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/konfirmasi_pembayaran_admin"));

	}
	function reject(){
		$id = $this->input->post('id');
		$this->layout = false;
		$konfirmasi_pembayaran = $this->konfirmasi_pembayaran->with_booking('fields:user_id')->get($id);
		$user = $this->user->get($konfirmasi_pembayaran->booking->user_id);
		if($this->booking->update(array('status_id'  => 3), $konfirmasi_pembayaran->booking_id) && $this->konfirmasi_pembayaran->update(array('pesan'  => $this->input->post('pesan')), $id)){
		$this->load->library('CI_Mailer');
        $result = $this->ci_mailer->send(array(
            'to' => $user->email,
            'subject' => 'Pembayaran Ditolak',
            'message' => $this->input->post('pesan'),
        ), TRUE);
        	$is = array('is_view' => 1);
			$this->konfirmasi_pembayaran->update($is, $id);
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Pembayaran berhasil ditolak."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Pembayaran gagal ditolak."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/konfirmasi_pembayaran_admin"));

	}
}