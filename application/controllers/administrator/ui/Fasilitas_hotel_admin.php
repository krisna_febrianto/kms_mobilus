<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fasilitas_hotel_admin extends Common {
	function __construct() {
		parent::__construct("fasilitas_hotel_admin");

		$this->load->model(array('fasilitas_hotel'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/fasilitas_hotel');
		$this->styles 			= array();
		$this->title 			= "Fasilitas Hotel";
		$this->menu = "Fasilitas Hotel";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Fasilitas Hotel',
			'page_description' => 'tambah, ubah, hapus fasilitas',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Fasilitas Hotel'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('fasilitas_hotel' => $this->fasilitas_hotel->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."fasilitas_hotel/index",$data);
	}

	function save(){
		$this->layout = FALSE;

		$id = $this->input->post('id');
		$data = array('fasilitas' => $this->input->post('fasilitas'),
					'icon' => $this->input->post('icon')
					);

		if($id == 0){
			$success = $this->fasilitas_hotel->insert($data);
		}else{
			$success = $this->fasilitas_hotel->update($data, $id);
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Fasilitas hotel berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Fasilitas hotel gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/fasilitas_hotel_admin"));
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array('detail' => $this->fasilitas_hotel->get($id));
		echo json_encode($data);		
	}

	function delete(){
		$this->layout = false;
		$id = $this->input->get('id');
		$success = $this->fasilitas_hotel->delete($id);
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Fasilitas hotel berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Fasilitas hotel gagal dihapus. "));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/fasilitas_hotel_admin"));		
	}

/*	function add(){
		$this->layout = false;
		$data = array('mode' 		=> 'add');
		$this->load->view(ADMIN_DIR."maskapai/form",$data);
	}
	
	function edit($id){
		$this->layout = false;
		$data = array('detail'  	=> $this->maskapai->get($id),
					  'mode'    	=> 'edit'
					  );
		$this->load->view(ADMIN_DIR."maskapai/form",$data);
	}

	function save(){
		$mode = $this->input->post('mode');
		
		$data_post = array('nama' => $this->input->post('nama'));
		if($mode == 'add'){
			if(!empty($_FILES['image']['name']))
	        {
	            $upload = $this->_do_upload();
				$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);

	        }

			$success = $this->maskapai->insert($data_post);
			// print_r($success);
			// die();
		}else{
			if(!empty($_FILES['image']['name']))
	        {
	            $upload = $this->_do_upload();
	             
	            //delete file
	            $maskapai = $this->maskapai->get($this->input->post('id'));
	            
	            if(file_exists('assets/images/logo_maskapai/'.$maskapai['logo']) && $maskapai['logo'])
	                unlink('assets/images/logo_maskapai/'.$maskapai['logo']);
				$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);

	        }
			$data_post = array('nama' => $this->input->post('nama'),'logo' => $upload);
			$success = $this->maskapai->update($data_post, $this->input->post('id'));
		}

		
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Maskapai berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Maskapai gagal disimpan. "));
		}
		redirect(base_url().ADMIN_DIR."maskapai");
	}
	
	function delete($id){
		$maskapai = $this->maskapai->get($id);
	    if(file_exists('assets/images/logo_maskapai/'.$maskapai['logo']) && $maskapai['logo'])
	            unlink('assets/images/logo_maskapai/'.$maskapai['logo']);
		if($this->maskapai->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Maskapai berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Maskapai pendidikan gagal dihapus."));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	 private function _do_upload()
    {
        $config['upload_path']          = 'assets/images/logo_maskapai/';
        $config['allowed_types']        = 'gif|jpg|png';
        //$config['max_size']             = 100; //set max size allowed in Kilobyte
        //$config['max_width']            = 1000; // set max width image allowed
        //$config['max_height']           = 1000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 
        $this->load->library('upload', $config);
 
        if(!$this->upload->do_upload('image')) //upload and validate
        {
            $data['inputerror'][] = 'image';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }*/
	
}
