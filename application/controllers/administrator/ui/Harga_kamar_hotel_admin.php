<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Harga_kamar_hotel_admin extends Common {
	function __construct() {
		parent::__construct("harga_kamar_hotel_admin");

		$this->load->model(array('Kamar_hotel','hotel','kota', 'harga_kamar_hotel', 'mata_uang'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/harga_kamar_hotel' ,'fullcalendar.min');
		$this->styles 			= array('assets/global/plugins/fullcalendar/fullcalendar.min.css');
		$this->title 			= "Harga Kamar Hotel";
		$this->menu 			= "Harga Kamar Hotel";
	}

	function index($id_hotel = 0,$id_kamar = 0){
		$data_breadcrumb = [
			'page_title' => 'Harga Kamar Hotel',
			'page_description' => 'tambah, ubah, hapus Harga Kamar Hotel',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Harga Kamar Hotel'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);
		if($id_hotel && $id_kamar){
			$hotel = $this->hotel->get($id_hotel);
			$kamar = $this->Kamar_hotel->get($id_kamar);
		}else{
			$hotel = 0;
			$kamar = 0;
		}
		$data = array('kamar_hotel' => $this->Kamar_hotel->get_all(),
					'kamar' => $kamar,
					'hotel' => $hotel,
					'flashdata'	=> $this->session->flashdata('form_msg')
					);
		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."harga_kamar_hotel/index",$data);
	}

	function get_kota(){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->kota->like('nama', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->kota->limit(null)->get_all();
            // print_r($json);
        }

        echo json_encode($json);
	}

	function get_hotel($kota){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		$newCity = str_replace('%20', ' ', $kota);

		if(!empty($keyword)){
            $json = $this->hotel->like('nama', $keyword)->limit(10)->where('kota', $newCity)->get_all();
        }else{
            $json = $this->hotel->limit(null)->where('kota', $newCity)->get_all();
            // print_r($json);
        }

        echo json_encode($json);
	}

	function get_kamar($hotel_id, $mode=null, $id_kamar=null){
		$this->layout = false;

		$json = [];
		if ($mode != 1) {
			$keyword = $this->input->get("q");

			if(!empty($keyword)){
	            $json = $this->Kamar_hotel->like('nama', $keyword)->limit(10)->where('hotel_id', $hotel_id)->get_all();
	        }else{
	            $json = $this->Kamar_hotel->limit(null)->where('hotel_id', $hotel_id)->get_all();
	            // print_r($json);
	        }

        	$semua = [];
	        $semua['id'] = 'all';
	        $semua['tipe_kamar'] = 'Semua Kamar';
	        array_unshift($json,$semua);
        }else{
        	$json = $this->Kamar_hotel->limit(null)->where('hotel_id', $hotel_id)->get_all();
        }
       
        echo json_encode($json);
	}

	function get_kamar_perId($id_kamar){
		$this->layout = false;
		$json = $this->Kamar_hotel->get($id_kamar); 

		echo json_encode($json);
	}

	function get_mata_uang(){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->mata_uang->like('nama', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->mata_uang->limit(null)->get_all();
            // print_r($json);
        }

        echo json_encode($json);
	}

	function get_harga($id_kamar, $tanggal){
		$this->layout = false;

		$data = $this->harga_kamar_hotel->with_mata_uang()->where(array('kamar_hotel_id'=>$id_kamar, 'tanggal'=>$tanggal))->get();

		if ($data) {
			echo json_encode($data);
		}else{
			echo json_encode(null);
		}
	}

	function getDaftarHarga($kamar, $hotel_id){
		$this->layout = false;

		$data = [];
		if ($kamar == 'all') {
			// $query = $this->harga_kamar_hotel->with_mata_uang()->with_kamar_hotel()->get_all();
			$query = $this->harga_kamar_hotel->get_custom(array('hotel_id'=>$hotel_id))->result_array();
			foreach ($query as $key => $value) {
				//if ($value->kamar_hotel->hotel_id == $hotel_id) {
					$event = array(
						'id' => $value['id'],
						'kamar_hotel_id' => $value['kamar_hotel_id'],
						'kamar'=> $value['tipe_kamar'],
						//'title' => $value->mata_uang->symbol.". ".number_format($value->harga, 2),
						'start' => $value['tanggal'],
						//'end' => $value->tanggal,
						'tanggal' => $value['tanggal'],
						'harga' => $value['harga'],
						'mata_uang' => $value['kode_mata_uang']." (".$value['symbol'].")",
						'mata_uang_id' => $value['mata_uang_id'],
						'symbol' => $value['symbol']
					);
					array_push($data, $event);
				//}
			}
		}else{
			$query = $this->harga_kamar_hotel->with_mata_uang()->with_kamar_hotel()->where('kamar_hotel_id', $kamar)->get_all();
			if ($query != null) {
				foreach ($query as $key => $value) {
				$event = array(
					'id' => $value->id,
					'kamar_hotel_id' => $value->kamar_hotel_id,
					'kamar'=> $value->kamar_hotel->tipe_kamar,
					//'title' => $value->mata_uang->symbol.". ".number_format($value->harga, 2),
					'start' => $value->tanggal,
					//'end' => $value->tanggal,
					'tanggal' => $value->tanggal,
					'harga' => $value->harga,
					'mata_uang' => $value->mata_uang->kode." (".$value->mata_uang->symbol.")",
					'mata_uang_id' => $value->mata_uang_id,
					'symbol' => $value->mata_uang->symbol
				);
				array_push($data, $event);
				}
			}
		}
		
		echo json_encode($data);	

	}

	// function check_kamar(){
	// 	$this->layout = false; 
	// 	$tanggal = $this->input->post('tanggal');
	// 	$kamar_hotel_id = $this->input->post('kamar_hotel_id');

	// 	$data = $this->harga_kamar_hotel->where(array('kamar_hotel_id'=> $kamar_hotel_id, 'tanggal'=>$tanggal))->get();

	// 	if ($data) {
	// 		$result = false;
	// 	}else{
	// 		$result = true;
	// 	}
	// 	echo json_encode($result);
	// }

	function save($id){
		$this->layout = FALSE;

		if ($this->input->post('tipe_kamar[]') != 0) {
			$tipe_kamar = $this->input->post('tipe_kamar[]');
		}

		$harga 		= $this->input->post('harga[]');
		$mata_uang 	= $this->input->post('mata_uang[]');
		$start  	= new DateTime($this->input->post('start_date'));
		$end 		= new DateTime($this->input->post('end_date'));
		$end 		= $end->modify( '+1 day' );
		$dateRange 	= new DatePeriod($start, new DateInterval('P1D'), $end);
		
		//if($id == 0){
			for ($i=0; $i < count($tipe_kamar); $i++) { 
				foreach ($dateRange as $date) {
					$tanggal = $date->format('Y-m-d');
					$data = array(
						'kamar_hotel_id' 	=> $tipe_kamar[$i],
						'tanggal'			=> $tanggal,
						'harga'				=> $harga[$i],
						'mata_uang_id'		=> $mata_uang[$i]
					);

					$check_data = $this->harga_kamar_hotel
										->where(array('kamar_hotel_id'=>$tipe_kamar[$i], 'tanggal'=>$tanggal))
										->get();
					if ($check_data) {
						$this->harga_kamar_hotel
								->where(array('kamar_hotel_id'=>$tipe_kamar[$i], 'tanggal'=>$tanggal))
								->update($data);
					}else{
						$this->harga_kamar_hotel->insert($data);
					}
				}
			}
			$success = true;
		// }else{
		// 	for ($i=0; $i < count($tipe_kamar); $i++) {
		// 		foreach ($dateRange as $date) {
		// 			$tanggal = $date->format('Y-m-d');
		// 			$data = array(
		// 				'kamar_hotel_id' 	=> $tipe_kamar[$i],
		// 				'tanggal'			=> $tanggal,
		// 				'harga'				=> $harga[$i],
		// 				'mata_uang_id'		=> $mata_uang[$i]
		// 			);
		// 			$check_data = $this->harga_kamar_hotel
		// 								->where(array('kamar_hotel_id'=>$tipe_kamar[$i], 'tanggal'=>$tanggal))
		// 								->get();
		// 			if ($check_data) {
		// 				$this->harga_kamar_hotel
		// 						->where(array('kamar_hotel_id'=>$tipe_kamar[$i], 'tanggal'=>$tanggal))
		// 						->update($data);
		// 			}else{
		// 				$this->harga_kamar_hotel->insert($data);
		// 			}
					
		// 		}
		// 	}
		// 	$success = true;
		// }

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "harga berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "harga gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/harga_kamar_hotel_admin"));
		//echo json_encode($success);
	}

	function delete(){
		$this->layout = false;

		if ($this->input->get('tipe_kamar[]') != 0) {
			$tipe_kamar = $this->input->get('tipe_kamar[]');
		}
		$start  	= new DateTime($this->input->get('start_date'));
		$end 		= new DateTime($this->input->get('end_date'));
		$end 		= $end->modify( '+1 day' );
		$dateRange 	= new DatePeriod($start, new DateInterval('P1D'), $end);

		for ($i=0; $i < count($tipe_kamar); $i++) { 
			foreach ($dateRange as $date) {
				$this->harga_kamar_hotel->where(array('kamar_hotel_id'=>$tipe_kamar[$i], 'tanggal'=>$date->format('Y-m-d')))->delete();
			}
		}
		// $success = true;
		// if($success){
		// 	$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "harga berhasil dihapus."));
		// }else{
		// 	$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "harga pendidikan gagal dihapus."));
		// }
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/harga_kamar_hotel_admin"));	
	}

	
}
