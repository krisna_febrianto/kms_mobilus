<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kurs_admin extends Common {
	function __construct() {
		parent::__construct("kurs_admin");

		$this->load->model(array('kurs', 'mata_uang'));

		$this->meta 			= array();
		$this->scripts 			= array('administrator/kurs');
		$this->styles 			= array('');
		$this->title 			= "Kurs";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Kurs',
			'page_description' => 'tambah, ubah, hapus Kurs',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'kurs'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('kurs' => $this->kurs->with_mata_uang_asal('fields:nama')->with_mata_uang_konversi('fields:nama')->get_all(),
					'mata_uang' => $this->mata_uang->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."kurs/index",$data);
	}

	function get_mata_uang(){
		$this->layout = false;

		$json = [];
		$keyword = $this->input->get("q");

		if(!empty($keyword)){
            $json = $this->mata_uang->like('nama', $keyword)->limit(10)->get_all();
        }else{
            $json = $this->mata_uang->limit(null)->get_all();
            // print_r($json);
        }

        echo json_encode($json);
	}

	function check_mata_uang(){
		$this->layout = false; 
		$mata_uang_asal = $_POST['mata_uang_asal'];
		$konversi = $_POST['konversi'];

		$data = $this->kurs->where(array('mata_uang_asal_id'=> $mata_uang_asal, 'mata_uang_konversi_id'=>$konversi))->get();

		if ($data) {
			$result = false;
		}else{
			$result = true;
		}


		echo json_encode($result);
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array(
			'detail'  => $this->kurs->with_mata_uang_asal('fields:nama')->with_mata_uang_konversi('fields:nama')->get($id),
			'mata_uang' => $this->mata_uang->get_all());
		echo json_encode($data);	
	}

	function save(){
		$this->layout = FALSE;

		$id = $this->input->post('id');
		if($id == 0){
			$data = array(
				'mata_uang_asal_id' => $this->input->post('mata_uang_asal'),
				'mata_uang_konversi_id' => $this->input->post('konversi'),
				'nilai' => $this->input->post('nilai')
			);
			$success = $this->kurs->insert($data);
		}else{
			$data = array('nilai' => $this->input->post('nilai'));
			$success = $this->kurs->update($data, $id);
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Kurs berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Kurs gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/kurs_admin"));
	}
	
	function delete($id){
		$this->layout = false;
		//$id = $this->kurs->get('id');
	
		if($this->kurs->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "kurs berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "kurs pendidikan gagal dihapus."));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/kurs_admin"));	
	}
	
}
