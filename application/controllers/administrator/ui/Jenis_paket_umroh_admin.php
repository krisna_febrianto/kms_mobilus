<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Jenis_paket_umroh_admin extends Common {
        function __construct() {
		parent::__construct("jenis_paket_umroh");

		$this->load->model(array('jenis_paket_umroh','hotel','paket_plus'));
		$this->meta 			= array();
		$this->scripts 			= array('administrator/jenis_paket_umroh');
		$this->styles 			= array();
		$this->title 			= "Jenis Paket Umroh";
	}
    function index(){
		$data_breadcrumb = [
			'page_title' => 'Jenis Paket Umroh',
			'page_description' => 'tambah, ubah, hapus Jenis Paket Umroh',
			'breadcrumbs' => [
				[
				'link' => base_url().ADMIN_DIR."site/dashboard",
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Jenis Paket Umroh'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);
		$data = array('jenis_paket_umroh' => $this->jenis_paket_umroh->order_by('id', $order = 'DESC')->get_all(),
					  'paket_plus' => $this->paket_plus->with_hotel('fields:nama')->get_all(),
					  'flashdata'	=> $this->session->flashdata('form_msg')
					);
		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);
		$this->load->view(ADMIN_DIR."jenis_paket_umroh/index",$data);
	}
	function edit($id){
		$this->layout = FALSE;
		$jenis_paket_umroh = $this->jenis_paket_umroh->get($id);
		echo json_encode($jenis_paket_umroh);	
	}
	function save(){

		$this->layout = FALSE;
		if ($this->input->post('is_plus') == NULL) {
			$is_plus = 0;
			$b = array('paket_plus_id' => NULL);
		}else{
			$is_plus = 1;
			$b = array(
				'paket_plus_id' => $this->input->post('paket_plus_id'),
			);
		}
		$id = $this->input->post('id');
		$a = array(
				'nama' => $this->input->post('nama'),
				'deskripsi' => $this->input->post('deskripsi'),
				'durasi' => $this->input->post('durasi'),
				'is_plus' => $is_plus
			);
		$data = array_merge($a,$b);

		if($id == 0){
			$success = $this->jenis_paket_umroh->insert($data);
		}else{
			$success = $this->jenis_paket_umroh->update($data, $id);
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Jenis paket Umroh berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Jenis paket Umroh gagal disimpan. "));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/jenis_paket_umroh_admin"));
	}
	function delete($id){
		$this->layout = false;
	
		if($this->jenis_paket_umroh->delete($id)){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Jenis paket Umroh berhasil dihapus."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Jenis paket Umroh gagal dihapus. "));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."ui/jenis_paket_umroh_admin"));
	}   
        
}
