<?php require_once APPPATH.'controllers/administrator/Common.php';
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Materi_admin extends Common {
	function __construct() {
		parent::__construct("Materi_admin");

		$this->load->model(array('materi','admin_session','kategori','sub_materi'));
		$this->load->helper(array('text'));
		$this->meta 			= array();
		$this->scripts 			= array('administrator/materi','../themes/assets/global/plugins/jquery-repeater/jquery.repeater','../themes/assets/global/plugins/tinymce/tinymce.min');
		$this->styles 			= array();
		$this->title 			= "Materi";
		$this->menu = "Materi";
	}

	function index(){
		$data_breadcrumb = [
			'page_title' => 'Materi',
			'page_description' => 'tambah, ubah, hapus materi',
			'breadcrumbs' => [
				[
				'link' => base_url(),
				'title' => 'Home'
				],
				[
				'link' => '#',
				'title' => 'Materi'
				]
			]
		];

		$data_breadcrumb['total_breadcrumbs'] = count($data_breadcrumb['breadcrumbs']);

		$data = array('kategori_list' => $this->kategori->get_all(),
					'materi' => $this->materi->with('sub_materi')->with('kategori')->get_all(),
					'flashdata'	=> $this->session->flashdata('form_msg')
					);

		$this->parts['breadcrumb'] = $this->load->view(ADMIN_DIR.'partial/breadcrumb', $data_breadcrumb, true);

		$this->load->view(ADMIN_DIR."materi/index",$data);
	}

	function save(){
		$this->layout = FALSE;
		$user = $this->admin_session->get();
		$id = $this->input->post('id');
		$data = array('created_by' => $user['id'],
					'deskripsi' => $this->input->post('deskripsi'),
					'judul' => $this->input->post('judul'),
					'kategori_id' => $this->input->post('kategori_id')
					);

		if($id == 0){
			$success = $this->materi->insert($data);
			if($success){ //create contributor
				if(isset($_FILES['attachment']['name']) && !empty($_FILES['attachment']['name'])){
					$this->upload_attachment($success);
				}
			}
			for ($i=0; $i < count($this->input->post('group-a')); $i++) { 
				# code...
				$data_sub = array(
					'id_materi' => $success,
					'judul' => $this->input->post('group-a')[$i]['judul'],
					'content' => $this->input->post('group-a')[$i]['content_sub']
					);
				$success_sub = $this->sub_materi->insert($data_sub);
			}
		}else{
			$materi=$this->materi->get($id);
			$success = $this->materi->update($data, $id);
			if(isset($_FILES['attachment']['name']) && !empty($_FILES['attachment']['name'])){
				
				if($materi->attachment){
					if(file_exists('assets/images/materi'.'/'.$materi->attachment)){
						unlink('assets/images/materi'.'/'.$materi->attachment);
					}
				}
				$this->upload_attachment($id);
			}
		}

		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Materi berhasil disimpan."));
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Materi gagal disimpan. "));
		}

		echo json_encode(array('url' => base_url().ADMIN_DIR."materi_admin"));
	}
	function save_sub(){
		$this->layout = FALSE;
		// $user = $this->admin_session->get();
		$id = $this->input->post('id');
		$data = array('content' => $this->input->post('content_sub'),
					'judul' => $this->input->post('judul'),
					'id_materi' => $this->input->post('id_materi')
					);

		if($id == 0){
			$success = $this->sub_materi->insert($data);
			
		}else{
			$success = $this->sub_materi->update($data, $id);
			
		}

		// if($success){
		// 	$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Materi berhasil disimpan."));
		// }else{
		// 	$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Materi gagal disimpan. "));
		// }

		echo $this->input->post('id_materi');
	}
	public function upload_attachment($materi_id)
	{
		$this->layout = false;

		//upload identification attachment
		$attachment = array();
		$attachment['encrypt_name'] 		= TRUE;
		$attachment['upload_path']          = "assets/images/materi/";
        $attachment['allowed_types']        = 'doc|docx|rar|zip|pdf|ppt|pptx';
        $attachment['max_size']             = 2048;
        $attachment['max_width']            = 2048;
        $attachment['max_height']           = 1536;

        $this->load->library('upload', $attachment);
        $this->upload->initialize($attachment);
        if ( ! $this->upload->do_upload('attachment')){
            return 0;
        }else{
        	$upload_data = $this->upload->data();
        	$data_update = array('attachment' => $upload_data['file_name']);
        	if($this->materi->update($data_update, $materi_id)){
        		// $this->user_data['contributor'] = $this->contributor_model->get($user_id);
        		// $this->user_session->set_front_end($this->user_data);
        		return 1;
        	}
        }
	}

	function edit(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array('detail' => $this->materi->with("sub_materi")->get($id));
		echo json_encode($data);		
	}
	function edit_sub(){
		$this->layout = false;
		$id = $this->input->get('id');
		$data = array('detail' => $this->sub_materi->get($id));
		echo json_encode($data);		
	}

	function delete(){
		$this->layout = false;
		$id = $this->input->get('id');
		$materi=$this->materi->get($id);
		$success = $this->materi->delete($id);
		if($success){
			$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Materi berhasil dihapus."));
			
			if($materi->attachment){
				if(file_exists('assets/images/materi'.'/'.$materi->attachment)){
					unlink('assets/images/materi'.'/'.$materi->attachment);
				}
			}
		}else{
			$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Materi gagal dihapus. "));
		}
		echo json_encode(array('url' => base_url().ADMIN_DIR."materi_admin"));		
	}

	function delete_sub(){
		$this->layout = false;
		$id = $this->input->get('id');
		$sub_materi=$this->sub_materi->get($id);
		$success = $this->sub_materi->delete($id);
		// if($success){
		// 	$this->session->set_flashdata('form_msg', array('success' =>true, 'msg' => "Materi berhasil dihapus."));
			
		// 	if($sub_materi->attachment){
		// 		if(file_exists('assets/images/sub_materi'.'/'.$sub_materi->attachment)){
		// 			unlink('assets/images/sub_materi'.'/'.$sub_materi->attachment);
		// 		}
		// 	}
		// }else{
		// 	$this->session->set_flashdata('form_msg', array('success' =>false, 'msg' => "Materi gagal dihapus. "));
		// }
		echo $sub_materi->id_materi;	
	}
	
}
