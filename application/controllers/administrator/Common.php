<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common extends CI_Controller {
	function __construct($module=null) {
		parent::__construct();
		
		//set admin layout
		$this->layout = "admin_default";

		//load model
		$this->load->model(array('admin_session'));

		//get admin session
		$user = $this->admin_session->get();
		$this->user_data = $user;
		if (!$user){
			if($module != 'site'){
				redirect(site_url(ADMIN_DIR."site/no_access/" ));
			}
			$this->is_logged_in = false;
		}else{
			$this->is_logged_in = true;
		}

		// $data_head = array('user' => $user);
		$data_head = NULL;
		
		//partial head
		$this->parts['head'] = $this->load->view(ADMIN_DIR.'partial/header', $data_head, true);
		$this->parts['sidebar'] = $this->load->view(ADMIN_DIR.'partial/sidebar', $data_head, true);
		$this->parts['quick_sidebar'] = $this->load->view(ADMIN_DIR.'partial/quick_sidebar', $data_head, true);
		$this->parts['foot'] = $this->load->view(ADMIN_DIR.'partial/footer', NULL, true);
	}
}