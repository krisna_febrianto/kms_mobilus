<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

        public function __construct() {
                parent::__construct();
                $this->layout = FALSE;
        }

        function index() {
                $this->load->view('welcome_message');
        }

        function add_hotel() {
                $this->load->model('hotel');
                $this->load->model('kamar_hotel', 'room');
                $hotel = array(
                    'nama' => 'Tune Hotel',
                    'bintang' => 3,
                    'alamat' => 'Jln Klia 2/2, 64000 Klia, Selangor, Malaysia',
                    'latitude' => 2.74494,
                    'longitude' => 101.6825716,
                    'deskripsi' => '5-star beds at budget rates! Tune Hotel is easily accessed through KLIA2 terminal, KLIA & the Sepang International Circuit. Book a room online now!',
                );
                $this->hotel->insert($hotel);
                echo 1;
        }

        function add_rooms() {
                $this->load->model('kamar_hotel', 'room');
                $rooms = array(
                    array(
                        'hotel_id' => 1,
                        'tipe_kamar' => 'Standard Double Room Only',
                        'jenis_makanan' => 'Without Breakfast',
                    ),
                    array(
                        'hotel_id' => 1,
                        'tipe_kamar' => 'Standard Double Room Only',
                        'jenis_makanan' => 'Without Breakfast',
                    ),
                    array(
                        'hotel_id' => 1,
                        'tipe_kamar' => 'Standard Double Room Only',
                        'jenis_makanan' => 'Without Breakfast',
                    ),
                );
                foreach ($rooms as $key => $room) {
                        $this->room->insert($room);
                }
                echo 1;
        }

        function add_room_images() {
                $this->load->model('gambar_kamar_hotel', 'image');
                $images = array(
                    array(
                        'kamar_hotel_id' => 1,
                        'nama_file' => '1.jpg',
                    ),
                    array(
                        'kamar_hotel_id' => 1,
                        'nama_file' => '2.jpg',
                    ),
                    array(
                        'kamar_hotel_id' => 1,
                        'nama_file' => '3.jpg',
                    ),
                );
                foreach ($images as $key => $image) {
                        $this->image->insert($image);
                }
                echo 1;
        }

        function get() {
                $this->load->model('hotel');
                $this->load->model('kurs');
                $data = $this->kurs
                        ->with_mata_uang_asal('fields:nama,kode,symbol')
                        ->with_mata_uang_konversi('fields:nama,kode,symbol')
                        ->get_all();
                $hotels = $this->hotel->with_kamars(
                                array(
                                    'with' => array(
                                        'relation' => 'gambars'
                                    ))
                        )->get_all();
                echo json_encode($data);
        }

        function init_mata_uang() {
                $this->load->model('mata_uang');
                $countries = json_decode(file_get_contents('https://restcountries.eu/rest/v2/all'), TRUE);
                foreach ($countries as $key => $country) {
                        $currencies = $country['currencies'];
                        foreach ($currencies as $key => $currency) {
                                $mata_uang = array(
//                                    'negara' => $country['name'],
                                    'nama' => $currency['name'],
                                    'kode' => $currency['code'],
                                    'symbol' => $currency['symbol'],
                                );
                                $existings = $this->mata_uang->get(array(
                                    'kode' => $mata_uang['kode'],
                                ));
                                if ($mata_uang['kode'] && !$existings) {
                                        $this->mata_uang->insert($mata_uang);
                                }
                        }
                }

                echo 1;
                exit;
        }

        function send_email() {
                $this->load->library('ci_mailer');
                $result = $this->ci_mailer->send(array(
                    'to' => 'fikran.fu@gmail.com',
                    'subject' => 'Test mailgun',
                    'message' => 'Dear',
                ), TRUE);
                
                var_dump($result);
        }
        
        function session(){
                var_dump($this->session);
        }

}
