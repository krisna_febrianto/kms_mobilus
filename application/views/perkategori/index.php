<input type="hidden" id="total_data" value="<?=$total_data;?>"></input>
		<!-- Icon Boxes 34 -->
		<div class="bg-grey">
			<div class="container content-md">
				<!-- Tab v1 -->
					<div class="tab-v1">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#home" data-toggle="tab">A - Z</a></li>
							<li><a href="#terbaru" data-toggle="tab">Terbaru</a></li>
							<li><a href="#populer" data-toggle="tab">Populer</a></li>
							<li class="li-right">
								<select class="form-control select2 margin-bottom-10" name="kategori" style="width: 150px;"></select>
							</li>
						</ul>
						<ul class="nav nav-tabs">
							
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade in active" id="home">
								<div class="row equal-height-columns margin-bottom-20" id="content-home">
									<!--<?php 
									if ($materi != null){
										foreach ($materi as $key => $value): ?>
                                        <div class="col-md-4">
                                            <div class="panel" style="border-color: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                <div class="panel-heading" style="background: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                    <img class="image-sm no-margin-bottom no-top-space" src="<?= base_url().PATH_TO_IMG ?>kategori/<?=$value->kategori->icon?>"> 
                                                    <span class="right"><h3 class="panel-title"><i class="fa fa-clock-o"></i> <?=$value->created_at?></h3></span>
                                                </div>
                                                <div class="panel-body height-100">
                                                    <h4><?=$value->judul?></h4>
                                                </div>
                                                <div class="panel-footer">
                                                    <button class="btn-u btn-brd rounded-2x btn-u-dark btn-u-xs" type="button">View <i class="fa fa-arrow-right"></i></button>
                                                    <span class="right no-margin-bottom text-footer"><?=$value->updated_at != null ? 'Updated '.date_format(new DateTime($value->updated_at), "M j, Y") : '';?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php 
                                    	endforeach; 
                                    }else{?>
                                    	<div class="title-v1 margin-top-50"><h2>Tidak ada materi di kategori ini</h2></div>
                                    <?php }?>-->
								</div>
							</div>
							<div class="tab-pane fade in" id="terbaru">
								<div class="row equal-height-columns margin-bottom-20" id="content-terbaru">
									<!--<?php 
									if ($terbaru != null){
										foreach ($terbaru as $key => $value): ?>
                                        <div class="col-md-4">
                                            <div class="panel" style="border-color: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                <div class="panel-heading" style="background: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                    <img class="image-sm no-margin-bottom no-top-space" src="<?= base_url().PATH_TO_IMG ?>kategori/<?=$value->kategori->icon?>"> 
                                                    <span class="right"><h3 class="panel-title"><i class="fa fa-clock-o"></i> 10 Min</h3></span>
                                                </div>
                                                <div class="panel-body height-100">
                                                    <h4><?=$value->judul?></h4>
                                                </div>
                                                <div class="panel-footer">
                                                    <button class="btn-u btn-brd rounded-2x btn-u-dark btn-u-xs" type="button">View <i class="fa fa-arrow-right"></i></button>
                                                    <span class="right no-margin-bottom text-footer"><?=$value->updated_at != null ? 'Updated '.date_format(new DateTime($value->updated_at), "M j, Y") : '';?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php 
                                    	endforeach; 
                                    }else{?>
                                    	<div class="title-v1 margin-top-50"><h2>Tidak ada materi di kategori ini</h2></div>
                                    <?php }?>-->
								</div>
							</div>
							<div class="tab-pane fade in" id="populer">
								<div class="row equal-height-columns margin-bottom-20" id="content-populer">
									<!--<?php 
									if ($populer != null){
										foreach ($populer as $key => $value): ?>
                                        <div class="col-md-4">
                                            <div class="panel" style="border-color: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                <div class="panel-heading" style="background: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                    <img class="image-sm no-margin-bottom no-top-space" src="<?= base_url().PATH_TO_IMG ?>kategori/<?=$value->kategori->icon?>"> 
                                                    <span class="right"><h3 class="panel-title"><i class="fa fa-clock-o"></i> 10 Min</h3></span>
                                                </div>
                                                <div class="panel-body height-100">
                                                    <h4><?=$value->judul?></h4>
                                                </div>
                                                <div class="panel-footer">
                                                    <button class="btn-u btn-brd rounded-2x btn-u-dark btn-u-xs" type="button">View <i class="fa fa-arrow-right"></i></button>
                                                    <span class="right no-margin-bottom text-footer"><?=$value->updated_at != null ? 'Updated '.date_format(new DateTime($value->updated_at), "M j, Y") : '';?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php 
                                    	endforeach; 
                                    }else{?>
                                    	<div class="title-v1 margin-top-50"><h2>Tidak ada materi di kategori ini</h2></div>
                                    <?php }?>-->
								</div>
							</div>

						</div>
					</div>
					<!-- End Tab v1 -->
			</div><!--/container-->
		</div>
		<!-- End Icon Boxes 34 