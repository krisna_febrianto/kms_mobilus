<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1><?= $page_title ?>
            <small><?= $page_description ?></small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->
    <!-- BEGIN PAGE TOOLBAR -->
    
    <!-- END PAGE TOOLBAR -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<?php foreach ($breadcrumbs as $key => $value) { ?>
	<li>
		<?php if($key+1 == $total_breadcrumbs){ ?>
        <span class="active"><?= $value['title'] ?></span>
		<?php }else{ ?>
		<a href="<?= $value['link'] ?>"><?= $value['title'] ?></a>
        <i class="fa fa-circle"></i>
		<?php } ?>
    </li>	
	<?php } ?>
</ul>
<!-- END PAGE BREADCRUMB -->