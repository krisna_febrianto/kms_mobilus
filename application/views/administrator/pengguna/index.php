
<!-- BEGIN SAMPLE TABLE PORTLET-->
<style type="text/css">
    #modal-user .modal-dialog{
        width: 1000px !important;
    }
</style>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-user font-red"></i>
            <span class="caption-subject font-red sbold uppercase"><?= $page_title ?></span>
        </div>

        <div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
                <a class="btn btn-primary" id="add-user">Tambah Pengguna</a>
            </div>
        </div>
    </div>
    <?php if($flashdata != NULL){ ?>
		<div class="alert alert-<?= ($flashdata['success'] == true ? 'success':'danger') ?>" role="alert">
        	<strong><?=($flashdata['success'] == true ? 'Well done!':'Owww no') ?>.</strong> <?= $flashdata['msg']; ?>
        </div>
	<?php } ?>
    <div class="portlet-body">
        <table id="table-user" class="table table-striped table-bordered table-hover">
            	
        </table>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

<!--Placeholder modal-->
<div class="modal fade" id="modal-user" >
	<div class="modal-dialog modal-lg">
        <div class="modal-content">
        	<div class="modal-header">Form Pengguna</div>
        	<form id="form-user" class="form-horizontal">
        		<input type="hidden" name="id" value="0">
    	        	<div class="modal-body">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="nama" class="form-control" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Username
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="username" class="form-control" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Email
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                            </div>
        	        		<div class="form-group">
                                <label class="control-label col-md-3">User Role
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select name="role_id" class="form-control">
                                        <option value="">--- Select User Role ---</option>
                                        <option value="1">Administrator</option>
                                        <option value="2">Pengguna</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="password">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Password
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="password" name="new_password" class="form-control" id="new_password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Confirm Password
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="password" name="new_password_conf" class="form-control" placeholder="Password Confirm">
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn blue btn-pass" style="margin-bottom: 10px;" onclick="openPassword()">Change Password</button>
                            
                        </div>
                    </div>
	        	</div>
	        	<div class="modal-footer">
	        		<button type="reset" class="btn dark btn-outline close-modal">Cancel</button>
	                <button type="submit" class="btn green submit-button">Save</button>
	        	</div>
	        </form>
        </div> <!-- /.modal-content -->
    </div>
</div>
