<div class="row">
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Total materi per kategori</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="box-body chart-responsive">
                    <div class="chart" id="chart-materi-kategori" style="height: 300px;"></div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share font-red-sunglo hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Top 10 Materi</span>
                </div>
                
            </div>
            <div class="portlet-body">
                <div class="box-body chart-responsive">
                    <table class="table table-responsive table-bordered table-striped">
                        <th>No</th><th>Judul</th><th>Total Lihat</th>
                        <?php foreach ($materi_top as $key => $value) {
                            # code...
                        ?>
                        <tr><td><?=$key+1?></td><td><?=$value->judul?></td><td><?=$value->total_view?></td></tr>
                        <?php
                        }?>
                    </table>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->