
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-list font-red"></i>
            <span class="caption-subject font-red sbold uppercase"><?= $page_title ?></span>
        </div>

        <div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
                <a class="btn btn-primary" id="add-materi">Tambah Materi</a>
            </div>
        </div>
    </div>
    <?php if($flashdata != NULL){ ?>
		<div class="alert alert-<?= ($flashdata['success'] == true ? 'success':'danger') ?>" role="alert">
        	<strong><?=($flashdata['success'] == true ? 'Well done!':'Owww no') ?>.</strong> <?= $flashdata['msg']; ?>
        </div>
	<?php } ?>
    <div class="portlet-body">
            <table id="table-materi" class="table table-striped table-bordered">
            	<thead>
		            <tr>
			            <!-- <th width="5%">No</th> -->
			            <th width="30%">Judul</th>
			            <th width="10%">Kategori</th>
                        <th width="30%">Deskripsi</th>
			            <th width="*">Aksi</th>
			        </tr>
			    </thead>
                <tbody>
	        	<?php if($materi){
	        		foreach($materi as $key=>$value){ ?>
					<tr>
	          			<!-- <th scope="row"><?= $key+1; ?></th> -->
						<td><?=$value->judul?></td>
						<td><?=$value->kategori->kategori?></td>
                        <td><?=word_limiter($value->deskripsi,40)?></td>
						<td>
							<a data-id="<?= $value->id; ?>" class="edit-materi"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>
                            <a data-id="<?= $value->id; ?>" class="view-sub-materi"><div class="btn btn-outline btn-circle btn-sm green"><i class="fa fa-list" title="Sub Materi"></i> Sub Materi</div></a>
							<a data-id="<?= $value->id ?>" data-nama="<?= $value->judul ?>" class="delete-materi"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>
						</td>
					</tr>
				<?php 	} 
					} ?>
	        </tbody>
            </table>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
<div class="modal fade" id="modal-sub-materi" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">Sub Materi</div>
            <form id="form-sub-materi">
                <input type="hidden" name="id" value="0">
                <div class="modal-body">
                    <div class="row sub-materi">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div> <!-- /.modal-content -->
    </div>
</div>
<div class="modal fade" id="modal-add-sub-materi" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">Form Sub Materi</div>
            <form id="form-add-sub-materi">
                <input type="hidden" name="id" value="0">
                <input type="hidden" name="id_materi" value="0">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Judul</label>
                                <input type="text" name="judul" class="form-control" placeholder="Judul">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="content_sub" class="tinymce"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn green">Simpan</button>
                </div>
            </form>
        </div> <!-- /.modal-content -->
    </div>
</div>
<!--Placeholder modal-->
<div class="modal fade" id="modal-materi" >
	<div class="modal-dialog modal-lg">
        <div class="modal-content">
        	<div class="modal-header">Form Materi</div>
        	<form id="form-materi">
        		<input type="hidden" name="id" value="0">
	        	<div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
        	        		<div class="form-group">
                                <label>Judul</label>
                                <input type="text" name="judul" class="form-control" placeholder="Judul">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea name="deskripsi" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kategori</label>

                                <select name="kategori_id" class="form-control">
                                    <option value="">Pilih kategori</option>
                                    <?php
                                        if($kategori_list){
                                        foreach ($kategori_list as $kat) {
                                    ?>
                                            # code...
                                        <option value='<?=$kat->id?>'><?=$kat->kategori?></option>";
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Attachment</label>
                                <input type="file" name="attachment" class="form-control" placeholder="Attachment">
                                <div class="image-responsive materi-attachment">
                                </div>
                            </div>

                        </div>
                        <div class="mt-repeater col-md-12">
                            <div data-repeater-list="group-a">
                                <div data-repeater-item class="mt-repeater-item">
                                    <!-- jQuery Repeater Container -->
                                    <!-- <div class="mt-repeater-input">
                                        <div class="form-group">
                                            <label class="control-label">Tipe Kamar</label>
                                            <br/>
                                            <input type="text" name="tipe_kamar" class="form-control" required> 
                                        </div>
                                    </div>
                                    <div class="mt-repeater-input">
                                        <div class="form-group">
                                            <label class="control-label">Kapasitas</label>
                                            <br/>
                                            <input type="number" name="kapasitas" class="form-control" required> 
                                        </div>
                                    </div>
                                    <div class="mt-repeater-input">
                                        <div class="form-group">
                                            <label class="control-label">Jenis Makanan</label>
                                            <br/>
                                            <input type="text" name="jenis_makanan" class="form-control" required> 
                                        </div>
                                    </div>
                                    <div class="mt-repeater-input">
                                        <div class="form-group">
                                            <label class="control-label">Foto Kamar</label>
                                            <br/>
                                            <input type="file" name="foto_kamar" multiple class="form-control"> 
                                        </div>
                                    </div> -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption" style="width:70%">
                                                <div class="form-group">
                                                    <input type="text" name="judul" class="form-control" placeholder="Judul sub materi" required> 
                                                </div>
                                            </div>
                                            <div class="tools">
                                                <a href="" class="fullscreen"> </a>
                                                <a href="javascript:;" data-repeater-delete class="mt-repeater-delete remove" style="margin-top:0px"></a>
                                                <a href="" class="collapse"> </a>
                                                <!-- <a href="" class="remove"> </a> -->
                                                <!-- <div class="mt-repeater-input"> -->
                                                <!-- </div> -->
                                            </div>
            
                                        </div>
                                        <div class="portlet-body">
                                            <textarea name="content_sub" class="tinymce"></textarea>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                <i class="fa fa-plus"></i> Tambah Sub Materi</a>
                        <!-- </form> -->
                        </div>
                    </div>
	        	</div>
	        	<div class="modal-footer">
	        		<button type="reset" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
	                <button type="submit" class="btn green">Simpan</button>
	        	</div>
	        </form>
        </div> <!-- /.modal-content -->
    </div>
</div>