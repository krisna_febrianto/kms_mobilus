
<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-tag font-red"></i>
            <span class="caption-subject font-red sbold uppercase"><?= $page_title ?></span>
        </div>

        <div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
                <a class="btn btn-primary" id="add-kategori">Tambah Kategori</a>
            </div>
        </div>
    </div>
    <?php if($flashdata != NULL){ ?>
		<div class="alert alert-<?= ($flashdata['success'] == true ? 'success':'danger') ?>" role="alert">
        	<strong><?=($flashdata['success'] == true ? 'Well done!':'Owww no') ?>.</strong> <?= $flashdata['msg']; ?>
        </div>
	<?php } ?>
    <div class="portlet-body">
            <table id="table-kategori" class="table table-striped table-bordered">
            	<thead>
		            <tr>
			            <!-- <th width="5%">No</th> -->
			            <th width="30%">Kategori</th>
			            <th width="20%">Icon</th>
                        <th width="20%">Warna</th>
			            <th width="20%">Aksi</th>
			        </tr>
			    </thead>
                <tbody>
	        	<?php if($kategori){
	        		foreach($kategori as $key=>$value){ ?>
					<tr>
	          			<!-- <th scope="row"><?= $key+1; ?></th> -->
						<td><?=$value->kategori?></td>
						<td><?=$value->icon?></td>
                        <td style="background:<?=$value->warna?>"><?=$value->warna?></td>
						<td>
							<a data-id="<?= $value->id; ?>" class="edit-kategori"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>

							<a data-id="<?= $value->id ?>" data-nama="<?= $value->kategori ?>" class="delete-kategori"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>
						</td>
					</tr>
				<?php 	} 
					} ?>
	        </tbody>
            </table>
    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->

<!--Placeholder modal-->
<div class="modal fade" id="modal-kategori" >
	<div class="modal-dialog modal-md">
        <div class="modal-content">
        	<div class="modal-header">Form Kategori</div>
        	<form id="form-kategori">
        		<input type="hidden" name="id" value="0">
	        	<div class="modal-body">
	        		<div class="form-group">
                        <label>Kategori</label>
                        <input type="text" name="kategori" class="form-control" placeholder="Kategori">
                    </div>
                    <div class="form-group">
                        <label>Warna</label>
                        <input type="color" name="warna" class="form-control" placeholder="Warna">
                    </div>
                    <div class="form-group">
                        <label>Icon</label>
                        <input type="file" name="icon" class="form-control" placeholder="Icon">
                        <div class="image-responsive kategori-icon">
                        </div>
                    </div>
	        	</div>
	        	<div class="modal-footer">
	        		<button type="reset" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
	                <button type="submit" class="btn green">Simpan</button>
	        	</div>
	        </form>
        </div> <!-- /.modal-content -->
    </div>
</div>