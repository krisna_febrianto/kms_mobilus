<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title"><?= $page_title ?></h2>
        </div>
        <ul class="breadcrumbs pull-right">
        	<?php 
        	foreach ($breadcrumbs as $key => $value) {
				if($key+1 == $total_breadcrumbs){ 
			?>
			<li class="active"> <?= $value['title'] ?> </li>
			<?php }else{ ?>
			<li>
				<a href="<?= $value['link'] ?>"><?= $value['title'] ?></a>
		    </li>	
			<?php 
				}
			} 
			?>
        </ul>
    </div>
</div>