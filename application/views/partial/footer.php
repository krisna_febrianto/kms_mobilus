        <!--=== Footer v6 ===-->
        <div id="footer-v6" class="footer-v6">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <!-- About Us -->
                        <div class="col-md-4 sm-margin-bottom-40">
                            <div class="heading-footer"><h2>About Mobilus Interactive</h2></div>
                            <p>Mobilus Interactive is a fast growing IT Service Company based in Bandung, Indonesia. Our core business are web application development, mobile application and semantic web research center. We are experienced in domestic and aboard.</p>
                        </div>
                        <!-- End About Us -->

                        <!-- Recent News -->
                        <div class="col-md-4 sm-margin-bottom-40">
                            <div class="heading-footer"><h2>Recent Contents</h2></div>
                            <ul class="list-unstyled link-news">
                                <?php foreach ($materi as $key => $value): ?>
                                    <li>
                                        <a href="#"><?=$value->judul?></a>
                                        <small><?=date_format(new DateTime($value->created_at), "M j, Y")?></small>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <!-- End Recent News -->

                        <!-- Useful Links -->
                        <!-- <div class="col-md-3 sm-margin-bottom-40">
                            <div class="heading-footer"><h2>Useful Links</h2></div>
                            <ul class="list-unstyled footer-link-list">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Portfolio</a></li>
                                <li><a href="#">Latest jobs</a></li>
                                <li><a href="#">Community</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div> -->
                        <!-- End Useful Links -->

                        <!-- Contacts -->
                        <div class="col-md-4">
                            <div class="heading-footer"><h2>Contacts</h2></div>
                            <ul class="list-unstyled contacts">
                                <li>
                                    <i class="radius-3x fa fa-map-marker"></i>
                                    Jl. Cigadung Raya Timur - Griya Cigadung Baru D-2, Bandung, Jawa Barat, Indonesia
                                </li>
                                <li>
                                    <i class="radius-3x fa fa-phone"></i>
                                    022-2512474
                                </li>
                                <li>
                                    <i class="radius-3x fa fa-globe"></i>
                                    <a href="#">contact@mobilus-interactive.com</a><br>
                                    <a href="http://mobilus-interactive.com/" target="_blank">www.mobilus-interactive.com</a>
                                </li>
                            </ul>
                        </div>
                        <!-- End Contacts -->
                    </div>
                </div><!--/container -->
            </div>

            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 sm-margon-bottom-10">
                            <ul class="list-inline terms-menu">
                                <li class="silver"> &copy; 2018 All right reserved. Developed by <a href="http://mobilus-interactive.com/" target="_blank">Mobilus Interactive<a href="#"></li>
                            </ul>
                        </div>
                        <!-- <div class="col-md-4">
                            <ul class="list-inline dark-social pull-right space-bottom-0">
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Facebook" href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Twitter" href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Vine" href="#">
                                        <i class="fa fa-vine"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Google plus" href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Pinterest" href="#">
                                        <i class="fa fa-pinterest"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Instagram" href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Tumblr" href="#">
                                        <i class="fa fa-tumblr"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Youtube" href="#">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a data-placement="top" data-toggle="tooltip" class="tooltips" data-original-title="Soundcloud" href="#">
                                        <i class="fa fa-soundcloud"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
 -->                    </div>
                </div>
            </div>
        </div>
        <!--=== End Footer v6 ===-->