<!--=== Header v6 ===-->
        <div class="header-v6 header-classic-dark header-sticky">
            <!-- Navbar -->
            <div class="navbar mega-menu box-shadow shadow-effect-1" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container">
                        <!-- <button type="button" class="navbar-toggle sliding-panel__btn sliding-panel__btn--dark">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button> -->
                        <!-- Navbar Brand -->
                        <div class="navbar-brand">
                            <a href="<?=base_url();?>site">
                                <img class="default-logo" src="<?= base_url().PATH_TO_IMG ?>mobilus-white.png" alt="Logo">
                                <img class="shrink-logo" src="<?= base_url().PATH_TO_IMG ?>mobilus-white.png" alt="Logo">
                            </a>
                        </div>


                        <!-- Header Inner Right -->
                        <div class="header-inner-right">
                            <ul class="menu-icons-list">
                                <li class="menu-icons">
                                    <i class="menu-icons-style search search-close search-btn fa fa-search"></i>
                                    <div class="search-open">
                                        <input type="text" name="search" class="animated fadeIn form-control" placeholder="Start searching ..." style="text-transform: none !important;">
                                    </div>
                                </li>
                                <li class="menu-icons">
                                    <a href="<?= base_url()?>site/logout">
                                        <i class="menu-icons-style fa fa-sign-out" title="Log Out"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- End Header Inner Right -->
                    </div>
                </div>
            </div>
            <!-- End Navbar -->
        </div>