        <div class="bg-white" id="home-head">
            <div class="container content-md">
                <div class="title-v1 no-margin-bottom">
                    <h2>Welcome to E-Learning Mobilus Interactive</h2>
                    <p class="no-margin-bottom"><strong>E-Learning</strong> is a guide, tutorial, hands-on coding experience. This web will step you through the process of building an application, or adding a new feature to an existing application.</p>
                </div>
                <div class="row">
                    <?php foreach ($kategori as $key => $value): ?>
                    <div class="col-md-3">
                        <div class="service-block-v7 box-shadow shadow-effect-1">
                            <a href="<?=base_url();?>perkategori/index/<?=$value->id;?>"><img class="image-md margin-bottom-20" src="<?= base_url().PATH_TO_IMG ?>kategori/<?=$value->icon;?>"></a>
                            <h3 class="title-v3-bg"><?=$value->kategori;?></h3>
                            <!-- <p>Curabitur ut augue at mi eleifend lobortis. Ut tincidunt lacinia nisi pharetra. Quis lacinia dolor</p>
                            <a class="text-uppercase" href="#">Views</a> -->
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div><!--/container-->
        </div>
        <!-- End Icon Boxes 34 -->

        <!-- Icon Boxes 34 -->
        <div class="bg-darkgrey">
            <div class="container content-md" id="home-body">
                <!-- Tab v1 -->
                    <div class="tab-v1">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#home" data-toggle="tab">Terbaru</a></li>
                            <li><a href="#populer" data-toggle="tab">Populer</a></li>
                            <li class="li-right">
                                <!-- <select class="form-control select2 margin-bottom-10">
                                    <option>Kategori</option>
                                </select> -->
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="home">
                                <div class="row equal-height-columns margin-bottom-20" id="content-home">
                                    <!--<?php foreach ($materi as $key => $value): ?>
                                        <div class="col-md-4">
                                            <div class="panel" style="border-color: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                <div class="panel-heading" style="background: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                    <img class="image-sm no-margin-bottom no-top-space" src="<?= base_url().PATH_TO_IMG ?>kategori/<?=$value->kategori->icon?>"> 
                                                    <span class="right"><h3 class="panel-title"><i class="fa fa-clock-o"></i> 10 Min</h3></span>
                                                </div>
                                                <div class="panel-body height-100">
                                                    <h4><?=$value->judul?></h4>
                                                </div>
                                                <div class="panel-footer">
                                                    <button class="btn-u btn-brd rounded-2x btn-u-dark btn-u-xs" type="button">View <i class="fa fa-arrow-right"></i></button>
                                                    <span class="right no-margin-bottom text-footer"><?=$value->updated_at != null ? 'Updated '.date_format(new DateTime($value->updated_at), "M j, Y") : '';?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach ?>-->
                                </div>
                            </div>
                            <div class="tab-pane fade in" id="populer">
                                <div class="row equal-height-columns margin-bottom-20" id="content-populer">
                                    <!--<?php foreach ($populer as $key => $value): ?>
                                        <div class="col-md-4">
                                            <div class="panel" style="border-color: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                <div class="panel-heading" style="background: <?=$value->kategori->warna != null ? $value->kategori->warna : 'panel-grey'?>;">
                                                    <img class="image-sm no-margin-bottom no-top-space" src="<?= base_url().PATH_TO_IMG ?>kategori/<?=$value->kategori->icon?>"> 
                                                    <span class="right"><h3 class="panel-title"><i class="fa fa-clock-o"></i> 10 Min</h3></span>
                                                </div>
                                                <div class="panel-body height-100">
                                                    <h4><?=$value->judul?></h4>
                                                </div>
                                                <div class="panel-footer">
                                                    <button class="btn-u btn-brd rounded-2x btn-u-dark btn-u-xs" type="button">View <i class="fa fa-arrow-right"></i></button>
                                                    <span class="right no-margin-bottom text-footer"><?=$value->updated_at != null ? 'Updated '.date_format(new DateTime($value->updated_at), "M j, Y") : '';?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach ?>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Tab v1 -->
            </div><!--/container-->
        </div>