<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>E-Learning | Mobilus Interactive</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="KMS - Mobilus Interactive">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= base_url().PATH_TO_IMG ?>mobilus-fav.png">

    <!-- Web Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/style.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/shop.style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/headers/header-v6.css">
    <!-- <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/headers/header-v7.css"> -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/footers/footer-v6.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>animate.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>sliding-panel/style.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>parallax-slider/css/parallax-slider.css">
    <!-- BEGIN SELECT2 PLUGINS -->
    <link href="<?= base_url().PATH_TO_PLUGINS ?>select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url().PATH_TO_PLUGINS ?>select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/plugins/style-switcher.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/theme-colors/dark-grey.css" id="style_color">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/custom.css">

    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>jquery-steps/css/custom-jquery.steps.css">
    
</head>
<body style="background: #ECEFF1;">
    <div id="wrapper" class="bg-darkgrey">
        <!-- <div class="header-v6 header-classic-dark header-sticky">
            <div class="navbar mega-menu box-shadow shadow-effect-1" role="navigation">
                <div class="container">
                    <div class="menu-container">
                        <div class="navbar-brand">
                            <a href="<?=base_url();?>site">
                                <i class="fa fa-arrow-left" style="color: #fff;"></i>
                            </a>
                        </div>

                        <div class="navbar-brand">
                            <a href="#">
                                <h3 style="color: #fff;">Testing Header</h3>
                            </a>
                        </div>

                        <div class="header-inner-right">
                            <ul class="menu-icons-list">
                                <li class="menu-icons">
                                    <span class="right"><h3 class="panel-title" style="color: #fff;"><i class="menu-icons-style fa fa-clock-o"></i> 10 Min</h3></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="head">
            <div class="container">
                <div class="head-container">
                    <div class="col-md-1 back">
                        <a href="<?=base_url();?>perkategori/index/<?=$materi->kategori_id;?>">
                            <i class="fa fa-arrow-left" style="color: #fff;"></i>
                        </a>
                    </div>
                    <div class="col-md-9 head-title" style="padding-left: 0px;">
                        <h3><?=$materi->judul?></h3>
                    </div>
                    <div class="col-md-2">
                        <span class="right"><h3 class="panel-title" style="color: #fff;"><i class="menu-icons-style fa fa-clock-o"></i> 10 Min</h3></span>
                    </div>
                </div>
            </div>
        </div>

        <!--=== Content Medium Part ===-->
        <div class="content bg-darkgrey">
            <div class="margin-left-25 margin-right-25">
                <form class="shopping-cart" action="#">
                    <div>
                        <?php foreach ($sub as $key => $value): ?>
                            <div class="header-tags">
                                <div class="overflow-h">
                                    <!-- <h2>Shopping Cart</h2> -->
                                    <p><?=$value->judul?></p>
                                    <!-- <i class="rounded-x fa fa-check"></i> -->
                                </div>
                            </div>
                            <section>
                                <div class="bg-white box-shadow shadow-effect-1">
                                    <div class="content-md">
                                        <div class="margin-left-20 margin-right-20" style="margin-top: -50px;">
                                            <h2 style="font-weight: bold; color: #000;"><?= ($key+1).". ".$value->judul;?></h2>
                                            <?=$value->content?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        <?php endforeach ?>
                    </div>
                </form>
            </div><!--/end container-->
        </div>
    </div>


    <!-- Javascript -->
    <script>
        var admin_base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;
        var base_url = <?php echo json_encode(base_url());?>;
        var api_url = <?php echo json_encode(base_url().'administrator/api/');?>;
    </script>

    
   <!-- JS Global Compulsory -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery.min.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery-migrate.min.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- JS Implementing Plugins -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>back-to-top.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>smoothScroll.js"></script> 
    <script src="<?= base_url().PATH_TO_PLUGINS ?>skrollr/skrollr-ini.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>backstretch/backstretch-ini.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>sliding-panel/jquery.sliding-panel.js"></script>

    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-date-format/jquery-dateFormat.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-date-format/dateFormat.js"></script>

    <!-- BEGIN SELECT2 PLUGINS -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>select2/js/select2.full.min.js" type="text/javascript"></script>
    

    <!-- JS Page Level -->
    <script src="<?= base_url().PATH_TO_JS ?>site/app.js"></script>
    <script src="<?= base_url().PATH_TO_JS ?>site/plugins/style-switcher.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>site/plugins/parallax-slider.js"></script>
    <!-- JS Customization -->
    <script src="<?= base_url().PATH_TO_JS ?>site/custom.js"></script>

    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-steps/build/jquery.steps.js"></script>
    <!--<script src="<?= base_url().PATH_TO_JS ?>site/plugins/stepWizard.js"></script>-->

    {{scripts}}
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            StyleSwitcher.initStyleSwitcher();

            $(".shopping-cart").children("div").steps({
                headerTag: ".header-tags",
                bodyTag: "section",
                //transitionEffect: "slideLeft",
                stepsOrientation: "vertical",
                transitionEffect: "fade",
                enableFinishButton: false,
                enableKeyNavigation: true,
                labels: {
                    next: "<i class='fa fa-chevron-right'></i>",
                    previous: "<i class='fa fa-chevron-left'></i>",
                }
            });
        });

        $(document).ready(function(){
            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);

            $.ajax({ 
                type: "POST",
                dataType: 'json',
                url: base_url + "detail/get_materi/"+id,
                success: function(data){
                    $(".panel-title").html('<i class="menu-icons-style fa fa-clock-o"></i> '+timeInterval(data.created_at)+'');
                }
            });
        });

        function timeInterval(time){
            var now = new Date();
            var before = new Date(time)

            var second = (now.getTime() - before.getTime()) / 1000;
            var minute = (now.getTime() - before.getTime()) / 60000;
            var hour = (now.getTime() - before.getTime()) / 3600000;
            var day = (now.getTime() - before.getTime()) / 86400000;
            var week = (now.getTime() - before.getTime()) / 604800000;
            var month = (now.getTime() - before.getTime()) / 2419200000;
            var year = (now.getTime() - before.getTime()) / 29030400000;

            if (second <= 60) {
                newTime = second.toFixed(0)+' detik';
            } else if (minute <= 60) {
                newTime = minute.toFixed(0)+' menit';
            } else if (hour <= 24) {
                newTime = hour.toFixed(0)+' jam';
            } else if (day <= 7) {
                newTime = day.toFixed(0)+' hari';
            } else if (week <= 4) {
                newTime = week.toFixed(0)+' minggu';
            } else if (month <= 12) {
                newTime = month.toFixed(0)+' bulan';
            } else {
                newTime = year.toFixed(0)+' tahun';
            }

            return newTime;
        }
    </script>
</body>
</html>

