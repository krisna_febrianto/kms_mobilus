<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>404 Error | E-Learning - Mobilus Interactivete</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= base_url().PATH_TO_IMG ?>mobilus-fav.png">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/style.css">

    <!-- CSS Page Style -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/pages/page_404_error1.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/custom.css">
</head>

<body>
    <!--=== Content Part ===-->
    <div class="container valign__middle">
        <!--Error Block-->
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="error-v2">
                    <span class="error-v2-title">404</span>
                    <span>That’s an error!</span>
                    <p>The requested URL was not found on this server. <br> That’s all we know.</p>
                </div>
            </div>
        </div>
        <!--End Error Block-->
    </div><!--/container-->
    <!--=== End Content Part ===-->

    <!-- Javascript -->
    <script>
        var admin_base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;
        var base_url = <?php echo json_encode(base_url());?>;
        var api_url = <?php echo json_encode(base_url().'administrator/api/');?>;
        var images_url = <?php echo json_encode(base_url().PATH_TO_IMG);?>
    </script>

    <!-- JS Global Compulsory -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>back-to-top.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>backstretch/jquery.backstretch.min.js"></script>
    <!-- JS Customization -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>site/custom.js"></script>
    <!-- JS Page Level -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>site/app.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>site/plugins/style-switcher.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            StyleSwitcher.initStyleSwitcher();
        });
    </script>
    <script type="text/javascript">
        $.backstretch([
            images_url+"login.jpg",
            images_url+"login2.jpg",
            images_url+"login3.jpg",
            ], {
                fade: 1000,
                duration: 7000
            });
    </script>
    <!--[if lt IE 9]>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>respond.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>html5shiv.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>placeholder-IE-fixes.js"></script>
    <![endif]-->
</body>
</html>
