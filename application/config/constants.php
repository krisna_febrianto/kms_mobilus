<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('ADMIN_SESSION','admin_session');
define('USER_SESSION','user_session');

/*FLIGHTS STATUS*/
define('FLIGHT_DEPARTURE', 1);
define('FLIGHT_HOLIDAY', 2);
define('FLIGHT_RETURN', 3);

/*HOTEL STATUS*/
define('HOTEL_MAKKAH', 1);
define('HOTEL_HOLIDAY', 3);
define('HOTEL_MADINAH', 2);

/*DIRECTORY*/
define('PATH_TO_THEMES', 'assets/themes/');
define('PATH_TO_CSS', 'assets/css/');
define('PATH_TO_JS', 'assets/js/');
define('PATH_TO_COMPONENTS', 'assets/components/');
define('PATH_TO_IMG', 'assets/images/');
define('PATH_TO_CONFIRM_PAYMENT_IMG', 'assets/images/bukti_pembayaran/');
define('PATH_TO_MASKAPAI_IMG', 'assets/images/maskapai/');
define('PATH_TO_HOTEL_IMG', 'assets/images/hotel/');
define('PATH_TO_PAKET_IMG', 'assets/images/paket_umroh/');
define('PATH_TO_HOTEL_IMG_DEFAULT', 'http://placehold.it/270x160');
define('PATH_TO_POPUP_IMG_DEFAULT', 'http://placehold.it/900x500');
define('PATH_TO_ROOM_IMG', 'assets/images/hotel/room/');
define('ADMIN_DIR','administrator/');
define('PATH_TO_PLUGINS', 'assets/plugins/');

/*MAIL CONSTANT*/
/*
define('MAIL_HOST','smtp.gmail.com');
define('MAIL_PORT', 587);
define('MAIL_AUTH',     TRUE);
define('MAIL_SECURE',   'tls');
define('MAIL_USERNAME', 'kikimobilus@gmail.com');
define('MAIL_PASSWORD', 'mobilus123?');
define('MAIL_FROM','support@mobilus-interactive.com');
define('MAIL_FROMNAME', 'Umroh360 Support');
define('MAIL_SENDER', 'admin@umroh360.com');
define('MAIL_WORDWRAP', 65);
 */
define('MAIL_PROTOCOL','smtp');
define('MAIL_HOST','smtp.mailgun.org');
define('MAIL_PORT', 587);
define('MAIL_AUTH', TRUE);
define('MAIL_SECURE', 'tls');
define('MAIL_USERNAME', 'postmaster@sandbox78a1ebba2f144a1187cb1163c6826920.mailgun.org');
define('MAIL_PASSWORD', 'd7b3a065344647334a76aca626866190');
define('MAIL_FROM', 'cs@umroh360.com');
define('MAIL_FROMNAME', 'Umroh360 Support');
define('MAIL_SENDER', MAIL_FROM);
define('MAIL_WORDWRAP', 65);
define('MAIL_CHARSET', 'utf-8');
define('MAIL_TIMEOUT', 60);

define('ROLE_ADMIN', 1);
define('ROLE_APPROVER', 2);
define('ROLE_AGENT', 3);
define('ROLE_CUSTOMER', 4);

/* PAKET LIST */
define('PAKET_LIST', serialize(array(
    1 => [
    	'name' => 'Umroh Reguler (9 hari)',
    	'day' => 9
    ],
    2 => [
    	'name' => 'Umroh Ramadhan (12 hari)',
    	'day' => 12
    ],
    3 => [
    	'name' => 'Umroh Ramadhan (30 hari)',
    	'day' => 30
    ],
    4 => [
    	'name' => 'Umroh Plus (15 hari)',
    	'day' => 15
    ],
    5 => [
    	'name' => 'Umroh Private (15 hari)',
    	'day' => 15
    ]
)));

/* HANDLING LIST */
define('HANDLING_LIST', serialize(array(
    '1' => [
    	'name_of_handling' => 'Bus Farouk Jamil',
    	'price' => 100
    ],
    '2' => [
    	'name_of_handling' => 'Porter airport kedatangan',
    	'price' => 50
    ],
    '3' => [
    	'name_of_handling' => 'Porter airport kepulangan',
    	'price' => 50
    ],
    '4' => [
    	'name_of_handling' => 'Air Zam-zam (5lt/jamaah)',
    	'price' => 20
    ],
    '5' => [
    	'name_of_handling' => 'Porter Hotel (Mekah-Madinah/Madinah-Mekah)',
    	'price' => 50
    ],
    '6' => [
    	'name_of_handling' => 'City Tour 2x (Mekah-Madinah/Madinah-Mekah)',
    	'price' => 70
    ],
    '7' => [
    	'name_of_handling' => 'Snack 3x, Nasi Box 2x',
    	'price' => 150
    ],
    '8' => [
    	'name_of_handling' => 'Muthawwif Berpengalaman (Program 9 hari)',
    	'price' => 500
    ],
    '9' => [
    	'name_of_handling' => 'Muasasah Mekah Madinah Jeddah',
    	'price' => 200
    ]
)));

define('RANGE_JUMLAH_JAMAAH', serialize(array("1", "10", "20", "30", "40", "50")));
define('RANGE_JUMLAH_JAMAAH_TO', serialize(array("1", "10", "20", "30", "40", "50", "∞")));
define('RANGE_JUMLAH_JAMAAH_UNLIMITED', "∞");