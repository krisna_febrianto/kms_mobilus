<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Persistent {
	/**
	 * Validates 'keep me logged in' cookie, if exists,
	 * and authorizes user to access Joota.
	 *
	 * @author Adityo Jiwandono
	 * @access public
	 * @param void
	 * @return void
	 */
	function validate_cookie() {
		$CI = &get_instance();
		$CI->load->model('persistence');
		$CI->load->model('user_session');
		if(($cookie = $CI->input->cookie($CI->config->item('cookie_prefix') . REMEMBERME_COOKIE, TRUE)) && !$CI->user_session->get())  {
			if($user_id = $CI->persistence->validate($cookie)) {
				$CI->user_session->create($user_id);
				header('Location: ' . $_SERVER['REQUEST_URI']);
			}
		}
	}
}
