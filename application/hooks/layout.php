<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Layout {
	function render() {
		global $OUT;
		$CI = &get_instance();
		$output = $CI->output->get_output();

		if (!isset($CI->layout)) {
			$CI->layout = "default";
		}

		if ($CI->layout != false) {
			if (!preg_match('/(.+).php$/', $CI->layout)) {
				$CI->layout .= '.php';
			}

			$requested = BASEPATH . '../application/layouts/' . $CI->layout;
			$default = BASEPATH . '../application/layouts/default.php';

			if (file_exists($requested)) {
				$layout = $CI->load->file($requested, true);
			} else {
				$layout = $CI->load->file($default, true);
			}

			$view = str_replace("{{content}}", $output, $layout);
			$view = str_replace("{{title}}", @$CI->title, $view);
			$view = str_replace("{{header}}", @$CI->header, $view);

			$scripts = "";
			$styles = "";
			$metas = "";

			if ($CI->layout == 'smarty-admin.php') {
				$view = str_replace("{{section_left_menu}}", @$CI->section_left_menu, $view);
				$view = str_replace("{{page_title}}", @$CI->page_title, $view);
				$view = str_replace("{{extra_page_header}}", @$CI->extra_page_header, $view);

				if (!empty(@$CI->inline_scripts)) {
					$inline_scripts = implode(' ',$CI->inline_scripts);
					$view = str_replace("{{inline_scripts}}", $inline_scripts, $view);
				}

				if (count(@$CI->styles) > 0) {
					foreach ($CI->styles as $style) {
						$styles .= "<link rel='stylesheet' type='text/css' href='" . base_url() . "assets/" . $style .
						           ".css' />";
					}
				}

				if (count(@$CI->breadcrumbs) > 0) {
					$breadcrumbs = $CI->load->view('administrator/partial/breadcrumbs',['breadcrumbs'=>$CI->breadcrumbs],true);
					$view = str_replace("{{breadcrumbs}}", $breadcrumbs, $view);
				}
			} else {
				$view = str_replace("{{additional_content}}", @$CI->additional_content, $view);
				$view = str_replace("{{left}}", @$CI->left, $view);
				$view = str_replace("{{error_message}}", @$CI->error_message, $view);
				$view = str_replace("{{success_message}}", @$CI->success_message, $view);
				$view = str_replace("{{left_sidebar}}", @$CI->left_sidebar, $view);
				$view = str_replace("{{right_sidebar}}", @$CI->right_sidebar, $view);
				$view = str_replace("{{footer}}", @$CI->footer, $view);
				// $view = str_replace("{{content_style}}", @$CI->content_style, $view);
				// $view = str_replace("{{signin_status}}", @$CI->signin_status, $view);
				$view = str_replace("{{page_color}}", @$CI->page_color, $view);

				if (count(@$CI->styles) > 0) {
					foreach ($CI->styles as $style) {
						$styles .= "<link rel='stylesheet' type='text/css' href='" . base_url() . "assets/css/" . $style .
						           ".css' />";
					}
				}
			}

			if (count(@$CI->metas) > 0) {
				foreach ($CI->metas as $meta) {
					$metas .= $meta;
				}
				//$metas = implode("\n", $CI->meta);
			}
			// var_dump($CI->scripts);die();
			if (count(@$CI->scripts) > 0) {
				foreach ($CI->scripts as $script) {
					$base_path = 'assets/js/';
					$ext = '.js';
					$scripts .= "<script type='text/javascript' src='" . base_url() . $base_path . $script . $ext .
					            "'></script>";
				}
			}

			$scripts .= "<script type='text/javascript'>var base_url = '" . base_url() . "';</script>";


			if (count(@$CI->parts) > 0) {
				foreach ($CI->parts as $name => $part) {
					$view = str_replace("{{" . $name . "}}", $part, $view);
				}
			}

			$view = str_replace("{{metas}}", $metas, $view);
			$view = str_replace("{{scripts}}", $scripts, $view);
			$view = str_replace("{{styles}}", $styles, $view);
			$view = preg_replace("/{{.*?}}/ims", "", $view);
		} else {
			$view = $output;
		}

		$OUT->_display($view);
	}
}
