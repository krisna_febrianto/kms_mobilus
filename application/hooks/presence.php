<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Presence {
	/**
	 * Update user's last activity timestamp.
	 *
	 * @author Adityo Jiwandono
	 * @access public
	 * @param void
	 * @return void
	 */
	function update_last_activity() {
		$CI = &get_instance();
		if(!$CI->input->is_ajax_request()) {
			$CI->load->model('account');
			$CI->account->update_last_activity();
		}
	}
}
