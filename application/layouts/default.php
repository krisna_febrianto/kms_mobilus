<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>E-Learning | Mobilus Interactive</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="KMS - Mobilus Interactive">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= base_url().PATH_TO_IMG ?>mobilus-fav.png">

    <!-- Web Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/headers/header-v6.css">
    <!-- <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/headers/header-v7.css"> -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/footers/footer-v6.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>animate.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>sliding-panel/style.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>parallax-slider/css/parallax-slider.css">
    <!-- BEGIN SELECT2 PLUGINS -->
    <link href="<?= base_url().PATH_TO_PLUGINS ?>select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url().PATH_TO_PLUGINS ?>select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/plugins/style-switcher.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/theme-colors/dark-grey.css" id="style_color">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/custom.css">

    {{styles}}
    
</head>
<body class="header-fixed header-fixed-space sliding-panel-ini sliding-panel-flag-right">
    <div id="wrapper">
        {{head}}
        {{breadcrumbs}}
        {{content}}
        {{foot}}
    </div>


    <!-- Javascript -->
    <script>
        var admin_base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;
        var base_url = <?php echo json_encode(base_url());?>;
        var api_url = <?php echo json_encode(base_url().'administrator/api/');?>;
    </script>

    
   <!-- JS Global Compulsory -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery.min.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery-migrate.min.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- JS Implementing Plugins -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>back-to-top.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>smoothScroll.js"></script> 
    <script src="<?= base_url().PATH_TO_PLUGINS ?>skrollr/skrollr-ini.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>backstretch/backstretch-ini.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>sliding-panel/jquery.sliding-panel.js"></script>

    <!--<script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-steps-master/dist/jquery-steps.min.js"></script>-->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery.steps.min.js"></script>

    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-date-format/jquery-dateFormat.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-date-format/dateFormat.js"></script>

    <!-- BEGIN SELECT2 PLUGINS -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>select2/js/select2.full.min.js" type="text/javascript"></script>
    

    <!-- JS Page Level -->
    <script src="<?= base_url().PATH_TO_JS ?>site/app.js"></script>
    <script src="<?= base_url().PATH_TO_JS ?>site/plugins/style-switcher.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>site/plugins/parallax-slider.js"></script>
    <!-- JS Customization -->
    <script src="<?= base_url().PATH_TO_JS ?>site/custom.js"></script>
    {{scripts}}
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            StyleSwitcher.initStyleSwitcher();
        });
    </script>
</body>
</html>

