<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> 
<html lang="en"> 
<!--<![endif]-->
	<!-- BEGIN HEAD -->
	<head>
		<meta charset="utf-8"/>
		<title>E-Learning Mobilus Interactive | {{title}}</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport"/>
		<meta content="" name="description"/>
		<meta content="" name="author"/>
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <!-- BEGIN DATATABLE -->
        <!-- <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"> -->
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
        <!-- END DATATABLE -->

        
        <!-- BEGIN DATEPICKER -->
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">
        <!-- END DATEPICKER -->

        <!-- BEGIN TIMEPICKER -->
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
        <!-- END TIMEPICKER -->
        <!-- BEGIN SELECT2 PLUGINS -->
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/css/custom-admin.css" rel="stylesheet" type="text/css" />
        <!-- END SELECT2 PLUGINS -->
        <link rel="shortcut icon" href="favicon.ico" />
	</head>
	<!-- END HEAD -->
    
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
    	{{head}}
    	<!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
    	<!-- BEGIN CONTAINER -->
        <div class="page-container">
    		{{sidebar}}
    		<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
			    <!-- BEGIN CONTENT BODY -->
			    <div class="page-content">
			        {{breadcrumb}}
    				{{content}}
			    </div>
			    <!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
    		{{quick_sidebar}}
    	</div>
        <!-- END CONTAINER -->
        {{foot}}
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
		<!-- BEGIN CORE PLUGINS -->
		<!--[if lt IE 9]>
		<script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/respond.min.js"></script>
		<script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/excanvas.min.js"></script> 
		<![endif]-->
		<script>
			var admin_base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;
			var base_url = <?php echo json_encode(base_url());?>;
            var api_url = <?php echo json_encode(base_url().'administrator/api/');?>;
		</script>

		<script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/highcharts/js/highcharts.js" type="text/javascript"></script>
        <!-- BEGIN DATEPICKER -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- END DATEPICKER -->

        <!-- BEGIN TIMEPICKER -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- END TIMEPICKER -->
        <!-- BEGIN SELECT2 PLUGINS -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
        <!-- END SELECT2 PLUGINS -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-typeahead/bootstrap3-typeahead.min.js" type="text/javascript"></script>

        <!-- NOTIFICATION JS -->
         <script src="<?= base_url().PATH_TO_JS ?>administrator/notifikasi.js" type="text/javascript"></script>

        {{scripts}}
		<!-- END JAVASCRIPTS -->
    </body>
</html>
