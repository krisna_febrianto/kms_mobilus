<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>Login | E-learning - Mobilus Interactive</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?= base_url().PATH_TO_IMG ?>mobilus-fav.png">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/style.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>animate.css">
	<link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>font-awesome/css/font-awesome.min.css">

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/pages/page_log_reg_v2.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/theme-colors/dark-blue.css" id="style_color">
	<link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/custom.css">
</head>

<body class="login">
	<!--=== Content Part ===-->
	<div class="container">
		<!--Reg Block-->
		<div class="reg-block">
			<div class="reg-block-header">
				<h2>Sign In</h2>
				<!-- <ul class="social-icons text-center">
					<li><a class="rounded-x social_facebook" data-original-title="Facebook" href="#"></a></li>
					<li><a class="rounded-x social_twitter" data-original-title="Twitter" href="#"></a></li>
					<li><a class="rounded-x social_googleplus" data-original-title="Google Plus" href="#"></a></li>
					<li><a class="rounded-x social_linkedin" data-original-title="Linkedin" href="#"></a></li>
				</ul> -->
				<!-- <p>Don't Have Account? Click <a class="color-green" href="page_registration1.html">Sign Up</a> to registration.</p> -->
			</div>
			<!-- <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span>Enter any username and password. </span>
            </div> -->
            <?php if($flashdata) {?>
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span><?=$flashdata['msg']?></span>
            </div>
            <?php } ?>
			<form action="<?= base_url()?>site/login" class="login-form" method="post">
				<div class="input-group margin-bottom-20">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off" required/>
				</div>
				<div class="input-group margin-bottom-20">
					<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					<input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off" required/>
				</div>
				<hr>

				<!-- <div class="checkbox">
					<label class="rememberme">
						<input type="checkbox" name="remember" value="1"/>
						<p>Remember me</p>
					</label>
				</div> -->

				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<button type="submit" class="btn-u btn-block">Log In</button>
						<!-- <div class="forgot-password margin-top-10">
	                        <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
	                    </div> -->
					</div>
				</div>
			</form>
			<!-- <form class="forget-form" action="<?= base_url().ADMIN_DIR?>site/forgot_password" method="post">
                <h3>Forgot Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email-forgot" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn-u btn-u-red btn-outline">Back</button>
                    <button type="submit" class="btn-u uppercase pull-right">Submit</button>
                </div>
            </form> -->
		</div>
		<!--End Reg Block-->
	</div><!--/container-->
	<!--=== End Content Part ===-->

	<!-- Javascript -->
    <script>
        var admin_base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;
        var base_url = <?php echo json_encode(base_url());?>;
        var api_url = <?php echo json_encode(base_url().'administrator/api/');?>;
        var images_url = <?php echo json_encode(base_url().PATH_TO_IMG);?>
    </script>

	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery.min.js"></script>
	<script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>back-to-top.js"></script>
	<script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>backstretch/jquery.backstretch.min.js"></script>
	
	<!-- JS Customization -->
	<script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>site/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>site/app.js"></script>
	<!-- <script src="<?= base_url().PATH_TO_THEMES ?>assets/pages/scripts/login-5.min.js" type="text/javascript"></script> -->
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			// Login.init();
		});

		$.backstretch([
			images_url+"login.jpg",
			images_url+"login2.jpg",
			images_url+"login3.jpg",
			], {
				fade: 1000,
				duration: 7000
		});

		// var Login = function(){
		// 	var r = function(){
		// 		$(".login-form").validate({
		// 			errorElement:"span",
		// 			errorClass:"help-block",
		// 			focusInvalid:!1,
		// 			rules:{
		// 				username:{
		// 					required:!0
		// 				},
		// 				password:{
		// 					required:!0
		// 				},
		// 				remember:{
		// 					required:!1
		// 				}
		// 			},
		// 			messages:{
		// 				username:{
		// 					required:"Username is required."
		// 				},
		// 				password:{
		// 					required:"Password is required."
		// 				}
		// 			},
		// 			invalidHandler:function(r,e){
		// 				$(".alert-danger",
		// 					$(".login-form")).show()
		// 			},
		// 			highlight:function(r){
		// 				$(r).closest(".form-group").addClass("has-error")
		// 			},
		// 			success:function(r){
		// 				r.closest(".form-group").removeClass("has-error"),
		// 				r.remove()
		// 			},
		// 			errorPlacement:function(r,e){
		// 				r.insertAfter(e.closest(".input-icon"))
		// 			},
		// 			submitHandler:function(r){
		// 				r.submit()
		// 			}
		// 		}),
		// 		$(".login-form input").keypress(function(r){
		// 			return 13==r.which?($(".login-form").validate().form()&&$(".login-form").submit(),!1):void 0
		// 		}),
		// 		$(".forget-form input").keypress(function(r){
		// 			return 13==r.which?($(".forget-form").validate().form()&&$(".forget-form").submit(),!1):void 0
		// 		}),
		// 		$("#forget-password").click(function(){
		// 			$(".login-form").hide(),$(".forget-form").show()
		// 		}),
		// 		$("#back-btn").click(function(){
		// 			$(".login-form").show(),$(".forget-form").hide()
		// 		})
		// 	};
		// 	return{
		// 		init:function(){
		// 			r(),
		// 			// $(".login-bg").backstretch([
		// 			// 	"assets/images/login.jpg",
		// 			// 	"assets/images/login2.jpg",
		// 			// 	"assets/images/login3.jpg",
		// 			// 	], {fade:1e3,duration:8e3}
		// 			// 	),
		// 			$(".display-hide").hide();
		// 			$(".forget-form").hide();
		// 		}
		// 	}
		// }();
		// jQuery(document).ready(function(){
		// 	Login.init()
		// });
	</script>
	<!--[if lt IE 9]>
	<script src="<?= base_url().PATH_TO_PLUGINS ?>respond.js"></script>
	<script src="<?= base_url().PATH_TO_PLUGINS ?>html5shiv.js"></script>
	<script src="<?= base_url().PATH_TO_PLUGINS ?>placeholder-IE-fixes.js"></script>
	<![endif]-->
</body>
</html>
