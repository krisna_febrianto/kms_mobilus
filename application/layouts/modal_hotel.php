<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Umroh - Travel, Tour Booking</title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>animate.min.css">
    
    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="<?= base_url().PATH_TO_COMPONENTS ?>revolution_slider/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().PATH_TO_COMPONENTS ?>revolution_slider/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().PATH_TO_COMPONENTS ?>jquery.bxslider/jquery.bxslider.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?= base_url().PATH_TO_COMPONENTS ?>flexslider/flexslider.css" media="screen" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>style.css">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>custom.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>responsive.css">
    {{styles}}
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->

    <!-- Javascript Page Loader -->
    <!-- <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>pace.min.js" data-pace-options='{ "ajax": false }'></script> -->
    <!-- <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>page-loading.js"></script> -->
</head>
<body>
    <div id="page-wrapper">
        {{content}}
    </div>


    <!-- Javascript -->
    <script>
        var admin_base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;
        var base_url = <?php echo json_encode(base_url());?>;
        var api_url = <?php echo json_encode(base_url().'administrator/api/');?>;
    </script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>jquery.noconflict.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>jquery.placeholder.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>jquery-ui.1.10.4.min.js"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>bootstrap.min.js"></script>
    
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_COMPONENTS ?>revolution_slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_COMPONENTS ?>revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
    
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_COMPONENTS ?>jquery.bxslider/jquery.bxslider.min.js"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_COMPONENTS ?>flexslider/jquery.flexslider.js"></script>
    
    <!-- Google Map Api -->
    <!-- <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script> -->

    <!-- parallax -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>jquery.stellar.min.js"></script>

    <!-- waypoint -->
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>waypoints.min.js"></script>

    <!-- load page Javascript -->
    <!-- <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>theme-scripts.js"></script> -->

    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>scripts.js"></script>
    
    {{scripts}}
    <script type="text/javascript">
        tjq(document).ready(function() {
            // calendar panel
            // var cal = new Calendar();
            // var unavailable_days = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
            // var price_arr = {3: '$170', 4: '$170', 5: '$170', 6: '$170', 7: '$170', 8: '$170', 9: '$170', 10: '$170', 11: '$170', 12: '$170', 13: '$170', 14: '$170', 15: '$170', 16: '$170', 17: '$170'};

            // var current_date = new Date();
            // var current_year_month = (1900 + current_date.getYear()) + "-" + (current_date.getMonth() + 1);
            // tjq("#select-month").find("[value='" + current_year_month + "']").prop("selected", "selected");
            // cal.generateHTML(current_date.getMonth(), (1900 + current_date.getYear()), unavailable_days, price_arr);
            // tjq(".calendar").html(cal.getHTML());
            
            // tjq("#select-month").change(function() {
            //     var selected_year_month = tjq("#select-month option:selected").val();
            //     var year = parseInt(selected_year_month.split("-")[0], 10);
            //     var month = parseInt(selected_year_month.split("-")[1], 10);
            //     cal.generateHTML(month - 1, year, unavailable_days, price_arr);
            //     tjq(".calendar").html(cal.getHTML());
            // });
            
            
            tjq(".goto-writereview-pane").click(function(e) {
                e.preventDefault();
                tjq('#hotel-features .tabs a[href="#hotel-write-review"]').tab('show')
            });
            
            // editable rating
            tjq(".editable-rating.five-stars-container").each(function() {
                var oringnal_value = tjq(this).data("original-stars");
                if (typeof oringnal_value == "undefined") {
                    oringnal_value = 0;
                } else {
                    //oringnal_value = 10 * parseInt(oringnal_value);
                }
                tjq(this).slider({
                    range: "min",
                    value: oringnal_value,
                    min: 0,
                    max: 5,
                    slide: function( event, ui ) {
                        
                    }
                });
            });
        });
        
        // tjq('a[href="#map-tab"]').on('shown.bs.tab', function (e) {
        //     var center = panorama.getPosition();
        //     google.maps.event.trigger(map, "resize");
        //     map.setCenter(center);
        // });
        // tjq('a[href="#steet-view-tab"]').on('shown.bs.tab', function (e) {
        //     fenway = panorama.getPosition();
        //     panoramaOptions.position = fenway;
        //     panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
        //     map.setStreetView(panorama);
        // });
        // var map = null;
        // var panorama = null;
        // var fenway = new google.maps.LatLng(48.855702, 2.292577);
        // var mapOptions = {
        //     center: fenway,
        //     zoom: 12
        // };
        // var panoramaOptions = {
        //     position: fenway,
        //     pov: {
        //         heading: 34,
        //         pitch: 10
        //     }
        // };
        // function initialize() {
        //     tjq("#map-tab").height(tjq("#hotel-main-content").width() * 0.6);
        //     map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);
        //     panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
        //     map.setStreetView(panorama);
        // }
        // google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</body>
</html>

