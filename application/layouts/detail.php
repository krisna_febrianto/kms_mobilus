<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>E-Learning | Mobilus Interactive</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="KMS - Mobilus Interactive">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/style.css">

    <!-- CSS Header and Footer -->
    <!-- <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/headers/header-v6.css"> -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/headers/header-v7.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/footers/footer-v6.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>animate.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>sliding-panel/style.css">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_PLUGINS ?>parallax-slider/css/parallax-slider.css">
    <!-- BEGIN SELECT2 PLUGINS -->
    <link href="<?= base_url().PATH_TO_PLUGINS ?>select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url().PATH_TO_PLUGINS ?>select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/plugins/style-switcher.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/theme-colors/dark-grey.css" id="style_color">
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="<?= base_url().PATH_TO_CSS ?>site/custom.css">

    {{styles}}
    
</head>
<body class="header-fixed header-fixed-space promo-padding-top sliding-panel-ini sliding-panel-flag-right">
    <div id="wrapper">
            <div class="header-v7 header-left-v7">
                <nav class="navbar navbar-default mCustomScrollbar" role="navigation" data-mcs-theme="minimal-dark">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container">
                        <!-- Toggle get grouped for better mobile display -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- End Toggle -->

                        <!-- Logo -->
                        <div class="logo">
                            <a href="index.html">
                                <img id="logo-header" src="assets/img/logo1-default.png" alt="Logo">
                            </a>
                        </div>
                        <!-- End Logo -->
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <div class="menu-container">
                            <ul class="nav navbar-nav">
                                <!-- Home -->
                                <li class="dropdown active">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                        Home
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="index.html">Option 1: Default Page</a></li>
                                    </ul>
                                </li>
                                <!-- End Home -->
                            </ul>

                            <ul class="list-inline header-socials">
                                <li><a href="'#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="'#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="'#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                            <p class="copyright-text">&copy; 2015 Unify. All Rights Reserved.</p>
                        </div>
                    </div>
                    <!-- End Navbar Collapse -->
                </nav>
            </div>
        <div class="bg-grey">
            <div class="content-side-right">
                <!--=== Slider ===-->
                <div class="slider-inner no-margin-top">
                    <div id="da-slider" class="da-slider">
                        <div class="da-slide">
                            <h2><i>CLEAN &amp; FRESH</i> <br /> <i>FULLY RESPONSIVE</i> <br /> <i>DESIGN</i></h2>
                            <p><i>Lorem ipsum dolor amet</i> <br /> <i>tempor incididunt ut</i> <br /> <i>veniam omnis </i></p>
                            <div class="da-img"><img class="img-responsive" src="assets/plugins/parallax-slider/img/1.png" alt=""></div>
                        </div>
                        <div class="da-slide">
                            <h2><i>RESPONSIVE VIDEO</i> <br /> <i>SUPPORT AND</i> <br /> <i>MANY MORE</i></h2>
                            <p><i>Lorem ipsum dolor amet</i> <br /> <i>tempor incididunt ut</i></p>
                            <div class="da-img">
                                <iframe src="http://player.vimeo.com/video/47911018" width="530" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                            </div>
                        </div>
                        <div class="da-slide">
                            <h2><i>USING BEST WEB</i> <br /> <i>SOLUTIONS WITH</i> <br /> <i>HTML5/CSS3</i></h2>
                            <p><i>Lorem ipsum dolor amet</i> <br /> <i>tempor incididunt ut</i> <br /> <i>veniam omnis </i></p>
                            <div class="da-img"><img src="assets/plugins/parallax-slider/img/html5andcss3.png" alt="image01" /></div>
                        </div>
                        <div class="da-arrows">
                            <span class="da-arrows-prev"></span>
                            <span class="da-arrows-next"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Javascript -->
    <script>
        var admin_base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;
        var base_url = <?php echo json_encode(base_url());?>;
        var api_url = <?php echo json_encode(base_url().'administrator/api/');?>;
    </script>

    
   <!-- JS Global Compulsory -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery.min.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery/jquery-migrate.min.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- JS Implementing Plugins -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>back-to-top.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>smoothScroll.js"></script> 
    <script src="<?= base_url().PATH_TO_PLUGINS ?>skrollr/skrollr-ini.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>backstretch/backstretch-ini.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>sliding-panel/jquery.sliding-panel.js"></script>

    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-steps-master/dist/jquery-steps.min.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery.steps.min.js"></script>

    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-date-format/jquery-dateFormat.js"></script>
    <script src="<?= base_url().PATH_TO_PLUGINS ?>jquery-date-format/dateFormat.js"></script>

    <script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>parallax-slider/js/modernizr.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_PLUGINS ?>parallax-slider/js/jquery.cslider.js"></script>

    <!-- BEGIN SELECT2 PLUGINS -->
    <script src="<?= base_url().PATH_TO_PLUGINS ?>select2/js/select2.full.min.js" type="text/javascript"></script>
    

    <!-- JS Page Level -->
    <script src="<?= base_url().PATH_TO_JS ?>site/app.js"></script>
    <script src="<?= base_url().PATH_TO_JS ?>site/plugins/style-switcher.js"></script>
    <script type="text/javascript" src="<?= base_url().PATH_TO_JS ?>site/plugins/parallax-slider.js"></script>
    <!-- JS Customization -->
    <script src="<?= base_url().PATH_TO_JS ?>site/custom.js"></script>
    {{scripts}}
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            StyleSwitcher.initStyleSwitcher();
            ParallaxSlider.initParallaxSlider();
        });
    </script>
</body>
</html>

