<!DOCTYPE html>
<html>
    <head>
        <title></title>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        {{metas}}

        <script>
            var base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;
            var front_url = <?php echo json_encode(base_url());?>;
        </script>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">

        {{styles}}
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->

        <script src="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?= base_url().PATH_TO_THEMES ?>assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>

        
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        {{content}}

        {{scripts}}
    </body>
</html>
