<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Description of Admin_REST_Controller
 *
 * @author Fikran
 */
class Admin_REST_Controller extends REST_Controller {
        const MESSAGE_UNAUTHORIZED = array(
                "errorCode" => "error.unauthorized",
                "fullErrorCode" => "error.unauthorized",
                "errorMessage" => "Invalid session or session expired"
        );

        protected $model_name = NULL;
                
        function __construct() {
                parent::__construct();
                
                $this->load->model('admin_session');
                $this->user = $this->admin_session->get();
                if(!$this->user){
                        $this->response(self::MESSAGE_UNAUTHORIZED,
                                self::HTTP_UNAUTHORIZED);    
                }
                
                $this->load->model($this->model_name, '_model');
        }
        
        function index_get($id = NULL){
                $size = $this->input->get('size') ?: 20;
                $page = $this->input->get('page') ?: 1;
                if(!$id){
                        $total_elements = $this->_model->count_rows();
                        $content = $this->_model->paginate($size, $total_elements, $page);
                        $result = array(
                            'content' => $content ?: array(),
                            'totalElements' => $total_elements,
                            'numberOfElements' => count($content),
                            'size' => $size,
                            'page' => $page,
                        );
                        
                        $this->response($result,
                                self::HTTP_OK);
                }
                $this->response(
                        $this->_model->get($id),
                        self::HTTP_OK
                );
        }
        
        function list_get(){
                $content = $this->_model->get_all();
                $this->response(
                        $content ?: array(),
                        self::HTTP_OK
                );
        }
        
        function index_post(){
                $post = $this->get_input_as_array();
                
                $id = $this->_model->insert($post);
                
                $this->response(
                        $this->_model->get($id),
                        self::HTTP_OK
                );
        }
        
        function index_put($id){
                $post = $this->get_input_as_array();
                $this->_model->update($post, array(
                    $this->_model->primary_key => $id
                ));
                
                $this->response(
                        $this->_model->get($id),
                        self::HTTP_OK
                );
        }
        
        function index_delete($id){
                $this->_model->delete(array(
                    $this->_model->primary_key => $id
                ));
                
                $this->response(
                        array(
                            'status' => 'Success'
                        ),
                        self::HTTP_OK
                );
        }
        
        protected final function get_model(){
                return $this->_model;
        }
        
        protected function get_input_as_array(){
                return json_decode($this->security->xss_clean($this->input->raw_input_stream));
        }
}
