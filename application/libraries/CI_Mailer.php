<?php

if (!defined('BASEPATH'))
        exit('No direct script access allowed');

class CI_Mailer {

        private $ci; // CI Instance

        public function __construct() {
                $this->ci = &get_instance();
                $this->ci->load->library('email');
        }

        public function send($params, $ishtml = false) {
                $config = array(
                    'protocol' => MAIL_PROTOCOL,
                    'smtp_host' => MAIL_HOST,
                    'smtp_port' => MAIL_PORT,
                    'smtp_user' => MAIL_USERNAME,
                    'smtp_pass' => MAIL_PASSWORD,
                    'smtp_crypto' => MAIL_SECURE,
                    'smtp_timeout' => MAIL_TIMEOUT,
                    'charset' => MAIL_CHARSET,
                    'mailtype' => $ishtml ? 'html' : 'text',
                    'wordwrap' => TRUE,
                    'wrapchars' => MAIL_WORDWRAP,
                );
                $this->ci->email->initialize($config);
                
                $this->ci->email->subject($params['subject']);
                $this->ci->email->message($params['message']);
                $from = isset($params['from']) ? $params['from'] : MAIL_FROM;
                $fromname = isset($params['fromname']) ? $params['fromname'] : MAIL_FROMNAME;
                $this->ci->email->from($from, $fromname);
                $this->ci->email->to($params['to']);
                if(isset($params['cc'])){
                        $this->ci->email->cc($params['cc']);
                }
                if(isset($params['bcc'])){
                        $this->ci->email->bcc($params['bcc']);
                }
                
                return $this->ci->email->send();
        }

}
