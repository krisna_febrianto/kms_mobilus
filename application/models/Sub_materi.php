<?php
/**
 * Description of Gambar_hotel
 *
 * @author Fikran
 */
class Sub_materi extends MY_Model{
        public $table = 'sub_materi';
        public $primary_key = 'id';
        public $fillable = array();
        public $protected = array('id', 'created_at', 'updated_at');
        public function __construct(){
                $this->return_as = 'object';
                $this->timestamps = TRUE;
                parent::__construct();
        }
}
