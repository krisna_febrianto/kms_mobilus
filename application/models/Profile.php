<?php
/**
 * Description of Gambar_hotel
 *
 * @author Fikran
 */
class Profile extends MY_Model{
        public $table = 'profile';
        public $primary_key = 'id';
        public $fillable = array();
        public $protected = array('id', 'created_at', 'updated_at');
        public function __construct(){
                $this->return_as = 'object';
                $this->timestamps = TRUE;
                $this->has_one['user'] = array('User','id','user_id');
                parent::__construct();
        }
}
