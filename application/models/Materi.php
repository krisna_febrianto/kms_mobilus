<?php
/**
 * Description of Gambar_hotel
 *
 * @author Fikran
 */
class Materi extends MY_Model{
        public $table = 'materi';
        public $primary_key = 'id';
        public $fillable = array();
        public $protected = array('id', 'created_at', 'updated_at');
        public function __construct(){
                $this->return_as = 'object';
                $this->timestamps = TRUE;
                $this->has_many['sub_materi'] = array('Sub_materi','id_materi','id');
                $this->has_one['kategori'] = array('Kategori','id','kategori_id');
                parent::__construct();
        }
        function count_materi_by_($where){
                if($where != ''){
                        $where = 'where '.$where; 
                }else{
                        $where = '';
                }
        $query = $this->db->query('SELECT count(*) as count_materi from (select count(*) from materi '.$where.' group by id) as p');
        return $query;
    }
}
