<?php
/**
 * Description of Gambar_hotel
 *
 * @author Fikran
 */
class User extends MY_Model{
        public $table = 'user';
        public $primary_key = 'id';
        public $fillable = array();
        public $protected = array('id', 'created_at', 'updated_at');
        public function __construct(){
                $this->return_as = 'object';
                $this->timestamps = TRUE;
                parent::__construct();
        }
        public $before_create = array( 'set_password' );
        public $before_update = array( 'check_password' );
        protected $return_type = 'array';

        protected function set_password($user)
        {
            $user['password'] = $this->get_hash($user['username'], $user['password']);
            return $user;
        }

        protected function check_password($user)
        {
            if(isset($user['password'])){
                $user['password'] = $this->get_hash($user['username'], $user['password']);
            }
            return $user;
        }

        protected function get_hash($username, $password) 
        {
            return md5($username.':'.$password);
        }

        public function is_email_exist($email, $id = 0){
                $where = array('email' => $email);
                if($id > 0){
                        $where['id != '] = $id;
                }

                $user = $this->user->where($where)->get();

                if (is_null($user)) {
                        return FALSE;
                }
                
                return $user;
        }
}
