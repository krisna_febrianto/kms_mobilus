<?php
/**
 * Description of Gambar_hotel
 *
 * @author Fikran
 */
class Kategori extends MY_Model{
        public $table = 'kategori';
        public $primary_key = 'id';
        public $fillable = array();
        public $protected = array('id', 'created_at', 'updated_at');
        public function __construct(){
                $this->return_as = 'object';
                $this->timestamps = TRUE;
                parent::__construct();
        }
}
