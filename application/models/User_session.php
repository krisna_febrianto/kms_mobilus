<?php

class User_session extends CI_Model {

        function __construct() {
                parent::__construct();
        }

        function get() {
                return $this->session->userdata(USER_SESSION);
        }

        function set($data) {
                $this->session->set_userdata(USER_SESSION, $data);
        }

        function create($username, $password) {
                // $this->load->model('user');
                $data = $this->auth($username, $password);
                if ($data) {
                        unset($data['password']);
                        $this->set($data);
                        return true;
                } else {
                        return false;
                }
        }

        function clear() {
                $this->session->unset_userdata(USER_SESSION);
        }

        function auth($username, $password) {
                $this->db->select('*');
                $this->db->from('user');
                $this->db->where(array(
                    'username' => $username,
                    'password' => $this->get_hash($username, $password),
                    'role_id' => 2,
                    'status_id' => 1
                ));
                $q = $this->db->get();
                if($q->num_rows() == 0){
                        return NULL;
                }
                $user = $q->row_array();
                if ($user['role_id'] == 2) {
                        return $user;
                }
                return NULL;
        }

        function get_hash($username, $password) {
                return md5($username . ':' . $password);
        }

}
