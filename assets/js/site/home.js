$(document).ready(function(){

	$('#content-home').html('<div class="title-v1 margin-top-50"><h2 style="text-transform:none !important;">Loading. . .</h2></div>');
	$('#content-populer').html('<div class="title-v1 margin-top-50"><h2 style="text-transform:none !important;">Loading. . .</h2></div>');

	init();
	search();
});

function init(){
	$.ajax({ 
	    	type: "POST",
	      	dataType: 'json',
	      	url: base_url + "site/get_materi",
	      	success: function(data){
	      		$('#content-home').empty();
	      		$('#content-populer').empty();

		        if (data.materi) {
		          	$.each(data.materi, function(key, value){
			          	var content = generateContent(value);
			          	$('#content-home').append(content);
			        });
		        }else{
		          	$('#content-home').append('<div class="title-v1 margin-top-50"><h2 style="text-transform:none !important;">Tidak ada materi di kategori ini</h2></div>');
		        }

		        if (data.populer) {
		          	$.each(data.populer, function(key, value){
			          	var content = generateContent(value);
			          	$('#content-populer').append(content);
			        });
		        }else{
		          	$('#content-populer').append('<div class="title-v1 margin-top-50"><h2 style="text-transform:none !important;">Tidak ada materi di kategori ini</h2></div>');
		        }
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
}

function search(){
	$("[name='search']").on('keyup', function(){
   
    	$('#home-head').empty();
    	//$('#home-head').append('<div class="title-v1 margin-top-50"><h2>Searching. . .</h2></div>');
  		$('#home-body').html('<div class="title-v1 margin-top-50"><h2 style="text-transform:none !important;">Searching. . .</h2></div>');

  		var search = $("[name='search']").val();

  		if (search) {
	    	$.ajax({ 
		    	type: "POST",
		      	dataType: 'json',
		      	url: base_url + "site/get_search/"+search,
		      	success: function(data){
		      		console.log(data);
		      		//$('#home-head').empty();
		      		$('#home-body').empty();

			        if (data) {
			          	$.each(data, function(key, value){
				          	var content = generateContent(value);
				          	//$('#home-head').append(content);
				          	$('#home-body').append(content);
				        });
			        }else{
			          	//$('#home-head').append('<div class="title-v1 margin-top-50"><h2>Materi tidak ditemukan</h2></div>');
			          	$('#home-body').append('<div class="title-v1 margin-top-50"><h2>Materi tidak ditemukan</h2></div>');
			        }
			    },
		      	error: function(XMLHttpRequest, textStatus, errorThrown) {
		        	var err = XMLHttpRequest.responseJSON;
			    }
		    });
		}else{
			$('#home-body').html('<div class="title-v1 margin-top-50"><h2 style="text-transform:none !important;">Harap Masukan Keyword</h2></div>');
		}
    });
}

function generateContent(data){
	if (data.updated_at != null) {
		var updated = 'Updated '+$.format.date(data.updated_at, "MMM d, yyyy");
	}else{
		var updated = ' ';
	}

	var	content = '<div class="col-md-4">';
		content += 	'<div class="panel" style="border-color: '+data.kategori.warna+';">';
		content += 		'<div class="panel-heading" style="background: '+data.kategori.warna+';">';
		content += 			'<img class="image-sm no-margin-bottom no-top-space" src="'+base_url+'assets/images/kategori/'+data.kategori.icon+'">';
		content += 			'<span class="right"><h3 class="panel-title"><i class="fa fa-clock-o"></i> '+timeInterval(data.created_at)+'</h3></span>';
		content += 		'</div>';
		content += 		'<div class="panel-body height-100"><h4>'+data.judul+'</h4></div>';
		content +=		'<div class="panel-footer">';
		content +=			'<a href="#" class="btn-u btn-brd rounded-2x btn-u-dark btn-u-xs">View <i class="fa fa-arrow-right"></i></a>';
		content +=			'<span class="right no-margin-bottom text-footer"> '+updated+' </span>';
		content +=		'</div>';
		content +=	'</div>';
		content += '</div>';

	return content;
}

function timeInterval(time){
	var now = new Date();
	var before = new Date(time)

	var second = (now.getTime() - before.getTime()) / 1000;
	var minute = (now.getTime() - before.getTime()) / 60000;
	var hour = (now.getTime() - before.getTime()) / 3600000;
	var day = (now.getTime() - before.getTime()) / 86400000;
	var week = (now.getTime() - before.getTime()) / 604800000;
	var month = (now.getTime() - before.getTime()) / 2419200000;
	var year = (now.getTime() - before.getTime()) / 29030400000;

	if (second <= 60) {
        newTime = second.toFixed(0)+' detik';
    } else if (minute <= 60) {
        newTime = minute.toFixed(0)+' menit';
    } else if (hour <= 24) {
        newTime = hour.toFixed(0)+' jam';
    } else if (day <= 7) {
        newTime = day.toFixed(0)+' hari';
    } else if (week <= 4) {
        newTime = week.toFixed(0)+' minggu';
    } else if (month <= 12) {
        newTime = month.toFixed(0)+' bulan';
    } else {
        newTime = year.toFixed(0)+' tahun';
    }

	return newTime;
}
