var StepWizard = function () {

    return {

        initStepWizard: function () {
            $(".shopping-cart").children("div").steps({
                    headerTag: ".header-tags",
                    bodyTag: "section",
                    //transitionEffect: "slideLeft",
                    stepsOrientation: "vertical",
                    transitionEffect: "fade",

                });
        }, 

    };
}();        