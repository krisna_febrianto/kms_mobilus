var table = $('#table-penerbangan'),
	form = $('#form-penerbangan'),
	modal = $('#modal-penerbangan');

$(document).ready(function() {
    $('.date-picker').datepicker({format : 'yyyy-mm-dd',autoclose: true});
    $('.timepicker-24').timepicker({ 
      minuteStep: 1,
      showMeridian: false,
      autoclose: true,
      defaultTime: '0:00'});
    // handle input group button click
    $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function(e){
        e.preventDefault();
        $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
    });
    $( document ).scroll(function(){
        $('#form-penerbangan .date-picker').datepicker('place'); //#modal is the id of the modal
    });

    // Workaround to fix timepicker position on window scroll
    $( document ).scroll(function(){
        $('.timepicker-24').timepicker('place'); //#modal is the id of the modal
    });
    $("#bandara_berangkat_filter").change(function(){
      var value=$(this).val();
      if(value){
         $('[name="bandara_tujuan_filter"] option').show();
        if (value != 'default') {
         $('[name="bandara_tujuan_filter"] option[value='+value+']').hide();
        }
      }
    });
    $("#bandara_berangkat_id").change(function(){
      var value=$(this).val();
      if(value){
         $('[name="bandara_tujuan_id"] option').show();
         $('[name="bandara_tujuan_id"] option[value='+value+']').hide();
      }
    });
     $("#tanggal_berangkat").change(function(){
      var Value=$('#tanggal_berangkat').find("input").val();
      $("#save_tanggal_mulai").datepicker('setDate', Value);
      $("#save_tanggal_selsai").datepicker('setDate', Value);
      $("#save_tanggal_mulai").datepicker('setStartDate', Value);
      $("#save_tanggal_selsai").datepicker('setStartDate', Value);
      $('[name="tanggal_berangkat"]').valid();
    });
    $("#jam_berangkat").change(function(){
      $('[name="jam_berangkat"]').valid();
    });
    $("#jam_tiba").change(function(){
      $('[name="jam_tiba"]').valid();
    });
});
$(function(){
	initValidation();
  $('.table-content').show();
  $('.content-penerbangan-table').empty();
  $('.content-penerbangan-table').append('<table id="table-penerbangan" class="table table-striped table-bordered table-hover"></table>');
  initDataTables('default','default','default','default','default');
});

function generateTable(){
  var bandara_tujuan_filter = $("[name=bandara_tujuan_filter]").val();
  var bandara_berangkat_filter = $("[name=bandara_berangkat_filter]").val();
  var maskapai_filter = $("[name=maskapai_filter]").val();
  if ($("[name=tanggal_berangkat_filter]").val()) {
    var tanggal_berangkat_filter = $("[name=tanggal_berangkat_filter]").val();
  }else{
    var tanggal_berangkat_filter = 'default';
  }
  if ($("[name=tanggal_tiba_filter]").val()) {
    var tanggal_tiba_filter = $("[name=tanggal_tiba_filter]").val();
  }else{
    var tanggal_tiba_filter = 'default';
  }
  $('.table-content').show();
  $('.content-penerbangan-table').empty();
  $('.content-penerbangan-table').append('<table id="table-penerbangan" class="table table-striped table-bordered table-hover"></table>');
  initDataTables(bandara_tujuan_filter,bandara_berangkat_filter,maskapai_filter,tanggal_berangkat_filter,tanggal_tiba_filter);
}
function initDataTables(bandara_tujuan_filter,bandara_berangkat_filter,maskapai_filter,tanggal_berangkat_filter,tanggal_tiba_filter){
  $('#table-penerbangan').DataTable({
    "order": [10, "desc"],
    "search": [1,2,3],
    "columns": [
      { "data": "id", "name": "no", "title": "No","bSearchable": false,
            "render": function ( data, type, full, meta ) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
        { "data": "maskapai_nama", "name": "colA", "title": "Nama Maskapai" },
        { "data": "bandara_berangkat_tampil", "name": "colB", "title": "Kota Berangkat" },
        { "data": "bandara_tujuan_tampil", "name": "colC", "title": "Kota Tujuan" },
        { "data": "tanggal_berangkat_format", "name": "colD", "title": "Tanggal Berangkat", "bSearchable": false},
        { "data": "jam_berangkat", "name": "colF", "title": "Jam Berangkat", "bSearchable": false,  
          "render": function ( data, type, full, meta ) {
            return data.substr(0, 5)
          }
        },
        { "data": "tanggal_tiba_format", "name": "colE", "title": "Tanggal Tiba", "bSearchable": false  },
        { "data": "jam_tiba", "name": "colG", "title": "Jam Tiba", "bSearchable": false,  
          "render": function ( data, type, full, meta ) {
            return data.substr(0, 5)
          }  

        },
        { "data": "harga", "name": "colI", "title": "Harga", "bSearchable": false ,
        "render": function ( data, type, full, meta ) {
            var a = parseInt(data); 
            var harga = a.toLocaleString();
            return full.symbol+" "+harga
          }
        },
        
        { "data": "is_low_season", "name": "colJ", "title": "Low Season", "bSearchable": false ,
          "render": function ( data, type, full, meta ) {
            if (data == 0) {
              return '<div class="label label-danger">Tidak</div>'
            }else{
              return '<div class="label label-success">Ya</div>'
            }
          }
        },
        { "data": "id", "name": "action", "title": "Action", "bSearchable": false ,
          "render": function ( data, type, full, meta ) {
            return '<a data-id="'+data+'" class="edit-penerbangan" onclick="initEdit(this);"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
            '<a data-id="'+data+'" class="delete-penerbangan" onclick="initDelete(this);"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>';
        } 
      }
        //repeat for each of my 20 or so fields
    ],
    "ajax": { "url" : admin_base_url + "ui/penerbangan_admin_table/get_datatables/"+bandara_tujuan_filter+"/"+bandara_berangkat_filter+"/"+maskapai_filter+"/"+tanggal_berangkat_filter+"/"+tanggal_tiba_filter,
            "type": "POST"
        },
        "serverSide":true
        // "processing":true
  });
}

function initAdd(){
     form.trigger("reset");
  $("#submit").attr("disabled", false);
      form.get(0).reset();
      form.data('changed', 0);
      form.validate().resetForm();
      form.find('.alert').hide();
      form.find(".form-group").removeClass("has-error");
      form.find(".form-group").removeClass("has-success");
      $(".timepicker-24").timepicker('setTime', '0:00');
      $("#form_tanggal_simpan").show();
      form_tanggal_simpan
		form.find('input[name=id]').val(0);
		form.find('input[name=maskapai_id]').val("");
		form.find('input[name=bandara_berangkat_id]').val("");
		form.find('input[name=bandara_tujuan_id]').val("");
		form.find('input[name=tanggal_berangkat]').val("");
		form.find('input[name=tanggal_tiba]').val("");
		form.find('input[name=jam_berangkat]').val("0:00");
		form.find('input[name=jam_tiba]').val("0:00");
		form.find('input[name=harga]').val("");
		form.find('input[name=mata_uang_id]').val("");
		form.find('input[name=is_low_season]').prop('checked', false);
		modal.modal('show');
}

function initValidation(){
  $.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
  }, "Value must not equal arg.");
  var validator = form.validate({
        rules: {
          tanggal_tiba: {
            required: true
          },
          maskapai_id: {
            valueNotEquals: "default"
          },
          bandara_berangkat_id: {
            valueNotEquals: "default"
          },
          bandara_tujuan_id: {
            valueNotEquals: "default"
          },
          
          tanggal_berangkat: {
            required: true
          },
          jam_berangkat: {
            valueNotEquals: "0:00"
          },
          jam_tiba: {
            valueNotEquals: "0:00"
          },
          harga: {
            required: true
          },
          mata_uang_id: {
            valueNotEquals: "default"
          }
        },
        messages: {
          maskapai_id: {
            valueNotEquals: "Bidang isian nama maskapai tidak boleh kosong"
          },
          bandara_berangkat_id: {
            valueNotEquals: "Bidang isian bandara berangkat tidak boleh kosong"
          },
          bandara_tujuan_id: {
            valueNotEquals: "Bidang isian bandara tujuan tidak boleh kosong"
          },
          tanggal_berangkat: {
            required: "Bidang isian tanggal berangkat tidak boleh kosong"
          },
          tanggal_tiba: {
            required: "Bidang isian tanggal tiba tidak boleh kosong"
          },
          jam_berangkat: {
            valueNotEquals: "Bidang isian jam berangkat tidak boleh kosong"
          },
          jam_tiba: {
            valueNotEquals: "Bidang isian jam tiba tidak boleh kosong"
          },
          harga: {
            required: "Bidang isian harga tidak boleh kosong"
          },
          mata_uang_id: {
            valueNotEquals: "Bidang isian mata uang tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
            if (element.attr("name") == "tanggal_berangkat" || element.attr("name") == "jam_berangkat" || element.attr("name") == "jam_tiba"){
              element.parent().parent().append(error);
            }else{
              element.parent().append(error);
            }
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
  $("#submit").attr("disabled", true);
	var arrayData = form.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

	$.ajax({ 
    	type: "POST",
      	dataType: 'json',
      	data: arrayData,
      	url: admin_base_url + "ui/penerbangan_admin_table/save",
      	success: function(data){
          modal.modal('toggle');
          bootbox.alert("Data penerbangan berhasil disimpan.", function(){
            generateTable();
          });    
	    },
      	error: function(XMLHttpRequest, textStatus, errorThrown) {
        	var err = XMLHttpRequest.responseJSON;
	    }
    });
}

function initEdit(el){
		var id = $(el).data('id');
		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	url: admin_base_url + "ui/penerbangan_admin_table/edit/"+id,
	      	success: function(data){
          $("#submit").attr("disabled", false);
          form.trigger("reset");
          form.get(0).reset();
          form.data('changed', 0);
          form.validate().resetForm();
          form.find('.alert').hide();
          form.find(".form-group").removeClass("has-error");
          form.find(".form-group").removeClass("has-success");
            $("#form_tanggal_simpan").hide();
		        form.find('input[name=id]').val(data.id);
		        form.find('input[name=tanggal_berangkat]').val(data.tanggal_berangkat);
		        form.find('input[name=tanggal_tiba]').val(data.tanggal_tiba);
		        form.find('input[name=jam_berangkat]').val(data.jam_berangkat);
		        form.find('input[name=jam_tiba]').val(data.jam_tiba);
		        form.find('input[name=harga]').val(data.harga);
            $("#jam_berangkat").timepicker('setTime', data.jam_berangkat);
            $("#jam_tiba").timepicker('setTime', data.jam_tiba);
		        $('#maskapai').val(data.maskapai.id);
		        $('#bandara_berangkat_id').val(data.bandara_berangkat.id);
		        $('#bandara_tujuan_id').val(data.bandara_tujuan.id);
		        $('#mata_uang_id').val(data.mata_uang.id);
		        if(data.is_low_season == 1){
		        	form.find('input[name=is_low_season]').prop('checked', true);	
		        }else{
		        	form.find('input[name=is_low_season]').prop('checked', false);	
		        }
		        
		        modal.modal('show');
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
}

function initDelete(el){
    var id = $(el).data('id');
      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data ini?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                url: admin_base_url + "ui/penerbangan_admin_table/delete/"+id,
                success: function(data){
                  bootbox.alert("Data penerbangan berhasil dihapus.", function(){
                    generateTable();
                  });   
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
}
