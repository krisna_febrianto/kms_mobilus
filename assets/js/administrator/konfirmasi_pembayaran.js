var form = $("#form-reject");
var table = $('#datatable');
var modal = $('#modal-konfirmasi-pembayaran');
var modalImageView = $('#modal-image');
var modalReject = $('#modal-reject');

$(document).ready(function(){
	
	table.DataTable({
        "order": [6, "asc"],
        "columns": [
          { "data": "id", "name": "no", "title": "No",  "bSearchable": false, "width": "5%" , 
            "render": function ( data, type, full, meta ) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
            { "data": "username", "width": "5%", "title": "username" },
            { "data": "nama_jenis_paket", "width": "5%", "title": "Jenis Paket"  },
            { "data": "tanggal_berangkat", "width": "5%", "title": "Tanggal Berangkat"  },
            { "data": "harga", "width": "5%", "title": "Total Harga",
		        "render": function ( data, type, full, meta ) {
		            return "Rp. "+parseInt(data).toLocaleString()
		          }  
            },
            { "data": "status_tag", "width": "5%", "title": "Status",  
            	"render": function ( data, type, full, meta ) {
                return '<div class="label label-'+full.status_color+'">'+data+'</div>';
              }
        	},
            { "data": "id", "name": "action", "width": "5%", "title": "Action" ,"bSearchable": false,   
              "render": function ( data, type, full, meta ) {
                return'<a href="#" data-id="'+data+'" onclick="viewDetail(this);"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-search" title="Detail"></i> Detail</div></a>'+
                '<a href="#" data-id="'+data+'" onclick="confirm(this);"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-check-circle" title="Confirm"></i> Konfirmasi</div></a>'+
                '<a href="#" data-id="'+data+'" onclick="belumDiterima(this);"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-close" title="Belum Diterima"></i> Belum Diterima</div></a>';
            } 
          }
           
        ],

        "ajax": {
            "url": admin_base_url+"ui/Konfirmasi_pembayaran_admin/get_datatables",
            "type": "POST"
        },
        "serverSide":true
        // "processing":true
    });
  initValidation();
	$('body').on('hidden.bs.modal', '.modal', function () {

      $('.nav-pills a:first').tab('show');
	    $(this).removeData('bs.modal');
      $(this).find('form').trigger('reset');
      //r.validate().resetForm();
      $(this).find('.alert').hide();
      $(this).find(".form-group").removeClass("has-error");
      $(this).find(".form-group").removeClass("has-success");
      $('#formPlus').hide();
      $('#formTanggalPlus').hide();
      form.validate().resetForm();
      form.find('.alert').hide();
      form.find(".form-group").removeClass("has-error");
      form.find(".form-group").removeClass("has-success");
    });
    modal.on('hide.bs.modal', function () {
    });
    
});
function initValidation(){
  var validator = form.validate({
        rules: {
          pesan: {
            required: true
          }
        },
        messages: {
          pesan: {
            required: "Bidang isian pesan tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
          element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}
function submitForm(){
  $("#submit").val('Mengirim...'); //change button text
  $("#submit").attr("disabled", true);
  var arrayData = form.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

  $.ajax({ 
      type: "POST",
        dataType: 'json',
        data: arrayData,
        url: admin_base_url + "ui/Konfirmasi_pembayaran_admin/reject",
        success: function(data){
          //modal.modal('toggle');
          modalReject.modal('hide');
          bootbox.alert("Pembayaran berhasil ditolak.", function(){
             window.location.href = data.url; 
          });    
      },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          var err = XMLHttpRequest.responseJSON;
      }
    });
}
function viewDetail(el){
    var id = $(el).data('id');
    $.ajax({ 
            type: "GET",
            dataType: 'json',
            data: {id: id},
            url: admin_base_url + "ui/Konfirmasi_pembayaran_admin/view/"+id,
            success: function(data){
                $("#booking_id").text(data.detail.booking.no_booking); 
                $("#no_rekening").text(data.detail.no_rekening); 
                $("#atas_nama").text(data.detail.atas_nama);
                $("#username").text(data.user.username);
                $("#jenis_paket_umroh").text(data.jenis_paket_umroh.nama);
                if (data.detail.bukti_pembayaran != '0') {
                  $("#bukti_pembayaran").html("<img id='image' src='"+base_url+"assets/images/bukti_pembayaran/"+data.detail.booking_id+"/"+data.detail.bukti_pembayaran+"' width='150' height='150' onclick='modalImage();'/>");
                }else{
                   $("#bukti_pembayaran").html("<div class='label label-danger'>No Image</div>");
                }
                $("#tanggal_berangkat").text(data.paket_umroh.tanggal_berangkat);
                $("#harga").text("Rp. "+parseInt(data.paket_umroh.harga).toLocaleString());
                console.log(data); 
                modal.modal('show');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var err = XMLHttpRequest.responseJSON;
            }
        });        
}

function confirm(el){
    var id = $(el).data('id');
      bootbox.confirm({
        message: "Apakah Anda yakin akan mengkonfirmasi pembayaran ini?",
        buttons: {
            confirm: {
                label: 'Ya',
                className: 'btn-success'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                url: admin_base_url + "ui/Konfirmasi_pembayaran_admin/confirm/"+id,
                success: function(data){
                  bootbox.alert("Pembayaran berhasil diterima.", function(){
                     window.location.href = data.url; 
                  });    
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });     
}
function belumDiterima(el){
    var id = $(el).data('id');
     $.ajax({ 
            type: "GET",
            dataType: 'json',
            data: {id: id},
            url: admin_base_url + "ui/Konfirmasi_pembayaran_admin/view/"+id,
            success: function(data){
                form.find('input[name=id]').val(id);
                form.find('input[name=pesan]').val(data.detail.pesan);
                $("textarea#pesan").val(data.detail.pesan);
                modalReject.modal('show');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var err = XMLHttpRequest.responseJSON;
            }
        }); 
    
}
function modalImage(){
  $('#imagepreview').attr('src', $('#image').attr('src'));
  modalImageView.modal('show');
}
