var tablePreBook = $('#table-pre-book'),
    tablePreBookNew = $('#table-pre-book-baru'),
    modalPreBook = $('#pre-book-modal'),
    table, tableNew;

$(document).ready(function(){
  initDataTablesNew();
  initDataTables();
  initDetail();
  // setInterval(reloadTable, 5000);
  //initConvert();

  var preBookId = $("#view-pre-book").val();
  if(preBookId > 0){
    openModalDetail(preBookId);
    // $.post(admin_base_url+"ui/Pre_book_admin/updateIsView");
  }
  
});

function initDataTablesNew(){
    //var local = localStorage.getItem('id');
    tableNew = tablePreBookNew.DataTable({
        "order": [],
        "columns": [
            { "data": "id", "name": "no", "title": "No",
              "render": function ( data, type, full, meta ) {
                return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            { "data": "username", "name": "username", "title": "Username" },
            { "data": "nama", "name": "Nama", "title": "Nama" },
            { "data": "tanggal_berangkat", "name": "tanggal_berangkat", "title": "Tanggal Berangkat"},
            { "data": "id", "name": "Aksi", "title": "Aksi", "sortable":false,
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="view-pre-book"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-search" title="Edit"></i> Detail</div></a>'+
                '<a data-id="'+data+'" class="update-pre-book" onclick="initConvert(this);"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Ubah ke Paket"></i> Ubah ke Paket</div></a>';
              } 
            }
        ],
        // "createdRow": function ( row, data, index ) {
        //     if ( data.id == local) {
        //         $('td', row).eq(data.id).parent().addClass('highlight');
        //         $('td', row).eq(data.id).parent().focus();
        //     }
        // },
        "ajax": {
            "url": admin_base_url+"ui/Pre_book_admin/get_list_new",
            "type": "POST"
        },
        "serverSide":true
    });
    //updateStatusNotif(local);
    
}

function initDataTables(){
    //var local = localStorage.getItem('id');
    table = tablePreBook.DataTable({
        "order": [],
        "columns": [
            { "data": "id", "name": "no", "title": "No",
              "render": function ( data, type, full, meta ) {
                return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            { "data": "username", "name": "username", "title": "Username" },
            { "data": "nama", "name": "Nama", "title": "Nama" },
            { "data": "tanggal_berangkat", "name": "tanggal_berangkat", "title": "Tanggal Berangkat"},
            { "data": "id", "name": "Aksi", "title": "Aksi", "sortable":false,
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="view-pre-book"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-search" title="Edit"></i> Detail</div></a>'+
                '<a data-id="'+data+'" class="update-pre-book" onclick="initConvert(this);"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Ubah ke Paket"></i> Ubah ke Paket</div></a>';
              } 
            }
        ],
        // "createdRow": function ( row, data, index ) {
        //     if ( data.id == local) {
        //         $('td', row).eq(data.id).parent().addClass('highlight');
        //         $('td', row).eq(data.id).parent().focus();
        //     }
        // },
        "ajax": {
            "url": admin_base_url+"ui/Pre_book_admin/get_list",
            "type": "POST"
        },
        "serverSide":true
    });
    //updateStatusNotif(local);
    if (table) {
      $.post(admin_base_url+"ui/Pre_book_admin/updateIsView");
    }
    
}

function openModalDetail(id){
    modalPreBook.modal({
      remote: base_url+'administrator/ui/pre_book_admin/detail/'+id
    });
    // $.post(admin_base_url+"ui/Pre_book_admin/updateIsView");
}

// function reloadTable(){
//   table.ajax.reload(null,false);
//   tableNew.ajax.reload(null,false);
// }

function initDetail(){
  tablePreBook.on('click','.view-pre-book', function(){

    var id = $(this).data('id');
    modalPreBook.modal({
      remote: base_url+'administrator/ui/pre_book_admin/detail/'+id
    });
    // reloadTable();
  });

  tablePreBookNew.on('click','.view-pre-book', function(){

    var id = $(this).data('id');
    modalPreBook.modal({
      remote: base_url+'administrator/ui/pre_book_admin/detail/'+id
    });
    // reloadTable();
  });
}

function initConvert(el){
  //tablePreBook.on('click', '.update-pre-book', function(){
    var id = $(el).data('id');
    window.location.href = base_url+"administrator/ui/paket_umroh_admin/index/"+id;
  //});
}

// function updateStatusNotif(id){
//     $.ajax({
//     url:admin_base_url + "ui/Pre_book_admin/updateStatusNotif/"+id,
//     method:"POST",
//     //data:{view:view},
//     dataType:"json",
//     success:function(data){
//       localStorage.clear("id");
//     }
//   });
// }