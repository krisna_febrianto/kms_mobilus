var tableKamar = $('#table-kamar-hotel'),
	formKamar = $('#form-kamar-hotel'),
	modalKamar = $('#modal-kamar-hotel');

$(function(){
	init();
	initAdd();
	initValidation();
	initEdit();
  initDelete();
  // initSelect2();
});
function init(){ 
    $("[name='kota']").select2({
        placeholder: '-- Pilih Kota --',
        // minimumInputLength: 1,
        ajax: {
          url: admin_base_url+"ui/kamar_hotel_admin/get_kota/",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.nama
                    }
                    console.log(item);
                    get_hotel(item.nama);
                })
            };
            get_hotel(data.nama);
          },
          cache: true
        }
    });

    $("[name='kota']").on('change', function(){
      var kota = $(this).val();
      //$("[name='kota']").val("").trigger('change');
      $("[name='hotel']").val("").trigger('change');
      $("[name='kamar']").val("").trigger('change');
      get_hotel(kota);
    });
};

function get_hotel($kota){
    $("[name='hotel']").select2({
        placeholder: '-- Pilih Hotel --',
        ajax: {
          url: admin_base_url+"ui/kamar_hotel_admin/get_hotel/"+$kota,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });

    $("[name='hotel']").on('change', function(){
      var id = $(this).val();
      //$("[name='hotel']").val("").trigger('change');
      $("[name='kamar']").val("").trigger('change');
      //get_kamar(id);

      $("[name='kamar']").val(0).trigger('change');
    });
}

function get_kamar($hotel_id){
    $("[name='kamar']").select2('destroy');
    
    $("[name='kamar']").select2({
        placeholder: '-- Pilih Kamar --',
        ajax: {
          url: admin_base_url+"ui/kamar_hotel_admin/get_kamar_select/"+$hotel_id,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            $('[name=kamar]').append($("<option></option>").attr("value",0)
                            .text("Semua Kamar"));
            return {
                
                results: $.map(data, function (item) {
                    return {
                        text: item.tipe_kamar,
                        id: item.id
                    }
                    console.log(item);
                    // $("[name='kamar']").append($('<option>', {value: 0, text: 'Semua Kamar'}));
                })
            };
            // $("[name='kamar']").append($('<option>', {value: 0, text: 'Semua Kamar'}));

          },
          cache: true
        }
    });
    // $("[name='kamar']").select2('data', {id: 0, text: 'Semua Kamar'}); 
}

function generateTable(){
  var id = $("[name=hotel]").val();
  //var kamar = $("[name=kamar]").val();
  if(id){
    $('.table-content').show();
    $('.content-kamar-table').empty();
    $('.content-kamar-table').append('<table id="table-kamar-hotel" class="table table-striped table-bordered table-hover"></table>');
    initDataTables(id);
  }
}
// function initSelect2(){
//   $('.select2').select2({
//       placeholder: ' Cari Nama Hotel',
//       minimumInputLength: 1,
//       ajax: {
//         url: admin_base_url + "ui/Kamar_hotel_admin/search_hotel",
//         dataType: 'json',
//         delay: 250,
//         processResults: function (data) {
//           return {
//             results: data
//           };
//         },
//         cache: true
//       }
//     } 
//   );
// }

function initDataTables(id,kamar){
	$('#table-kamar-hotel').DataTable({
    "columns": [
        { "data": "tipe_kamar", "name": "colA", "title": "Tipe Kamar" },
        { "data": "jenis_makanan", "name": "colB", "title": "Meals Plan" },
        { "data": "id", "name": "action", "title": "Action",
          "render": function ( data, type, full, meta ) {
            return '<a data-id="'+data+'" class="edit-kamar-hotel" onclick="edit_kamar(this);"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
            '<a data-id="'+data+'" class="view-images" onclick="view_gallery(this)"><div class="btn btn-outline btn-circle btn-sm blue"><i class="icon-picture" title="Lihat Gambar"></i> Gallery</div></a>'+
            '<a class="btn btn-outline btn-circle dark btn-sm blue" href="'+admin_base_url+'ui/harga_kamar_hotel_admin/index/'+id+'/'+data+'"><i class="fa fa-money"></i> Harga Kamar</a>'+
            '<a data-id="'+data+'" class="delete-kamar-hotel" onclick="confirm_delete(this);"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>';
        } 
      }
        //repeat for each of my 20 or so fields
    ],
    "ajax": admin_base_url + "ui/Kamar_hotel_admin/get_kamar/"+id
  });
}

function initAdd(){
	$('#add-kamar-hotel').on("click", function(){
    formKamar.validate().resetForm();
    $(this).find('.alert').hide();
    formKamar.find(".form-group").removeClass("has-error");
    formKamar.find(".form-group").removeClass("has-success");
		formKamar.find('input[name=id]').val(0);
    var hotel_id = $("[name=hotel]").val();
    formKamar.find('input[name=hotel_id]').val(hotel_id);
		formKamar.find('input[name=tipe_kamar]').val("");
    formKamar.find('input[name=kapasitas]').val("");
    formKamar.find('input[name=jenis_makanan]').val("");
		modalKamar.modal('show');
	});
}

function initValidation(){
  var validator = formKamar.validate({
        rules: {
          tipe_kamar: {
            required: true
          },
          jenis_makanan: {
            required: true
          },
          kapasitas: {
            required: true
          }
        },
        messages: {
          tipe_kamar: {
            required: "Bidang isian tipe kamar tidak boleh kosong"
          },
          kapasitas: {
            required: "Bidang isian kapasitas tidak boleh kosong"
          },
          jenis_makanan: {
            required: "Bidang isian meals plan tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
        	element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
	var arrayData = formKamar.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });
    $('.submit').attr('disabled', true);

	$.ajax({ 
    	type: "POST",
      	dataType: 'json',
      	data: arrayData,
      	url: admin_base_url + "ui/Kamar_hotel_admin/save",
      	success: function(data){
	        // window.location.href = data.url;  
          // $("[name=hotel]").select2('data', {id:data}).trigger('change'); 
          modalKamar.modal('toggle');
          // initDataTables(data);
          bootbox.alert("Data kamar berhasil disimpan.", function(){
            // console.log('This was logged in the callback!'); 
            generateTable();
            $('.submit').attr('disabled', false);
          });
          
	    },
      	error: function(XMLHttpRequest, textStatus, errorThrown) {
        	var err = XMLHttpRequest.responseJSON;
	    }
    });
}

function initEdit(){
	
}
function edit_kamar(el){
    // alert();
  var id = $(el).data('id');
  $.ajax({ 
      type: "GET",
        dataType: 'json',
        data: {id: id},
        url: admin_base_url + "ui/Kamar_hotel_admin/edit",
        success: function(data){
          formKamar.find('input[name=id]').val(data.detail.id);
          formKamar.find('input[name=tipe_kamar]').val(data.detail.tipe_kamar);
          formKamar.find('input[name=kapasitas]').val(data.detail.kapasitas);
          formKamar.find('input[name=jenis_makanan]').val(data.detail.jenis_makanan);
          formKamar.find('input[name=hotel_id]').val(data.detail.hotel_id);
          
          
          modalKamar.modal('show');
      },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          var err = XMLHttpRequest.responseJSON;
      }
  });
};
function initDelete(){

}
function confirm_delete(el){
  var id = $(el).data('id');

  bootbox.confirm({
    message: "Apakah Anda yakin akan menghapus data ini ?",
    buttons: {
        confirm: {
            label: 'Hapus',
            className: 'btn-danger'
        },
        cancel: {
            label: 'Batal',
            className: 'btn-default'
        }
    },
    callback: function (result) {
      if(result == true){
        $.ajax({ 
          type: "GET",
            dataType: 'json',
            data: {id: id},
            url: admin_base_url + "ui/Kamar_hotel_admin/delete",
            success: function(data){
              // window.location.href = data.url;  
              bootbox.alert("Data kamar berhasil dihapus.", function(){
                // console.log('This was logged in the callback!'); 
                generateTable();
              });
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
              var err = XMLHttpRequest.responseJSON;
            }
        });
      }
    }
  });
};

function view_gallery(el){
    // var id = $(this).closest('tr').attr('idx');
    $('[name="image[]"]').val("");
    $('#modal-gallery').modal('show');
    var id = $(el).data('id');
    $( "#galleryForm" ).find('.upload-button').html('<input type="button" class="btn btn-block btn-primary" value="Upload" onclick="upload_image(\'ui/Hotel_admin/post_gambar_kamar\');"></button>');
    $( "#galleryForm" ).find('input[name=id]').val(id);
    // $('[name="id"]').val(id);
    // get_gallery(id,"ui/Hotel_admin/get_gallery");
    get_gallery_kamar(id,"ui/Hotel_admin/get_gallery_kamar");
};
function get_gallery_kamar(id,uri){
    $('.gallery-container').empty();
    $.post(admin_base_url + uri, {id: id}).done(function( data1 ) {
        var json = $.parseJSON(data1);
        console.log(json.length);
        if(data1.length == 0){

        }else{
            for (var i = 0; i < json.length; i++) {
                $('.gallery-container').append('<div class="col-md-3"><img src="'+base_url+'assets/images/hotel/room/'+id+'/'+json[i].nama_file+'" width="100%" alt="..." class="img-thumbnail" style="min-height:150px">'+
                    '<div class="row"><a class="delete-img btn btn-block btn-white text-red" href="javascript:;" onclick=delete_img_kamar('+json[i].id+',"'+json[i].nama_file+'",'+id+');><i class="fa fa-trash-o fa-lg"></i></a></div>'+
                    '</div>');
            }
        }
    })
}
function delete_img_kamar(id,foto,id_provider){
    bootbox.confirm("Apakan anda yakin akan menghapus gambar ini?", function(result){
        if (result) {
            $.post( admin_base_url + "ui/Hotel_admin/hapus_gambar_kamar", {id : id,foto : foto,id_provider : id_provider}).done(function( data ) {
                if (data == '1'){
                    bootbox.alert("Data berhasil dihapus.", function(){
                        get_gallery_kamar(id_provider,"ui/Hotel_admin/get_gallery_kamar");
                    })
                } else {
                    bootbox.alert('Data gagal dihapus.');
                }
            });
        }else{
        }
    });
}
function upload_image(url){
    // var data = new FormData($('#galleryForm')[0]);
    var validator = $( "#galleryForm" ).validate({
        rules: {
               image: {
                required: true,
                accept: "image/*"
            },
        },
        messages:{
          image:{
            accept: "Belum ada file yang dipilih."
          }
        }
    });
    
    if(validator.form()){
        var arrayData = new FormData($('#galleryForm')[0])
        var jsonData = {};
        $('[value="Upload"]').attr('disabled', true);
        $('[value="Upload"]').attr('value', 'Uploading. . .');
        $.each(arrayData, function(index, item) {
            jsonData[item.name] = item.value;
        });

        $.ajax({ 
            type: "POST",
            dataType: 'json',
            data: arrayData,
            url: admin_base_url + url,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
           success:function(data)
            {
                if (data != "0"){
                    bootbox.alert("Data berhasil disimpan.", function(){
                        if(data.tipe == "hotel"){
                            get_gallery(data.id,"ui/Hotel_admin/get_gallery");
                        }else{
                            get_gallery_kamar(data.id,"ui/Hotel_admin/get_gallery_kamar");
                            $( "#galleryForm" ).find('input[name=image]').val('');
                        }
                        $("[name='image[]']").val("");
                        $('[value="Uploading. . ."]').attr('disabled', false);
                        $('[value="Uploading. . ."]').attr('value', 'Upload');
                    })
                }else{
                    bootbox.alert("Data gagal disimpan.");
                }       

            }
        });
    }
}