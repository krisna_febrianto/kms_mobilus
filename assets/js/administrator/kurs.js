var tableKurs = $('#datatable'),
	formKurs = $('#kurs-form'),
	modalKurs = $('#kurs-modal');

$(function(){
	initDataTables();
	initAdd();
	initValidation();
	initEdit();
	initDelete();
  get_mata_uang_asal();
  get_konversi();
});

function initDataTables(){
	tableKurs.DataTable();
}

function initAdd(){
	$('#add-kurs').on("click", function(){
    formKurs.validate().resetForm();
    //form.find('.alert').hide();
    formKurs.find(".form-group").removeClass("has-error");
    formKurs.find(".form-group").removeClass("has-success");
		$('#mata_uang').attr('disabled', false);
		$('#konversi').attr('disabled', false);
		formKurs.find('input[name=id]').val(0);
		// $('#mata_uang option:selected').text("");
		// $('#konversi option:selected').text("");
    $("[name='mata_uang_asal']").val("").trigger('change');
    $("[name='konversi']").val("").trigger('change');
		formKurs.find('input[name=nilai]').val("");
		modalKurs.modal('show');
	});
}

function initValidation(){
	 // add the rule here
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
	 	return arg !== value;
	}, "Value must not equal arg.");

  // var nilai = $('[name="nilai"]').val();
  // console.log(nilai);

  	var validator = formKurs.validate({
        rules: {
          mata_uang_asal: {
          	valueNotEquals: ""
          },
          konversi: {
          	required: true,
            remote: {
              url: admin_base_url + "ui/Kurs_admin/check_mata_uang",
              type: 'post',
              dataType: 'json',
              data: {
                mata_uang_asal: function(){
                  return $('[name="mata_uang_asal"]').val();
                },
                konversi: function(){
                  return $('[name="konversi"]').val();
                }
              }
            }
          },
          nilai: {
            required: true
          }
        },
        messages: {
          mata_uang_asal: {
          	valueNotEquals: "Pilihan mata uang asal tidak boleh kosong"
          },
          konversi: {
            remote: "Kurs mata uang sudah ada",
          	required: "Pilihan mata uang konversi tidak boleh kosong",
            
          },
          nilai: {
            required: "Bidang isian nilai tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
        	element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
  var validator = formKurs.validate();
  if(validator.form()){

  	var arrayData = formKurs.serializeArray();
      var jsonData = {};

      $.each(arrayData, function(index, item) {
          jsonData[item.nilai] = item.value;
      });

  	$.ajax({ 
      	type: "POST",
        	dataType: 'json',
        	data: arrayData,
        	url: admin_base_url + "ui/Kurs_admin/save",
        	success: function(data){
  	        window.location.href = data.url;     
  	    },
        	error: function(XMLHttpRequest, textStatus, errorThrown) {
          	var err = XMLHttpRequest.responseJSON;
  	    }
      });
  }
}

function initEdit(){
	tableKurs.on("click",".edit-kurs", function(){
    formKurs.validate().resetForm();
    //form.find('.alert').hide();
    formKurs.find(".form-group").removeClass("has-error");
    formKurs.find(".form-group").removeClass("has-success");

		$('#mata_uang').attr('disabled', true);
		$('#konversi').attr('disabled', true);
		var id = $(this).data('id');
		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	data: {id: id},
	      	url: admin_base_url + "ui/Kurs_admin/edit",
	      	success: function(data){
		        formKurs.find('input[name=id]').val(data.detail.id);
				// $('#mata_uang option:selected').text(data.detail.mata_uang_asal.nama);
				// $('#konversi option:selected').text(data.detail.mata_uang_konversi.nama);
            var nilai = data.detail.nilai;
    				formKurs.find('input[name=nilai]').val(parseFloat(Math.round(nilai * 100) / 100).toFixed(0));
            $("[name='mata_uang_asal']").select2('trigger', 'select', { data:{id: data.detail.mata_uang_asal_id, text:data.detail.mata_uang_asal.nama}});
    				$("[name='konversi']").select2('trigger', 'select', { data:{id: data.detail.mata_uang_konversi_id, text:data.detail.mata_uang_konversi.nama}});
            modalKurs.modal('show');
  		    },
  	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
  	        	var err = XMLHttpRequest.responseJSON;
  		    }
	    });
	});
}

function initDelete(){
  tableKurs.on("click",".delete-kurs", function(){
    var id = $(this).data('id');
      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "ui/Kurs_admin/delete/"+id,
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}

function get_mata_uang_asal(){
    $("[name='mata_uang_asal']").select2({
        placeholder: '-- Pilih Mata Uang --',
        ajax: {
          url: admin_base_url+"ui/kurs_admin/get_mata_uang",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });
}

function get_konversi(){
    $("[name='konversi']").select2({
        placeholder: '-- Pilih Mata Uang --',
        ajax: {
          url: admin_base_url+"ui/kurs_admin/get_mata_uang",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });
}