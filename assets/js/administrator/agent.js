var tableAgent = $('#table-agent'),
    modalAgent = $('#agent-modal'),
    formAgent  = $('#form-agent');

$(function(){
  $('.date-picker').datepicker({
      autoclose: true,
      format:'yyyy-mm-dd'
  });

	initDataTables();
  initValidation();
  initAdd();
  initEdit();
  initDelete();
});

function initDataTables(){
   tableAgent.DataTable({
        "order": [],
        "columns": [
            { "data": "id", "name": "no", "title": "No",
              "render": function ( data, type, full, meta ) {
                return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            { "data": "nama", "name": "nama", "title": "Nama" },
            { "data": "email", "name": "email", "title": "Email" },
            { "data": "ktp", "name": "ktp", "title": "No KTP", "sortable":false },
            { "data": "paspor", "name": "paspor", "title": "Paspor", "sortable":false },
            { "data": "promo_code", "name": "promo_code", "title": "Promo Code", "sortable":false},
            { "data": "status", "name": "status", "title": "Status", "sortable":false,
              "render": function ( data, type, full, meta ) {
                if (data == 1) {
                  return '<div class="btn btn-circle btn-sm blue"> Active </div>';
                }else{
                  return '<div class="btn btn-circle btn-sm red"> Inactive </div>';
                }
              } 
            },
            { "data": "id", "name": "Aksi", "title": "Aksi", "sortable":false,
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="edit-agent"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
                '<a data-id="'+data+'" class="delete-agent"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>';
              } 
            }
        ],

        "ajax": {
            "url": admin_base_url+"ui/Agent_admin/get_list",
            "type": "POST"
        },
        "serverSide":true
    });
}

function initValidation(){
    var validator = formAgent.validate({
        rules: {
          email: {
            required: true,
            remote: {
              url: admin_base_url + "ui/Agent_admin/check_email",
              type: 'post',
              dataType: 'json',
              data: {
                email: function(){
                  return $('[name="email"]').val();
                }
              }
            },
            email: true
          },
          nama_depan: {
            required: true
          },
          tempat_lahir: {
            required: true
          },
          tanggal_lahir: {
            required: true
          },
          ktp: {
            required: true
          }
        },
        messages: {
          email:{
            required: "Bidang isian nilai tidak boleh kosong",
            remote: "Email sudah terdaftar pada akun lain",
            email : "Masukan format email yang benar"
          },
           nama_depan: {
            required: "Bidang isian nilai tidak boleh kosong"
          },
          tempat_lahir: {
            required: "Bidang isian nilai tidak boleh kosong"
          },
          tanggal_lahir: {
            required: "Bidang isian nilai tidak boleh kosong"
          },
          ktp: {
            required: "Bidang isian nilai tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
          if (element.attr('name') == "tanggal_lahir") {
            element.parent().parent().append(error);
          }else{
            element.parent().append(error);
          }
          
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
  var validator = formAgent.validate();
  if(validator.form()){
    $('#submit').attr('disabled', true);
    if($('[name="status"]').is(":checked")){
      $('[name="status"]').val(1);
    }else{
      $('[name="status"]').val(0);
    }

    var arrayData = formAgent.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.nilai] = item.value;
    });

    $.ajax({ 
        type: "POST",
          dataType: 'json',
          data: arrayData,
          url: admin_base_url + "ui/Agent_admin/save",
          success: function(data){
            window.location.href = data.url;   
            $('#submit').attr('disabled', false);  
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
            $('#submit').attr('disabled', false);
        }
      });
  }
}

function initAdd(){
  $('#add-agent').on("click", function(){
    $("#agent-modal").find('form')[0].reset();
    $('[name="id"]').val(0);
    formAgent.validate().resetForm();
    formAgent.find(".form-group").removeClass("has-error");
    formAgent.find(".form-group").removeClass("has-success");
    //formAgent.find('input[name=email]').attr('disabled', false);
    $('#agent-modal').modal('show');
  });
}

function initEdit(){
  tableAgent.on("click",".edit-agent", function(){
    formAgent.validate().resetForm();
    //form.find('.alert').hide();
    formAgent.find(".form-group").removeClass("has-error");
    formAgent.find(".form-group").removeClass("has-success");

    var id = $(this).data('id');
    $.ajax({ 
        type: "GET",
          dataType: 'json',
          data: {id: id},
          url: admin_base_url + "ui/Agent_admin/edit",
          success: function(data){
            formAgent.find('input[name=id]').val(data.detail.id);
            formAgent.find('input[name=nama_depan]').val(data.detail.profile.nama_depan);
            formAgent.find('input[name=nama_tengah]').val(data.detail.profile.nama_tengah);
            formAgent.find('input[name=nama_belakang]').val(data.detail.profile.nama_belakang);
            formAgent.find('input[name=email]').val(data.detail.email);
            //formAgent.find('input[name=email]').attr('disabled', true);
            formAgent.find('input[name=tempat_lahir]').val(data.detail.profile.tempat_lahir);
            formAgent.find('input[name=ktp]').val(data.detail.profile.ktp);
            formAgent.find('input[name=paspor]').val(data.detail.profile.paspor);
            if (data.detail.agent.is_active == 1) {
              formAgent.find('input[name=status]').attr('checked', true);
            }else{
              formAgent.find('input[name=status]').attr('checked', true);
            }
            $('#tanggal_lahir').datepicker('setDate', data.detail.profile.tanggal_lahir);
            modalAgent.modal('show');
          },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
              var err = XMLHttpRequest.responseJSON;
          }
      });
  });
}

function initDelete(){
  tableAgent.on("click",".delete-agent", function(){
    var id = $(this).data('id');
      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "ui/Agent_admin/delete/"+id,
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}

