var table = $('#table-penerbangan'),
	form = $('#form-penerbangan'),
	modal = $('#modal-penerbangan'),
  currentDate, // Holds the day clicked when adding a new event
  currentEvent;

$(function(){
  $('.date-picker').datepicker({format : 'yyyy-mm-dd',autoclose: true});
  $('.timepicker-24').timepicker({ 
    minuteStep: 1,
    showMeridian: false,
    autoclose: true,
    showInputs : false,
    defaultTime: '0:00'});
  // handle input group button click
  $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function(e){
      e.preventDefault();
      $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
  });
  $( document ).scroll(function(){
      $('#form-penerbangan .date-picker').datepicker('place'); //#modal is the id of the modal
  });
  // Workaround to fix timepicker position on window scroll
  $( document ).scroll(function(){
      $('.timepicker-24').timepicker('place'); //#modal is the id of the modal
  });
  $("#tanggal_berangkat").change(function(){
      var Value=$('#tanggal_berangkat').find("input").val();
      $("#save_tanggal_mulai").datepicker('setDate', Value);
      $("#save_tanggal_selsai").datepicker('setDate', Value);
      $("#save_tanggal_mulai").datepicker('setStartDate', Value);
      $("#save_tanggal_selsai").datepicker('setStartDate', Value);
    });
  $("#jam_berangkat").change(function(){
      $('[name="jam_berangkat"]').valid();
    });
    $("#jam_tiba").change(function(){
      $('[name="jam_tiba"]').valid();
    });
  $('#calendarPenerbangan').fullCalendar( 'destroy' );
    $('#calendarPenerbangan').fullCalendar(
      {
        header: {
            left: 'prev,next today',
            center: 'title',
            right: null
          },
          timeFormat: 'H(:mm)',
          nextDayThreshold: "00:00:00",
          defaultDate: new Date(),
          selectable:true,
          selectHelper: true,
          eventLimit: 2, // If you set a number it will hide the itens
          displayEventTime: false,
          events: admin_base_url + "ui/penerbangan_admin/getPenerbanganAll/",
        eventRender: function(events, element) {
             var a = parseInt(events.harga);
             var harga = a.toLocaleString();
            element.find(".fc-content").prepend("<img src='"+base_url+"assets/images/maskapai/"+events.logo+"' width='28' height='28' class='pull-left gap-right' style='margin-right: 3px'/>"+events.bandara_berangkat_kode+"<right style='margin-right: 3px'/><img src='"+base_url+"assets/images/icon-plane.png' width='10'/><left style='margin-left: 3px'/></left>"+events.bandara_tujuan_kode+"<br>"+events.kode_mata_uang+harga+"<br><div class='col-md-12 pull-right' style='padding: 0px !important'><i class='fa fa-pencil pull-right' title='Edit'></i></div>");
            element.find(".fc-content").css('width', events.width);
            element.find(".fc-title").remove();
        },
        select: function(start, end) {
           currentDate = start.format();
            if (end.hasTime()) {
            }
            else {
                end.subtract(1, 'days');
            }
            modalPenerbangan({
                // Available buttons when adding
                buttons: {
                    add: {
                        id: 'add-penerbangan', // Buttons id
                        css: 'btn-success', // Buttons class
                        label: 'Save',
                        onclick: 'submitForm()'
                    }
                },

                tanggal_berangkat : start.format(),
                end : end.format(),
                title: 'Tambah Penerbangan',
                //showFormMaskapai : true
            });
        },       
        dayClick: function(date, jsEvent, view) {
            currentDate = date.format();
            $(".fc-content").popover({
              width: '95px',
          });
            modalPenerbangan({
                // Available buttons when adding
                buttons: {
                    add: {
                        id: 'add-penerbangan', // Buttons id
                        css: 'btn-success', // Buttons class
                        label: 'Save',
                        onclick: 'submitForm()'
                    }
                },
                tanggal_berangkat : date.format(),
                title: 'Tambah Penerbangan',
                //showFormMaskapai : true
            });
        },
        eventClick: function(calEvent, jsEvent, view) {
            // Set currentEvent variable according to the event clicked in the calendar
            currentEvent = calEvent;
            // Open modal to edit or delete event
            modalPenerbangan({
                // Available buttons when editing
                buttons: {
                    delete: {
                        id: 'delete-penerbangan',
                        css: 'btn-danger',
                        label: 'Delete',
                        onclick: 'initDelete(' + calEvent.id + ')'

                    },
                    update: {
                        id: 'update-penerbangan',
                        css: 'btn-success',
                        label: 'Update',
                        onclick: 'submitForm(' + calEvent.id + ')'
                    }
                },
                title: 'Ubah Penerbangan',
                //showFormMaskapai : true,
                event: calEvent
            });
        }
        
          
      });
    initValidation();
    initDelete();
    modalPenerbangan();
    initSelect2();
    selectBandara();
    $("#bandara_berangkat_id").change(function(){
      var value=$(this).val();
      if(value){
         $('[name="bandara_tujuan_id"] option').show();
         $('[name="bandara_tujuan_id"] option[value='+value+']').hide();
      }
    });
});
function initSelect2(){
  $('.select2').select2({
      placeholder: '-Pilih Maskapai-',
      // minimumInputLength: 1,
      // ajax: {
      //   url: admin_base_url + "ui/penerbangan_admin/search_maskapai",
      //   dataType: 'json',
      //   delay: 250,
      //   processResults: function (data) {
      //     return {
      //       results: data
      //     };
      //   },
      //   cache: true
      // }
    } 
  );
}
function selectBandara(){
  $('.selectBandara').select2({
      placeholder: 'Nama Bandara',
    }
  );

}

function filterMaskapai(){
    var id = $('[name="maskapai_filter"]').val();
    if (id == 'all') {
      var url = admin_base_url + "ui/penerbangan_admin/getPenerbanganAll/";
      //var showFormMaskapai = true;
    }else{
    //var showFormMaskapai = false;
     var url = admin_base_url + "ui/penerbangan_admin/getPenerbangan/"+id;
    }
    $('#calendarPenerbangan').fullCalendar( 'destroy' );
    $('#calendarPenerbangan').fullCalendar(
      {
        header: {
            left: 'prev,next today',
            center: 'title',
            right: null
          },
          defaultDate: new Date(),
          selectable:true,
          selectHelper: true,
          nextDayThreshold: "00:00:00",
          timeFormat: 'H(:mm)',
          eventLimit: 2, // If you set a number it will hide the itens
          displayEventTime: false,
          events: url,
        eventRender: function(events, element) {
            var a = parseInt(events.harga);
            var harga = a.toLocaleString();        
            element.find(".fc-content").prepend("<img src='"+base_url+"assets/images/maskapai/"+events.logo+"' width='28' height='28' class='pull-left gap-right' style='margin-right: 3px'/>"+events.bandara_berangkat_kode+"<right style='margin-right: 1px'/></right><img src='"+base_url+"assets/images/icon-plane.png' width='10'/><left style='margin-left: 1px'/></left>"+events.bandara_tujuan_kode+"<br>"+events.kode_mata_uang+harga+"<br><div class='col-md-12 pull-right' style='padding: 0px !important'><i class='fa fa-pencil pull-right' title='Edit'></i></div>");
            element.find(".fc-content").css('width', events.width);
            element.find(".fc-title").remove();
           
        },
        select: function(start, end) {
           currentDate = start.format();
            if (end.hasTime()) {
            }
            else {
                end.subtract(1, 'days');
            }
            modalPenerbangan({
                // Available buttons when adding
                buttons: {
                    add: {
                        id: 'add-penerbangan', // Buttons id
                        css: 'btn-success', // Buttons class
                        label: 'Save',
                        onclick: 'submitForm()'
                    }
                },

                tanggal_berangkat : start.format(),
                end : end.format(),
                maskapai_id : id,
                title: 'Tambah Penerbangan',
                //showFormMaskapai
            });
        },        
        dayClick: function(date, jsEvent, view) {
            currentDate = date.format();
            modalPenerbangan({
                // Available buttons when adding
                buttons: {
                    add: {
                        id: 'add-penerbangan', // Buttons id
                        css: 'btn-success', // Buttons class
                        label: 'Save',
                        onclick: 'submitForm()'
                    }
                },
                tanggal_berangkat : date.format(),
                maskapai_id : id,
                title: 'Tambah Penerbangan',
                //showFormMaskapai,
            });
        },
        eventClick: function(calEvent, jsEvent, view) {
            // Set currentEvent variable according to the event clicked in the calendar
            currentEvent = calEvent;
            // Open modal to edit or delete event
            modalPenerbangan({
                // Available buttons when editing
                buttons: {
                    delete: {
                        id: 'delete-penerbangan',
                        css: 'btn-danger',
                        label: 'Delete',
                        onclick: 'initDelete(' + calEvent.id + ')'

                    },
                    update: {
                        id: 'update-penerbangan',
                        css: 'btn-success',
                        label: 'Update',
                        onclick: 'submitForm(' + calEvent.id + ')'
                    }
                },
                title: 'Ubah Penerbangan',
                event: calEvent,
                //showFormMaskapai,
            });
        }
        
          
      });
}

function initValidation(){
   $.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
  }, "Value must not equal arg.");
  var validator = form.validate({
        rules: {
          tanggal_tiba: {
            required: true
          },
          maskapai_ids: {
            required: true
          },
          bandara_berangkat_id: {
            required: true
          },
          bandara_tujuan_id: {
            required: true
          },
          
          tanggal_berangkat: {
            required: true
          },
          jam_berangkat: {
            valueNotEquals: "0:00"
          },
          jam_tiba: {
            valueNotEquals: "0:00"
          },
          harga: {
            required: true
          },
          mata_uang_id: {
            required: true
          }
        },
        messages: {
          maskapai_ids: {
            required: "Bidang isian nama maskapai tidak boleh kosong"
          },
          bandara_berangkat_id: {
            required: "Bidang isian bandara berangkat tidak boleh kosong"
          },
          bandara_tujuan_id: {
            required: "Bidang isian bandara tujuan tidak boleh kosong"
          },
          tanggal_berangkat: {
            required: "Bidang isian tanggal berangkat tidak boleh kosong"
          },
          tanggal_tiba: {
            required: "Bidang isian tanggal tiba tidak boleh kosong"
          },
          jam_berangkat: {
            valueNotEquals: "Bidang isian jam berangkat tidak boleh kosong"
          },
          jam_tiba: {
            valueNotEquals: "Bidang isian jam tiba tidak boleh kosong"
          },
          harga: {
            required: "Bidang isian harga tidak boleh kosong"
          },
          mata_uang_id: {
            required: "Bidang isian mata uang tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
            if (element.attr("name") == "tanggal_berangkat" || element.attr("name") == "jam_berangkat" || element.attr("name") == "jam_tiba"){
              element.parent().parent().append(error);
            }else{
              element.parent().append(error);
            }
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(id){
  var validator = form.validate();
    if(validator.form()){
        $("#update-penerbangan").attr("disabled", true);
        $("#add-penerbangan").attr("disabled", true);
        $("#delete-penerbangan").attr("disabled", true);
    	var arrayData = form.serializeArray();
        var jsonData = {};
        if (id) {
          var  u = admin_base_url + "ui/penerbangan_admin/save/"+id;
        }else{
          var  u = admin_base_url + "ui/penerbangan_admin/save/"+0;
        }
        $.each(arrayData, function(index, item) {
            jsonData[item.name] = item.value;
        });
    	$.ajax({ 
        	type: "POST",
          	dataType: 'json',
          	data: arrayData,
            url : u,
          	success: function(data){
              modal.modal('hide');
              bootbox.alert("Data berhasil disimpan.", function(){
              });
              $('#calendarPenerbangan').fullCalendar("refetchEvents");   
    	    },
          	error: function(XMLHttpRequest, textStatus, errorThrown) {
            	var err = XMLHttpRequest.responseJSON;
    	    }
        });
    }
}

function modalPenerbangan(data){
		if (data) {
      // if (data.showFormMaskapai == true) {
      //   form.find('#formMaskapai').show();
      // }else{
      //   form.find('#formMaskapai').hide();
      // }
      form.trigger("reset");
      form.get(0).reset();
      form.data('changed', 0);
      form.validate().resetForm();
      form.find('.alert').hide();
      form.find(".form-group").removeClass("has-error");
      form.find(".form-group").removeClass("has-success");
      form.find('input[name=id]').val(0);
      $('#bandara_berangkat_id option:selected').text('-Pilih Bandara Berangkat-').val('');
      $('#bandara_tujuan_id option:selected').text('-Pilih Bandara Tujuan-').val('');
      $('#mata_uang_id option:selected').text('-Pilih Mata Uang-').val('');
      form.find('input[name=id]').val("");
      form.find('input[name=maskapai_id]').val("");
      form.find('input[name=harga]').val("");
      form.find('input[name=is_low_season]').prop('checked', false);
      form.find('input[name=tanggal_berangkat]').val("");
      form.find('input[name=tanggal_tiba]').val("");
      form.find('input[name=save_tanggal_mulai]').val("");
      form.find('input[name=save_tanggal_selsai]').val("");
      $(".timepicker-24").timepicker('setTime', '0:00');
      form.find('input[name=jam_berangkat]').val("0:00");
      form.find('input[name=jam_tiba]').val("0:00");
      $('.modal-footer button:not(".btn-default")').remove();

      if (data.event) {
        $('#form_tanggal_simpan').hide();
        //$('#tanggal_tiba').datepicker('setStartDate',data.event.tanggal_berangkat);
        //$("#tanggal_berangkat").datepicker('setDate', data.event.tanggal_berangkat);
        $("#tanggal_tiba").datepicker('setDate', data.event.tanggal_tiba);
        form.find('input[name=id]').val(data.event.id);
        //form.find('input[name=maskapai_id]').val(data.event.maskapai_id);
        form.find('input[name=tanggal_berangkat]').val(data.event.tanggal_berangkat);
        form.find('input[name=tanggal_tiba]').val(data.event.tanggal_tiba);
        form.find('input[name=jam_berangkat]').val(data.event.jam_berangkat);
        form.find('input[name=jam_tiba]').val(data.event.jam_tiba);
        form.find('input[name=harga]').val(data.event.harga);
        $("#jam_berangkat").timepicker('setTime', data.event.jam_berangkat);
        $("#jam_tiba").timepicker('setTime', data.event.jam_tiba);
        $('#maskapai_ids').val(data.event.maskapai_id);
        $('#bandara_berangkat_id').val(data.event.bandara_berangkat_id);
        $('#bandara_tujuan_id').val(data.event.bandara_tujuan_id);
        $('#mata_uang_id').val(data.event.mata_uang_id);
        if(data.event.is_low_season == 1){
          form.find('input[name=is_low_season]').prop('checked', true); 
        }else{
          form.find('input[name=is_low_season]').prop('checked', false);  
        }
        if(data.event.dua_hari == 1){
          form.find('input[name=dua_hari]').prop('checked', true); 
        }else{
          form.find('input[name=dua_hari]').prop('checked', false);  
        }
      }else{
        $('#form_tanggal_simpan').show();
        $('#save_tanggal_mulai').datepicker('setStartDate',data.tanggal_berangkat);
        $('#save_tanggal_selsai').datepicker('setStartDate',data.tanggal_berangkat);
        //$("#tanggal_tiba").datepicker('setDate', data.tanggal_berangkat);
        $("#tanggal_berangkat").datepicker('setDate', data.tanggal_berangkat);
        $("#save_tanggal_mulai").datepicker('setDate', data.tanggal_berangkat);
        $("#save_tanggal_selsai").datepicker('setDate', data.end);
        form.find('input[name=save_tanggal_mulai]').val(data.tanggal_berangkat);
        form.find('input[name=save_tanggal_selsai]').val(data.end);
        form.find('input[name=tanggal_berangkat]').val(data.tanggal_berangkat);
        if (data.maskapai_id && data.maskapai_id != 'all') {
          $('#maskapai_ids').val(data.maskapai_id);
        }
        //form.find('input[name=maskapai_id]').val(data.maskapai_id);
      }
      // console.log(data.buttons.length);
      // for (var i =  0; i < data.buttons.length; i++) {
      //   $('.modal-footer').prepend('<button type="button" onclick="' + data.buttons[i].onclick  + '" id="' + data.buttons[i].id  + '" class="btn ' + data.buttons[i].css + '">' + data.buttosn[i].label + '</button>')
      // console.log(data.buttons[i].onclick);
      // }
      $.each(data.buttons, function(index, button){
            $('.modal-footer').prepend('<button type="button" onclick="' + button.onclick  + '" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</button>')
        })
      $('.modal-title').html(data.title);
      modal.modal('show');
    }
		   
}

function initDelete(id){
      if (id) {
        modal.modal('hide');
        bootbox.confirm({
          message: "Apakah Anda yakin akan menghapus data ini?",
          buttons: {
              confirm: {
                  label: 'Hapus',
                  className: 'btn-danger'
              },
              cancel: {
                  label: 'Batal',
                  className: 'btn-default'
              }
          },
          callback: function (result) {
            if(result == true){
              $.ajax({ 
                type: "GET",
                  dataType: 'json',
                  url: admin_base_url + "ui/penerbangan_admin/delete/"+id,
                  success: function(data){
                    modal.modal('hide');
                    bootbox.alert("Data berhasil dihapus.", function(){
                    });
                    $('#calendarPenerbangan').fullCalendar("refetchEvents"); 
                  },
                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                    var err = XMLHttpRequest.responseJSON;
                  }
              });
            }
          }
        });
      }
}
