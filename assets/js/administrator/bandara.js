$(function(){
  initDelete();
});
$(document).ready(function(){
	$('#datatable').DataTable();

	$('#add-bandara').on("click", function(){
		console.log('add');
		$('#bandara-modal').modal({
			remote: base_url+'administrator/ui/bandara_admin/add'
		});
	});
	
	$('#datatable').on("click", '.edit-bandara', function(){
		console.log('edit');
		var id = $(this).attr('data-id');
		$('#bandara-modal').modal({
			remote: base_url+'administrator/ui/bandara_admin/edit/'+id
		});
	});
	$('body').on('hidden.bs.modal', '.modal', function () {
	  $(this).removeData('bs.modal');
	});
	// $('#bandara-form').bootstrapValidator({
 //        framework: 'bootstrap',
 //        icon: {
 //            valid: 'glyphicon glyphicon-ok',
 //            invalid: 'glyphicon glyphicon-remove',
 //            validating: 'glyphicon glyphicon-refresh'
 //        },
 //        fields: {
 //            nama: {
 //                validators: {
 //                    notEmpty: {
 //                        message: 'Nama harus diisi'
 //                    }
 //                }
 //            }
 //        }
 //    });
});

/*delete confirmation*/
function initDelete() {
   $('#datatable').on("click",".delete-bandara", function(){
    var id = $(this).data('id');

      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data ini?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "ui/bandara_admin/delete",
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}