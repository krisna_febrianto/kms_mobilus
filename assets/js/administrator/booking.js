var tableBooking = $('#table-booking'),
    tableNewBooking = $('#table-new-booking'),
    modalBooking = $('#booking-modal'),
    formBooking = $('#form-booking'),
    table, tableNew;

$(function(){
  initDataTablesNew();
  initDataTables();
  initDetail();
  initUpdateStatus();

  var bookingId = $("#id-booking").val();
  if(bookingId > 0){
    openModalDetail(bookingId);
    // $.post(admin_base_url+"ui/Pre_book_admin/updateIsView");
  }
});

function initDataTablesNew(){
  tableNew = tableNewBooking.DataTable({
        "order": [],
        "columns": [
            { "data": "id", "name": "no", "title": "No",
              "render": function ( data, type, full, meta ) {
                return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            { "data": "no_booking", "name": "no_booking", "title": "No Booking" },
            { "data": "nama", "name": "nama", "title": "Nama" },
            { "data": "paket_umroh", "name": "paket_umroh", "title": "Paket Umroh" },
            { "data": "status_tag", "name": "status", "title": "Status",
              "render": function ( data, type, full, meta ) {
                return '<div class="label label-'+full.status_color+'">'+data+'</div>';
              }
            },
            { "data": "id", "name": "Aksi", "title": "Aksi", "sortable":false,
              "render": function ( data, type, full, meta ) {
                var content = '<a data-id="'+data+'" class="view-booking"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-search" title="Detail"></i> Detail</div></a>';
                // if(full.status_id == 1){
                //   content += '<a data-id="'+data+'" class="update-booking"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Ubah ke Paket"></i> Update Status</div></a>';
                // }

                return content;
              } 
            }
        ],

        "ajax": {
            "url": admin_base_url+"ui/Booking_admin/get_list_new",
            "type": "POST"
        },
        "serverSide":true
    });
}

function initDataTables(){
  table = tableBooking.DataTable({
        "order": [],
        "columns": [
            { "data": "id", "name": "no", "title": "No",
              "render": function ( data, type, full, meta ) {
                return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            { "data": "no_booking", "name": "no_booking", "title": "No Booking" },
            { "data": "nama", "name": "nama", "title": "Nama" },
            { "data": "paket_umroh", "name": "paket_umroh", "title": "Paket Umroh" },
            { "data": "status_tag", "name": "status", "title": "Status",
              "render": function ( data, type, full, meta ) {
                return '<div class="label label-'+full.status_color+'">'+data+'</div>';
              }
            },
            { "data": "id", "name": "Aksi", "title": "Aksi", "sortable":false,
              "render": function ( data, type, full, meta ) {
                var content = '<a data-id="'+data+'" class="view-booking"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-search" title="Detail"></i> Detail</div></a>';
                // if(full.status_id == 1){
                //   content += '<a data-id="'+data+'" class="update-booking"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Ubah ke Paket"></i> Update Status</div></a>';
                // }

                return content;
              } 
            }
        ],

        "ajax": {
            "url": admin_base_url+"ui/Booking_admin/get_list",
            "type": "POST"
        },
        "serverSide":true
    });

  if (table) {
    $.post(admin_base_url+"ui/Booking_admin/updateIsView");
  }
  
}

function openModalDetail(id){
    modalBooking.modal({
      remote: base_url+'administrator/ui/booking_admin/detail/'+id
    });
    // $.post(admin_base_url+"ui/Pre_book_admin/updateIsView");
}

// function reloadTable(){
//   table.ajax.reload(null,false);
//   tableNew.ajax.reload(null,false);
// }

function initDetail(){
  tableBooking.on('click','.view-booking', function(){
    var id = $(this).data('id');
    modalBooking.modal({
      remote: base_url+'administrator/ui/booking_admin/detail/'+id
    });
    //reloadTable();
  });

  tableNewBooking.on('click','.view-booking', function(){
    var id = $(this).data('id');
    modalBooking.modal({
      remote: base_url+'administrator/ui/booking_admin/detail/'+id
    });
    //reloadTable();
  });
}

function initUpdateStatus(){
  tableBooking.on('click','.update-booking', function(){
    var id = $(this).data('id');
    modalBooking.modal({
      remote: base_url+'administrator/ui/booking_admin/edit/'+id
    });
  });

  tableNewBooking.on('click','.update-booking', function(){
    var id = $(this).data('id');
    modalBooking.modal({
      remote: base_url+'administrator/ui/booking_admin/edit/'+id
    });
  });
}