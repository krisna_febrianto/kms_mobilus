var tablePreBook = $('#table-pre-book');
$(document).ready(function() {
    setInterval(init, 10000);
    //init();
});

function init(){
	// initNotif();
	// initNotifPreBook();
	// initNotifBooking();
	// initNotifKonfirmasi();
	// getLastDataPreBook();
	// getLastDataBooking();
	// getLastDataKonfirmasi();
}

function timeInterval(time){
	var now = new Date();
	var before = new Date(time)

	var second = (now.getTime() - before.getTime()) / 1000;
	var minute = (now.getTime() - before.getTime()) / 60000;
	var hour = (now.getTime() - before.getTime()) / 3600000;
	var day = (now.getTime() - before.getTime()) / 86400000;
	var week = (now.getTime() - before.getTime()) / 604800000;
	var month = (now.getTime() - before.getTime()) / 2419200000;
	var year = (now.getTime() - before.getTime()) / 29030400000;

	if (second <= 60) {
        newTime = second.toFixed(0)+' detik';
    } else if (minute <= 60) {
        newTime = minute.toFixed(0)+' menit';
    } else if (hour <= 24) {
        newTime = hour.toFixed(0)+' jam';
    } else if (day <= 7) {
        newTime = day.toFixed(0)+' hari';
    } else if (week <= 4) {
        newTime = week.toFixed(0)+' minggu';
    } else if (month <= 12) {
        newTime = month.toFixed(0)+' bulan';
    } else {
        newTime = year.toFixed(0)+' tahun';
    }

	return newTime;
}

function initNotif(){
	$.ajax({
	  url:admin_base_url + "ui/Pre_book_admin/getCountNotif",
	  method:"POST",
	  //data:{view:view},
	  dataType:"json",
	  success:function(data){
  		if (data != 0) {
  			$('#notif').html('<i class="icon-bell"></i><span class="badge badge-success"> '+data+' </span>');
  			$('#header-notif').html('<span class="bold">'+data+' pending</span> notifications');
  		}
	  }
	});
}

function initNotifPreBook(){
	$.ajax({
	  url:admin_base_url + "ui/Pre_book_admin/getCountPreBook",
	  method:"POST",
	  //data:{view:view},
	  dataType:"json",
	  success:function(data){
  		if (data != 0) {
  			$('#preBook').html('<i class="icon-list"></i><span class="title">Pre Book</span><span class="badge badge-success"> '+data+' </span>');
  		}
	  }
	});
}

function initNotifBooking(){
	$.ajax({
	  url:admin_base_url + "ui/Pre_book_admin/getCountBooking",
	  method:"POST",
	  //data:{view:view},
	  dataType:"json",
	  success:function(data){
  		if (data != 0) {
  			$('#booking').html('<span class="title">Booking</span><span class="badge badge-success"> '+data+' </span>');
  		}
	  }
	});
}

function initNotifKonfirmasi(){
	$.ajax({
	  url:admin_base_url + "ui/Pre_book_admin/getCountKonfirmasi",
	  method:"POST",
	  //data:{view:view},
	  dataType:"json",
	  success:function(data){
  		if (data != 0) {
  			$('#konfirmasi').html('<span class="title">Konfirmasi Pembayaran</span><span class="badge badge-success"> '+data+' </span>');
  		}
	  }
	});
}

function getDataNotif(){
	$('#dataNotif').empty();
	$.ajax({
	  url:admin_base_url + "ui/Pre_book_admin/getDataNotif",
	  method:"POST",
	  //data:{view:view},
	  dataType:"json",
	  success:function(data){
	  	var dates = data;
	  	var date_sort_desc = function (date1, date2) {
		  if (date1.created_at > date2.created_at) return -1;
		  if (date1.created_at < date2.created_at) return 1;
		  return 0;
		};
		dates.sort(date_sort_desc);

  		if (data) {
			for (var i = 0; i < data.length; i++) {
				if (data[i].jenis_id) {
					//var date = timeInterval(data[i].created_at);
					if (data[i].profile) {
						var pesan = data[i].profile.nama_depan+ ' ' +data[i].profile.nama_tengah+ ' ' +data[i].profile.nama_belakang + ' menambahkan prebook baru';
					} else{
						var pesan = 'Pre book baru telah ditambahkan';
					}
					$('#dataNotif').append('<li><a href="'+admin_base_url+'ui/Pre_book_admin/index/'+data[i].id+'"><span class="time"> '+timeInterval(data[i].created_at)+' </span><span class="details">'+
												'<span class="label label-sm label-icon label-success"><i class="fa fa-list"></i>'+
		                                        '</span> '+pesan+'</span>'+
		                                	'</a></li>');
				} else if (data[i].no_booking) {
					if (data[i].profile) {
						var pesan = data[i].profile.nama_depan+ ' ' +data[i].profile.nama_tengah+ ' ' +data[i].profile.nama_belakang + ' menambahkan booking baru';
					} else{
						var pesan = 'Booking baru telah diterima';
					}
					$('#dataNotif').append('<li><a href="'+admin_base_url+'ui/Booking_admin/index/'+data[i].id+'"><span class="time"> '+timeInterval(data[i].created_at)+' </span><span class="details">'+
												'<span class="label label-sm label-icon label-danger"><i class="fa fa-book"></i>'+
		                                        '</span>'+pesan+'</span>'+
		                                	'</a></li>');
				} else if (data[i].booking_id) {
					if (data[i].profile) {
						var pesan = data[i].profile.nama_depan+ ' ' +data[i].profile.nama_tengah+ ' ' +data[i].profile.nama_belakang + ' melakukan konfirmasi pembayaran';
					} else{
						var pesan = 'Konfirmasi pembayaran baru telah diterima';
					}
					$('#dataNotif').append('<li><a href="'+admin_base_url+'ui/Konfirmasi_pembayaran_admin"><span class="time"> '+timeInterval(data[i].created_at)+' </span><span class="details">'+
												'<span class="label label-sm label-icon label-danger"><i class="fa fa-book"></i>'+
		                                        '</span>'+pesan+'</span>'+
		                                	'</a></li>');
				}
			}
  		}else{
  			$('#dataNotif').append('<li><a href="javascript:;">'+
										'<span class="time"></span><span class="details">'+
											'<span class="label label-sm label-icon label-success">'+
                                        	'</span> Tidak ada pemberitahuan terbaru. '+
                                        '</span>'+
                                	'</a></li>');
  		}
	  }
	});
}

// function saveToLocal(id){
// 	localStorage.clear();
// 	localStorage.setItem("id", id);
// 	window.location.href = admin_base_url+"ui/Pre_book_admin";
// }

function getLastDataPreBook(){
	$.ajax({
	  url:admin_base_url + "ui/Pre_book_admin/getLastDataPreBook",
	  method:"POST",
	  //data:{view:view},
	  dataType:"json",
	  success:function(data){
  		if (data) {
  			if (localStorage.getItem("id_prebook")) {
  				if (data.id > localStorage.getItem("id_prebook")) {
  					notifyMePreBook(data);
  				}
  			}
  			//localStorage.clear("id_prebook");
  			localStorage.setItem("id_prebook", data.id);
  		}
	  }
	});
}

function getLastDataBooking(){
	$.ajax({
	  url:admin_base_url + "ui/Pre_book_admin/getLastDataBooking",
	  method:"POST",
	  //data:{view:view},
	  dataType:"json",
	  success:function(data){
  		if (data) {
  			if (localStorage.getItem("id_booking")) {
  				if (data.id > localStorage.getItem("id_booking")) {
  					notifyMeBooking(data);
  				}
  			}
  			//localStorage.clear("id_booking");
  			localStorage.setItem("id_booking", data.id);
  		}
	  }
	});
}

function getLastDataKonfirmasi(){
	$.ajax({
	  url:admin_base_url + "ui/Pre_book_admin/getLastDataKonfirmasi",
	  method:"POST",
	  //data:{view:view},
	  dataType:"json",
	  success:function(data){
  		if (data) {
  			if (localStorage.getItem("id_konfirmasi")) {
  				if (data.id > localStorage.getItem("id_konfirmasi")) {
  					notifyMeKonfirmasi(data);
  				}
  			}
  			//localStorage.clear("id_booking");
  			localStorage.setItem("id_konfirmasi", data.id);
  		}
	  }
	});
}

function notifyMePreBook(data){
	// Let's check if the browser supports notifications
	if (data.profile) {
		var pesan = data.profile.nama_depan+' '+data.profile.nama_tengah+' '+data.profile.nama_belakang+" menambahkan prebook baru";
	}else{
		var pesan = "Prebook baru telah ditambahkan";
	}

  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification(pesan,{icon: base_url+'assets/images/logo.png'});
  	notification.onclick = function () {
        window.location.href = admin_base_url+"ui/Pre_book_admin/index/"+data.id;      
    };
    //setTimeout(notification.close.bind(notification), 5000);
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== "denied") {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        var notification = new Notification(pesan,{icon: base_url+'assets/images/logo.png'});
      	notification.onclick = function () {
	        window.location.href = admin_base_url+"ui/Pre_book_admin/index/"+data.id;      
	    };
        //setTimeout(notification.close.bind(notification), 5000);
      }
    });
  }
}

function notifyMeBooking(data){
	if (data.profile) {
		var pesan = data.profile.nama_depan+' '+data.profile.nama_tengah+' '+data.profile.nama_belakang+" melakukan booking";
	}else{
		var pesan = "Booking baru telah diterima";
	}

  	if (!("Notification" in window)) {
    	alert("This browser does not support desktop notification");
  	} else if (Notification.permission === "granted") {
    	var notification = new Notification(pesan,{icon: base_url+'assets/images/logo.png'});
  		notification.onclick = function () {
        	window.location.href = admin_base_url+"ui/Booking_admin/index/"+data.id;      
    	};
    	//setTimeout(notification.close.bind(notification), 5000);
  	} else if (Notification.permission !== "denied") {
    	Notification.requestPermission(function (permission) {
	      	if (permission === "granted") {
	        	var notification = new Notification(pesan,{icon: base_url+'assets/images/logo.png'});
	      		notification.onclick = function () {
		        	window.location.href = admin_base_url+"ui/Booking_admin/index/"+data.id;      
		    	};
	        //setTimeout(notification.close.bind(notification), 5000);
	      	}
    	});
 	 }
}

function notifyMeKonfirmasi(data){

	if (data.profile) {
		var pesan = data.profile.nama_depan+' '+data.profile.nama_tengah+' '+data.profile.nama_belakang+" melakukan konfirmasi pembayaran";
	}else{
		var pesan = "Konfirmasi pembayaran baru telah diterima";
	}
	// Let's check if the browser supports notifications
  	if (!("Notification" in window)) {
    	alert("This browser does not support desktop notification");
  	} else if (Notification.permission === "granted") {
	    var notification = new Notification(pesan,{icon: base_url+'assets/images/logo.png'});
	  	notification.onclick = function () {
	        window.location.href = admin_base_url+"ui/Konfirmasi_pembayaran_admin";      
	    };
    	//setTimeout(notification.close.bind(notification), 5000);
  	} else if (Notification.permission !== "denied") {
	    Notification.requestPermission(function (permission) {
	      // If the user accepts, let's create a notification
	      if (permission === "granted") {
	        var notification = new Notification(pesan,{icon: base_url+'assets/images/logo.png'});
	      	notification.onclick = function () {
		        window.location.href = admin_base_url+"ui/Konfirmasi_pembayaran_admin";      
		    };
	        //setTimeout(notification.close.bind(notification), 5000);
	      }
	    });
  	}
}
