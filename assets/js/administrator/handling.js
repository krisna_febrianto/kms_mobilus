var tableHandling = $('#table-handling'),
	formHandling = $('#form-handling'),
	modalHandling = $('#modal-handling');

$(function(){
	initDataTables();
	initAdd();
	initValidation();
	initEdit();
  initDelete();
  addPrice();
  deletePrice();
  addCommas();
  initCloseBtn();
});
function addCommas(nStr,index)
{
  if (nStr) {
    $(index).val(parseInt(nStr.replace(/[.]/g, "")).toLocaleString());
  }
}
function disableAlpha(eL)
{
    $(eL).on('keypress', function (event) {
    var regex = new RegExp("^[0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
  });
}
function initDataTables(){
   tableHandling.DataTable({
        "order": [1,'ASC'],
        "columns": [
            { "data": "id", "name": "no", "title": "No",
              "render": function ( data, type, full, meta ) {
                return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            { "data": "nama", "name": "nama", "title": "Nama" },
            { "data": "deskripsi", "name": "deskripsi", "title": "Deskripsi" },
            { "data": "is_mandatory", "name": "is_mandatory", "title": "Mandatory",
              "render": function ( data, type, full, meta ) {
                return '<div class="label label-'+(data == 1 ? 'primary' : 'default')+'">'+(data == 1 ? 'Ya' : 'Tidak')+'</div>';
              }
            },
            { "data": "id", "name": "Aksi", "title": "Aksi", "sortable":false,
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="edit-handling"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
                '<a data-id="'+data+'" data-nama="'+full.nama+'" class="delete-handling"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>';
              } 
            }
        ],

        "ajax": {
            "url": admin_base_url+"ui/Handling_admin/get_list",
            "type": "POST"
        },
        "serverSide":true
    });
}

function initAdd(){
	$('#add-handling').on("click", function(){

		formHandling.find('input[name=id]').val(0);
		formHandling.find('input[name=nama]').val("");
		formHandling.find('textarea[name=deskripsi]').val("");
		formHandling.find('input[name=is_mandatory]').prop('checked', false);
    $('.list-price').html("");
    var content = generatePriceRow(0);
    $('.list-price').append(content);

		modalHandling.modal('show');
    initPriceAutocomplete();
	});
}

function initValidation(){
  var validator = formHandling.validate({
        rules: {
          nama: {
            required: true
          },
          deskripsi: {
            required: true
          },
          "range_from[]":{
            min:1,
          },
          "range_to[]":{
            min:1,
          }
        },
        messages: {
          nama: {
            required: "Bidang isian nama tidak boleh kosong"
          },
          deskripsi: {
            required: "Bidang isian deskripsi tidak boleh kosong"
          },
          "range_from[]": {
            min: "Bidang isian ini minimal input adalah 1"
          },
          "range_to[]": {
            min: "Bidang isian ini minimal input adalah 1"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );

        	if (element.attr("name") == "range_from[]" || element.attr("name") == "range_to[]"){
              element.parent().parent().append(error);
            }else{
              element.parent().append(error);
            }
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
	var arrayData = formHandling.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

	$.ajax({ 
    	type: "POST",
      	dataType: 'json',
      	data: arrayData,
      	url: admin_base_url + "ui/Handling_admin/save",
      	success: function(data){
	        window.location.href = data.url;     
	    },
      	error: function(XMLHttpRequest, textStatus, errorThrown) {
        	var err = XMLHttpRequest.responseJSON;
	    }
    });
}

function initEdit(){
	tableHandling.on("click",".edit-handling", function(){
		var id = $(this).data('id');
		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	data: {id: id},
	      	url: admin_base_url + "ui/Handling_admin/edit",
	      	success: function(data){
		        formHandling.find('input[name=id]').val(data.detail.id);
		        formHandling.find('input[name=nama]').val(data.detail.nama);
		        formHandling.find('textarea[name=deskripsi]').val(data.detail.deskripsi);
		        if(data.detail.is_mandatory == 1){
		        	formHandling.find('input[name=is_mandatory]').prop('checked', true);	
		        }else{
		        	formHandling.find('input[name=is_mandatory]').prop('checked', false);	
		        }


            $('.list-price').html("");
            if(data.detail.hargas != null){
              $.each(data.detail.hargas, function(index, item) {
                  var content = generatePriceRow(1, item);

                  $('.list-price').append(content);
              });

            }else{
              var content = generatePriceRow(0);
              $('.list-price').append(content);
            }
		        
		        modalHandling.modal('show');

            initPriceAutocomplete();
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
	});
}

function initDelete(){
  tableHandling.on("click",".delete-handling", function(){
    var id = $(this).data('id'),
        nama = $(this).data('nama');

      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data <b>"+nama+"</b>?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "ui/Handling_admin/delete",
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}

function addPrice(){
  modalHandling.on('click', '.add-price', function(){
      var content = generatePriceRow(0);

      $('.list-price').append(content);

      initPriceAutocomplete();
  });
}

function deletePrice(){
  modalHandling.on('click', '.delete-price', function(){
    var elem = $(this),
        id = $(this).parents('tr').find('input[name="price_id[]"]').val();
    if(id > 0){
      $(elem).parents('.list-price').append('<input type="hidden" name="deleted_price_id[]" value="'+id+'">')
    }
    $(this).parents('tr').remove();
  });
}

function generatePriceRow(is_edit,data){
  var content = '<tr>';
        content += '<input type="hidden" name="price_id[]" value="'+(is_edit == 1 ? data.id : 0)+'">';
        content +=  '<td>';

        content +=    '<div class="form-group"><div class="input-group">';
        content +=      '<input type="number" name="range_from[]" class="form-control price_range_from" value="'+(is_edit == 1 ? data.limit_bawah_peserta : '')+'">';
        content +=      '<span class="input-group-addon">sampai</span>';
        content +=      '<input type="number" name="range_to[]" class="form-control price_range_to" value="'+(is_edit == 1 ? (data.limit_atas_peserta == null ? '∞' : data.limit_atas_peserta) : '')+'">';
        content +=    '</div></div>';
        content +=  '</td>';
        content +=  '<td>';
        content +=      '<select name="currency[]" class="form-control">';
        for (var i = 0; i < mataUang.length; i++) {
        content +=          '<option value="'+mataUang[i].id+'" '+(is_edit == 1 ? (data.mata_uang_id == mataUang[i].id ? 'selected' : '') : '')+'>'+mataUang[i].symbol+'</option>';
        }       
        content +=      '</select>';
        content +=  '</td>';
        content +=  '<td>';
        content +=    '<input type="text" style="text-align: right;" name="price[]" onkeypress="disableAlpha(this)" onchange="addCommas(this.value,this)" class="form-control disableAlpha" value="'+(is_edit == 1 ? parseInt(data.harga).toLocaleString() : '')+'">';
        content +=  '</td>';
        content +=  '<td>';
        if($('.list-price').find('tr').length > 0){
        content +=      '<a data-id="1" class="delete-price"><div class="btn btn-outline btn-circle btn-sm red"><i class="fa fa-minus" title="Add"></i> Delete</div></a>';
        }
        content +=  '</td>';
        content += '</tr>';

    return content;
}

function initPriceAutocomplete(){
  $(".price_range_from").typeahead({
    source: rangeJmlJamaah,
    showHintOnFocus : "all",
    minLength:0
  });

  $(".price_range_to").typeahead({
    source: rangeJmlJamaahTo,
    showHintOnFocus : "all",
    minLength:0
  });
}
function initCloseBtn(){
  var $form = $('form'),
    origForm = $form.serialize();

  $('form :input').on('change input', function() {
      if($form.serialize() !== origForm){
        formHandling.data('changed', 1);
      }
  });

  $('.close-modal').on('click', function(e){
    e.preventDefault();
    if(formHandling.data('changed') == 1) {
      bootbox.confirm({
        message: "Apakah anda yakin akan menutup form ini? form akan direset ketika ditutup",
        buttons: {
            confirm: {
                label: 'Confirm',
                className: 'btn-primary'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            modalHandling.modal('hide');
          }else{
            modalHandling.modal('show');
          }
        }
      });
    }else{
      modalHandling.modal('hide');
    }
  });
}
