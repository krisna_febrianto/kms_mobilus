var tableMateri = $('#table-materi'),
	formMateri = $('#form-materi'),formSubMateri = $('#form-add-sub-materi'),
  modalSubMateri = $('#modal-sub-materi'),
  modalFormSubMateri = $('#modal-add-sub-materi'),
	modalMateri = $('#modal-materi');

$(function(){
	initDataTables();
	initAdd();
	initValidation();
  initViewSubMateri();
	initEdit();
  initDelete();
  FormRepeater.init();
  initValidationSub();
  initTinymce();

});
function initTinymce(){
  
  tinymce.init({
    selector: ".tinymce",theme: "modern",
    remove_script_host:false,
    relative_urls:false,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
         "table contextmenu directionality emoticons paste textcolor responsivefilemanager code codesample"
   ],
   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code codesample",
   image_advtab: true ,
   
   external_filemanager_path:base_url+"filemanager/",
   filemanager_title:"Responsive Filemanager" ,
   external_plugins: { "filemanager" : base_url+"filemanager/plugin.min.js"}
 });
}
// $(document).on('focusin', function(e) {
//     // alert();
//     if ($(event.target).closest(".mce-window").length) {
//         e.stopImmediatePropagation();
//     }
// });
function initDataTables(){
	tableMateri.DataTable();
}

function initAdd(){
	$('#add-materi').on("click", function(){
		formMateri.find('input[name=id]').val(0);
		formMateri.find('input[name=judul]').val("");
		formMateri.find('input[name=attachment]').val("");
    formMateri.find('input[name=deskripsi]').val("");
		// formMateri.find('input[name=is_mandatory]').prop('checked', false);
		modalMateri.modal('show');
	});
}

function initValidation(){
  var validator = formMateri.validate({
        rules: {
          judul: {
            required: true
          },
          attachment: {
            required: false
          }
        },
        messages: {
          judul: {
            required: "Bidang isian judul tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
        	element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function initValidationSub(){
  var validator = formSubMateri.validate({
        rules: {
          judul: {
            required: true
          },
          content_sub: {
            required: false
          }
        },
        messages: {
          judul: {
            required: "Bidang isian judul tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
          element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitFormSub();
        }
    });
}

function submitForm(){
  // tinymce.get(".tinymce").save();
  tinymce.triggerSave(true, true);
	var arrayData = new FormData(formMateri[0]);
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

  $.ajax({ 
      type: "POST",
        dataType: 'json',
        data: arrayData,
        url: admin_base_url + "materi_admin/save",
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function(data){
          window.location.href = data.url;     
      },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          bootbox.hideAll();
          var err = XMLHttpRequest.responseJSON;
          bootbox.alert('Failed to save data materi.');
      }
    });
}
function submitFormSub(){
  // tinymce.get(".tinymce").save();
  tinymce.triggerSave(true, true);
  var arrayData = new FormData(formSubMateri[0]);
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

  $.ajax({ 
      type: "POST",
        dataType: 'json',
        data: arrayData,
        url: admin_base_url + "materi_admin/save_sub",
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function(data){
          // window.location.href = data.url;   
          // initViewSubMateri(data);
          modalFormSubMateri.modal("hide");
          getListSub(data);
          // getListSub(data);
      },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          bootbox.hideAll();
          var err = XMLHttpRequest.responseJSON;
          bootbox.alert('Failed to save data materi.');
      }
    });
}

function initEdit(){
	tableMateri.on("click",".edit-materi", function(){
		var id = $(this).data('id');
		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	data: {id: id},
	      	url: admin_base_url + "materi_admin/edit",
	      	success: function(data){
		        formMateri.find('input[name=id]').val(data.detail.id);
		        formMateri.find('input[name=judul]').val(data.detail.judul);
            formMateri.find('[name=kategori_id]').val(data.detail.kategori_id);
            formMateri.find('[name=deskripsi]').val(data.detail.deskripsi);
		        // formMateri.find('input[name=attachment]').val(data.detail.attachment);
            if(data.detail.attachment){
              $(".materi-attachment").html("<center><a href='"+base_url+"assets/images/materi/"+data.detail.attachment+"'>Download Attachment</a></center>");
            }
		        // var myJson = '{"group-a":[{"input-grocery-list-item":"Apple","input-grocery-list-item-quantity":"3"}]}';
            // var myObj = JSON.parse(data.detail.sub_materi);
            $(".mt-repeater").hide();
             modalMateri.modal('show');
		       
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
	});
}
function deleteSub(id){
  // var id = $(this).data('id');

      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data ini?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "materi_admin/delete_sub",
                success: function(data){
                  getListSub(data);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
}
function initViewSubMateri(){
  tableMateri.on("click",".view-sub-materi", function(){
    var id = $(this).data('id');
    modalSubMateri.modal('show');
    getListSub(id);
    
  });
}
function initFormSubMateri(id,materi_id){
  console.log(materi_id);
  modalSubMateri.modal('show');
  formSubMateri.find('[name=id_materi]').val(materi_id);
  if(id == 0){
    modalFormSubMateri.modal('show');
  }else{
    $.ajax({ 
        type: "GET",
          dataType: 'json',
          data: {id: id},
          url: admin_base_url + "materi_admin/edit_sub",
          success: function(data){
            formSubMateri.find('input[name=id]').val(data.detail.id);
            formSubMateri.find('input[name=id_materi]').val(data.detail.id_materi);
            formSubMateri.find('input[name=judul]').val(data.detail.judul);
            // formSubMateri.find('[name=content_sub]').val(data.detail.content);
            tinymce.get('content_sub').setContent(data.detail.content);
            
            // var myJson = '{"group-a":[{"input-grocery-list-item":"Apple","input-grocery-list-item-quantity":"3"}]}';
            // var myObj = JSON.parse(data.detail.sub_materi);
             modalFormSubMateri.modal('show');
           
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
      });
  }
}
function getListSub(id){
  $.ajax({ 
    type: "GET",
      dataType: 'json',
      data: {id: id},
      url: admin_base_url + "materi_admin/edit",
      success: function(data){
        // formMateri.find('input[name=id]').val(data.detail.id);
        // formMateri.find('input[name=judul]').val(data.detail.judul);
        // formMateri.find('[name=kategori_id]').val(data.detail.kategori_id);
        // formMateri.find('[name=deskripsi]').val(data.detail.deskripsi);
        // formMateri.find('input[name=attachment]').val(data.detail.attachment);
        // if(data.detail.attachment){
        //   $(".materi-attachment").html("<center><a href='"+base_url+"assets/images/materi/"+data.detail.attachment+"'>Download Attachment</a></center>");
        // }
        // var myJson = '{"group-a":[{"input-grocery-list-item":"Apple","input-grocery-list-item-quantity":"3"}]}';
        // var myObj = JSON.parse(data.detail.sub_materi);
        // $(".mt-repeater").hide();
         var list = '';
         if(data.detail.sub_materi != null){
         for (var i = 0; i < data.detail.sub_materi.length; i++) {
           console.log(data.detail.sub_materi[i].judul);

           list+='<div class="col-md-12"><div class="portlet light bordered">'+
                                '<div class="portlet-title">'+
                                    '<div class="caption">'+
                                        '<i class="icon-equalizer font-dark hide"></i>'+
                                        '<span class="caption-subject font-dark">'+data.detail.sub_materi[i].judul+'</span>'+
                                    '</div>'+
                                    '<div class="tools">'+
                                        '<a class="collapse" data-original-title="" title=""> </a>'+
                                        '<a class="edit-sub" data-original-title="" title="" onclick="initFormSubMateri('+data.detail.sub_materi[i].id+','+data.detail.id+')"> <i class="fa fa-pencil" title="Edit"></i></a>'+
                                        '<a class="delete-sub" data-original-title="" title="" onclick="deleteSub('+data.detail.sub_materi[i].id+')"> <i class="fa fa-trash-o" title="delete"></i></a>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="portlet-body collapse">'+
                                    '<div class="row"><div class="col-md-12">'+
                                        data.detail.sub_materi[i].content+
                                    '</div></div>'+
                                '</div>'+
                            '</div></div>';
         };
        }
         list+="<button type='button' class='btn btn-success' onclick='initFormSubMateri(0,"+data.detail.id+")'>Tambah Sub Materi</button>";
         console.log(list);
         $(".sub-materi").html(list);
       
    },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        var err = XMLHttpRequest.responseJSON;
    }
  });
}
var FormRepeater = function() {
    return {
        init: function() {
            // alert();
            $(".mt-repeater").each(function() {

                $(this).repeater({
                    show: function() {
                        $(this).slideDown();
                        initTinymce();
                    },
                    hide: function(e) {
                        confirm("Are you sure you want to delete this element?") && $(this).slideUp(e)
                    },
                    ready: function(e) {}
                })
            })
        }
    }
}();
$.extend( $.validator.prototype, {
    checkForm: function () {
    this.prepareForm();
    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
    if (this.findByName(elements[i].name).length != undefined && this.findByName(elements[i].name).length > 1) {
    for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
    this.check(this.findByName(elements[i].name)[cnt]);
    }
    } else {
    this.check(elements[i]);
    }
    }
    return this.valid();
    }
});
function initDelete(){
  tableMateri.on("click",".delete-materi", function(){
    var id = $(this).data('id'),
        nama = $(this).data('nama');

      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data <b>"+nama+"</b>?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "materi_admin/delete",
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}
