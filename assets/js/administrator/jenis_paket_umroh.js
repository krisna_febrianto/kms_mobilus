var table = $('#datatable'),
	form = $('#form-jenis-paket-umroh'),
	modal = $('#modal-jenis-paket-umroh');

$(function(){
  table.DataTable();
	initValidation();
});
function checkPlus(checkbox)
{
    if (checkbox.checked)
    {
      $('#formPaketPlus').show();
    }else{
      $('#formPaketPlus').hide();
      $('#paket_plus_id').val("");
    }
}
function initAdd(){
     form.trigger("reset");
  $("#submit").attr("disabled", false);
      form.get(0).reset();
      form.data('changed', 0);
      form.validate().resetForm();
      form.find('.alert').hide();
      form.find(".form-group").removeClass("has-error");
      form.find(".form-group").removeClass("has-success");
		form.find('input[name=id]').val(0);
		modal.modal('show');
}

function initValidation(){
  var validator = form.validate({
        rules: {
          nama: {
            required: true
          },
          durasi: {
            required: true
          }
        },
        messages: {
          nama: {
            required: "Bidang isian nama tidak boleh kosong"
          },
          durasi: {
            required: "Bidang isian durasi tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            error.addClass( "help-block" );
            element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
  $("#submit").attr("disabled", true);
	var arrayData = form.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

	$.ajax({ 
    	type: "POST",
      	dataType: 'json',
      	data: arrayData,
      	url: admin_base_url + "ui/jenis_paket_umroh_admin/save",
      	success: function(data){
           window.location.href = data.url;
	    },
      	error: function(XMLHttpRequest, textStatus, errorThrown) {
        	var err = XMLHttpRequest.responseJSON;
	    }
    });
}

function initEdit(id){
		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	url: admin_base_url + "ui/jenis_paket_umroh_admin/edit/"+id,
	      	success: function(data){
          $("#submit").attr("disabled", false);
            form.trigger("reset");
            form.get(0).reset();
            form.data('changed', 0);
            form.validate().resetForm();
            form.find('.alert').hide();
            form.find(".form-group").removeClass("has-error");
            form.find(".form-group").removeClass("has-success");
		        form.find('input[name=id]').val(data.id);
		        form.find('input[name=nama]').val(data.nama);
		        $("textarea#deskripsi").val(data.deskripsi);
		        form.find('input[name=durasi]').val(data.durasi);
		        if(data.is_plus == 1){
		        	form.find('input[name=is_plus]').prop('checked', true);
              $('#paket_plus_id').val(data.paket_plus_id);
              $('#formPaketPlus').show();
		        }else{
		        	form.find('input[name=is_plus]').prop('checked', false);
              $('#formPaketPlus').hide();
		        }
		        modal.modal('show');
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
}

function initDelete(id){
      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data ini?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                url: admin_base_url + "ui/jenis_paket_umroh_admin/delete/"+id,
                success: function(data){
                   window.location.href = data.url;
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
}
