var table = $('#table'),
    modalAgent = $('#mata-uang-modal'),
    form  = $('#form-mata-uang');

$(function(){
	initDataTables();
  initValidation();
  initAdd();
  initEdit();
  initDelete();
});

function initDataTables(){
   table.DataTable({
        "order": [],
        "columns": [
            { "data": "id", "name": "no", "title": "No",
              "render": function ( data, type, full, meta ) {
                return meta.row + meta.settings._iDisplayStart + 1;
              }
            },
            { "data": "nama", "name": "nama", "title": "Nama" },
            { "data": "kode", "name": "kode", "title": "Kode" },
            { "data": "symbol", "name": "symbol", "title": "Symbol", "sortable":false },
            { "data": "id", "name": "Aksi", "title": "Aksi", "sortable":false,
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="edit-mata-uang"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
                '<a data-id="'+data+'" class="delete-mata-uang"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>';
              } 
            }
        ],

        "ajax": {
            "url": admin_base_url+"ui/Mata_uang_admin/get_list",
            "type": "POST"
        },
        "serverSide":true
    });
}

function initValidation(){
    var validator = form.validate({
        rules: {
          nama: {
            required: true,
          },
          kode: {
            required: true
          },
          symbol: {
            required: true
          }
        },
        messages: {
          nama:{
            required: "Bidang isian nilai tidak boleh kosong"
          },
           kode: {
            required: "Bidang isian nilai tidak boleh kosong"
          },
          symbol: {
            required: "Bidang isian nilai tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
          if (element.attr('name') == "tanggal_lahir") {
            element.parent().parent().append(error);
          }else{
            element.parent().append(error);
          }
          
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
  var validator = form.validate();
  if(validator.form()){
    $('#submit').attr('disabled', true);

    var arrayData = form.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.nilai] = item.value;
    });

    $.ajax({ 
        type: "POST",
          dataType: 'json',
          data: arrayData,
          url: admin_base_url + "ui/Mata_uang_admin/save",
          success: function(data){
            window.location.href = data.url;   
            $('#submit').attr('disabled', false);  
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
            $('#submit').attr('disabled', false);
        }
      });
  }
}

function initAdd(){
  $('#add-mata-uang').on("click", function(){
    $("#mata-uang-modal").find('form')[0].reset();
    $('[name="id"]').val(0);
    //form.validate().resetForm();
    form.find(".form-group").removeClass("has-error");
    form.find(".form-group").removeClass("has-success");
    $('#mata-uang-modal').modal('show');
  });
}

function initEdit(){
  table.on("click",".edit-mata-uang", function(){
    //form.validate().resetForm();
    //form.find('.alert').hide();
    $("#mata-uang-modal").find('form')[0].reset();
    form.find(".form-group").removeClass("has-error");
    form.find(".form-group").removeClass("has-success");

    var id = $(this).data('id');
    $.ajax({ 
        type: "GET",
          dataType: 'json',
          data: {id: id},
          url: admin_base_url + "ui/Mata_uang_admin/edit",
          success: function(data){
            form.find('input[name=id]').val(data.detail.id);
            form.find('input[name=nama]').val(data.detail.nama);
            form.find('input[name=kode]').val(data.detail.kode);
            form.find('input[name=symbol]').val(data.detail.symbol);
            modalAgent.modal('show');
          },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
              var err = XMLHttpRequest.responseJSON;
          }
      });
  });
}

function initDelete(){
  table.on("click",".delete-mata-uang", function(){
    var id = $(this).data('id');
      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "ui/Mata_uang_admin/delete/"+id,
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}

