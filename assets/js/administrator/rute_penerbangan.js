var r = $("#submit_form");
var form = $("#rute-penerbangan-form");
var table = $('#datatable');
var modal = $('#modal-rute-penerbangan');
var harga;
var durasi;
var tanggal_tiba_penerbangan1;
var jam_tiba_penerbangan1;
var id_berangkat1;
var id_berangkat2;
var id_pulang1;
var id_pulang2;
var id_plus1;
var id_plus2;
var harga_berangkat1 = 0;
var harga_berangkat2 = 0;
var harga_pulang1 = 0;
var harga_pulang2 = 0;
var harga_plus1 = 0;
var harga_plus2 = 0;
var mt_berangkat1 = 'undefined';
var mt_berangkat2  = 'undefined';
var mt_pulang1  = 'undefined';
var mt_pulang2  = 'undefined';
var mt_plus1  = 'undefined';
var mt_plus2  = 'undefined';
var temp_mt_berangkat1 = 'a';
var temp_mt_berangkat2  = 'a';
var temp_mt_pulang1  = 'a';
var temp_mt_pulang2  = 'a';
var temp_mt_plus1  = 'a';
var temp_mt_plus2  = 'a';
var av;
var is_edit;
var kurs = [];

$(document).ready(function(){
	$(".select2").select2({
    });
	$('.date-picker').datepicker({format : 'yyyy-mm-dd',autoclose: true});
	$( document ).scroll(function(){
      $('#form-penerbangan .date-picker').datepicker('place'); //#modal is the id of the modal
  	});
  	$("#jenis_paket_umroh").change(function(){
      $("[name='penerbangan_berangkat1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_berangkat2'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_pulang1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_pulang2'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_plus1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_plus2'] option").each(function() {
          $(this).remove();
      });
      is_edit = 0;
      null_harga();
      hitung_harga();
      $(this).find('form').trigger('reset');
      form.find('input[name=tanggal_berangkat]').val("");
      form.find('input[name=tanggal_berangkat_plus]').val("");
      var value=$(this).val();
      if(value){
         $.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	url: admin_base_url + "ui/rute_penerbangan_admin/get_paket_umroh/"+value,
	      	success: function(data){
	      		if (data.is_plus == 1) {
              $('#formPlus').show();
		        	$('#formTanggalPlus').show();
	      		}else{
              $('#formPlus').hide();
		        	$('#formTanggalPlus').hide();
	      		}
              durasi = data.durasi;
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
      }
    });


    $("#tanggal_berangkat").change(function(){
      $("[name='penerbangan_berangkat2'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_pulang1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_pulang2'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_plus1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_plus2'] option").each(function() {
          $(this).remove();
      });
      form.find('input[name=tanggal_berangkat_plus]').val("");
      is_edit = 0;
      null_harga();
      hitung_harga();
      var tanggal_berangkat=$('#tanggal_berangkat').find("input").val();
      if(tanggal_berangkat){
        get_berangkat1(tanggal_berangkat);
      }
    });


    $("[name='penerbangan_berangkat1']").change(function(){
      $("[name='penerbangan_pulang1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_pulang2'] option").each(function() {
          $(this).remove();
      });
      form.validate().resetForm();
      form.find('.alert').hide();
      form.find(".form-group").removeClass("has-error");
      form.find(".form-group").removeClass("has-success");
      null_harga();
      is_edit = 0;
      value=$(this).val();
      id_berangkat1 = value;
       $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url + "ui/rute_penerbangan_admin/get_penerbangan/"+value,
          success: function(data){
            mt_berangkat1 = data.mata_uang_id;
            harga_berangkat1= data.harga;
            hitung_harga();
            form.find('input[name=tanggal_berangkat_fix]').val(data.tanggal_berangkat);
            get_berangkat2(data.tanggal_tiba,data.jam_tiba,data.id);
            get_pulang1(data.tanggal_tiba);
            
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
      });
      hitung_harga();
    });


    $("[name='penerbangan_berangkat2']").change(function(){
      id_berangkat2=$(this).val();
      $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url + "ui/rute_penerbangan_admin/get_penerbangan/"+id_berangkat2,
          success: function(data){
            mt_berangkat2 = data.mata_uang_id;
            harga_berangkat2 = data.harga;
            hitung_harga();
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
    });
    });
    $("[name='penerbangan_pulang1']").change(function(){
      id_pulang1=$(this).val();
      is_edit = 0;
      form.find('.alert').hide();
      form.find(".form-group").removeClass("has-error");
      form.find(".form-group").removeClass("has-success");
      var value=$(this).val();
       $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url + "ui/rute_penerbangan_admin/get_penerbangan/"+value,
          success: function(data){
            form.find('input[name=tanggal_pulang]').val(data.tanggal_berangkat);
            form.find('input[name=tanggal_tiba]').val(data.tanggal_tiba);
            mt_pulang1 = data.mata_uang_id;
            harga_pulang1 = data.harga;
            hitung_harga();
            get_pulang2(data.tanggal_tiba,data.jam_tiba,data.id);
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
      });
    });
    $("[name='penerbangan_pulang2']").change(function(){
      id_pulang2=$(this).val();
      $.ajax({ 
          type: "GET",
            dataType: 'json',
            url: admin_base_url + "ui/rute_penerbangan_admin/get_penerbangan/"+id_pulang2,
            success: function(data){
              mt_pulang2 = data.mata_uang_id;
              harga_pulang2 = data.harga;
              hitung_harga();
          },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
              var err = XMLHttpRequest.responseJSON;
          }
      });
    });
       $("#tanggal_berangkat_plus").change(function(){
      var tanggal_berangkat_plus=$('#tanggal_berangkat_plus').find("input").val();
      if(tanggal_berangkat_plus){
        get_plus1(tanggal_berangkat_plus,id_berangkat1,id_berangkat2);
      }
    });
    $("[name='penerbangan_plus1']").change(function(){
      is_edit = 0;
      id_plus1=$(this).val();
       $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url + "ui/rute_penerbangan_admin/get_penerbangan/"+id_plus1,
          success: function(data){
            form.find('input[name=tanggal_plus]').val(data.tanggal_berangkat);
            mt_plus1 = data.mata_uang_id;
            harga_plus1 = data.harga;
            hitung_harga();
            get_plus2(data.tanggal_tiba,data.jam_tiba,data.id);
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
      });
    });

    $("[name='penerbangan_plus2']").change(function(){
      id_plus2=$(this).val();
      $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url + "ui/rute_penerbangan_admin/get_penerbangan/"+id_plus2,
          success: function(data){
            mt_plus2 = data.mata_uang_id;
            harga_plus2 = data.harga;
            hitung_harga();
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
    });
  });


	table.DataTable({
        "order": [11, "desc"],
        "columns": [
          { "data": "id", "name": "no", "bSearchable": false , 
            "render": function ( data, type, full, meta ) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
            { "data": "jenis_paket_umroh_nama", "width": "5%" },
            { "data": "tanggal_berangkat_format", "width": "5%" , "bSearchable": false  },
            { "data": "tampil_berangkat1", "width": "15%", 
            	"render": function ( data, type, full, meta ) {
            		return '<img src="'+base_url+'assets/images/maskapai/'+full.berangkat1_logo+'" width="30" class="pull-left gap-right" style="margin-right: 3px"/>'+" "+data
          		}
        	},
            { "data": "tampil_berangkat2", "width": "15%", "bSearchable": false,   
            	"render": function ( data, type, full, meta ) {
                if (data) {
                    return '<img src="'+base_url+'assets/images/maskapai/'+full.berangkat2_logo+'" width="30" class="pull-left gap-right" style="margin-right: 3px"/>'+" "+data
                }else{
                  return '<div class="label label-danger"><small>No Data</small></div>'
                }
          		}
        	},
          
            { "data": "tanggal_pulang_format", "width": "5%", "bSearchable": false   },
            { "data": "tampil_pulang1", "width": "15%", 
            	"render": function ( data, type, full, meta ) {
            		return '<img src="'+base_url+'assets/images/maskapai/'+full.pulang1_logo+'" width="30" class="pull-left gap-right" style="margin-right: 3px"/>'+" "+data
          		}
        	},
            { "data": "tampil_pulang2", "width": "15%", "bSearchable": false,
            	"render": function ( data, type, full, meta ) {
                if (data) {
              		return '<img src="'+base_url+'assets/images/maskapai/'+full.pulang2_logo+'" width="30" class="pull-left gap-right" style="margin-right: 3px"/>'+" "+data
            		 }else{
                    return '<div class="label label-danger"><small>No Data</small></div>'
                  }
              }
        	},
          { "data": "tanggal_berangkat_plus_format", "width": "5%", "bSearchable": false , 
            "render": function ( data, type, full, meta ) {
                if (data && data != '00-00-0000') {
                  return data
                 }else{
                    return '<div class="label label-danger"><small>No Data</small></div>'
                  }
              }

          },
            { "data": "tampil_plus1", "width": "15%", "bSearchable": false,
              "render": function ( data, type, full, meta ) {
                if (data && full.tampil_plus2) {
                  return '<img src="'+base_url+'assets/images/maskapai/'+full.plus1_logo+'" width="30" class="pull-left gap-right" style="margin-right: 3px"/>'+" "+data+" --> "+'<img src="'+base_url+'assets/images/maskapai/'+full.plus2_logo+'" width="30" class="pull-left gap-right" style="margin-right: 3px"/>'+" "+full.tampil_plus2
                 }else if(data){
                  return '<img src="'+base_url+'assets/images/maskapai/'+full.plus1_logo+'" width="30" class="pull-left gap-right" style="margin-right: 3px"/>'+" "+data
               }else{
                    return '<div class="label label-danger"><small>No Data</small></div>'
                  }
              }
          },
          //   { "data": "tampil_plus2", "width": "15%", 
          //     "render": function ( data, type, full, meta ) {
          //       if (data) {
          //         return '<img src="'+base_url+'assets/images/maskapai/'+full.plus2_logo+'" width="30" class="pull-left gap-right" style="margin-right: 3px"/>'+" "+data
          //        }else{
          //           return '<div class="label label-danger"><small>No Data</small></div>'
          //         }
          //     }
          // },
            //{ "data": "tanggal_tiba", "width": "5%", "bSearchable": false   },
            { "data": "harga", "width": "5%", "bSearchable": false   ,
              "render": function ( data, type, full, meta ) {
                if (data) {
                 return 'Rp. '+parseInt(data).toLocaleString();
                }
              }
            },
            { "data": "id", "name": "action", "width": "5%","bSearchable": false,   
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="edit-rute-penerbangan"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
                '<a href="#" data-id="'+data+'" onclick="initDelete(this);"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>';
            } 
          }
            //repeat for each of my 20 or so fields
        ],

        "ajax": {
            "url": admin_base_url+"ui/Rute_penerbangan_admin/get_datatables",
            "type": "POST"
        },
        "serverSide":true
        // "processing":true
    });

	$('body').on('hidden.bs.modal', '.modal', function () {

      $('.nav-pills a:first').tab('show');
	  $(this).removeData('bs.modal');
      $(this).find('form').trigger('reset');
      //r.validate().resetForm();
      $(this).find('.alert').hide();
      $(this).find(".form-group").removeClass("has-error");
      $(this).find(".form-group").removeClass("has-success");
      $("[name='penerbangan_berangkat1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_berangkat2'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_pulang1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_pulang2'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_plus1'] option").each(function() {
          $(this).remove();
      });
      $("[name='penerbangan_plus2'] option").each(function() {
          $(this).remove();
      });
      $('#formPlus').hide();
      $('#formTanggalPlus').hide();
      form.validate().resetForm();
      form.find('.alert').hide();
      form.find(".form-group").removeClass("has-error");
      form.find(".form-group").removeClass("has-success");
      null_harga();
    });
    modal.on('hide.bs.modal', function () {
    });
    
});
$(function(){
  initValidation();
  get_kurs();
  initEdit();
});
function add(){
      console.log('add');
      is_edit = 0;
      modal.find('form')[0].reset();
      modal.modal('show');
  };
function get_kurs(){
      $.ajax({ 
      type: "GET",
        dataType: 'json',
        url: admin_base_url+"ui/rute_penerbangan_admin/get_kurs/",
        success: function(data){
          kurs = data;
      },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          var err = XMLHttpRequest.responseJSON;
      }

    });

}
function null_harga(){
  harga_berangkat1 = 0;
  harga_berangkat2 = 0;
  harga_pulang1 = 0;
  harga_pulang2 = 0;
  harga_plus1 = 0;
  harga_plus2 = 0;
  id_berangkat1 = 'undefined';
  id_berangkat2 = 'undefined';
  id_pulang1 = 'undefined';
  id_pulang2 = 'undefined';
  id_plus1 = 'undefined';
  id_plus2 = 'undefined';
  mt_berangkat1 = 'undefined';
  mt_berangkat2  = 'undefined';
  mt_pulang1  = 'undefined';
  mt_pulang2  = 'undefined';
  mt_plus1  = 'undefined';
  mt_plus2  = 'undefined';
  temp_mt_berangkat1 = 'a';
  temp_mt_berangkat2  = 'a';
  temp_mt_pulang1  = 'a';
  temp_mt_pulang2  = 'a';
  temp_mt_plus1  = 'a';
  temp_mt_plus2  = 'a';
}
function hitung_harga(){
  if (mt_berangkat1 != 1 && mt_berangkat1 != 'undefined' && temp_mt_berangkat1 != id_berangkat1) {
    for (var i = 0; i < kurs.length; i++) {
        if (kurs[i].mata_uang_asal_id == mt_berangkat1) {
          harga_berangkat1 = harga_berangkat1*kurs[i].nilai;
          temp_mt_berangkat1 = id_berangkat1;
        }
    }
  }
  if (mt_berangkat2 != 1 && mt_berangkat2 != 'undefined' && temp_mt_berangkat2 != id_berangkat2) {
    for (var i = 0; i < kurs.length; i++) {
        if (kurs[i].mata_uang_asal_id == mt_berangkat2) {
          harga_berangkat2 = harga_berangkat2*kurs[i].nilai;
          temp_mt_berangkat2 = id_berangkat2;
        }
    }
  }
  if (mt_pulang1 != 1 && mt_pulang1 != 'undefined' && temp_mt_pulang1 != id_pulang1) {
    for (var i = 0; i < kurs.length; i++) {
        if (kurs[i].mata_uang_asal_id == mt_pulang1) {
          harga_pulang1 = harga_pulang1*kurs[i].nilai;
          temp_mt_pulang1 = id_pulang1;
        }
    }
  }
  if (mt_pulang2 != 1 && mt_pulang2 != 'undefined' && temp_mt_pulang2 != id_pulang2) {
    for (var i = 0; i < kurs.length; i++) {
        if (kurs[i].mata_uang_asal_id == mt_pulang2) {
          harga_pulang2 = harga_pulang2*kurs[i].nilai;
          temp_mt_pulang2 = id_pulang2;
        }
    }
  }
  if (mt_plus1 != 1 && mt_plus1 != 'undefined' && temp_mt_plus1 != id_plus1) {
    for (var i = 0; i < kurs.length; i++) {
        if (kurs[i].mata_uang_asal_id == mt_plus1) {
          harga_plus1 = harga_plus1*kurs[i].nilai;
          temp_mt_plus1 = id_plus1;
        }
    }
  }
  if (mt_plus2 != 1 && mt_plus2 != 'undefined' && temp_mt_plus2 != id_plus2) {
    for (var i = 0; i < kurs.length; i++) {
        if (kurs[i].mata_uang_asal_id == mt_plus2) {
          harga_plus2 = harga_plus2*kurs[i].nilai;
          temp_mt_plus2 = id_plus2;
        }
    }
  }
  // console.log(parseInt(harga_berangkat1));
  // console.log(parseInt(harga_berangkat2));
  // console.log(parseInt(harga_pulang1));
  // console.log(parseInt(harga_pulang2));
  // console.log(parseInt(harga_plus1));
  // console.log(parseInt(harga_plus2));
  var harga = parseInt(harga_berangkat1)+ parseInt(harga_berangkat2) + parseInt(harga_pulang1) + parseInt(harga_pulang2) + parseInt(harga_plus1) + parseInt(harga_plus2);
  var harga_rp = "Rp. "+harga.toLocaleString();
  form.find('input[name=harga_rp]').val(harga_rp);
  form.find('input[name=harga]').val(harga);
}
function get_berangkat1($tanggal_berangkat,$ids){
        $("[name='penerbangan_berangkat1'] option").each(function() {
          $(this).remove();
      });
        $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url+"ui/rute_penerbangan_admin/get_rute/"+$tanggal_berangkat+"/"+is_edit+"/"+$ids,
          success: function(value){
            $("[name='penerbangan_berangkat1']").select2({
              placeholder: '-- Pilih Penerbangan Berangkat 1 --',
              data: value,
              templateResult: function (d) { return $(d.text); },
              templateSelection: function (d) { return $(d.select); },
              
            })
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }

      });

}
function get_berangkat2($tanggal_tiba_penerbangan1,$jam_tiba_penerbangan1,$id,$ids){
        $("[name='penerbangan_berangkat2'] option").each(function() {
          $(this).remove();
        });
        $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url+"ui/rute_penerbangan_admin/get_berangkat2/"+$tanggal_tiba_penerbangan1+"/"+$jam_tiba_penerbangan1+"/"+$id+"/"+is_edit+"/"+$ids,
          success: function(value){
            $("[name='penerbangan_berangkat2']").select2({
              placeholder: '-- Pilih Penerbangan Berangkat 2 --',
              data: value,
              templateResult: function (d) { return $(d.text); },
              templateSelection: function (d) { return $(d.select); },
              
            })
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }

      });

}
function get_pulang1($tanggal_tiba_penerbangan1,$ids){
        $("[name='penerbangan_pulang1'] option").each(function() {
          $(this).remove();
        });
        $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url+"ui/rute_penerbangan_admin/get_pulang1/"+$tanggal_tiba_penerbangan1+"/"+durasi+"/"+is_edit+"/"+$ids,
          success: function(value){
            $("[name='penerbangan_pulang1']").select2({
              placeholder: '-- Pilih Penerbangan Pulang 1 --',
              data: value,
              templateResult: function (d) { return $(d.text); },
              templateSelection: function (d) { return $(d.select); },
              
            })
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }

      });

}
function get_pulang2($tanggal_tiba_penerbangan1,$jam_tiba_penerbangan1,$id,$ids){
        $("[name='penerbangan_pulang2'] option").each(function() {
          $(this).remove();
        });
        $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url+"ui/rute_penerbangan_admin/get_pulang2/"+$tanggal_tiba_penerbangan1+"/"+$jam_tiba_penerbangan1+"/"+$id+"/"+is_edit+"/"+$ids,
          success: function(value){
            $("[name='penerbangan_pulang2']").select2({
              placeholder: '-- Pilih Penerbangan Pulang 2 --',
              data: value,
              templateResult: function (d) { return $(d.text); },
              templateSelection: function (d) { return $(d.select); },
              
            })
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }

      });

}
function get_plus1($tanggal_berangkat_plus,$ids){
        $("[name='penerbangan_plus1'] option").each(function() {
          $(this).remove();
      });
        $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url+"ui/rute_penerbangan_admin/get_plus1/"+$tanggal_berangkat_plus+"/"+id_berangkat1+"/"+id_berangkat2+"/"+is_edit+"/"+$ids,
          success: function(value){
            $("[name='penerbangan_plus1']").select2({
              placeholder: '-- Pilih Penerbangan Berangkat Plus 1 --',
              data: value,
              templateResult: function (d) { return $(d.text); },
              templateSelection: function (d) { return $(d.select); },
              
            })
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }

      });

}
function get_plus2($tanggal_tiba_penerbangan1,$jam_tiba_penerbangan1,$id,$ids){
        $("[name='penerbangan_plus2'] option").each(function() {
          $(this).remove();
        });
        $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url+"ui/rute_penerbangan_admin/get_plus2/"+$tanggal_tiba_penerbangan1+"/"+$jam_tiba_penerbangan1+"/"+$id+"/"+id_berangkat1+"/"+id_berangkat2+"/"+is_edit+"/"+$ids,
          success: function(value){
            $("[name='penerbangan_plus2']").select2({
              placeholder: '-- Pilih Penerbangan Plus 2 --',
              data: value,
              templateResult: function (d) { return $(d.text); },
              templateSelection: function (d) { return $(d.select); },
              
            })
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }

      });

}
function initValidation(){
  $.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
  }, "Value must not equal arg.");
  var validator = form.validate({
        rules: {
          jenis_paket_umroh: {
            valueNotEquals: "default"
          },
          penerbangan_berangkat1: {
            required: true
          },
          penerbangan_pulang1: {
            required: true
          },
          tanggal_berangkat: {
            required: true
          }
        },
        messages: {
          jenis_paket_umroh: {
            valueNotEquals: "Bidang isian jenis paket umroh tidak boleh kosong"
          },
          penerbangan_berangkat1: {
            required: "Bidang isian penerbangan berangkat 1 tidak boleh kosong"
          },
          penerbangan_pulang1: {
            required: "Bidang isian penerbangan pulang 1 tidak boleh kosong"
          },
          tanggal_berangkat: {
            required: "Bidang isian tanggal berangkat tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
          element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}
function submitForm(){
  $("#submit").attr("disabled", true);
  var arrayData = form.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

  $.ajax({ 
      type: "POST",
        dataType: 'json',
        data: arrayData,
        url: admin_base_url + "ui/rute_penerbangan_admin/save",
        success: function(data){
          //modal.modal('toggle');
          //bootbox.alert("Data rute penerbangan berhasil disimpan.", function(){
             window.location.href = data.url; 
          //});    
      },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          var err = XMLHttpRequest.responseJSON;
      }
    });
}
function initEdit(){
    table.on("click",".edit-rute-penerbangan", function(){
      var id = $(this).data('id');
        $.ajax({ 
            type: "GET",
            dataType: 'json',
            data: {id: id},
            url: admin_base_url + "ui/rute_penerbangan_admin/edit/"+id,
            success: function(data){
                form.find('input[name=id]').val(data.id);
                $('#jenis_paket_umroh').val(data.jenis_paket_umroh_id);
                form.find('input[name=tanggal_berangkat]').val(data.tanggal_berangkat);
                form.find('input[name=tanggal_berangkat_fix]').val(data.tanggal_berangkat);
                form.find('input[name=tanggal_tiba]').val(data.tanggal_tiba);
                form.find('input[name=tanggal_pulang]').val(data.tanggal_pulang);

                is_edit = 1;
                durasi = data.jenis_paket_umroh.durasi;
                id_berangkat1 = data.p_berangkat1;
                id_berangkat2 = data.p_berangkat2;
                if (data.jenis_paket_umroh.is_plus == 1) {
                  $('#formPlus').show();
                  $('#formTanggalPlus').show();
                }

                get_berangkat1(data.tanggal_berangkat,data.p_berangkat1);
                get_pulang1(data.tanggal_pulang,data.p_pulang1);

                harga_berangkat1 = data.penerbangan_berangkat.harga;
                mt_berangkat1 = data.penerbangan_berangkat.mata_uang_id;
                mt_pulang1  = data.penerbangan_pulang.mata_uang_id;
                harga_pulang1 = data.penerbangan_pulang.harga;

                if (data.p_berangkat2 != null) {
                  get_berangkat2(data.penerbangan_berangkat.tanggal_tiba,data.penerbangan_berangkat.jam_tiba,data.p_berangkat1,data.p_berangkat2);
                  mt_berangkat2  = data.penerbangan_berangkat_2.mata_uang_id;
                  harga_berangkat2 = data.penerbangan_berangkat_2.harga;
                }
                
                if (data.p_pulang2 != null) {
                  get_pulang2(data.penerbangan_pulang.tanggal_tiba,data.penerbangan_pulang.jam_tiba,data.p_pulang1,data.p_pulang2);
                  mt_pulang2  = data.penerbangan_pulang_2.mata_uang_id;
                  harga_pulang2 = data.penerbangan_pulang_2.harga;
                }

               if (data.p_plus1 != null) {
                  get_plus1(data.tanggal_berangkat_plus,data.p_plus1);
                  form.find('input[name=tanggal_berangkat_plus]').val(data.tanggal_berangkat_plus);
                  mt_plus1  = data.penerbangan_plus.mata_uang_id;
                  harga_plus1 = data.penerbangan_plus.harga;
               } 
                
               if (data.p_plus2 != null) {
                  get_plus2(data.penerbangan_plus.tanggal_tiba,data.penerbangan_plus.jam_tiba,data.p_plus1,data.p_plus2);
                  harga_plus2 = data.penerbangan_plus_2.harga;
                  mt_plus2  = data.penerbangan_plus_2.mata_uang_id;
               }
               hitung_harga(); 
                modal.modal('show');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var err = XMLHttpRequest.responseJSON;
            }
        });        
    });
}
function initDelete(el){
    var id = $(el).data('id');
      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data ini?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                url: admin_base_url + "ui/rute_penerbangan_admin/delete/"+id,
                success: function(data){
                  window.location.href = data.url; 
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
}