var tableKategori = $('#table-kategori'),
	formKategori = $('#form-kategori'),
	modalKategori = $('#modal-kategori');

$(function(){
	initDataTables();
	initAdd();
	initValidation();
	initEdit();
  initDelete();
});

function initDataTables(){
	tableKategori.DataTable();
}

function initAdd(){
	$('#add-kategori').on("click", function(){
		formKategori.find('input[name=id]').val(0);
		formKategori.find('input[name=kategori]').val("");
		formKategori.find('input[name=icon]').val("");
    formKategori.find('input[name=warna]').val("");
		// formKategori.find('input[name=is_mandatory]').prop('checked', false);
		modalKategori.modal('show');
	});
}

function initValidation(){
  var validator = formKategori.validate({
        rules: {
          kategori: {
            required: true
          },
          icon: {
            required: false
          }
        },
        messages: {
          kategori: {
            required: "Bidang isian kategori tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
        	element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
	var arrayData = new FormData(formKategori[0]);
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

  $.ajax({ 
      type: "POST",
        dataType: 'json',
        data: arrayData,
        url: admin_base_url + "kategori_admin/save",
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function(data){
          window.location.href = data.url;     
      },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          bootbox.hideAll();
          var err = XMLHttpRequest.responseJSON;
          bootbox.alert('Failed to save data kategori.');
      }
    });
}

function initEdit(){
	tableKategori.on("click",".edit-kategori", function(){
		var id = $(this).data('id');
		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	data: {id: id},
	      	url: admin_base_url + "kategori_admin/edit",
	      	success: function(data){
		        formKategori.find('input[name=id]').val(data.detail.id);
		        formKategori.find('input[name=kategori]').val(data.detail.kategori);
            formKategori.find('input[name=warna]').val(data.detail.warna);
		        // formKategori.find('input[name=icon]').val(data.detail.icon);
            if(data.detail.icon){
              $(".kategori-icon").html("<center><img src='"+base_url+"assets/images/kategori/"+data.detail.icon+"' width='150px'></center>");
            }
		        
		        
		        modalKategori.modal('show');
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
	});
}

function initDelete(){
  tableKategori.on("click",".delete-kategori", function(){
    var id = $(this).data('id'),
        nama = $(this).data('nama');

      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data <b>"+nama+"</b>?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "kategori_admin/delete",
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}
