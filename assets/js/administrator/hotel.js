var r = $("#submit_form");
var from_edit = $("#hotel-form");
var from_kamar = $("#kamarForm");
var dtable = $('#datatable');
$(document).ready(function(){
    $('.wysi').wysihtml5();
	dtable.DataTable({
        "order": [],
        "columns": [
            { "data": "nama", "name": "nama", "title": "Nama Hotel" },
            { "data": "bintang", "name": "bintang", "title": "Bintang" },
            { "data": "alamat", "name": "alamat", "title": "Alamat",
              "render" : function (data,type,full, meta){
                var str = data;
                return str.split(/\s+/).slice(0,8).join(" ");
              }
            },
            { "data": "id", "name": "action", "title": "Action", "sortable":false,
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="edit-hotel"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
                '<a data-id="'+data+'" class="view-room"><div class="btn btn-outline btn-circle btn-sm blue"><i class="icon-login" title="Lihat Kamar"></i> Kamar</div></a>'+
                '<a data-id="'+data+'" class="view-images"><div class="btn btn-outline btn-circle btn-sm blue"><i class="icon-picture" title="Lihat Gambar"></i> Gallery</div></a>'+
                '<a href="#" data-id="'+data+'" onclick="confirm_delete(this);"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>';
            } 
          }
            //repeat for each of my 20 or so fields
        ],

        "ajax": {
            "url": admin_base_url+"ui/Hotel_admin/get_datatables",
            "type": "POST"
        },
        "serverSide":true
        // "processing":true
    });

	$('#add-hotel').on("click", function(){
		console.log('add');
		// table.destroy();
        // $('.nav-pills a:first').tab('show');
        $("#hotel-modal").find('form')[0].reset();
		$('#hotel-modal').modal('show');
	});
	
	// $('#datatable').on("click", '.edit-hotel', function(){
	// 	// console.log('edit');
	// 	var id = $(this).attr('data-id');
	// 	$('#hotel-modal-edit').modal('show');

	// });
	$('body').on('hidden.bs.modal', '.modal', function () {

      $('.nav-pills a:first').tab('show');
	  $(this).removeData('bs.modal');
      $(this).find('form').trigger('reset');
      r.validate().resetForm();
      $(this).find('.alert').hide();
      $(this).find(".form-group").removeClass("has-error");
      $(this).find(".form-group").removeClass("has-success");
    });
    $('#hotel-modal').on('hide.bs.modal', function () {
        // $("#form_wizard_1").bootstrapWizard('show',1);
        // alert();
        // $('.nav-pills a:first').tab('show')
    });
    // $('#modal-admin').on('hide.bs.modal', function () {
    //     $("#myForm").trigger("reset");
    //     $('.password').show();
    //     $("#myForm").validate().resetForm();
    //     $("#myForm").get(0).reset();
    //     $("#myForm").data('changed', 0);
    // });
    FormRepeater.init();
    FormWizard.init();
    initValidation();
    initEdit();
    get_kota();
});
// function resetModal(){
//     $("#myForm").trigger("reset");
//     $('.password').show();
//     $("#myForm").validate().resetForm();
//     $("#myForm").get(0).reset();
//     $("#myForm").data('changed', 0);
// }
function initEdit(){
    // $("#hotel-modal-edit").find('form')[0].reset();
    // from_edit.find(".fasilitas")
    dtable.on("click",".edit-hotel", function(){
        var id = $(this).data('id');
        $.ajax({ 
            type: "GET",
            dataType: 'json',
            data: {id: id},
            url: admin_base_url + "ui/hotel_admin/edit",
            success: function(data){
                from_edit.find('input[name=id]').val(data.detail.id);
                from_edit.find('input[name=nama]').val(data.detail.nama);
                // from_edit.find('input[name=bintang]').val(data.detail.bintang);
                from_edit.find("input[name=bintang][value='"+data.detail.bintang+"']").prop("checked",true);
                from_edit.find('input[name=latitude]').val(data.detail.latitude);
                from_edit.find('input[name=longitude]').val(data.detail.longitude);
                // from_edit.find('textarea[name=deskripsi]').val(data.detail.deskripsi);
                from_edit.find("[name='kota']").select2('trigger', 'select', { data:{id: data.detail.kota, text:data.detail.kota}});
                from_edit.find('textarea[name=alamat]').val(data.detail.alamat);
                from_edit.find(".foto-path").empty();
                if(data.detail.path){
                    from_edit.find(".foto-path").html('<img src="'+base_url+'assets/images/hotel/'+data.detail.id+'/'+data.detail.path+'" width="200" style="float:left; margin-bottom:15px;"/><br/>');
                }
                from_edit.find('textarea[name=keterangan]').val(data.detail.keterangan);
                from_edit.find('.wysi').data("wysihtml5").editor.setValue(data.detail.deskripsi);
                if(data.detail.fasilitass != null){
                    var values = [];
                    for (var i = 0; i < data.detail.fasilitass.length; i++) {
                    // console.log(data.detail.fasilitass[i].TrcTypeID);
                    values.push(String(data.detail.fasilitass[i].fasilitas_id));
                    // $("#user-list [value=" + data.detail.fasilitass[i].TrcTypeID + "]").attr("checked", "checked");
                    }
                    console.log(values);
                    if(data.detail.fasilitass.length > 0){
                        from_edit.find(".fasilitas").find('[value=' + values.join('], [value=') + ']').prop("checked", true);
                    }
                }
                $('#hotel-modal-edit').modal('show');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                var err = XMLHttpRequest.responseJSON;
            }
        });
    });
}
function confirm_delete(el) {
    var id = $(el).data('id');
    bootbox.confirm("Apakan anda yakin akan menghapus hotel ini?", function(result){
        if (result) {
            window.location.href = admin_base_url+"ui/hotel_admin/delete/"+id;  
        }
    })
}
function initValidation(){
  var validator = from_edit.validate({
        rules: {
            nama: {
                minlength: 5,
                required: !0
            },
            alamat: {
                required: !0
            },
            bintang: {
                required: !0
            },
            deskripsi: {
                required: !1
            },
            latitude: {
                required: !1

            },
            longitude: {
                required: !1
            }
        },
        messages: {
          nama: {
            required: "Bidang isian nama tidak boleh kosong"
          },
          deskripsi: {
            required: "Bidang isian alamat tidak boleh kosong"
          },
          bintang: {
            required: "Bidang isian bintang tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
            element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitFormEdit();
        }
    });
}
function get_kota(){
    $("[name='kota']").select2({
        placeholder: '-- Pilih Kota --',
        ajax: {
          url: admin_base_url+"ui/hotel_admin/get_kota",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.nama
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });
}
var FormWizard = function() {
    return {
        init: function() {
            function e(e) {
                return e.id ? "<img class='flag' src='../../assets/global/img/flags/" + e.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + e.text : e.text
            }
            // console.log('asd');
            if (jQuery().bootstrapWizard) {
               
                var t = $(".alert-danger", r),
                    i = $(".alert-success", r);
                r.validate({
                    doNotHideMessage: !0,
                    errorElement: "span",
                    errorClass: "help-block help-block-error",
                    focusInvalid: !1,
                    rules: {
                        nama: {
                            minlength: 5,
                            required: !0
                        },
                        alamat: {
                            required: !0
                        },
                        bintang: {
                            required: !0
                        },
                        deskripsi: {
                            required: !1
                        },
                        latitude: {
                            required: !1

                        },
                        longitude: {
                            required: !1
                        },
                        tipe_kamar: {
                            required: !0
                        },
                        jenis_masakan: {
                            required: !0
                        }
                    },
                    messages: {
                        "payment[]": {
                            required: "Please select at least one option",
                            minlength: jQuery.validator.format("Please select at least one option")
                        }
                    },
                    errorPlacement: function(e, r) {
                        "gender" == r.attr("name") ? e.insertAfter("#form_gender_error") : "payment[]" == r.attr("name") ? e.insertAfter("#form_payment_error") : e.insertAfter(r)
                    },
                    invalidHandler: function(e, r) {
                        i.hide(), t.show(), App.scrollTo(t, -200)
                    },
                    highlight: function(e) {
                        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
                    },
                    unhighlight: function(e) {
                        $(e).closest(".form-group").removeClass("has-error")
                    },
                    success: function(e) {
                        "gender" == e.attr("for") || "payment[]" == e.attr("for") ? (e.closest(".form-group").removeClass("has-error").addClass("has-success"), e.remove()) : e.addClass("valid").closest(".form-group").removeClass("has-error").addClass("has-success")
                    },
                    submitHandler: function(e) {
                        i.show(), t.hide(), e[0].submit()
                    }
                });
                var a = function() {
                        $("#tab2 .form-control-static", r).each(function() {
                            var e = $('[name="' + $(this).attr("data-display") + '"]', r);
                            if (e.is(":radio") && (e = $('[name="' + $(this).attr("data-display") + '"]:checked', r)), e.is(":text") || e.is("textarea")) $(this).html(e.val());
                            else if (e.is("select")) $(this).html(e.find("option:selected").text());
                            else if (e.is(":radio") && e.is(":checked")) $(this).html(e.attr("data-title"));
                            else if ("payment[]" == $(this).attr("data-display")) {
                                var t = [];
                                $('[name="payment[]"]:checked', r).each(function() {
                                    t.push($(this).attr("data-title"))
                                }), $(this).html(t.join("<br>"))
                            }
                        })
                    },
                    o = function(e, r, t) {
                        var i = r.find("li").length,
                            o = t + 1;
                        $(".step-title", $("#form_wizard_1")).text("Step " + (t + 1) + " of " + i), jQuery("li", $("#form_wizard_1")).removeClass("done");
                        for (var n = r.find("li"), s = 0; s < t; s++) jQuery(n[s]).addClass("done");
                        1 == o ? $("#form_wizard_1").find(".button-previous").hide() : $("#form_wizard_1").find(".button-previous").show(), o >= i ? ($("#form_wizard_1").find(".button-next").hide(), $("#form_wizard_1").find(".button-submit").show(), a()) : ($("#form_wizard_1").find(".button-next").show(), $("#form_wizard_1").find(".button-submit").hide()), App.scrollTo($(".page-title"))
                    };
                $("#form_wizard_1").bootstrapWizard({
                    nextSelector: ".button-next",
                    previousSelector: ".button-previous",
                    onTabClick: function(e, r, t, i) {
                        return !1
                    },
                    onNext: function(e, a, n) {
                    	// alert('asd');
                        return i.hide(), t.hide(), 0 != r.valid() && void o(e, a, n)
                    },
                    onPrevious: function(e, r, a) {
                        i.hide(), t.hide(), o(e, r, a)
                    },
                    onTabShow: function(e, r, t) {
                        var i = r.find("li").length,
                            a = t + 1,
                            o = a / i * 100;
                        $("#form_wizard_1").find(".progress-bar").css({
                            width: o + "%"
                        })
                    }
                }), $("#form_wizard_1").find(".button-previous").hide(), $("#form_wizard_1 .button-submit").click(function() {
                    if(r.valid()){
                        submitForm();
                    }
                }).hide(), $("#country_list", r).change(function() {
                    r.validate().element($(this))
                })
            }
        }
    }
}();
// jQuery(document).ready(function() {
//     
// });
function submitForm(){
    var arr = r.find("input[name='fasilitas']:checked").getCheckboxVal()
    if(arr.length <= 0){
        var arr = 0;
    }
    r.find('input[name="fasilitas_list"]').val(arr);
    var arrayData = new FormData(r[0])
    // arrayData['fasilitas'] = arr;
    // console.log(arrayData);

    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

    $.ajax({ 
        type: "POST",
        dataType: 'json',
        data: arrayData,
        url: admin_base_url + "ui/Hotel_admin/save",
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function(data){
            window.location.href = data.url;     
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
    });
}
function submitFormEdit(){
    var arr = from_edit.find("input[name='fasilitas']:checked").getCheckboxVal()
    if(arr.length <= 0){
        var arr = 0;
    }
    from_edit.find('input[name="fasilitas_list"]').val(arr);
    // var arrayData = new FormData(r[0])
    var arrayData = new FormData(from_edit[0]);
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

    $.ajax({ 
        type: "POST",
        dataType: 'json',
        data: arrayData,
        url: admin_base_url + "ui/hotel_admin/save_edit",
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function(data){
            window.location.href = data.url;     
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
    });
}

var FormRepeater = function() {
    return {
        init: function() {
            // alert();
            $(".mt-repeater").each(function() {
                $(this).repeater({
                    show: function() {
                        $(this).slideDown();
                    },
                    hide: function(e) {
                        confirm("Are you sure you want to delete this element?") && $(this).slideUp(e)
                    },
                    ready: function(e) {}
                })
            })
        }
    }
}();
$.extend( $.validator.prototype, {
    checkForm: function () {
    this.prepareForm();
    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
    if (this.findByName(elements[i].name).length != undefined && this.findByName(elements[i].name).length > 1) {
    for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
    this.check(this.findByName(elements[i].name)[cnt]);
    }
    } else {
    this.check(elements[i]);
    }
    }
    return this.valid();
    }
});

dtable.on('click','.view-images',function(){
    // var id = $(this).closest('tr').attr('idx');
    $('#modal-gallery').modal('show');
    var id = $(this).data('id');
    $( "#galleryForm" ).find('.upload-button').html('<input type="button" class="btn btn-block btn-primary" value="Upload" onclick="upload_image(\'ui/Hotel_admin/post_gambar\');"></button>');
    $( "#galleryForm" ).find('input[name=id]').val(id);
    // $('[name="id"]').val(id);
    get_gallery(id,"ui/Hotel_admin/get_gallery");
});
function get_gallery(id,uri){
    $('.gallery-container').empty();
    $.post(admin_base_url + uri, {id: id}).done(function( data1 ) {
        var json = $.parseJSON(data1);
        console.log(json.length);
        if(data1.length == 0){

        }else{
            for (var i = 0; i < json.length; i++) {
                $('.gallery-container').append('<div class="col-md-3"><img src="'+base_url+'assets/images/hotel/'+id+'/'+json[i].nama_file+'" width="100%" alt="..." class="img-thumbnail" style="min-height:150px">'+
                    '<div class="row"><a class="delete-img btn btn-block btn-white text-red" href="javascript:;" onclick=delete_img('+json[i].id+',"'+json[i].nama_file+'",'+id+');><i class="fa fa-trash-o fa-lg"></i></a></div>'+
                    '</div>');
            }
        }
    })
}
function delete_img(id,foto,id_provider){
    bootbox.confirm("Apakan anda yakin akan menghapus gambar ini?", function(result){
        if (result) {
            $.post( admin_base_url + "ui/Hotel_admin/hapus_gambar", {id : id,foto : foto,id_provider : id_provider}).done(function( data ) {
                if (data == '1'){
                    bootbox.alert("Data berhasil dihapus.", function(){
                        get_gallery(id_provider,"ui/Hotel_admin/get_gallery");
                    })
                } else {
                    bootbox.alert('Data gagal dihapus.');
                }
            });
        }else{
        }
    });
}
function upload_image(url){
    // var data = new FormData($('#galleryForm')[0]);
    var validator = $( "#galleryForm" ).validate({
        rules: {
               image: {
                required: true,
                accept: "image/*"
            },
        },
        messages:{
          image:{
            accept: "Belum ada file yang dipilih."
          }
        }
    });
    
    if(validator.form()){
        var arrayData = new FormData($('#galleryForm')[0])
        var jsonData = {};

        $.each(arrayData, function(index, item) {
            jsonData[item.name] = item.value;
        });

        $.ajax({ 
            type: "POST",
            dataType: 'json',
            data: arrayData,
            url: admin_base_url + url,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
           success:function(data)
            {
                if (data != "0"){
                    bootbox.alert("Data berhasil disimpan.", function(){
                        if(data.tipe == "hotel"){
                            get_gallery(data.id,"ui/Hotel_admin/get_gallery");
                        }else{
                            get_gallery_kamar(data.id,"ui/Hotel_admin/get_gallery_kamar");
                        }
                    })
                }else{
                    bootbox.alert("Data gagal disimpan.");
                }       

            }
        });
    }
}
dtable.on('click','.view-room',function(){
    var id = $(this).data('id');
    // alert();
    $('#modal-kamar').modal('show');
    // $('[name="id"]').val(id);
    $( "#kamarForm" ).find('input[name=id]').val(id);
    initKamar(id);

});
function initKamar(idx){
    // $('[name="id"]').val(idx);
    $('.lapang-row').remove();
    $.post( admin_base_url + "ui/Hotel_admin/get_kamar", {idx: idx}).done(function( data1 ) {
        var json = $.parseJSON(data1);
        if(json.length == 0){
            $('#tabel-kamar').append(
                '<tr class="lapang-row"><td class="form-group"><input type="text" class="form-control" name="tipe_kamar[]" required></td>'+
                '<td class="form-group"><input type="number" class="form-control" name="kapasitas[]" required></td>'+
                '<td class="form-group"><input type="text" class="form-control" name="jenis_makanan[]" required></td><input type="hidden" class="form-control" value="0" name="id_kamar[]">'+
                '<td>'+
                // '<a class="btn btn-sm btn-white text-black" onclick="tambah_kamar();"><i class="fa fa-plus-square-o fa-lg"></i></a>'+ 
                '<a class="btn btn-danger btn-sm" onclick="hapus_kamar(this);"><i class="fa fa-close"></i> Hapus Kamar</a>'+ 
                '</td></tr>'
            );
        }else{
            console.log(json);
            for (var i = 0; i < json.length; i++) {
                $('#tabel-kamar').append('<tr class="lapang-row"><td class="form-group"><input type="text" class="form-control" value="'+json[i].tipe_kamar+'" name="tipe_kamar[]" required></td>'+
                '<td class="form-group"><input type="number" class="form-control" value="'+json[i].kapasitas+'" name="kapasitas[]" required></td>'+
                '<td class="form-group"><input type="text" value="'+json[i].jenis_makanan+'" class="form-control" name="jenis_makanan[]" required><input type="hidden" value="'+json[i].id+'" class="form-control" name="id_kamar[]" required></td>'+
                '<td>'+
                // '<a class="btn btn-sm btn-white text-black" onclick="tambah_kamar();"><i class="fa fa-plus-square-o fa-lg"></i></a>'+ 
                '<a class="btn btn-danger btn-xs" onclick="hapus_kamar(this);"><i class="fa fa-close"></i> Hapus Kamar</a>'+ 
                '<a class="btn btn-info btn-xs" onclick="gallery_kamar(this);"><i class="icon-picture"></i> Gallery Kamar</a>'+
                '<a class="btn btn-success btn-xs" href="'+admin_base_url+'ui/harga_kamar_hotel_admin/index/'+idx+'/'+json[i].id+'"><i class="fa fa-money"></i> Harga Kamar</a>'+             
                '</td></tr>');
            }
        }
    });
}
function tambah_kamar(){
    $('#tabel-kamar').append(
        '<tr class="lapang-row"><td class="form-group"><input type="text" class="form-control" name="tipe_kamar[]" required></td>'+
        '<td class="form-group"><input type="number" class="form-control" name="kapasitas[]" required></td>'+
        '<td class="form-group"><input type="text" class="form-control" name="jenis_makanan[]" required><input type="hidden" class="form-control" value="0" name="id_kamar[]"></td>'+
        '<td>'+
        // '<a class="btn btn-sm btn-white text-black" onclick="tambah_kamar();"><i class="fa fa-plus-square-o fa-lg"></i></a>'+ 
        '<a class="btn btn-danger btn-sm" onclick="hapus_kamar(this);"><i class="fa fa-close"></i> Hapus Kamar</a>'+ 
        '</td></tr>'
    );
}
function hapus_kamar(el){
    var idx = $(el).closest('tr').find('[name="id_kamar[]"]').val();
    var kode = $(el).closest('tr').find('[name="tipe_kamar[]"]').val();
    if(idx == 0){
        $(el).parent().parent().remove();
    }else{
        bootbox.confirm("Apakan anda yakin akan menghapus kamar "+kode+"?", function(result){
            if (result) {
                $.post(  admin_base_url + "ui/Hotel_admin/delete_kamar", {id : idx}).done(function( data ) {
                    if (data == '1'){
                        bootbox.alert("Data berhasil dihapus.", function(){
                            $(el).parent().parent().remove();
                        })
                    } else {
                        bootbox.alert('Data gagal dihapus.');
                    }
                });
            }else{
            }
        });
    }
}
function gallery_kamar(el){
    var idx = $(el).closest('tr').find('[name="id_kamar[]"]').val();
    // var kode = $(el).closest('tr').find('[name="tipe_kamar[]"]').val();
    $( "#galleryForm" ).find('.upload-button').html('<input type="button" class="btn btn-block btn-primary" value="Upload" onclick="upload_image(\'ui/Hotel_admin/post_gambar_kamar\');"></button>');
    
    $('#modal-gallery').modal('show');
    // var id = $(this).data('id');
    $( "#galleryForm" ).find('input[name=id]').val(idx);
    // $('[name="id"]').val(id);
    // console.log(idx+"sadsda");
    get_gallery_kamar(idx,"ui/Hotel_admin/get_gallery_kamar");
    $('#modal-kamar').modal('hide');
}
function post_kamar(){
    var data = $('#kamarForm').serialize();
    var api =  admin_base_url + "ui/Hotel_admin/post_kamar";
    var validator = $( "#kamarForm" ).validate({
        rules: {
               tipe_kamar: {
                required: true
            },
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
            // element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    });
    
    if(validator.form()){
        $.post(api, data).done(function( data ) {
          if (data == "1"){
            bootbox.alert("Data kamar berhasil disimpan.", function(){
              initKamar($( "#kamarForm" ).find('input[name=id]').val());
            })
          }else{
            bootbox.alert("Gagal menyimpan data.");
          }   
        });
    }
    console.log(base_url);
}
function get_gallery_kamar(id,uri){
    $('.gallery-container').empty();
    $.post(admin_base_url + uri, {id: id}).done(function( data1 ) {
        var json = $.parseJSON(data1);
        console.log(json.length);
        if(data1.length == 0){

        }else{
            for (var i = 0; i < json.length; i++) {
                $('.gallery-container').append('<div class="col-md-3"><img src="'+base_url+'assets/images/hotel/room/'+id+'/'+json[i].nama_file+'" width="100%" alt="..." class="img-thumbnail" style="min-height:150px">'+
                    '<div class="row"><a class="delete-img btn btn-block btn-white text-red" href="javascript:;" onclick=delete_img_kamar('+json[i].id+',"'+json[i].nama_file+'",'+id+');><i class="fa fa-trash-o fa-lg"></i></a></div>'+
                    '</div>');
            }
        }
    })
}
function delete_img_kamar(id,foto,id_provider){
    bootbox.confirm("Apakan anda yakin akan menghapus gambar ini?", function(result){
        if (result) {
            $.post( admin_base_url + "ui/Hotel_admin/hapus_gambar_kamar", {id : id,foto : foto,id_provider : id_provider}).done(function( data ) {
                if (data == '1'){
                    bootbox.alert("Data berhasil dihapus.", function(){
                        get_gallery_kamar(id_provider,"ui/Hotel_admin/get_gallery_kamar");
                    })
                } else {
                    bootbox.alert('Data gagal dihapus.');
                }
            });
        }else{
        }
    });
}
jQuery.fn.getCheckboxVal = function(){
    var vals = [];
    var i = 0;
    this.each(function(){
        vals[i++] = jQuery(this).val();
    });
    return vals;
}
// jQuery(document).ready(function() {
//     FormRepeater.init();
//     // alert();
// });