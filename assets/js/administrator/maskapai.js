
$(function(){
  initDelete();
});
$(document).ready(function(){
	$('#datatable').DataTable();
	$('#add-maskapai').on("click", function(){
		console.log('add');
		$('#maskapai-modal').modal({
			remote: base_url+'administrator/ui/maskapai_admin/add'
		});
	});
	
	$('#datatable').on("click", '.edit-maskapai', function(){
		console.log('edit');
		var id = $(this).attr('data-id');
		$('#maskapai-modal').modal({
			remote: base_url+'administrator/ui/maskapai_admin/edit/'+id
		});
	});
	$('body').on('hidden.bs.modal', '.modal', function () {
	  $(this).removeData('bs.modal');
	});

	 $('#form-maskapai').validate({
        rules: {
          nama: {
            required: true
          }
        },
        messages: {
          nama: {
            required: "Bidang isian nama tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
        	element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
});
 

/*delete confirmation*/
function initDelete() {
   $('#datatable').on("click",".delete-maskapai", function(){
    var id = $(this).data('id');

      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data ini?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "ui/maskapai_admin/delete",
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}