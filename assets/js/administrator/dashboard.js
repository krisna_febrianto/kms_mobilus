$(document).ready(function() {
    // initMap();
    // get_geocode();
    chartMateriKategori();
    // initMarkerMap(0);
    chartMateri();
        // initmarkerMap2();

});

function chartMateriKategori(){
  var options = {
        chart: {
            renderTo: 'chart-materi-kategori',
            type: 'column',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
            }
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            title: {
                text: 'Jumlah Materi'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            formatter: function() {
                    return '<b>'+ this.series.name +'</b> '+
                    this.x +': '+ this.y+' Materi';
            }
        },
        

        series: [],
        credits: {
          enabled : false
        }
    }

    $.getJSON("kategori_materi", function(json) {
    options.xAxis.categories = json[0]['data'];
        options.series[0] = json[1];
        chart = new Highcharts.Chart(options);
    });
}
function chartMateri(){
  var options = {
        chart: {
            renderTo: 'chart-materi',
            type: 'column',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
            }
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: []
        },
        yAxis: {
            title: {
                text: 'Jumlah dilihat'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            formatter: function() {
                    return '<b>'+ this.series.name +'</b> '+
                    this.x +': '+ this.y+' dilihat';
            }
        },
        

        series: [],
        credits: {
          enabled : false
        }
    }

    $.getJSON("chart_materi", function(json) {
    options.xAxis.categories = json[0]['data'];
        options.series[0] = json[1];
        chart = new Highcharts.Chart(options);
    });
}