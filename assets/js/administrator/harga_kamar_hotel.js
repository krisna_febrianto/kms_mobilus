var formHarga = $('#form-harga'),
    modal = $('#modal-harga'),
    //modal-selected = $('#modal-harga-selected'),
    currentDate, // Holds the day clicked when adding a new event
    currentEvent;


$(function(){
  init();
  initValidation();
  modalHarga();
  get_mata_uang();

  var hotel = $('[name="selected_hotel"]').val();
  if (hotel) {
    get_selected_kamar(hotel);
  }
});

function init(){ 
    generateCalendar($('[name="kamar_id"]').val());
    //datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd'
    });

    $("[name='kota']").select2({
        placeholder: '-- Pilih Kota --',
        //minimumInputLength: 1,
        ajax: {
          url: admin_base_url+"ui/Harga_kamar_hotel_admin/get_kota",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.nama
                    }
                })
            };
          },
          cache: true
        }
    });

    $("[name='kota']").on('change', function(){
      var kota = $(this).val();
      //$("[name='kota']").val("").trigger('change');
      $("[name='hotel']").val("").trigger('change');
      $("[name='kamar']").val("").trigger('change');
      get_hotel(kota);
    });
};

function get_hotel(kota){

  // alert($('[name="kamar_id"]').val());
  // generateCalendar($('[name="selected_kamar"]').val());
    $("[name='hotel']").select2({
        placeholder: '-- Pilih Hotel --',
        ajax: {
          url: admin_base_url+"ui/Harga_kamar_hotel_admin/get_hotel/"+kota,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
    });

    $("[name='hotel']").on('change', function(){
      var id = $(this).val();
      $("[name='hotel']").val();
      $("[name='kamar']").val("").trigger('change');
      get_kamar(id);
      $("[name='kamar']").val('all').trigger('change');
    });
}

function get_selected_kamar(hotel_id = null){
    $("[name='kamar']").select2('destroy');
    $("[name='kamar']").select2({
        placeholder: '-- Pilih Kamar --',
        ajax: {
          url: admin_base_url+"ui/Harga_kamar_hotel_admin/get_kamar/"+hotel_id,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            //$('[name=kamar]').append($("<option></option>").attr("value",0).text("Semua Kamar"));
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.tipe_kamar,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
    });
}

function get_kamar(hotel_id = null){
    $("[name='kamar']").select2('destroy');
    $("[name='kamar']").select2({
        placeholder: '-- Pilih Kamar --',
        ajax: {
          url: admin_base_url+"ui/Harga_kamar_hotel_admin/get_kamar/"+hotel_id,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            //$('[name=kamar]').append($("<option></option>").attr("value",0).text("Semua Kamar"));
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.tipe_kamar,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
    });
    $('[name="kamar"]').select2('trigger', 'select', { data:{id:"all", text:"Semua Kamar"}});
}

function get_mata_uang(){
    $("[name='mata_uang[]']").select2({
        placeholder: '-- Mata Uang --',
        ajax: {
          url: admin_base_url+"ui/Harga_kamar_hotel_admin/get_mata_uang",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.kode+" ("+item.symbol+")",
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });
}

function initValidation(){

    var validator = formHarga.validate({
        rules: {
          start_date:{
            required: true
          },
          end_date:{
            required: true
          },
          "mata_uang[]": {
            required: true          
          },
          // kamar_hotel_id: {
          //   required: true,
          //   remote: {
          //     url: admin_base_url + "ui/Harga_kamar_hotel_admin/check_kamar",
          //     type: 'post',
          //     dataType: 'json',
          //     data: {
          //       tanggal: function(){
          //         return $("[name='tanggal']").val();
          //       },
          //       kamar_hotel_id: function(){
          //         return $("[name='kamar_hotel_id']").val();
          //       }
          //     }
          //   }
          // },
          "harga[]": {
            required: true
          }
        },
        messages: {
          start_date:{
            required: "Tanggal wajib diisi"
          },
          end_date:{
            required: "Tanggal wajib diisi"
          },
          "mata_uang[]": {
            required: "Pilihan mata uang tidak boleh kosong"
          },
          // kamar_hotel_id: {
          //   remote: "Harga hotel ditanggal ini sudah ada",
          //   required: "Pilihan kamar hotel tidak boleh kosong",
            
          // },
          "harga[]": {
            required: "Bidang isian harga tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
          if (element.attr('name') == "start_date" || element.attr('name') == "end_date") {
            element.parent().parent().append(error);
          }else{
            element.parent().append(error);
          }
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function generateCalendar(idx = null){
  var hotel_id = $('[name="hotel"]').val();
  if(idx == null){
    var kamar = $('[name="kamar"]').val();
  }else{
    var kamar = idx;
  }

  if (kamar == 'all') {
    $("[name='kamar']").select2('trigger', 'select', { data:{id: 'all', text:'Semua Kamar'}});
  }
    if (kamar != null) {
      if(kamar != 0){
        $('.calendar-harga').show();
        $('#calendarKamar').fullCalendar( 'destroy' );
        $('#calendarKamar').fullCalendar({
          header: {
            left: 'prev, next',
            center: 'title',
            right: 'today'
          },
          defaultView: 'month', 
          defaultDate: new Date(),
          selectable:true,
          selectHelper: true,
          eventLimit: 2,
          events: admin_base_url + "ui/Harga_kamar_hotel_admin/getDaftarHarga/"+kamar+'/'+hotel_id,
          select: function(start, end, jsEvent, view) {
              var date = start;
              var events = $('#calendarKamar').fullCalendar('clientEvents');
    
              if (kamar != 'all') {
                if (events.length != 0) {
                  for (var i = 0; i < events.length; i++) {
                      if (moment(date).isSame(moment(events[i].start))) {
                          return false;
                      }else if (i == events.length - 1) {
                          add(start.format(), end.format(), kamar);
                      }
                  }  
                }else{
                  add(start.format(), end.format(), kamar);
                }
              }else{
                add(start.format(), end.format(), null);
              }
          },
          // dayClick: function(date, jsEvent, view) {

          //     currentDate = date.format();
          //     var events = $('#calendarKamar').fullCalendar('clientEvents');
    
          //     if (kamar != 'all') {
          //       if (events.length != 0) {
          //         for (var i = 0; i < events.length; i++) {
          //             if (moment(date).isSame(moment(events[i].start))) {
          //                 return false;
          //             }else if (i == events.length - 1) {
          //                 add(date.format(), date.format(), kamar);
          //             }
          //         }  
          //       }else{
          //         add(date.format(), date.format(), kamar);
          //       }
          //     }else{
          //       add(date.format(), date.format());
          //       //$("[name='kamar']").val('all').trigger('change');
          //     }
          // },
          eventRender: function(events, element) {
            var a = parseInt(events.harga);
            var harga = a.toLocaleString();

            element.find(".fc-content").prepend("<h1 style='margin-top:0px; margin-left:0px; font-size: 10px; white-space: normal;'>"+events.kamar+"</h1><p style='font-size:15px; margin-bottom:0px; text-align:right; white-space: normal;'>"+events.symbol+". "+harga+"</p>");

            element.find(".portlet.calendar.fc-header").css('width', '200px');
            element.find(".fc-content").css('width', '90%');

            element.find(".fc-title").remove();
          },
          eventClick: function(calEvent, jsEvent, view) {

              currentEvent = calEvent;
              
              modalHarga({
                // Available buttons when editing
                // buttons: {
                //     delete: {
                //         id: 'delete-harga',
                //         css: 'btn-danger',
                //         label: 'Hapus',
                //         onclick: 'initDelete(' + calEvent.id + ')'

                //     },
                //     update: {
                //         id: 'update-harga',
                //         css: 'btn-success',
                //         label: 'Edit',
                //         onclick: 'submitForm(' + calEvent.id + ')'
                //     }
                // },
                title: 'Ubah Harga',
                event: calEvent
            });
          }
        });
      }      
    }else{
      bootbox.alert('Isi pilihan kota, hotel dan kamar terlebih dahulu');
    }
}

function add(start, end, kamar){
    modalHarga({
        kamar_hotel_id : kamar,
        title: 'Set Harga'
    });
    formHarga.find('input[name=start_date]').val(start);
    //mengurangi 1 hari end date
    var end = new Date(end);
    var endDate = end.setDate(end.getDate()-1);
    $('#end_date').datepicker('setDate', new Date(endDate));
    $('#end_date').datepicker('setStartDate', new Date(start));
    //init_kamar();
}

function modalHarga(data){
  var kamar = $('[name="kamar"]').val();

  if (kamar != 'all') {
    $("#tipe_kamar").attr('hidden', true);
  }else{
    $("#tipe_kamar").attr('hidden', false);;
  }

  if (data) {
      formHarga.trigger("reset");
      formHarga.get(0).reset();
      // formHarga.data('changed', 0);
      formHarga.validate().resetForm();
      formHarga.find('.alert').hide();
      formHarga.find(".form-group").removeClass("has-error");
      formHarga.find(".form-group").removeClass("has-success");

      formHarga.find('input[name=id]').val(0);
      formHarga.find('input[name=harga]').val("");
      $("[name='kamar_hotel_id']").empty();
      $("[name='mata_uang']").val("").trigger('change');

      $('.modal-footer button:not(".btn-default")').remove();
      $("#kamar_hotel_id").attr('disabled', false);

      //button
      $('#simpan').show();
      $('#edit').hide();
      $('#delete').hide();
      if (data.event) {
        // init_kamar();
        $("#kamar_hotel_id").attr('disabled', true);
        var kamar_id        = data.event.kamar_hotel_id;
        var mata_uang_id    = data.event.mata_uang_id;
        var nama_mata_uang  = data.event.mata_uang;
        var harga           = parseFloat(Math.round(data.event.harga * 100) / 100).toFixed(0);
        var tanggal         = data.event.tanggal;

        if ($('[name="kamar"]').val()=="all") {
          init_kamar(kamar_id, mata_uang_id, nama_mata_uang, harga, tanggal);
        }else{
          init_kamar_perId(kamar_id, mata_uang_id, nama_mata_uang, harga, tanggal);
        }
        
        formHarga.find('input[name=id]').val(data.event.id);
        formHarga.find('input[name=start_date]').val(data.event.tanggal);
        formHarga.find('input[name=end_date]').val(data.event.tanggal);
        $('[id="harga-'+data.event.kamar_hotel_id+'"]').val(parseFloat(Math.round(data.event.harga * 100) / 100).toFixed(0));
        $('[id="mata-uang-'+data.event.kamar_hotel_id+'"]').select2('trigger', 'select', { data:{id: data.event.mata_uang_id, text:data.event.mata_uang}});
        $('#end_date').datepicker('setStartDate', new Date(data.event.tanggal));
        // button
        $('#simpan').hide();
        $('#edit').show();
        $('#delete').show();
        // $.each(data.buttons, function(index, button){
        //     $('.modal-footer').prepend('<button type="button" onclick="' + button.onclick  + '" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</button>')
        // })
      }else{
        if ($('[name="kamar"]').val()=="all") {
          init_kamar();
        }else{
          init_kamar_perId($('[name="kamar"]').val());
        }
        formHarga.find('input[name=end_date]').val(data.tanggal);
        formHarga.find('input[name=start_date]').val(data.tanggal);
      }
      
      $('.modal-title').html(data.title);
      modal.modal('show');
  }
}

function submitForm(){
    var id = $('[name="id"]').val();
    var validator = formHarga.validate();

    if(validator.form()){
      $('#simpan').attr('disabled', true);
      $('#edit').attr('disabled', true);
      $('[name="tipe_kamar[]"]').attr('disabled', false);
      var arrayData = formHarga.serializeArray();

      var jsonData = {};

      $.each(arrayData, function(index, item) {
          jsonData[item.harga] = item.value;
      });
      $.ajax({ 
        type: "POST",
          dataType: 'json',
          data: arrayData,
          url: admin_base_url + "ui/Harga_kamar_hotel_admin/save/"+id,
          success: function(data){
            $('#simpan').attr('disabled', false);
            $('#edit').attr('disabled', false);
            $('[name="tipe_kamar[]"]').attr('disabled', true);
            modal.modal('hide');
            bootbox.alert("Data berhasil disimpan.");
            $('#calendarKamar').fullCalendar("refetchEvents");   
            
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            $('#simpan').attr('disabled', false);
            $('#edit').attr('disabled', false);
            var err = XMLHttpRequest.responseJSON;
        }
      });
    }
}

function initDelete(){
  $('[name="tipe_kamar[]"]').attr('disabled', false);
  var id        = $('[name="id"]').val();
  var arrayData = formHarga.serializeArray();
    if (id) {
      modal.modal('hide');
      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data ini?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: arrayData,
                url: admin_base_url + "ui/Harga_kamar_hotel_admin/delete",
                success: function(data){
                  modal.modal('hide');
                  //bootbox.alert("Data berhasil dihapus.");
                  $('[name="tipe_kamar[]"]').attr('disabled', true);
                  $('#calendarKamar').fullCalendar("refetchEvents"); 
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
    }
}

function init_kamar(id_kamar=null, mata_uang_id=null, nama_mata_uang=null, harga=null, tanggal=null){
  
  var mode = 1;
  var hotel_id = $('[name="hotel"]').val();

    $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url + "ui/Harga_kamar_hotel_admin/get_kamar/"+hotel_id+'/'+mode,
          success: function(data){

            $('#tipe_kamar').html("");
            $.each(data, function(index, item) {
                var content = generatePrice(item);
                $('#tipe_kamar').append(content); 
                get_mata_uang();

                $('[name="harga[]"]').attr('disabled', true);
                $('[name="mata_uang[]"]').attr('disabled', true);
                // get data kamar, mata uang dan harga ketika edit
                if (id_kamar != null) {
                  $("[value='"+id_kamar+"']").attr('checked', true);
                  if ($("[value='"+id_kamar+"']").attr('checked', true)) {
                    $('[id="'+id_kamar+'"]').attr('disabled', false);
                    $('[id="mata-uang-'+id_kamar+'"]').attr('disabled', false);
                    $('[id="harga-'+id_kamar+'"]').attr('disabled', false);

                    $('[id="harga-'+id_kamar+'"]').val(harga);
                    $('[id="mata-uang-'+id_kamar+'"]').select2('trigger', 'select', {data:{id: mata_uang_id, text:nama_mata_uang}});
                  }
                }

                $('[value="'+item.id+'"]').on('change', function(){ 
                    if (this.checked) {
                      $('[id="mata-uang-'+item.id+'"]').attr('disabled', false);
                      $('[id="harga-'+item.id+'"]').attr('disabled', false);
                      get_harga(item.id, tanggal);
                    }else{
                      $('[id="mata-uang-'+item.id+'"]').attr('disabled', true);
                      $('[id="harga-'+item.id+'"]').attr('disabled', true);
                      $('[id="mata-uang-'+item.id+'"]').empty();
                      $('[id="harga-'+item.id+'"]').val("");
                    }
                });
            });
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
      });
}

function init_kamar_perId(id_kamar=null, mata_uang_id=null, nama_mata_uang=null, harga=null, tanggal=null){
    $.ajax({ 
        type: "GET",
          dataType: 'json',
          url: admin_base_url + "ui/Harga_kamar_hotel_admin/get_kamar_perId/"+id_kamar,
          success: function(data){

            $('#tipe_kamar').html("");
            var content = generatePrice(data);
            $('#tipe_kamar').append(content); 
            $('#tipe_kamar').attr('hidden', false);
            $('[name="tipe_kamar[]"]').attr('checked', true);
            $('[name="tipe_kamar[]"]').attr('disabled', true);
            get_mata_uang();

            //get data kamar, mata uang dan harga ketika edit
            if (mata_uang_id != null) {
              $('[id="harga-'+id_kamar+'"]').val(harga);
              $('[id="mata-uang-'+id_kamar+'"]').select2('trigger', 'select', {data:{id: mata_uang_id, text:nama_mata_uang}});
            }
            
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            var err = XMLHttpRequest.responseJSON;
        }
      });
}

function generatePrice(data){
    var content =   '<div class="col-md-12" style="padding-left: 0;">';
        content +=      '<div class="col-md-4" style="padding-left: 0;">';
        content +=        '<div class="mt-checkbox-list kamar_hotel_id" style="max-height:200px;overflow:auto">';
        content +=           '<label class="mt-checkbox mt-checkbox-outline"><input type="checkbox" name="tipe_kamar[]" id="'+data.id+'" value="'+data.id+'"/> '+data.tipe_kamar+'<span></span></label>';
        content +=        '</div>';
        content +=      '</div>';
        content +=      '<div class="col-md-3" style="padding-right: 0;" id="harga">';
        content +=         '<select required name="mata_uang[]" class="select2 form-control" id="mata-uang-'+data.id+'"><option value=""></option></select>';
        content +=      '</div>';
        content +=      '<div class="col-md-5" id="harga">';
        content +=        '<input type="number" name="harga[]" class="form-control" required min="0" placeholder="Harga" id="harga-'+data.id+'">';
        content +=      '</div>';
        content +=    '</div>';
    return content;
}

function get_harga(id_kamar, tanggal){
  $.ajax({ 
    type: "GET",
      dataType: 'json',
      url: admin_base_url + "ui/Harga_kamar_hotel_admin/get_harga/"+id_kamar+"/"+tanggal,
      success: function(data){  
        if (data != null) {
            $('[id="harga-'+id_kamar+'"]').val(parseFloat(Math.round(data.harga * 100) / 100).toFixed(0));
            $('[id="mata-uang-'+id_kamar+'"]').select2('trigger', 'select', {data:{id: data.mata_uang_id, text:data.mata_uang.kode+" ("+data.mata_uang.symbol+")"}});
        }
      }
    });
}