var tableFasilitasHotel = $('#table-fasilitas-hotel'),
	formFasilitasHotel = $('#form-fasilitas-hotel'),
	modalFasilitasHotel = $('#modal-fasilitas-hotel');

$(function(){
	initDataTables();
	initAdd();
	initValidation();
	initEdit();
  initDelete();
});

function initDataTables(){
	tableFasilitasHotel.DataTable();
}

function initAdd(){
	$('#add-fasilitas-hotel').on("click", function(){
		formFasilitasHotel.find('input[name=id]').val(0);
		formFasilitasHotel.find('input[name=fasilitas]').val("");
		formFasilitasHotel.find('input[name=icon]').val("");
		// formFasilitasHotel.find('input[name=is_mandatory]').prop('checked', false);
		modalFasilitasHotel.modal('show');
	});
}

function initValidation(){
  var validator = formFasilitasHotel.validate({
        rules: {
          fasilitas: {
            required: true
          },
          icon: {
            required: false
          }
        },
        messages: {
          fasilitas: {
            required: "Bidang isian fasilitas tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
        	element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
	var arrayData = formFasilitasHotel.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

	$.ajax({ 
    	type: "POST",
      	dataType: 'json',
      	data: arrayData,
      	url: admin_base_url + "ui/Fasilitas_hotel_admin/save",
      	success: function(data){
	        window.location.href = data.url;     
	    },
      	error: function(XMLHttpRequest, textStatus, errorThrown) {
        	var err = XMLHttpRequest.responseJSON;
	    }
    });
}

function initEdit(){
	tableFasilitasHotel.on("click",".edit-fasilitas-hotel", function(){
		var id = $(this).data('id');
		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	data: {id: id},
	      	url: admin_base_url + "ui/Fasilitas_hotel_admin/edit",
	      	success: function(data){
		        formFasilitasHotel.find('input[name=id]').val(data.detail.id);
		        formFasilitasHotel.find('input[name=fasilitas]').val(data.detail.fasilitas);
		        formFasilitasHotel.find('input[name=icon]').val(data.detail.icon);
		        
		        
		        modalFasilitasHotel.modal('show');
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
	});
}

function initDelete(){
  tableFasilitasHotel.on("click",".delete-fasilitas-hotel", function(){
    var id = $(this).data('id'),
        nama = $(this).data('nama');

      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data <b>"+nama+"</b>?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "ui/Fasilitas_hotel_admin/delete",
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}
