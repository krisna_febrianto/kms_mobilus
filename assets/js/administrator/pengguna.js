var tableUser = $('#table-user'),
	formUser = $('#form-user'),
	modalUser = $('#modal-user');

$(function(){
	initDataTables();
	initAdd();
	initValidation();
	initEdit();
  initDelete();
  initChangeSelectListener();
  initCloseBtn();
});

function initDataTables(){
	tableUser.DataTable({
        "order": [],
        "columns": [
            { "data": "nama", "name": "nama", "title": "Nama" },
            { "data": "email", "name": "email", "title": "Email" },
            { "data": "role_id", "name": "role_id", "title": "Role" , 
              "render": function ( data, type, full, meta ) {
                if(data == 2){
                  return "Pengguna";
                }else if(data == 1){
                  return "Administrator";
                }else{
                  return "-";
                }
              }
            },
            { "data": "id", "name": "action", "title": "Action", "sortable":false,
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="edit-user"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
                '<a data-id="'+data+'" data-nama="'+full.email+'" class="delete-user"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>';
            } 
          }
            //repeat for each of my 20 or so fields
        ],

        "ajax": {
            "url": admin_base_url+"Pengguna_admin/get_list",
            "type": "POST"
        },
        "serverSide":true
        // "processing":true
    });
}

function initAdd(){
	$('#add-user').on("click", function(){
		formUser.find('input[name=id]').val(0);
		formUser.find('[name=email]').val("");
    formUser.find('[name=nama]').val("");
    formUser.find('[name=username]').val("");
    formUser.find('[name=role_id]').val("");
    formUser.find('.password').show();
    formUser.find('.contributor-form').hide();
    formUser.find('.btn-pass').hide();
    formUser.find('.submit-button').removeAttr("disabled");
    formUser.find('[name=username]').removeAttr("disabled");
    // formUser.find('input[name=price]').val("");
		// formUser.find('input[name=is_mandatory]').prop('checked', false);
		modalUser.modal('show');
	});
}

function initValidation(){
  var validator = formUser.validate({
        rules: {
          email: {
            required: true,
            remote: {
              url: admin_base_url+"Pengguna_admin/check_email",
              type: "post",
              data: {
                  email: function() {
                      return formUser.find( "input[name=email]" ).val();
                  },
                  id: function() {
                      return formUser.find( "input[name=id]" ).val();
                  }
              }
            } 
          },
          role_id: {
            required: true
          },
          new_password: {
            required: true
          },
          new_password_conf:{
            required: true,
            equalTo : "#new_password"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
        	element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
  disableButtonSubmit();
  bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>' });
	var arrayData = new FormData(formUser[0]);
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

	$.ajax({ 
    	type: "POST",
      	dataType: 'json',
      	data: arrayData,
      	url: admin_base_url + "Pengguna_admin/save",
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
      	success: function(data){
	        window.location.href = data.url;     
	    },
      	error: function(XMLHttpRequest, textStatus, errorThrown) {
          bootbox.hideAll();
        	var err = XMLHttpRequest.responseJSON;
          bootbox.alert('Failed to save data user.');
	    }
    });
}

function initEdit(){
	tableUser.on("click",".edit-user", function(){
    formUser.find('.submit-button').removeAttr("disabled");
    
    formUser.find('.avatar-preview').empty('');
		var id = $(this).data('id');
		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	data: {id: id},
	      	url: admin_base_url + "Pengguna_admin/edit",
	      	success: function(data){
		        formUser.find('input[name=id]').val(data.detail.id);
            formUser.find('[name=nama]').val(data.detail.nama);
            formUser.find('[name=username]').val(data.detail.username);
		        formUser.find('[name=email]').val(data.detail.email);
            formUser.find('[name=role_id]').val(data.detail.role_id);
            // console.log(data.detail_residential_address);
            // console.log(data.detail_contributor);
            // console.log(data.detail_mailing_address);
            // formUser.find('[name=storage]').val(data.detail.storage);
            formUser.find('.password').hide();
            // formUser.find('.password').hide();
            formUser.find('.btn-pass').show();
            formUser.find('[name=username]').attr('disabled','disabled');
		        // formUser.find('.wysi').data("wysihtml5").editor.setValue(data.detail.content);
		        
		        modalUser.modal('show');
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
	});
}

function initDelete(){
  tableUser.on("click",".delete-user", function(){
    var id = $(this).data('id'),
        nama = $(this).data('nama');

      bootbox.confirm({
        message: "Are you sure want to delete data <b>"+nama+"</b>?",
        buttons: {
            confirm: {
                label: 'Delete',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Cancel',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                url: admin_base_url + "Pengguna_admin/delete/"+id,
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  bootbox.alert('Cannot delete this user.');
                }
            });
          }
        }
      });
  });
}

function initChangeSelectListener(){
  formUser.on("change","[name=role_id]", function(){
    // alert($(this).val());
    if($(this).val() == 2){
      $('.contributor-form').show();
    }else{
      $('.contributor-form').hide();
    }
  })
}
function openPassword(){
  if($('.password').is(":visible")){
    $('.password').hide();
  }else{
    $('.password').show();
  }
}
function disableButtonSubmit(){
  // $('form#id').submit(function(){
    formUser.find('.submit-button').attr('disabled','disabled');
  // });
}

function initCloseBtn(){
  var $form = $('form'),
    origForm = $form.serialize();

  $('form :input').on('change input', function() {
      if($form.serialize() !== origForm){
        formUser.data('changed', 1);
      }
  });

  $('.close-modal').on('click', function(e){
    e.preventDefault();
    if(formUser.data('changed') == 1) {
      bootbox.dialog({
        message: "Are you sure want to close this window? All changes will be discarded.",
        buttons: {
          close: {
            label: "Cancel",
            className: "btn-default flat",
            callback: function () {
              $(this).modal('hide');
            }
          },
          danger: {
            label: "Confirm",
            className: "btn-primary flat",
            callback: function () {
              modalUser.modal('hide');
            }
          }
        }
      });
    }else{
      modalUser.modal('hide');
    }
  });
}