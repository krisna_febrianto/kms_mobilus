var tablePaketUmroh = $('#datatable'),
	formPaketUmroh = $('#paketUmroh-form'),
	modalPaketUmroh = $('#paketUmroh-modal'),
  modalViewDetail = $('#view-detail-modal');
var dtable = $('#datatable');

$(document).ready(function(){
    dtable.DataTable({
        "order": [],
        "columns": [
            { "data": "nama", "name": "nama", "title": "Nama" },
            { "data": "tanggal_berangkat", "name": "tanggal_berangkat", "title": "Tanggal Berangkat" },
            { "data": "durasi", "name": "durasi", "title": "Durasi (hari)" },
            { "data": "kuota", "name": "kuota", "title": "Kuota" },
            { "data": "kuota_terpakai", "name": "kuota_terpakai", "title": "Kuota Terpakai" },
            { "data": "sisa_kuota", "name": "sisa_kuota", "title": "Kuota Tersedia" },
            { "data": "harga", "name": "harga", "title": "Harga",
              "render" : function (data,type,full, meta){
                var a = parseInt(data);
                var harga = a.toLocaleString();
                return harga;
              }
            },
            { "data": "id", "name": "action", "title": "Action", "sortable":false,
              "render": function ( data, type, full, meta ) {
                return '<a data-id="'+data+'" class="edit"><div class="btn btn-outline btn-circle btn-sm blue"><i class="fa fa-pencil" title="Edit"></i> Edit</div></a>'+
                '<a data-id="'+data+'" class="delete"><div class="btn btn-outline btn-circle dark btn-sm red"><i class="fa fa-trash-o" title="Delete"></i> Delete</div></a>'+
                '<a data-id="'+data+'" class="view-images"><div class="btn btn-outline btn-circle btn-sm blue"><i class="icon-picture" title="Lihat Gambar"></i> Gallery</div></a>'+
                '<a data-id="'+data+'" class="view-detail"><div class="btn btn-outline btn-circle btn-sm green"><i class="fa fa-list-alt" title="View Detail"></i> View Detail</div></a>';
            } 
          }
            //repeat for each of my 20 or so fields
        ],

        "ajax": {
            "url": admin_base_url+"ui/paket_umroh_admin/get_datatables",
            "type": "POST"
        },
        "serverSide":true
        // "processing":true
    });

    //datepicker
    $('.date-picker').datepicker({
        autoclose: true,
        format:'yyyy-mm-dd'
    });

    formPaketUmroh.find('input[name=durasi]').attr('disabled', true);
    $('#paket_plus').attr('hidden', true);
    $('.wysi').wysihtml5();

    //init();
    //initDataTables();
    initAdd();
    initValidation();
    initEdit();
    initDelete();
    iniViewDetail();

    get_jenis();
    //get_rute();
    get_paket_plus();

    get_hotel_mekkah();
    get_hotel_madinah();

    get_mata_uang();
    //get_handling();

    var preBookId = $('#trigger-convert').val();
    if(preBookId > 0){
      initConvertForm(preBookId);
    }

    $("[name='tanggal_berangkat']").on('change', function(){
          var jenis = $('[name="jenis"]').val();
          //var tanggal = $('[name="tanggal_berangkat"]').val();
          get_rute(jenis);
    });
});

function initValidation(){
	 // add the rule here
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
	 	return arg !== value;
	}, "Value must not equal arg.");

  	var validator = formPaketUmroh.validate({
        rules: {
          hotel_mekkah: {
            required: true
          },
          harga: {
            required: true
          },
          nama: {
            required: true
          },
          tanggal_berangkat: {
            required: true
          },
          durasi: {
            required: true
          },
          hari_mekkah: {
            required: true
          },
          hari_madinah: {
            required: true
          },
          hotel_mekkah: {
            required: true
          },
           hotel_madinah: {
            required: true
          },
           kamar_mekkah: {
            required: true
          },
           kamar_madinah: {
            required: true
          },
          kuota: {
            required: true
          },
           sisa_kuota: {
            required: true
          },
          mata_uang: {
          	required: true
          },
          rute_penerbangan: {
            required: true
          },
          deskripsi: {
            required: true
          }
        },
        messages: {
          jenis: {
            required: "Pilihan tidak boleh kosong"
          },
          harga: {
            required: "Bidang isian tidak boleh kosong"
          },
          nama: {
            required: "Bidang isian tidak boleh kosong"
          },
          tanggal_berangkat: {
            required: "Bidang isian tidak boleh kosong"
          },
          durasi: {
            required: "Bidang isian tidak boleh kosong"
          },
          hari_mekkah: {
            required: "Bidang isian tidak boleh kosong"
          },
          hari_madinah: {
            required: "Bidang isian tidak boleh kosong"
          },
          hotel_mekkah: {
            required: "Pilihan tidak boleh kosong"
          },
          hotel_madinah: {
            required: "Pilihan tidak boleh kosong"
          },
          kamar_mekkah: {
            required: "Pilihan tidak boleh kosong"
          },
          kamar_madinah: {
            required: "Pilihan tidak boleh kosong"
          },
          rute_penerbangan: {
            required: "Pilihan tidak boleh kosong"
          },
          kuota: {
            required: "Bidang isian tidak boleh kosong"
          },
          sisa_kuota: {
            required: "Bidang isian tidak boleh kosong"
          },
          mata_uang: {
          	required: "Pilihan tidak boleh kosong"
          }, 
          deskripsi: {
            required: "Bidang isian tidak boleh kosong"
          }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
          	error.addClass( "help-block" );
        	if (element.attr('name') == "tanggal_berangkat") {
            element.parent().parent().append(error);
          }else{
            element.parent().append(error);
          }
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        },
        submitHandler: function(form) {
          submitForm();
        }
    });
}

function submitForm(){
  var validator = formPaketUmroh.validate();
  if(validator.form()){
    $('.submit').attr('disabled', true);
    formPaketUmroh.find('input[name=durasi]').attr('disabled', false);
    formPaketUmroh.find('input[name="handling[]"]').attr('disabled', false);

    var arrayData = formPaketUmroh.serializeArray();
    var jsonData = {};

    $.each(arrayData, function(index, item) {
        jsonData[item.name] = item.value;
    });

	$.ajax({ 
    	type: "POST",
      	dataType: 'json',
      	data: arrayData,
      	url: admin_base_url + "ui/paket_umroh_admin/save",
      	success: function(data){
          $('.submit').attr('disabled', true);
	        window.location.href = data.url;     
          // formPaketUmroh.find('input[name=durasi]').attr('disabled', true);
	    },
      	error: function(XMLHttpRequest, textStatus, errorThrown) {
        	var err = XMLHttpRequest.responseJSON;
	    }
    });
  }
}

function initAdd(){
  $('#add-paketUmroh').on("click", function(){
    //formPaketUmroh.find('form')[0].reset();
    formPaketUmroh.validate().resetForm();
    //form.find('.alert').hide();
    formPaketUmroh.find(".form-group").removeClass("has-error");
    formPaketUmroh.find(".form-group").removeClass("has-success");

    $("[name='tanggal_berangkat']").datepicker("update", "");

    formPaketUmroh.find('input[name=id]').val(0);
    formPaketUmroh.find('input[name=nama]').val("");
    formPaketUmroh.find('input[name=durasi]').val("");
    formPaketUmroh.find('input[name=hari_mekkah]').val("");
    formPaketUmroh.find('input[name=hari_madinah]').val("");
    formPaketUmroh.find('input[name=hari_kota_tambahan]').val("");
    formPaketUmroh.find('input[name=harga]').val("");
    formPaketUmroh.find('input[name=deskripsi]').val("");
    formPaketUmroh.find('input[name=kuota]').val("");
    formPaketUmroh.find('input[name=sisa_kuota]').val("");
    $("[name='hotel_mekkah']").val("").trigger('change');
    $("[name='kamar_mekkah']").val("").trigger('change');
    $("[name='hotel_madinah']").val("").trigger('change');
    $("[name='kamar_madinah']").val("").trigger('change');
    $("[name='mata_uang']").val("").trigger('change');
    $("[name='rute_penerbangan']").val("").trigger('change');
    $("[name='jenis']").val("").trigger('change');
    $("[name='handling[]']").empty();
    //$("[name='handling[]']").attr('checked', false);
    formPaketUmroh.find('.wysi').data("wysihtml5").editor.setValue("");
    modalPaketUmroh.modal('show');
  });
}

function initEdit(){
	tablePaketUmroh.on("click",".edit", function(){
		var id = $(this).data('id');

		$.ajax({ 
	    	type: "GET",
	      	dataType: 'json',
	      	data: {id: id},
	      	url: admin_base_url + "ui/paket_umroh_admin/edit",
	      	success: function(data){
            var dataHandling = data.detail.handlings;
            formPaketUmroh.find('input[name=id]').val(data.detail.id);
            // $("[name='handling[]']").empty();
            fillForm(data, dataHandling);
				    modalPaketUmroh.modal('show');
		    },
	      	error: function(XMLHttpRequest, textStatus, errorThrown) {
	        	var err = XMLHttpRequest.responseJSON;
		    }
	    });
	});
}

function fillForm(data, dataHandling){
  formPaketUmroh.validate().resetForm();
  //form.find('.alert').hide();
  formPaketUmroh.find(".form-group").removeClass("has-error");
  formPaketUmroh.find(".form-group").removeClass("has-success");
  $("[name='tanggal_berangkat']").datepicker({format: 'yyyy-mm-dd'});
  $("[name='tanggal_berangkat']").datepicker("update", data.detail.tanggal_berangkat);

  formPaketUmroh.find('input[name=nama]').val(data.detail.nama);
  formPaketUmroh.find('input[name=durasi]').val(data.detail.durasi);
  formPaketUmroh.find('input[name=hari_mekkah]').val(data.detail.jumlah_hari_mekkah);
  formPaketUmroh.find('input[name=hari_madinah]').val(data.detail.jumlah_hari_madinah);
  formPaketUmroh.find('input[name=hari_kota_tambahan]').val(data.detail.jumlah_hari_kota_tambahan);
  formPaketUmroh.find('input[name=harga]').val(data.detail.harga);
  formPaketUmroh.find('input[name=deskripsi]').val(data.detail.deskripsi);
  formPaketUmroh.find('input[name=kuota]').val(data.detail.kuota);
  formPaketUmroh.find('input[name=sisa_kuota]').val(data.detail.sisa_kuota);
  $("[name='jenis']").select2('trigger', 'select', { data:{id: data.detail.jenis_id, text:data.detail.jenis_paket.nama}});
  $("[name='rute_penerbangan']").select2('trigger', 'select', { data:{id: data.detail.rute_penerbangan_id, text: data.rute.bb1_kode+' - '+data.rute.bt1_kode}});
  $("[name='hotel_mekkah']").select2('trigger', 'select', { data:{id: data.detail.hotel_mekkah_id, text:data.detail.hotel_mekkah.nama}});
  $("[name='hotel_madinah']").select2('trigger', 'select', { data:{id: data.detail.hotel_madinah_id, text:data.detail.hotel_madinah.nama}});
  $("[name='kamar_mekkah']").select2('trigger', 'select', { data:{id: data.detail.kamar_mekkah_id, text:data.detail.kamar_mekkah.tipe_kamar}});
  $("[name='kamar_madinah']").select2('trigger', 'select', { data:{id: data.detail.kamar_madinah_id, text:data.detail.kamar_madinah.tipe_kamar}});
  $("[name='mata_uang']").select2('trigger', 'select', { data:{id: data.detail.mata_uang_id, text:data.detail.mata_uang.nama}});
  formPaketUmroh.find('.wysi').data("wysihtml5").editor.setValue(data.detail.deskripsi);

  if(data.handlings != null){
      var values = [];
      for (var i = 0; i < data.handlings.length; i++) {
      // console.log(data.detail.handlings[i].TrcTypeID);
      values.push(String(data.handlings[i].handling_id));
      // $("#user-list [value=" + data.detail.handlings[i].TrcTypeID + "]").attr("checked", "checked");
      }
      console.log(values);
      if(data.handlings.length > 0){
          formPaketUmroh.find(".handling").find('[value=' + values.join('], [value=') + ']').prop("checked", true);
      }
  }
  // $.each(data.detail.handlings, function(index, item) {
  //   $("[name='handling[]']").select2('trigger', 'select', { data:{id: item.id, text:item.nama}});
  // });
}

function initDelete(){
  tablePaketUmroh.on("click",".delete", function(){
    var id = $(this).data('id');
      bootbox.confirm({
        message: "Apakah Anda yakin akan menghapus data?",
        buttons: {
            confirm: {
                label: 'Hapus',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Batal',
                className: 'btn-default'
            }
        },
        callback: function (result) {
          if(result == true){
            $.ajax({ 
              type: "GET",
                dataType: 'json',
                data: {id: id},
                url: admin_base_url + "ui/paket_umroh_admin/delete/"+id,
                success: function(data){
                  window.location.href = data.url;  
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  var err = XMLHttpRequest.responseJSON;
                }
            });
          }
        }
      });
  });
}

function iniViewDetail(){
  tablePaketUmroh.on("click", '.view-detail', function(){
    var id = $(this).data('id');
    modalViewDetail.modal({
      remote:admin_base_url + 'ui/paket_umroh_admin/view_detail/'+id
    });
  });
}

function get_jenis(){
    $("[name='jenis']").select2({
        placeholder: '-- Pilih Jenis Paket --',
        ajax: {
          url: admin_base_url+"ui/paket_umroh_admin/get_jenis",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
    });

    $("[name='jenis']").on('change', function(){
        var id = $("[name='jenis']").val();

        $.ajax({ 
          type: "GET",
            dataType: 'json',
            data: {id: id},
            url: admin_base_url + "ui/paket_umroh_admin/get_jenis/"+id,
            success: function(data){
              get_rute(data.id);
              formPaketUmroh.find('input[name=durasi]').val(data.durasi);
              if (data.is_plus == 1) {
                  $('#paket_plus').attr('hidden', false);
              }else{
                  $('#paket_plus').attr('hidden', true);
              }
            }
        });
    });
}

function formatPaket(paket) {
  if (!paket.id) {
    return paket.text;
  }
  var imageUrl = "assets/images/maskapai/";
  var $paket = $(
    '<span><img src="' + imageUrl + '/' + paket.element.value.toLowerCase() + '.png" class="img-flag" /> ' + paket.text + '</span>'
  );
  return $paket;
};

function formatData (data) {
  if (data.loading) return "Mencari rute. . .";

  if (data.bb2_kode == null) {
    var kode = data.bb1_kode + ' - ' + data.bt1_kode;
    markup1 = '<img src="'+base_url+'assets/images/maskapai/' + data.data.mb1_logo + '" class="img-flag" style="width:20px;"/> ' + data.data.bb1_kode + ' - ' + data.data.bt1_kode;
  }else{
    var kode = data.data.bb1_kode + ' - ' + data.data.bt1_kode + ' - ' + data.data.bt2_kode;
    markup1 = '<img src="'+base_url+'assets/images/maskapai/' + data.data.mb1_logo + '" class="img-flag" style="width:20px;"/> ' + data.data.bb1_kode + ' - ' + data.data.bt1_kode + ' - ' + data.data.bt2_kode;
  }

  if (data.data.bbp2_kode == null) {
    markup2 = '<img src="'+base_url+'assets/images/maskapai/' + data.data.mp1_logo + '" class="img-flag" style="width:20px;"/> ' + data.data.bbp1_kode + ' - ' + data.data.btp1_kode;
  }else{
     markup2 = '<img src="'+base_url+'assets/images/maskapai/' + data.data.mp1_logo + '" class="img-flag" style="width:20px;"/> ' + data.data.bbp1_kode + ' - ' + data.data.btp1_kode + ' - ' + data.data.btp2_kode;
  }

  if (data.data.bp2_kode == null) {
    markup3 = '<img src="'+base_url+'assets/images/maskapai/' + data.data.mt1_logo + '" class="img-flag" style="width:20px;"/> ' + data.data.bp1_kode + ' - ' + data.data.btp1_kode;
  }else{
    markup3 = '<img src="'+base_url+'assets/images/maskapai/' + data.data.mt1_logo + '" class="img-flag" style="width:20px;"/> ' + data.data.bp1_kode + ' - ' + data.data.btp1_kode + ' - ' + data.data.btp2_kode;
  }

  if (data.bbp1_kode != null) {
    markup =  markup1 + '<hr style="margin:3px;">' + markup2 + '<hr style="margin:3px;">' + markup3;
  }else{
    markup =  markup1 + '<hr style="margin:3px;">' + markup3;
  }

  return markup;
}

function get_rute(id_jenis){ 
      var tanggal = $('[name="tanggal_berangkat"]').val();
    
    $("[name='rute_penerbangan']").select2({
        placeholder: '-- Pilih Rute --',
        ajax: {
          url: admin_base_url+"ui/paket_umroh_admin/get_rute/"+id_jenis+"/"+tanggal,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    if (item.bb2_kode == null) {
                      var kode = item.bb1_kode + ' - ' + item.bt1_kode;
                    }else{
                      var kode = item.bb1_kode + ' - ' + item.bt1_kode + ' - ' + item.bt2_kode;
                    }
                    return {
                        text: kode,
                        id: item.id,
                        data: item
                    }
                    //return item;
                    console.log(item);
                })
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        templateResult: formatData,
        templateSelection: function(data) {
          return data.text;
        }
    });
}
//'<img src="'+base_url+'assets/images/maskapai/'+item.mb1_logo+'" width="39" class="pull-left gap-right" style="margin-right: 3px"/> '

function get_paket_plus(){
    var id = $(this).data('id');
    $("[name='paket_plus']").select2({
        placeholder: '-- Pilih Paket Plus --',
        ajax: {
          url: admin_base_url+"ui/paket_umroh_admin/get_paket_plus/"+id,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.kota,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });
}

function get_hotel_mekkah(){
    var id = $(this).data('id');
    var kota = "Mekkah";
    $("[name='hotel_mekkah']").select2({
        placeholder: '-- Pilih Hotel --',
        ajax: {
          url: admin_base_url+"ui/paket_umroh_admin/get_hotel/"+id+"/"+kota,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });

    $("[name='hotel_mekkah']").on('change', function(){
      var id = $(this).val();
      get_kamar_mekkah(id);
    });
}

function get_hotel_madinah(){
    var id = $(this).data('id');
    var kota = "Madinah";
    $("[name='hotel_madinah']").select2({
        placeholder: '-- Pilih Hotel --',
        ajax: {
          url: admin_base_url+"ui/paket_umroh_admin/get_hotel/"+id+"/"+kota,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });

    $("[name='hotel_madinah']").on('change', function(){
      var id = $(this).val();
      get_kamar_madinah(id);
    });
}

function get_kamar_mekkah(hotel_id){
  $("[name='kamar_mekkah']").select2({
        placeholder: '-- Mekkah --',
        ajax: {
          url: admin_base_url+"ui/paket_umroh_admin/get_kamar/"+hotel_id,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.tipe_kamar,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });
}

function get_kamar_madinah(hotel_id){
  $("[name='kamar_madinah']").select2({
        placeholder: '-- Madinah --',
        ajax: {
          url: admin_base_url+"ui/paket_umroh_admin/get_kamar/"+hotel_id,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.tipe_kamar,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });
}

function get_mata_uang(){
    var id = $(this).data('id');
    $("[name='mata_uang']").select2({
        placeholder: '-- Pilih Mata Uang --',
        ajax: {
          url: admin_base_url+"ui/paket_umroh_admin/get_mata_uang/"+id,
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.id
                    }
                    console.log(item);
                })
            };
          },
          cache: true
        }
    });
}

// function get_handling(){
//     var id = $(this).data('id');
//     $("[name='handling[]']").select2({
//         placeholder: '-- Pilih Handling --',
//         ajax: {
//           url: admin_base_url+"ui/paket_umroh_admin/get_handling/"+id,
//           dataType: 'json',
//           delay: 250,
//           processResults: function (data) {
//             return {
//                 results: $.map(data, function (item) {
//                     return {
//                         text: item.nama,
//                         id: item.id
//                     }
//                     console.log(item);
//                 })
//             };
//           },
//           cache: true
//         }
//     });
// }

function initConvertForm(id){
  $.ajax({ 
    type: "GET",
      dataType: 'json',
      data: {id: id},
      url: admin_base_url + "ui/paket_umroh_admin/get_pre_book",
      success: function(data){
        fillForm(data, data.detail.list_handling);

        modalPaketUmroh.modal('show');
    },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        var err = XMLHttpRequest.responseJSON;
    }
  });
}

dtable.on('click','.view-images',function(){
    // var id = $(this).closest('tr').attr('idx');
    $('#modal-gallery').modal('show');
    var id = $(this).data('id');
    $("[name='image[]']").val("");
    $( "#galleryForm" ).find('.upload-button').html('<input type="button" class="btn btn-block btn-primary" value="Upload" onclick="upload_image(\'ui/Paket_umroh_admin/post_gambar\');"></button>');
    $( "#galleryForm" ).find('input[name=id]').val(id);
    // $('[name="id"]').val(id);
    get_gallery(id,"ui/Paket_umroh_admin/get_gallery");
});

function get_gallery(id,uri){
    $('.gallery-container').empty();
    $.post(admin_base_url + uri, {id: id}).done(function( data1 ) {
        var json = $.parseJSON(data1);
        console.log(json.length);
        if(data1.length == 0){

        }else{
            for (var i = 0; i < json.length; i++) {
                $('.gallery-container').append('<div class="col-md-3"><img src="'+base_url+'assets/images/paket_umroh/'+id+'/'+json[i].nama_file+'" width="100%" alt="..." class="img-thumbnail" style="min-height:150px">'+
                    '<div class="row"><a class="delete-img btn btn-block btn-white text-red" href="javascript:;" onclick=delete_img('+json[i].id+',"'+json[i].nama_file+'",'+id+');><i class="fa fa-trash-o fa-lg"></i></a></div>'+
                    '</div>');
            }
        }
    })
}
function delete_img(id,foto,id_paket){
    bootbox.confirm("Apakan anda yakin akan menghapus gambar ini?", function(result){
        if (result) {
            $.post( admin_base_url + "ui/Paket_umroh_admin/hapus_gambar", {id : id,foto : foto,id_paket : id_paket}).done(function( data ) {
                if (data == '1'){
                    bootbox.alert("Data berhasil dihapus.", function(){
                        get_gallery(id_paket,"ui/Paket_umroh_admin/get_gallery");
                    })
                } else {
                    bootbox.alert('Data gagal dihapus.');
                }
            });
        }else{
        }
    });
}
function upload_image(url){
    // var data = new FormData($('#galleryForm')[0]);
    var validator = $( "#galleryForm" ).validate({
        rules: {
               image: {
                required: true,
                accept: "image/*"
            },
        },
        messages:{
          image:{
            accept: "Belum ada file yang dipilih."
          }
        }
    });
    
    if(validator.form()){
        var arrayData = new FormData($('#galleryForm')[0])
        var jsonData = {};
        $('[value="Upload"]').attr('disabled', true);
        $('[value="Upload"]').attr('value', 'Uploading. . .');
        $.each(arrayData, function(index, item) {
            jsonData[item.name] = item.value;
        });

        $.ajax({ 
            type: "POST",
            dataType: 'json',
            data: arrayData,
            url: admin_base_url + url,
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
           success:function(data)
            {
                if (data != "0"){
                    bootbox.alert("Data berhasil disimpan.", function(){
                      get_gallery(data.id,"ui/Paket_umroh_admin/get_gallery");
                      $("[name='image[]']").val("");
                      $('[value="Uploading. . ."]').attr('disabled', false);
                      $('[value="Uploading. . ."]').attr('value', 'Upload');
                    })
                }else{
                    bootbox.alert("Data gagal disimpan.");
                }       

            }
        });
    }
}