tjq(document).ready(function () {
  var navListItems = tjq('div.setup-panel div a'),
          allWells = tjq('.setup-content'),
          allNextBtn = tjq('.nextBtn');

  allWells.hide();

  navListItems.on("click", function (e) {
      e.preventDefault();
      var $target = tjq(tjq(this).attr('href')),
              $item = tjq(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-primary').addClass('btn-default');
          $item.addClass('btn-primary');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });

  allNextBtn.on("click", function(){
    alert('test')
      var curStep = tjq(this).closest(".setup-content"),
          curStepBtn = curStep.attr("id"),
          nextStepWizard = tjq('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      tjq(".form-group").removeClass("has-error");
      for(var i=0; i<curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              tjq(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepWizard.removeAttr('disabled').trigger('click');
  });

  tjq('div.setup-panel div a.btn-primary').trigger('click');
});