/*
 * Title:   Travelo - Travel, Tour Booking HTML5 Template - Custom Javascript file
 * Author:  http://themeforest.net/user/soaptheme
 */
var form_regis = $('.form-registration');
var form_login = $('.login-form');
var form_prebook = $('.form-prebooking');

$(document).ready(function() {
    // UI Form Element
    $('.detail-hotel').on("click", function(){
    	var hotel_status = $("input[name=hotel_status]").val();
        var id_hotel = $(this).attr('data-id');
		$('#modal-detail').modal({
            remote: base_url+'hotels/detail/'+hotel_status+'/'+id_hotel
		});
	});

    $('body').on('hidden.bs.modal', '.modal', function () {
      $(this).removeData('bs.modal');
    });

    var status_peserta = $("input[name$='status_peserta']:checked").val();
    if(status_peserta == 2){
    	$('.hidden-content').html('<input type="number" name="jumlah_peserta" class="input-text full-width input-jumlah-peserta" value="5" min="5"/>');
		$('.hidden-jumlah-peserta').remove();
	}else{
    	$('.hidden-content').html('<input type="hidden" name="jumlah_peserta" value="1">');
		$('.input-jumlah-peserta').remove();
	}

	$("input[name$='status_peserta']").on("click", function() {
		var value = $(this).val();

		if(value == 2){
			$('.hidden-content').html('<input type="number" name="jumlah_peserta" class="input-text full-width input-jumlah-peserta" value="5" min="5"/>');
			$('.hidden-jumlah-peserta').remove();
		}else{
			$('.hidden-content').html('<input type="hidden" name="jumlah_peserta" value="1">');
			$('.input-jumlah-peserta').remove();
		}
	});
	
	$(".select-paket").on("change", function() {
		$('.msg_holiday').hide();
		
		var id_paket = $(this).val();
		var day_paket = $(this).find('option:selected').attr('day');

		var day_mekah = 0;
		var day_madinah = 0;

		var count_paket = day_paket - 2;
		if(id_paket == 4){
			count_paket = count_paket - 1;
			$('.msg_holiday').show();
		}

		for (var i = 0; i < count_paket; i++) {
			if(i%2 == 0){
				day_mekah = day_mekah + 1;
			}else{
				day_madinah = day_madinah + 1;	
			}
		}
		$('.day_mekah').val(day_mekah);
		$('.day_madinah').val(day_madinah);
		// alert(day_paket)
	});
	
	initRegistrationValidation();
	submitFlight();
	submitHotel();
	submitHandling();

	$(document).scroll(function() {
	    checkOffset();
	});
});

function checkOffset() {
	var sidebar = $('.search-tab-content');
	var footer = $('#footer');
	var row = $('.new-row');

    if(window.pageYOffset > 670 && window.outerWidth > 1002){
		// if(sidebar.offset().top + sidebar.height() >= footer.offset().top - 10){
	 //        sidebar.css({position: 'absolute', bottom: '50px', right:'0px', width: '', top: ''});
	 //        row.css({position: 'relative'});
	 //    }

	    if($(document).scrollTop() + window.innerHeight < footer.offset().top){
	        row.css({position: ''});
	        sidebar.css({position: 'fixed', top:'6%', background: 'rgba(127, 219, 249, 0.85)', left: '0'}); // restore when you scroll up
	    }
	}else{
	    row.css({position: ''});
		sidebar.css({position: '', top:'', background: '', left: ''});
	}

    // sidebar.text($(document).scrollTop() + window.innerHeight);
}

$(".close-button").on("click", function() {
	// $(this).closest('.modal').close();
	 $('#modal-alert').removeClass('show');
});

function submitFlight(){
	$(".submit-flight").on("click", function(){
		// var flight_status = $("input[name=flight_status]").val();
		var flight_id = $(this).parent().parent().parent().parent().parent().parent('.box').find("input[name=flight_id]").val();
		// alert(flight_id)
		$.post( base_url+"flights/post/"+flight_id, {}, function( returns ) {
			window.location.replace(returns);
		});
	});
}

function submitHotel(){
	$(".submit-hotel").on("click", function(){
		var hotel_status = $("input[name=hotel_status]").val();
		var hotel_id = $(this).parent().parent().parent().parent().parent().parent('.panel').find("input[name=hotel_id]").val();
		var room_id = $(this).parent().parent('.panel').find("input[name=room_id]").val();

		$.post( base_url+"hotels/post/"+hotel_status+"/"+hotel_id+"/"+room_id, {}, function( returns ) {
			window.location.replace(returns);
		});
	});
}

function submitHandling(){
	$(".check").on("change", function() {
	  	var data = [];
	  	$(this).attr('checked',this.checked);
	  	$("input:checkbox[name=handling]:checked").each(function(){
		    data.push($(this).val());
		});

	  	$.post( base_url+"handlings/post", {data:data}, function( returns ) {
			console.log(returns)		  	
		  	$( ".cal-handling" ).html(returns);
		  	
		  	var flight = +($("input[name=total_penerbangan]").val());
		  	var hotel = +($("input[name=total_hotel]").val());
		  	var handling = +($("input[name=total_handling]").val());

		  	var total = flight + handling + hotel;
		  	
		  	$(".total_pembayaran").html(total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	  	});
	});
}

function submitPrebooking(){
	var validator = form_prebook.validate({
		rules: {
            nama: {
                required: true,
            },
            deskripsi: {
                required: true
            }
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
            element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    });
	if(validator.form()){
		var data = form_prebook.serialize();
		$.post(base_url+'prebooking/save', data, function(response){
	        bootbox.alert(response.msg, function(){
	          	window.location.replace(response.url);
	          	// location.reload();
	        })
	    },'json');
	}
}

function initRegistrationValidation(){
  var validator = form_regis.validate({
        rules: {
            username: {
                required: true,
		        remote: {
			        url: base_url+"site/check_username",
			        type: "post",
			        data: {
			            email: function() {
			              	return $( "input[name=username]" ).val();
			            }
			        }
		        }
            },
            email:{
		        required: true, 
		        email:true,
		        remote: {
			        url: base_url+"site/check_email",
			        type: "post",
			        data: {
			            email: function() {
			              	return $( "input[name=email]" ).val();
			            }
			        }
		        } 
		    },
            password: {
                required: !0
            },
            confirm_password:{
		        required: true,
		        equalTo : "#password",
		        minlength: 4
		     }
        },
        messages:{
	      email:{
	        required: "This field is required.",
	        remote: "This email address already exist."
	      }, 
	      name:{
	        required: "This field is required.",
	      },
	      admit_term:{
	        required: "This field is required.",
	        number: "Please enter a valid number."
	      },
	      username:{
	        remote: "This username already exist."
	      }
	    },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
            element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    });
}

function submitFormRegis(){
	// console.log('asd');var validator = $( "#form-data-student-import" ).validate(
	var validator = form_regis.validate();
	if(validator.form()){
		var data = form_regis.serialize();
	    $.post(base_url+'/site/user_registration', data, function(response){
	        bootbox.alert(response.msg, function(){
	          	location.reload();
	        })
	    },'json');
	}
}
function submitFormLogin(){
	var validator = form_login.validate({
		rules: {
            username: {
                required: true,
            },
            password: {
                required: !0
            }
        },
        messages:{
	      email:{
	        required: "This field is required.",
	        remote: "This email address already exist."
	      }, 
	      name:{
	        required: "This field is required.",
	      },
	      admit_term:{
	        required: "This field is required.",
	        number: "Please enter a valid number."
	      },
	      username:{
	        remote: "This username already exist."
	      }
	    },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
          // Add the `help-block` class to the error element
            error.addClass( "help-block" );
            element.parent().append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
          $( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element, errorClass, validClass) {
          $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
        }
    });
	if(validator.form()){
		var data = form_login.serialize();
		$.post(base_url+'site/login', data, function(response){
	        bootbox.alert(response.msg, function(){
	          	if(response.url == base_url+'prebooking/save'){
	          		$('.travelo-box').hide();
	          		submitPrebooking();
	          	}else{
	          		location.reload();
	          		// window.location.replace(response.url);
	          	}
	          	// location.reload();
	        })
	    },'json');
	}
}
function submitFormForgot(){
	var email = $('[name=forgot_email]').val();
	if(email){
		var data = $('.forgot-form').serialize();
		$.post(base_url+'site/forgot_password', data, function(response){
	        bootbox.alert(response.msg, function(){
	          	location.reload();
	        })
	    },'json');
	}
}
